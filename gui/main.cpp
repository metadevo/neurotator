#include <memory>
#include <string>
#include <QDebug>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>
#include <QQmlContext>
#include <QQuickStyle>

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
//#include "Project.hpp"
#include "RoiDataModel.hpp"
#include "RoiDataSparseModel.hpp"
#include "SliceImage.hpp"
#include "SliceOverlay.hpp"

#include "ProjectAnnotationDense.hpp"
#include "ProjectAnnotationSparse.hpp"
#include "ProjectRegistration.hpp"
#include "RegisterRigid3D.h"

int main(int argc, char *argv[])
{
    QString versionStr("2.1.15");
    QString appNameStr = "Neurotator";
    qInfo() << "Starting " << appNameStr;

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setApplicationName(appNameStr);
    QCoreApplication::setOrganizationName(QStringLiteral("Boston University"));
    QCoreApplication::setApplicationVersion(versionStr);

    QQuickStyle::setStyle("Material");

    // register qml types (custom widgets)
    qmlRegisterType<SliceImage>("com.bu", 1, 0, "SliceImage");
    qmlRegisterType<SliceOverlay>("com.bu", 1, 0, "SliceOverlay");
    qmlRegisterType<RoiDataModel>("com.bu", 1, 0, "RoiDataModel");
    qmlRegisterType<RoiDataSparseModel>("com.bu", 1, 0, "RoiDataSparseModel");
    qmlRegisterType<SearchResult>("com.bu", 1, 0, "SearchResult");
    qmlRegisterType<LandmarkDataModel>("com.bu", 1, 0, "LandmarkDataModel");
    qRegisterMetaType<LandmarkDataModel*>("LandmarkDataModel*");

    ApplicationData& appdata = ApplicationData::instance();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("appdata", &appdata);
    engine.load(QUrl(QStringLiteral("qrc:/gui/qml/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    engine.addImageProvider(QLatin1String("subvolumecaps"), new VolumeBufferCapsProvider());

    MAKE_PROTOTYPE(dense annotation, ProjectAnnotationDense)
    MAKE_PROTOTYPE(registration, ProjectRegistration)
    MAKE_PROTOTYPE(sparse annotation, ProjectAnnotationSparse)

    try
    {
        app.exec();
    }
    catch (const std::bad_alloc &e)
    {
        qCritical() << "Ran out of memory!" << e.what();
    }
    catch (const std::exception &e)
    {
        qCritical() << "Exception:" << e.what();
    }
    catch (...)
    {
        qCritical() << "Unknown exception";
    }

    return 0;
}
