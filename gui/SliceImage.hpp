/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SLICEIMAGE_HPP
#define SLICEIMAGE_HPP

#include <QQuickItem>
#include <QImage> // for testing
#include <QDateTime>
#include <QStringListModel>
#include "Project.hpp"
#include "RoiDataDense.hpp"
#include "HyperstackTiff.hpp"
#include <vtkSmartPointer.h>
#include <vtkAbstractTransform.h>

const std::vector<QColor> PREDEFINED_CHANNEL_COLORMAP = {
    QColor("#90ee90"), QColor("#ff0000"), QColor("#ff00ff"),
    QColor("#40e0d0"), QColor("#add8e6"), QColor("#ff007f"), QColor("#ffcc99")
};

Q_DECLARE_METATYPE(std::vector<int>)

/// A widget to use in QML for showing slices of hyperstacks
class SliceImage : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(float mouseX READ getMouseX WRITE setMouseX)
    Q_PROPERTY(float mouseY READ getMouseY WRITE setMouseY)
    Q_PROPERTY(int zoomFactor READ getZoomFactor WRITE setZoomFactor NOTIFY zoomFactorChanged)
    Q_PROPERTY(int minWidth READ getMinWidth)
    Q_PROPERTY(int minHeight READ getMinHeight)
    Q_PROPERTY(bool isRef READ isRef NOTIFY isRefChanged)
    Q_PROPERTY(bool zoomButton READ getZoomButton WRITE setZoomButton)
    Q_PROPERTY(bool histEq READ getHistEq WRITE setHistEq)
    Q_PROPERTY(bool registrationEnabled READ registrationEnabled
               WRITE setRegistrationEnabled)
    Q_PROPERTY(int maxZ READ getMaxZ NOTIFY maxZChanged)
    Q_PROPERTY(float fadeRegistered READ fadeRegistered WRITE setFadeRegistered
               NOTIFY fadeRegisteredChanged)
    Q_PROPERTY(QStringList channelList READ getChannelStringList NOTIFY systemChangedChannelList)
    Q_PROPERTY(bool centerSelected READ getCenterSelected WRITE setCenterSelected)

public:
    SliceImage();
    virtual ~SliceImage() override;
    Q_INVOKABLE QStringListModel *getChannelsModel() { return new QStringListModel(this->getChannelStringList()); }
    Q_INVOKABLE QString getName() const { return m_name; }
    Q_INVOKABLE int getNumChannels() const { return (m_stack) ? static_cast<int>(m_stack->size().c) : 0; }
    Q_INVOKABLE int getMaxX() const { return (m_stack) ? static_cast<int>(m_stack->size().x) : 0; }
    Q_INVOKABLE int getMaxY() const { return (m_stack) ? static_cast<int>(m_stack->size().y) : 0; }
    Q_INVOKABLE int getMaxZ() const { return (m_stack) ? static_cast<int>(m_stack->size().z) : 0; }
    Q_INVOKABLE int getMaxT() const { return (m_stack) ? static_cast<int>(m_stack->size().t) : 0; }
    Q_INVOKABLE bool atMaxZ() const { return (m_stack) ? (m_currentZ + m_iBase>= getMaxZ()) : false;}
    Q_INVOKABLE int getIndexZ() const { return m_currentZ; }
    Q_INVOKABLE int getIndexTime() const { return (m_stack) ? static_cast<int>(m_stack->index().time) : 0; }
    Q_INVOKABLE QColor getMaxColorValue() const { return m_maxColorValue; }
    Q_INVOKABLE double getMaxColorValueF() const { return m_maxColorValue.redF(); }
    Q_INVOKABLE QString getIndexStr() const;
    Q_INVOKABLE bool getSynced() const { return m_synced; }
    Q_INVOKABLE void enableRegistrationMode() { m_registrationMode = true; }

    Q_INVOKABLE void setScaledWidth(const int w);
    Q_INVOKABLE void setScaledHeight(const int h);
    Q_INVOKABLE void adjustOffset(const int x, const int y);
    Q_INVOKABLE void setOffsets(const float x, const float y);
    Q_INVOKABLE void resetZoom();
    Q_INVOKABLE void loadWindowConfig();
    Q_INVOKABLE void loadCoords(const int x, const int y, const int z) {
        m_coords.x = x;
        m_coords.y = y;
        m_coords.z = z;
    }

    void openStack(HyperstackTiff* stack);
    bool registrationEnabled() const { return m_registrationEnabled; }
    void setRegistrationEnabled(bool val) {
        m_registrationEnabled = val;
        updateRegistration();
    }
    qreal getMouseX() { return m_mouseX; }
    qreal getMouseY() { return m_mouseY; }
    void setMouseX(const qreal x){ m_mouseX = x; }
    void setMouseY(const qreal y){ m_mouseY = y; }
    int getZoomFactor() { return m_zoomFactor; }
    void setZoomFactor(const int zoomFactor);
    int getMinWidth() { return 500; }
    int getMinHeight() { return 420; }
    bool getCenterSelected() const { return m_centerSelected;}
    bool isRef();
    bool isMoving() const;
    bool getHistEq() { return m_histEqualization; }
    bool getZoomButton() { return m_zoomButton; }
    void setHistEq(bool flag) { m_histEqualization = flag; }
    void setZoomButton(bool flag) { m_zoomButton = flag; }
    vtkSmartPointer<vtkAbstractTransform> getTransform() const;
    float fadeRegistered() const { return m_fadeRegistered; }
    void setFadeRegistered(float val) {
        m_fadeRegistered = val;
        m_fadeRegisteredUpdated = true;
        emit fadeRegisteredChanged();
    }
    void setCenterSelected(bool flag) { m_centerSelected = flag;}
    void setPreRotation(int val);

protected:
    virtual QSGNode* updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData) override;
//    void updateZoomThresh() { m_zoomThresh = static_cast<int>(width() / static_cast<float>(getMaxX()) * 100.0f); }

signals:
    void navChanged();
    void preRotationChanged(int preRotation);
    void zoomFactorChanged(int zoomFactor);
    void scaleChanged(float windowFactor, float zoomFactor, float xOffset, float yOffset, float x0, float y0);
    void isRefChanged();
    void changeStackWindowPosition(int x, int y);
    void maxZChanged();
    void fadeRegisteredChanged();
    void systemChangedChannelList();
    void userChangedChannelColor(QString color);
    void systemChangedChannelWindowLevel();
    void initializeWindowLevelUi(std::vector<int>, std::vector<int>);
    void channelToggled();
public slots:
    void nextTime() {}
    void nextZ();
    void prevTime() {}
    void prevZ();
    void gotoZ(const int z);
    void gotoTime(const int t);
    void setZThick(int zThick) { m_zThick = zThick; m_imageChanged = true; }
    void setProjection(QString projection) { m_projection = projection; m_imageChanged = true; }
    void roiSelectionRequest(QString sessionKey, QString roiKey);
    void setSynced(bool flag) { m_synced = flag; }
    void checkIsRef() { emit isRefChanged(); }
    void windowPositionChanged(const int x, const int y);
    // void windowHidden();
    void updateRegistration();
    void showColorSelector(QString channel);
    QColor getColorForChannel(QString channel);
    void setChannelColorsSparse(QList<int> channel) ;
    void setColorForChannel(QString channel, QColor color);
    std::vector< int> getScalarRangeForChannel(QString channel);
    std::vector< int> getWindowLevelForChannel(QString channel);
    void setWindowLevelForChannel(QString channel, int wmin, int wmax);
    void toggleChannelOnOff(QString channel, bool state);
    int numActiveChannels() {return (m_stack) ? (m_stack->numActiveChannel()) : 0;}
    bool getChannelState(QString channel);
    qint64 currentSecsSinceEpoch() {return QDateTime::currentSecsSinceEpoch();}
    qint64 currentMSecsSinceEpoch() {return QDateTime::currentMSecsSinceEpoch();}
    int getPreRotation() { return m_preRotation; }
    void updateMappingColor(){
        m_colorMappingChanged = true;
        update();
    }
    void setMouseXs(const qreal x) { setMouseX(x); }
    void setMouseYs(const qreal y) { setMouseY(y); }
    void setZoomFactors(const int zoomFactor) { setZoomFactor(zoomFactor); }
    void adjustOffsets(const int x, const int y) { adjustOffset(x, y); }

private:
    Project* m_project;
    std::vector<QColor> channelColors;
    QString m_name = "empty stack";
    stack_index_t m_stackWindowIndex = 0;
    QColor m_maxColorValue = QColor(qRgba(0xFF, 0xFF, 0xFF, 0xFF));
    int m_numChannels = 0;
    HyperstackTiff* m_stack = nullptr;
    QList<HyperstackTiff*> m_sparseStacks;
    int m_preRotation = 0;
    float m_scaling = 1.0;
    int m_zoomFactor = 100;
    int m_zoomThresh = 100;
    float m_scaledWidth = 0;
    float m_scaledHeight = 0;
    float m_scaledInvWidth = 0;
    float m_scaledInvHeight = 0;
    qreal m_offsetX = 0;
    qreal m_offsetY = 0;
    qreal m_textureOffsetX = 0;
    qreal m_textureOffsetY = 0;
    //int m_textureWidth = 0;
    //int m_textureHeight = 0;
    bool m_fadeRegisteredUpdated = false;
    bool m_registrationUpdated = false;
    bool m_windowSizeChanged = false;
    bool m_colorMappingChanged = false;
    bool m_imageChanged = false;
    bool m_offsetChanged = false;
    bool m_zoomButton = false;
    uchar m_iBase;
    bool m_synced = false;
    bool m_histEqualization = false;
    bool m_loadingConfig = false;
    // NOTE: enable in-place registration view. May want to remove if we
    // prefer the separate registration window.
    bool m_registrationEnabled = false;
    int m_currentZ = 0;
    int m_zThick = 0;
    QString m_projection;
    // Registration mode: all moving sessions are overlaid on the fixed
    // session.
    bool m_registrationMode = false;
    // Amount to fade out the registered image (fully transparent at 1.0)
    float m_fadeRegistered = 0.5;
    //QHash<QString, RoiData::roi_t> m_visibleRois;
    QStringList getChannelStringList();

    // raw mouse coordinates of slice image widget
    qreal m_mouseX = 0;
    qreal m_mouseY = 0;
    // scaled with size of image/size of slice widget
    qreal m_mouseX_tr = 0;
    qreal m_mouseY_tr = 0;
    qreal mouseXFactor = 1;
    qreal mouseYFactor = 1;
    // mouse location based on image pixel
    qreal m_mouseXOnImage = 0;
    qreal m_mouseYOnImage = 0;
    qreal m_preMouseXOnImage = 0;
    qreal m_preMouseYOnImage = 0;
    // width and height for displayed qRect of the slice
    qreal w = 0;
    qreal h = 0;
    qreal preW = 0;
    qreal preH = 0;
    // coordinates (x, y) of the upper left corner for displayed qRect of the slice
    qreal x = 0;
    qreal y = 0;
    qreal preX = 0;
    qreal preY = 0;
    qreal x0;
    qreal y0;

    int preZoomFactor = 100;
    qreal sizeX;
    qreal sizeY;

    bool m_getZoomSettings = false;
    bool m_centerSelected = false; // when a landmark or ROI is clicked for centering in zoom
    VoxelCoordinate m_coords; // coordinates of landmark or ROIs
    qint64 lastRenderedTime;
};

#endif // SLICEIMAGE_HPP
