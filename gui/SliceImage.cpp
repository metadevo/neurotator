﻿/// @author Sam Kenyon <sam@metadevo.com>
#include <QDebug>
#include <QObject>
#include <QPainter>
#include <QSGNode>
#include <QSGImageNode>
#include <QSGSimpleRectNode>
#include <QString>
#include <QQuickWindow>
#include <QColorDialog>

#include <stdlib.h>
#include <cmath>
#include <iostream>

#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkIndent.h>
#include <vtkQImageToImageSource.h>

#include "ApplicationData.hpp"
#include "HyperstackTiff.hpp"
#include "Render.hpp"
#include "SliceImage.hpp"

SliceImage::SliceImage()
{
    try
    {
        m_iBase = ApplicationData::instance().getIbase();
        setFlag(QQuickItem::ItemHasContents);
        m_project = ApplicationData::instance().getProject();


        if (m_project->isSparseAnnotation()){
            // reset to previous zoom settings
            if(ApplicationData::instance().getZoomFactor() != 100){
                m_getZoomSettings = true;
            }
            auto start = std::chrono::high_resolution_clock::now();
            openStack(dynamic_cast<ProjectAnnotationSparse*>(m_project)->currentStack());
            auto finish = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = finish - start;
            qDebug() << "Elapsed time: (sliceimage opens an stack) " << elapsed.count() << " s\n";
        }else{
            openStack(dynamic_cast<ProjectDense*>(m_project)->currentStack());
        }

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiSelected(QString, QString)),
                         this, SLOT(roiSelectionRequest(QString, QString)));
        QObject::connect(&ApplicationData::instance(), SIGNAL(refSessionChanged()),
                         this, SLOT(checkIsRef()));
        QObject::connect(&ApplicationData::instance(), SIGNAL(fixedSessionChanged()),
                         this, SLOT(updateRegistration()));
        QObject::connect(&ApplicationData::instance(), SIGNAL(channelColorChanged()),
                         this, SLOT(updateMappingColor()));

        // for sparse annotation mode
        if (m_project->isSparseAnnotation()) {
            QObject::connect(&ApplicationData::instance(), SIGNAL(updateAllSliceImages()),
                             this, SLOT(update()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(gotoZ(const int)),
                             this, SLOT(gotoZ(const int)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(nextZ()),
                             this, SLOT(nextZ()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(prevZ()),
                             this, SLOT(prevZ()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(setAllMouseX(const qreal)),
                             this, SLOT(setMouseXs(const qreal)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(setAllMouseY(const qreal)),
                             this, SLOT(setMouseYs(const qreal)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(setAllZoomFactors(const int)),
                             this, SLOT(setZoomFactors(const int)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(adjustOffset(const int, const int)),
                             this, SLOT(adjustOffsets(const int, const int)));
        }

    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
    }
}

SliceImage::~SliceImage()
{
    if (m_stack) {
        m_stack->removeClient();
    }
}

void SliceImage::openStack(HyperstackTiff* stack){

    m_stack = stack;
    if (m_stack) {
        //m_stack->addClient(); // for future stuff
        m_maxColorValue = m_stack->maxColorValue();
        m_name = m_stack->name();
        m_stackWindowIndex = m_project->currentStackWindowIndex();

        // when loading registration window, restore previous channel colors from the two sessions
        // instead of resetting the colors to pre-defined colors
        if (m_stackWindowIndex == REG_WINDOW_INDEX) {
            for (unsigned int c =0 ; c < m_stack->size().c; c++){
                std::vector<QColor> colors = ApplicationData::instance().getPreChannelColors(m_name);
                if (!colors.size())
                    colors = PREDEFINED_CHANNEL_COLORMAP;
                channelColors.push_back(colors.at(c));
            }
        // when this is not a registration window, set and store the pre-defined channel colors
        }else{
            for (unsigned int c =0 ; c < m_stack->size().c; c++){
                channelColors.push_back(PREDEFINED_CHANNEL_COLORMAP.at(c));
            }
            ApplicationData::instance().setPreChannelColors(m_name,channelColors);
        }

        m_stack->updateChannelColormap(channelColors);
        emit initializeWindowLevelUi(getScalarRangeForChannel(0), getWindowLevelForChannel(0));
        if (m_stackWindowIndex == REG_WINDOW_INDEX) {
            QObject::connect(&dynamic_cast<ProjectRegistration*>(m_project)->landmarkModel(), SIGNAL(transformsChanged()),
                this, SLOT(updateRegistration()));
        }
    } else {
        qWarning() << "ERROR: stack is null!";
    }
}

QStringList SliceImage::getChannelStringList() {
    QStringList list;
    for (unsigned int i=0; i < m_stack->size().c; i++)
        list << QString::number(i);
    return list;
}

void SliceImage::showColorSelector(QString channel) {
    QColor color = QColorDialog::getColor(getColorForChannel(channel));
    if( color.isValid() )
    {
      setColorForChannel(channel, color);
      ApplicationData::instance().setPreChannelColors(m_name,channelColors);
      if (m_stack) m_stack->updateChannelColormap(channelColors);
      ApplicationData::instance().broadcastChannelColorChanged();
      emit userChangedChannelColor(color.name(QColor::HexRgb));
    }
}

// initialize channel colors (number of channels could be different for each slice)
void SliceImage::setChannelColorsSparse(QList<int> channels) {
    std::vector<QColor> colorMapSparse = ApplicationData::instance().getColorMapSparse();
    for(int it : channels){
        QColor color = colorMapSparse[it];
        if( color.isValid() )
        {
          setColorForChannel(QString::number(channels.indexOf(it)), color);
          if (m_stack) m_stack->updateChannelColormap(channelColors);
          ApplicationData::instance().broadcastChannelColorChanged();
          emit userChangedChannelColor(color.name(QColor::HexRgb));
        }
    }
}

void SliceImage::setColorForChannel(QString channel, QColor color) {
    unsigned int channelIndex = channel.toUInt();
    channelColors[channelIndex] = color;
    m_colorMappingChanged = true;
}

QColor SliceImage::getColorForChannel(QString channel) {
    unsigned int channelIndex = channel.toUInt();
    return channelColors.at(channelIndex);
}

std::vector< int> SliceImage::getScalarRangeForChannel(QString channel) {
    unsigned int channelIndex = channel.toUInt();
    std::vector<int> result = (m_stack)? (m_stack->getScalarRangeForChannel(channelIndex)) : std::vector< int> {0,0};
    return result;
}

std::vector< int> SliceImage::getWindowLevelForChannel(QString channel) {
    unsigned int channelIndex = channel.toUInt();
    std::vector<int> result = (m_stack)? (m_stack->getWindowLevelForChannel(channelIndex)) : std::vector< int> {0,0};
    return result;
}


bool SliceImage::getChannelState(QString channel) {
    unsigned int channelIndex = channel.toUInt();
    return (m_stack)? (m_stack->getChannelState(channelIndex)) : false;
}

void SliceImage::setWindowLevelForChannel(QString channel, int wmin, int wmax) {
    if (m_stack) {
        unsigned int channelIndex = channel.toUInt();
        m_stack->updateWindowLevelForChannel(channelIndex, std::vector<int>{wmin, wmax});
        m_colorMappingChanged = true;
    }
}

void SliceImage::toggleChannelOnOff(QString channel, bool state) {
    if (m_stack) {
        unsigned int channelIndex = channel.toUInt();
        m_stack->toggleChannelState(channelIndex, state);
        m_colorMappingChanged = true;
        emit channelToggled();
    }
}

QString SliceImage::getIndexStr() const
{
    QString s;
    if (m_stack)
    {
        HyperstackIndex index = m_stack->index();
        index.z = m_currentZ;
        s = QStringLiteral("z: %1  t: %2").arg(index.z + m_iBase).arg(index.time + m_iBase);
    } else {
        s = "???";
    }
    return s;
}

bool SliceImage::isMoving() const {
    if (REGISTRATION_PROJECT(m_project)) {
        auto fixed = REGISTRATION_PROJECT(m_project)->fixedSessionName();
        return !fixed.isEmpty() && fixed != m_name;
    }
    return false;
}

vtkSmartPointer<vtkAbstractTransform> SliceImage::getTransform() const
{
    if (registrationEnabled())
        return REGISTRATION_PROJECT(ApplicationData::instance().getProject())->getTransform(m_name);
    return nullptr;
}

QImage blendImages(const QImage &back, const QImage &front, float fade)
{
    QImage result(back.width(), back.height(), QImage::Format_ARGB32_Premultiplied);
    QPainter painter(&result);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage(0, 0, back);
    painter.setOpacity(1.0 - fade);
    painter.drawImage(0, 0, front);
    return result;
}

///  Called on the render thread. Main thread is blocked while this function is executed.
QSGNode* SliceImage::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData)
{

    Q_UNUSED(updatePaintNodeData)
    QSGImageNode* n = static_cast<QSGImageNode*>(oldNode);

    if (!m_stack) {
        qWarning() << "ERROR: stack is null!";
        setWidth(800);
        setHeight(600);
        QSGSimpleRectNode* emptyNode = new QSGSimpleRectNode();
        emptyNode->setColor(Qt::blue);
        emptyNode->setRect(boundingRect());
        return emptyNode;
    }

    if (!n) {
        n = window()->createImageNode();
        n->setOwnsTexture(true);
        n->setFiltering(QSGTexture::Linear);
        setWidth(m_stack->size().x);
        setHeight(m_stack->size().y);

        // We're loading part of config here because it doesn't work if we do it before the first paint
        m_loadingConfig = true;

        const StackWindow& config = m_project->getStackWindowConfig(m_stackWindowIndex);

        // order of operations matters here
        if (static_cast<int>(width()) != static_cast<int>(config.width)) {
            setScaledWidth(static_cast<int>(config.width));
        }
        if (m_zoomFactor != config.zoomFactor) {
            setZoomFactor(config.zoomFactor);
        }

        if (m_offsetX != config.offsetX || m_offsetY != config.offsetY) {
            setOffsets(config.offsetX, config.offsetY);
        }

        if (m_preRotation != config.preRotation) {
            setPreRotation(config.preRotation);
        }

        m_loadingConfig = false;
        m_imageChanged = true;
        m_windowSizeChanged=true;
    }

    // Get the latest buffer if it's ready

    if (m_imageChanged || m_colorMappingChanged || (m_registrationMode && (m_fadeRegisteredUpdated || m_registrationUpdated))) {
        QImage image;
        if (m_stack->ready()) {
            if (m_project->isSparseAnnotation()){

                int zLower;
                int zUpper;
                if(m_projection == "max"){
                    zLower = m_currentZ;
                    zUpper = getMaxZ();
                }else if(m_projection == "min"){
                    zLower = 0;
                    zUpper = m_currentZ;
                }else{
                    zLower = m_currentZ - m_zThick;
                    zUpper = m_currentZ + m_zThick;
                }

                if(zLower < 0) zLower = 0;
                if(zUpper > getMaxZ()) zUpper = getMaxZ();
                // takes <= 0.02s
                image = m_stack->imageBufferSparse(zLower, zUpper);
            }else{
                image = m_stack->imageBuffer(m_currentZ);
            }

                if (!m_registrationMode) {
                    QTransform qRot;
                    qRot.rotate(m_preRotation, Qt::ZAxis);
                    QImage image_rotated = image.transformed( qRot , Qt::SmoothTransformation);
                    int xoffset = (image_rotated.width() - image.width())/2;
                    int yoffset = (image_rotated.height() - image.height())/2;
                    image = image_rotated.copy(xoffset, yoffset, image.width(), image.height());
                }
        }
        if (image.isNull()) {
            qWarning() << "ERROR: image is null!";
            return n;
        }
        if (m_registrationMode && (m_imageChanged || m_colorMappingChanged || m_registrationUpdated || m_fadeRegisteredUpdated )) {
            auto stacks = dynamic_cast<ProjectRegistration*>(m_project)->renderList();
            QImage image2 = renderSlice(stacks, m_currentZ, m_stack->size());

            image = blendImages(image, image2, m_fadeRegistered);
            m_registrationUpdated = false;
            m_fadeRegisteredUpdated = false;
        }

        QSGTexture* texture = window()->createTextureFromImage(image, QQuickWindow::TextureIsOpaque);
        if (!texture)
        {
            qWarning() << "ERROR: Texture null!";
            return n;
        }
        n->setTexture(texture);
        m_imageChanged = false;
        m_colorMappingChanged = false;
    }

    // reset or check offsets
    if (m_zoomFactor <= 100.0){
        m_offsetX = m_offsetY = 0;
        preX = 0;
        preY = 0;
        preW = m_stack->size().x;
        preH = m_stack->size().y;
        sizeX = m_stack->size().x;
        sizeY = m_stack->size().y;
        x0 = 0;
        y0 = 0;
        mouseXFactor = 1;
        mouseYFactor = 1;
    }    
    if(m_project->isSparseAnnotation() && m_getZoomSettings){
        m_zoomFactor = ApplicationData::instance().getZoomFactor();
        m_zoomButton = ApplicationData::instance().getZoomButton();
        QHash<QString, qreal> zoomSettings = ApplicationData::instance().getZoomSettings();
        m_offsetX = zoomSettings["offsetX"];
        m_offsetY = zoomSettings["offsetY"];
        x0 = zoomSettings["x0"];
        y0 = zoomSettings["y0"];
        preX = zoomSettings["preX"];
        preY = zoomSettings["preY"];
        preW = zoomSettings["preW"];
        preH = zoomSettings["preH"];
        mouseXFactor = zoomSettings["mouseXFactor"];
        mouseYFactor = zoomSettings["mouseYFactor"];

    }

    if (m_offsetChanged || m_centerSelected || m_getZoomSettings) {

        // emit scaleChanged(m_scaling, m_zoomFactor/100.0, -m_textureOffsetX, -m_textureOffsetY, preX0, preY0);

        // if using zoom buttons, zoom center is the center of the displayed slice
        if (m_zoomButton){
            m_mouseX_tr = m_stack->size().x/2;
            m_mouseY_tr = m_stack->size().y/2;

        }else{
            // transform mouse coordinates into the scale of QImage pixel coordinates.
            m_mouseX_tr = m_mouseX*m_stack->size().x/width();
            m_mouseY_tr = m_mouseY*m_stack->size().y/height();
        }

        // adjust width and height of slice to zoom
        w = m_stack->size().x/(m_zoomFactor/100.0);
        h = m_stack->size().y/(m_zoomFactor/100.0);

        m_mouseXOnImage = x0 + m_mouseX_tr * mouseXFactor;
        m_mouseYOnImage = y0 + m_mouseY_tr * mouseYFactor;

        // if zoomed and mouse location on image changed, the next zoom is based on the current zoomed slice
        if (   (m_mouseXOnImage != m_preMouseXOnImage ||
                m_mouseYOnImage != m_preMouseYOnImage) &&
                preZoomFactor != 100 &&
                m_zoomFactor != 100){

            //adjust mouse coordinate factor
            mouseXFactor = mouseXFactor * preW/sizeX;
            mouseYFactor = mouseYFactor * preH/sizeY;

            //update previous slice size
            sizeX = preW;
            sizeY = preH;

            // update previous upper left corner coordinates
            x0 = preX;
            y0 = preY;

        }


        // adjust upper left corner coordinates to keep zoom center at where the mouse is hovering while zooming
        x = x0 + m_mouseX_tr * mouseXFactor - m_mouseX_tr* mouseXFactor*(w/sizeX);
        y = y0 + m_mouseY_tr * mouseYFactor - m_mouseY_tr* mouseYFactor*(h/sizeY);

        if(m_centerSelected){
            x = m_coords.x - w/2;
            y = m_coords.y - h/2;
            // reset offsets to keep landmark displayed in the center
            m_textureOffsetX = 0;
            m_textureOffsetY = 0;
            m_offsetX = 0;
            m_offsetY = 0;
            x0 = preX;
            y0 = preY;
        }

        // check boundaries
        if (x < 0)
            x = 0;
        else if (x > m_stack->size().x - m_stack->size().x/(m_zoomFactor/100.0))
            x = m_stack->size().x - m_stack->size().x/(m_zoomFactor/100.0);

        if(y < 0)
            y = 0;
        else if (y > m_stack->size().y - m_stack->size().y/(m_zoomFactor/100.0))
            y = m_stack->size().y - m_stack->size().y/(m_zoomFactor/100.0);

        // check offset
        if (x + m_offsetX < 0)
            m_offsetX = - x;
        else if (x + m_offsetX > (m_stack->size().x - m_stack->size().x/(m_zoomFactor/100.0)))
            m_offsetX = (m_stack->size().x - m_stack->size().x/(m_zoomFactor/100.0)) - x;
        if (y + m_offsetY < 0)
            m_offsetY = -y;
        else if (y + m_offsetY > (m_stack->size().y - m_stack->size().y/(m_zoomFactor/100.0)))
            m_offsetY = (m_stack->size().y - m_stack->size().y/(m_zoomFactor/100.0)) - y;

        m_textureOffsetX = m_offsetX;
        m_textureOffsetY = m_offsetY;

        n->setSourceRect(x + m_textureOffsetX, y + m_textureOffsetY, w, h);

        preZoomFactor = m_zoomFactor;
        m_preMouseXOnImage = x0 + m_mouseX_tr * mouseXFactor;
        m_preMouseYOnImage = y0 + m_mouseY_tr * mouseYFactor;
        preX = x;
        preY = y;
        preW = w;
        preH = h;

        if(m_project->isSparseAnnotation()){
            QHash<QString, qreal> zoomSettings;
            zoomSettings["offsetX"] = m_offsetX;
            zoomSettings["offsetY"] = m_offsetY;
            zoomSettings["x0"] = x0;
            zoomSettings["y0"] = y0;
            zoomSettings["preX"] = preX;
            zoomSettings["preY"] = preY;
            zoomSettings["preW"] = preW;
            zoomSettings["preH"] = preH;
            zoomSettings["mouseXFactor"] = mouseXFactor;
            zoomSettings["mouseYFactor"] = mouseYFactor;
            ApplicationData::instance().setZoomSettings(zoomSettings, m_zoomButton);
        }
        m_getZoomSettings = false;
        m_zoomButton = false;
        m_offsetChanged = false;
        m_centerSelected = false;

        emit scaleChanged(m_scaling, m_zoomFactor/100.0, -m_textureOffsetX, -m_textureOffsetY, x, y);
    }

    if (m_windowSizeChanged) {
        n->setRect(0, 0, width(), height());
        m_windowSizeChanged = false;
    }
    return n;
}

void SliceImage::windowPositionChanged(const int x, const int y)
{
    m_project->saveStackWindowPosition(m_stackWindowIndex, x, y);
}

//void SliceImage::windowHidden()
//{
//    m_project->saveStackWindowHidden(m_stackWindowIndex);
//}

void SliceImage::adjustOffset(const int x, const int y)
{
//    if (m_zoomFactor <= m_zoomThresh) {
//        return;
//    }
    m_offsetX -= x;
    m_offsetY -= y;

    m_offsetChanged = true;
    if (m_synced) {
        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY, m_mouseX, m_mouseY, m_zoomButton);
    }
    if (!m_loadingConfig) {
        m_project->saveStackOffset(m_stackWindowIndex, m_offsetX, m_offsetY);
    }
}

void SliceImage::setOffsets(const float x, const float y)
{
    if (x != m_offsetX) {
        m_offsetX = x;
    }
    if (y != m_offsetY) {
        m_offsetY = y;
    }

    m_offsetChanged = true;

    if (!m_loadingConfig) {
        m_project->saveStackOffset(m_stackWindowIndex, m_offsetX, m_offsetY);
    }
}


void SliceImage::setZoomFactor(const int zoomFactor)
{
//    qDebug() << "Width: " << width() << ", getMaxX: " << getMaxX();
//    qDebug() << "zoom input: " << zoomFactor;
//    qDebug() << "zoomFactor: " << m_zoomFactor;
//    qDebug() << "m_zoomThresh: " << m_zoomThresh;
    m_zoomFactor = zoomFactor;
    if (m_zoomFactor <= 100) { //m_zoomThresh) {
        m_zoomFactor = 100;
        m_textureOffsetX = 0;
        m_textureOffsetY = 0;
        m_offsetX = 0;
        m_offsetY = 0;
    }

    if (m_zoomFactor < 1) {
        m_zoomFactor = 1;
    }
    if (m_zoomFactor > 2000) {
        m_zoomFactor = 2000;
    }
//    if (m_zoomFactor < m_zoomThresh) {
//        m_zoomFactor = m_zoomThresh;
//    }
    //qDebug() << "new m_zoomFactorh: " << m_zoomFactor;

//    m_scaling = static_cast<float>(m_zoomFactor) / 100.0f;
//    m_scaledWidth = getMaxX() * m_scaling;
//    m_scaledHeight = getMaxY() * m_scaling;
//    qDebug() << "Z scaled: " << m_scaledWidth  << ", " << m_scaledHeight;
//    m_scaledInvWidth = width() / m_scaling;
//    m_scaledInvHeight = height() / m_scaling;

//    qDebug() << "scaled: " << m_scaledWidth << ", " << m_scaledHeight;
//    qDebug() << "invscaled: " << m_scaledInvWidth << ", " << m_scaledInvHeight;

    emit zoomFactorChanged(m_zoomFactor);

    if (!m_loadingConfig) {
        m_project->saveStackZoom(m_stackWindowIndex, m_zoomFactor);
    }

    m_offsetChanged = true;
    if (m_synced) {
        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY, m_mouseX, m_mouseY, m_zoomButton);
    }
}

void SliceImage::setPreRotation(int val)
{
    if REGISTRATION_PROJECT(m_project) {
        m_preRotation = val;
        REGISTRATION_PROJECT(m_project)->cachePreRotation( m_name, m_preRotation);
        m_imageChanged = true;
        if (!m_loadingConfig) {
            REGISTRATION_PROJECT(m_project)->saveStackPreRotation(m_stackWindowIndex, m_preRotation);
       }
        emit preRotationChanged(m_preRotation);
    }
}

/// Setting this overwrites whatever the zoomfactor was
void SliceImage::setScaledWidth(const int w)
{
    m_scaledWidth = w;
    if (m_scaledWidth < 1) {
        m_scaledWidth = 1;
    }
    m_scaling = static_cast<float>(m_scaledWidth) / getMaxX();
    m_scaledHeight = getMaxY() * m_scaling;

    setWidth(m_scaledWidth);
    setHeight(m_scaledHeight);
    m_scaledInvWidth = m_scaledWidth / m_scaling;
    m_scaledInvHeight = m_scaledHeight / m_scaling;

//    m_textureOffsetX = 0;
//    m_textureOffsetY = 0;
//    m_offsetX = 0;
//    m_offsetY = 0;

//    updateZoomThresh();
//    m_zoomFactor = static_cast<int>(m_scaling * 100.0f);
//    emit zoomFactorChanged(m_zoomFactor);
    m_windowSizeChanged = true;
    emit scaleChanged(m_scaling, m_zoomFactor/100.0, -m_textureOffsetX, -m_textureOffsetY, preX, preY);
    if (!m_loadingConfig) {
        m_project->saveStackZoom(m_stackWindowIndex, m_zoomFactor);
        m_project->saveStackWidth(m_stackWindowIndex, m_scaledWidth); // only saving width since aspect ratio is maintained
    }
}

/// Setting this overwrites whatever the zoomfactor was
void SliceImage::setScaledHeight(const int h)
{
    m_scaledHeight = h;
    if (m_scaledHeight < 1) {
        m_scaledHeight = 1;
    }
    m_scaling = static_cast<float>(m_scaledHeight) / getMaxY();
    m_scaledWidth = getMaxX() * m_scaling;
    setWidth(m_scaledWidth);
    setHeight(m_scaledHeight);
    m_scaledInvWidth = m_scaledWidth / m_scaling;
    m_scaledInvHeight = m_scaledHeight / m_scaling;

//    m_textureOffsetX = 0;
//    m_textureOffsetY = 0;
//    m_offsetX = 0;
//    m_offsetY = 0;

//    updateZoomThresh();
//    m_zoomFactor = static_cast<int>(m_scaling * 100.0f);
//    emit zoomFactorChanged(m_zoomFactor);
    m_windowSizeChanged = true;
    emit scaleChanged(m_scaling, m_zoomFactor/100.0, -m_textureOffsetX, -m_textureOffsetY, preX, preY);

    if (!m_loadingConfig) {
        m_project->saveStackZoom(m_stackWindowIndex, m_zoomFactor);
        m_project->saveStackWidth(m_stackWindowIndex, m_scaledWidth); // only saving width since aspect ratio is maintained
    }
}

void SliceImage::resetZoom()
{
    setZoomFactor(100);
}

/// This adjusts the viewing configuration from a saved project
void SliceImage::loadWindowConfig()
{
    m_loadingConfig = true;

    const StackWindow& config = m_project->getStackWindowConfig(m_stackWindowIndex);

    if (getIndexZ() != static_cast<int>(config.coords.z)) {
        gotoZ(static_cast<int>(config.coords.z + m_iBase));
    }

    // width, zoom and offsets are loaded in updatePaintNode() when it first creates the node

    emit changeStackWindowPosition(config.windowX, config.windowY);

    m_loadingConfig = false;
}

///@param z: user index which is typically base 1 NOT 0 (base defined by member m_iBase)
void SliceImage::gotoZ(const int z)
{
    int z0 = z - m_iBase; // ensure it's a zero-based index;
    if (z0 < 0 || !m_stack || z0 >= getMaxZ()) {
        return;
    }
    if (z0 == m_currentZ)
        return;

    m_currentZ = z0;
    m_imageChanged = true;

    // HyperstackIndex index;
    // index.z = static_cast<unsigned int>(z - m_iBase); // ensure it's a zero-based index
    // m_stack->loadSlice(index);

    if (m_synced) {
        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime(), m_zoomFactor, m_offsetX, m_offsetY, m_mouseX, m_mouseY, m_zoomButton);
    }

    if(m_project->isSparseAnnotation()){
        ApplicationData::instance().setIndexZ(m_currentZ);
    }

    if (!m_loadingConfig) {
       m_project->saveStackZ(m_stackWindowIndex, m_currentZ);
    }
}

///@param t: user index which is typically base 1 NOT 0 (base defined by member m_iBase)
void SliceImage::gotoTime(const int t)
{
    if (t < 0 || !m_stack) {
        return;
    }
    HyperstackIndex index;
    index.time = static_cast<unsigned int>(t - m_iBase); // ensure it's a zero-based index
//    m_stack->loadSlice(index);

//    if (m_synced) {
//        ApplicationData::instance().broadcastSyncChange(getIndexZ(), getIndexTime());
//    }
//    if (!m_loadingConfig) {
//        m_project->saveStackTime(m_stackWindowIndex, index.time);
//    }
}

void SliceImage::nextZ() {
    gotoZ(m_currentZ + m_iBase + 1);
}

void SliceImage::prevZ() {
    gotoZ(m_currentZ + m_iBase - 1);
}

/// If this stack is the specified session then navigate to the correct slice
/// and select the ROI.
void SliceImage::roiSelectionRequest(QString sessionKey, QString roiKey)
{
    if (DENSE_PROJECT(m_project)) {
        if (sessionKey == m_name) {
            // get the data structure
            RoiDataBase::roi_t roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
            if (!roi)
            {
                qWarning() << "ROI " << roiKey << " does not exist in this session.";
                return;
            }

            //LOG(DBUG) << "navigate to slice";
            gotoZ(roi->getCentroid().z + m_iBase);

            m_coords = roi->getCentroid();
            m_centerSelected = true;

            emit navChanged();
        }
    }
}

bool SliceImage::isRef()
{
    if (DENSE_PROJECT(m_project))
        return (m_name == DENSE_PROJECT(m_project)->roiData()->refSessionName());
    return false;
}

void SliceImage::updateRegistration()
{
    // NOTE: this whole function is probably vestigial now
    // if (m_stack) {
    //     if (registrationEnabled()) {
    //         m_stack->setTransformedSize(m_project->fixedStack()->size());
    //     }

    //     m_stack->setTransform(getTransform());
    //     if (m_currentZ >= getMaxZ()) {
    //         m_currentZ = 0;
    //     }
    // }
    m_registrationUpdated = true;
    update();
    emit maxZChanged();
}
