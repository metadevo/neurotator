/// @author Sam Kenyon <sam@metadevo.com>
#ifndef APPLICATIONDATA_HPP
#define APPLICATIONDATA_HPP
#include <memory>
#include <utility>
#include <QCoreApplication>
#include <QList>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QUrl>
#include <QQmlListProperty>

#include "AppSettings.hpp"
#include "Project.hpp"
#include "ProjectRegistration.hpp"
#include "ProjectAnnotationSparse.hpp"
#include "SearchResults.hpp"

const std::vector<QColor> PREDEFINED_CHANNEL_COLORMAP_SPARSE = {
    QColor("#90ee90"), QColor("#ff0000"), QColor("#ff00ff"),
    QColor("#40e0d0"), QColor("#add8e6"), QColor("#ff007f"), QColor("#ffcc99")
};

#define DENSE_PROJECT(x) (dynamic_cast<ProjectDense*>(x))
#define SPARSE_PROJECT(x) (dynamic_cast<ProjectAnnotationSparse*>(x))
#define REGISTRATION_PROJECT(x) (dynamic_cast<ProjectRegistration*>(x))

/// Wrapper for QML access to current project data and app settings
class ApplicationData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString windowTitle READ getWindowTitle WRITE setWindowTitle NOTIFY windowTitleChanged)
    Q_PROPERTY(QString projectName READ getProjectName WRITE setProjectName NOTIFY projectNameChanged)
    Q_PROPERTY(QString refSession READ refSession WRITE setRefSession NOTIFY refSessionChanged)
    Q_PROPERTY(QString dirty READ getDirty NOTIFY dirtyChanged)
    Q_PROPERTY(QString cmdText READ getLastCmd NOTIFY lastCmdChanged)
    Q_PROPERTY(bool linkInProgress READ isLinking NOTIFY linkInProgressChanged)
    Q_PROPERTY(bool refExists READ refExists NOTIFY refExistsChanged)
    Q_PROPERTY(RoiDataModel* confirmedModel READ getConfirmedModel NOTIFY newModel)

    Q_PROPERTY(LandmarkDataModel* landmarkModel READ getLandmarkModel NOTIFY newLandmarkModel)
    Q_PROPERTY(QStringList searchableSources READ getSearchableSources NOTIFY searchableSourcesChanged)
    Q_PROPERTY(QQmlListProperty<SearchResult> searchResults READ getSearchResults NOTIFY searchResultsChanged)
    Q_PROPERTY(QStringList sessionNames READ getSessionNames NOTIFY sessionNamesChanged)
    Q_PROPERTY(QString fixedSession READ fixedSession WRITE setFixedSession NOTIFY fixedSessionChanged)
    Q_PROPERTY(QString transformMode READ transformMode WRITE setTransformMode
               NOTIFY transformModeChanged)
    Q_PROPERTY(QString roiTransformMode READ roiTransformMode WRITE setRoiTransformMode
               NOTIFY roiTransformModeChanged)
    Q_PROPERTY(bool landmarkPredictionDown READ landmarkPredictionDown WRITE setLandmarkPredictionDown)
    Q_PROPERTY(QStringList availableTransformModes READ availableTransformModes
               NOTIFY availableTransformModesChanged)
    Q_PROPERTY(QString projectMode READ getProjectMode NOTIFY projectModeChanged)

    // for sparse annotation mode
    Q_PROPERTY(float mouseX READ getMouseX WRITE setMouseX)
    Q_PROPERTY(float mouseY READ getMouseY WRITE setMouseY)
    Q_PROPERTY(int zoomFactor READ getZoomFactor WRITE setZoomFactor NOTIFY zoomFactorChanged)
    Q_PROPERTY(RoiDataSparseModel* sparseRoiModel READ getSparseRoiModel)
    Q_PROPERTY(QString roiEditMode READ getRoiEditMode WRITE setRoiEditMode NOTIFY roiEditModeChanged)
    Q_PROPERTY(bool showCandidates READ getShowCandidates WRITE setShowCandidates NOTIFY showCandidatesChanged)
    Q_PROPERTY(QList<qreal> brightnesses READ getBrightnesses WRITE setBrightnesses NOTIFY brightnessesChanged)
    Q_PROPERTY(QList<qreal> contrasts READ getContrasts WRITE setContrasts NOTIFY contrastsChanged)
    Q_PROPERTY(QStringList channelList READ getChannelStringList NOTIFY channelListChanged)

    Q_PROPERTY(bool autoRegisterOnDown READ autoRegisterOnDown WRITE setAutoRegisterOnDown)
    Q_PROPERTY(VoxelCoordinate autoRegisterSize READ getAutoRegisterSize WRITE setAutoRegisterSize NOTIFY autoRegisterSizeChanged)
    Q_PROPERTY(VoxelCoordinate autoRegisterSearchSpace READ getAutoRegisterSearchSpace WRITE setAutoRegisterSearchSpace NOTIFY autoRegisterSearchSpaceChanged)
    Q_PROPERTY(VoxelCoordinate autoPopulateSpacing READ getAutoPopulateSpacing WRITE setAutoPopulateSpacing NOTIFY autoPopulateSpacingChanged)
    Q_PROPERTY(QString autoRegisterResult READ getAutoRegisterResult WRITE setAutoRegisterResult NOTIFY autoRegisterResultChanged)
    Q_PROPERTY(QString autoRegisterResultSTD READ getAutoRegisterResultSTD WRITE setAutoRegisterResultSTD NOTIFY autoRegisterResultSTDChanged)
    Q_PROPERTY(int autoRegisterSTD READ getAutoRegisterSTD WRITE setAutoRegisterSTD NOTIFY autoRegisterSTDChanged)
    Q_PROPERTY(int autoRegisterCorr READ getAutoRegisterCorr WRITE setAutoRegisterCorr NOTIFY autoRegisterCorrChanged)

public:
    ApplicationData(ApplicationData&) = delete;
    ApplicationData& operator=(ApplicationData&) = delete;

    /// Singleton
    static ApplicationData& instance() {
        static ApplicationData instance;
        return instance;
    }
    Q_INVOKABLE void createNewProject(QString projectType, QUrl url);
    Q_INVOKABLE QVariant getSetting(QString name) const { return AppSettings::instance().get(name); }
    Q_INVOKABLE QString getSettingString(QString name) const { return AppSettings::instance().get(name).toString(); }
    Q_INVOKABLE void unloadProject();
    Q_INVOKABLE void loadProject(QUrl url);
    Q_INVOKABLE void saveProjectAs(QUrl url);
    Q_INVOKABLE void saveAllRoiFiles();
    Q_INVOKABLE void loadStack(QUrl url);
    Q_INVOKABLE void loadStacks(QList<QUrl> url);
    Q_INVOKABLE void unloadStack(const QString& name);
    Q_INVOKABLE void loadRoiData(QUrl url);
    Q_INVOKABLE QList<QUrl> openSparseRoiFileDialog();
    Q_INVOKABLE QVariantList getSparseStackUrlMatrix(QString RoiDirectoryName){ return SPARSE_PROJECT(m_project)->getStackUrlMatrix(RoiDirectoryName);}
    Q_INVOKABLE void unloadRoiData(QString name);
//    Q_INVOKABLE void setRecentProjectDir(QUrl url) const { AppSettings::instance().set("defaultprojectdir", url.toString()); }
    Q_INVOKABLE QUrl getRecentProjectDir() const { return AppSettings::instance().recentProjectDir(); }
    Q_INVOKABLE QUrl getRecentImportDir() const { return QUrl::fromLocalFile(AppSettings::instance().get("default_import_dir").toString()); }
    Q_INVOKABLE void setRecentImportDir(QUrl url) { AppSettings::instance().set("default_import_dir", url.toString()); }

    // get project info
    Q_INVOKABLE bool isProjectAnnotation() {return m_project->isProjectAnnotation(); }
    Q_INVOKABLE unsigned int numLoadedStacks() {return m_project ? m_project->numLoadedStacks() : 0; }
    Q_INVOKABLE unsigned int numLoadedRoiFiles() { return m_project ? m_project->numLoadedRoiFiles() : 0; }
    Q_INVOKABLE unsigned int maxRoiFilesAllowed() { return m_project ? m_project->maxRoiFilesAllowed() : 0; }
    Q_INVOKABLE int getNumOfSessions() const { return DENSE_PROJECT(m_project) ? DENSE_PROJECT(m_project)->roiData()->sessionCount(): 0; }
    Q_INVOKABLE bool selectRoi(const QString& sessionKey, const QString& roiKey, bool selectInConfirmed = false);
    //Q_INVOKABLE RoiDataModel* getLastUnconfirmedModel() const { return m_project->lastUnconfirmedModel(); }
    Q_INVOKABLE RoiDataModel* getUnconfirmedModel(const int index) const { return DENSE_PROJECT(m_project) ? DENSE_PROJECT(m_project)->unconfirmedModel(index) : nullptr; }
    Q_INVOKABLE QList<QUrl> getRecentProjects() { return AppSettings::instance().recentProjects(); }
    Q_INVOKABLE QStringList getRecentProjectNames() { return AppSettings::instance().recentProjectNames(); }
    Q_INVOKABLE void clearRecentProjects() { AppSettings::instance().clearRecentProjects(); }
    Q_INVOKABLE int getCurrentStackWindow() { return m_project->currentStackWindowIndex(); }
    Q_INVOKABLE void setCurrentStackWindow(const int index) { m_project->setCurrentStackWindow(index); }
    Q_INVOKABLE void makeRegistrationStackCurrent() { m_project->setCurrentStackWindow(REG_WINDOW_INDEX); }
    Q_INVOKABLE QList<QUrl> getStackWindowUrls() { return m_project->stackWindowUrls(); }
    Q_INVOKABLE void searchRois(const QString& sessionKey, QString namePattern);
    Q_INVOKABLE void updateTransforms();
    Q_INVOKABLE void renderTransform();
    Q_INVOKABLE void autoRegisterSelectedLandmarks();
    Q_INVOKABLE void populateLandmarks();
    Q_INVOKABLE void sortLandmarksByCorrelation();

    Q_INVOKABLE float getAutoRegisterCorrelation(int);

    // for sparse annotation mode
    Q_INVOKABLE QString getRoiEditMode() { return m_roiEditMode; }
    Q_INVOKABLE bool getShowCandidates() { return m_showCandidates; }
    Q_INVOKABLE QList<qreal> getBrightnesses() {return m_brightnesses; }
    Q_INVOKABLE QList<qreal> getContrasts() { return m_contrasts; }
    Q_INVOKABLE void setRoiEditMode(const QString editMode) { m_roiEditMode = editMode; emit roiEditModeChanged(); }
    Q_INVOKABLE void setShowCandidates(const bool showCandidate) { m_showCandidates = showCandidate; emit showCandidatesChanged(); }
    Q_INVOKABLE void setBrightnesses(const QList<qreal> brightnesses) { m_brightnesses = brightnesses; }
    Q_INVOKABLE void setContrasts(const QList<qreal> contrasts) { m_contrasts = contrasts; }
    Q_INVOKABLE int getCentroidInvivoZ(const QString& sessionKey){ return dynamic_cast<ProjectAnnotationSparse*>(getProject())->roiData()->getCentroidInvivoZ(sessionKey); }
    Q_INVOKABLE QColor getConfirmedMaskColor() { return m_confirmedColor; }
    Q_INVOKABLE QColor getUnconfirmedMaskColor() { return m_unconfirmedColor; }
    Q_INVOKABLE QColor getColorForChannel(QString channel) {
        unsigned int channelIndex = channel.toUInt();
        return channelColors.at(channelIndex);
    }
    Q_INVOKABLE std::vector<QColor> getColorMapSparse(){
        return channelColors;
    }
    QStringList getChannelStringList() {
        QStringList list;
        for (unsigned int i=0; i < m_c; i++)
            list << QString::number(i);
        return list;
    }

    // for synchronize
    Q_INVOKABLE int getIndexZ() { return m_indexZ; }
    void setIndexZ(int indexZ) { m_indexZ = indexZ; }
    Q_INVOKABLE qreal getX() { return m_x; }
    Q_INVOKABLE qreal getY() { return m_y; }
    Q_INVOKABLE qreal getW() { return m_w; }
    Q_INVOKABLE qreal getH() { return m_h; }
    qreal getMouseX() { return m_mouseX; }
    qreal getMouseY() { return m_mouseY; }
    void setMouseX(qreal x){ m_mouseX = x; emit setAllMouseX(x); }
    void setMouseY(qreal y){ m_mouseY = y; emit setAllMouseY(y); }
    Q_INVOKABLE int getZoomFactor() { return m_zoomFactor; }
    Q_INVOKABLE void setZoomFactor(const int zoomFactor) {
        m_zoomFactor = zoomFactor;
        if (m_zoomFactor <= 100) { m_zoomFactor = 100; }
        if (m_zoomFactor > 2000) { m_zoomFactor = 2000; }
        emit zoomFactorChanged(m_zoomFactor);
        emit setAllZoomFactors(m_zoomFactor);
    }
    bool getZoomButton() { return m_zoomButton; }
    QHash<QString, qreal> getZoomSettings() { return m_zoomSettings; }
    void setZoomSettings (QHash<QString, qreal> zoomSettings, bool zoomButton) { m_zoomSettings = zoomSettings; m_zoomButton = zoomButton; }

    QString getWindowTitle() const { return m_windowTitle; }
    QString getProjectName() const { return m_project->name(); }
    bool isLinking() const { return (m_currLinkSource.roiKey != ""); }
    QString refSession() { return DENSE_PROJECT(m_project) ? DENSE_PROJECT(m_project)->roiData()->refSessionName() : ""; }
    QString fixedSession() { return ( REGISTRATION_PROJECT(m_project) ) ? REGISTRATION_PROJECT(m_project)->fixedSessionName() : ""; }
    bool getDirty() const { return m_dirty; }
    QString getLastCmd() { return m_lastCmd; }
    bool refExists() const { return DENSE_PROJECT(m_project) ? DENSE_PROJECT(m_project)->roiData()->refSessionExists() : false; }
    RoiDataModel* getConfirmedModel() { return DENSE_PROJECT(m_project) ? DENSE_PROJECT(m_project)->confirmedModel() : nullptr; }
    RoiDataSparseModel* getSparseRoiModel() { return SPARSE_PROJECT(m_project) ? SPARSE_PROJECT(m_project)->roiDataModel() : nullptr; }
    LandmarkDataModel* getLandmarkModel() { return ( REGISTRATION_PROJECT(m_project) ) ? REGISTRATION_PROJECT(m_project)->landmarkModel().model() : nullptr; }
    QStringList getSearchableSources();
    QStringList getSessionNames();
    QQmlListProperty<SearchResult> getSearchResults();
    int searchResultsCount() const;
    SearchResult* searchResultAt(int) const;
    // get previous settings of channel colors after setting fixed session in registration mode
    std::vector<QColor> getPreChannelColors(QString sessionName) {
        if (sessionName == getSessionNames().at(0)){
            if (!preChannelColors0.size())
                return std::vector<QColor>();
            return preChannelColors0;
        }else if (sessionName == getSessionNames().at(1)){
            if (!preChannelColors1.size())
                return std::vector<QColor>();
            return preChannelColors1;
        }
        qDebug() << "ApplicationData.getPreChannelColors: stack Index error";
        return std::vector<QColor>();
    }

    void setWindowTitle(const QString& title);
    void setProjectName(const QString& name);
    void setProject(Project* project);
    Project* getProject() { return m_project; }
    void setRefSession(const QString& sessionName);
    void setFixedSession(const QString& sessionName);
    // store previous channel colors before setting fixed session
    void setPreChannelColors(QString sessionName,std::vector<QColor> channelColors){
        if (sessionName == getSessionNames().at(0)){
            preChannelColors0 = channelColors;
        }else if (sessionName == getSessionNames().at(1)){
            preChannelColors1 = channelColors;
        }
    }

    void broadcastSyncChange(int z, int t, int zoom, float offsetX, float offsetY, float mouseX, float mouseY, bool zoombutton) {
        emit syncNavChanged(z, t, zoom, offsetX, offsetY, mouseX, mouseY, zoombutton);
    }

    uchar getIbase() { return m_iBase; }
    Q_INVOKABLE void broadcastAddingLandmark() {
        emit addingLandmark();
    }
    Q_INVOKABLE void broadcastLandmarkCompleted() {
        emit landmarkCompleted();
    }
    // for sparse annotation mode
    Q_INVOKABLE void broadcastSelectedRoiViaMouse(const float mouseX, const float mouseY, const int action){
        emit selectedRoiViaMouse(mouseX, mouseY, action);
    }
    Q_INVOKABLE void broadcastMouseClicked(const float mouseX, const float mouseY){
        emit mouseClicked(mouseX, mouseY);
    }

    void broadcastROIModified() {
        emit roiModified();
    }

    void broadcastChannelColorChanged() {
        emit channelColorChanged();
    }

    const QString transformMode() const {
        return REGISTRATION_PROJECT(m_project) ? REGISTRATION_PROJECT(m_project)->transformMode() : "";
    }

    void setTransformMode(const QString &mode) {
        if (REGISTRATION_PROJECT(m_project)) {
            REGISTRATION_PROJECT(m_project)->setTransformMode(mode);
        }
    }

    QStringList availableTransformModes() const {
        return REGISTRATION_PROJECT(m_project) ? REGISTRATION_PROJECT(m_project)->availableTransformModes() : QStringList();
    }

    QString getProjectMode() const { return m_project->mode(); }

    const QString roiTransformMode() const {
        if (dynamic_cast<ProjectRegistration*>(m_project)) {
            return dynamic_cast<ProjectRegistration*>(m_project)->roiTransformMode();
        }
        return "";
    }

    void setRoiTransformMode(const QString &mode) {
        if (dynamic_cast<ProjectRegistration*>(m_project)) {
            dynamic_cast<ProjectRegistration*>(m_project)->setRoiTransformMode(mode);
            emit roiTransformModeChanged();
        }
    }

    bool b_landmarkPredictionDown;
    bool landmarkPredictionDown() const {
        return b_landmarkPredictionDown;
    }

    bool b_autoRegisterOnDown;
    bool autoRegisterOnDown() const {
        return b_autoRegisterOnDown;
    }

    void setLandmarkPredictionDown(const bool down) {
        b_landmarkPredictionDown = down;
    }

    void setAutoRegisterOnDown(const bool down) {
        b_autoRegisterOnDown = down;
    }

    //auto register
    VoxelCoordinate m_autoRegisterSize;
    Q_INVOKABLE VoxelCoordinate getAutoRegisterSize() { return m_autoRegisterSize; }
    Q_INVOKABLE void setAutoRegisterSize(VoxelCoordinate size) {


        emit autoRegisterSizeChanged(m_autoRegisterSize);
    }

    VoxelCoordinate m_autoRegisterSearchSpace;
    Q_INVOKABLE VoxelCoordinate getAutoRegisterSearchSpace() { return m_autoRegisterSearchSpace; }
    Q_INVOKABLE void setAutoRegisterSearchSpace(VoxelCoordinate size) {
        if (size.x > 0) m_autoRegisterSearchSpace.x = size.x;
        if (size.y > 0) m_autoRegisterSearchSpace.y = size.y;
        if (size.z > 0) m_autoRegisterSearchSpace.z = size.z;
        emit autoRegisterSearchSpaceChanged(m_autoRegisterSearchSpace);
    }

    VoxelCoordinate m_autoPopulateSpacing;
    Q_INVOKABLE VoxelCoordinate getAutoPopulateSpacing() { return m_autoPopulateSpacing; }
    Q_INVOKABLE void setAutoPopulateSpacing(VoxelCoordinate size) {
        if (size.x > 0) m_autoPopulateSpacing.x = size.x;
        if (size.y > 0) m_autoPopulateSpacing.y = size.y;
        if (size.z > 0) m_autoPopulateSpacing.z = size.z;
        emit autoRegisterSearchSpaceChanged(m_autoPopulateSpacing);
    }

    QString m_autoRegisterResult;
    Q_INVOKABLE QString getAutoRegisterResult() { return m_autoRegisterResult; }
    Q_INVOKABLE void setAutoRegisterResult(QString txt) {
        m_autoRegisterResult = txt;
        emit autoRegisterResultChanged(m_autoRegisterResult);
    }

    QString m_autoRegisterResultSTD;
    Q_INVOKABLE QString getAutoRegisterResultSTD() { return m_autoRegisterResultSTD; }
    Q_INVOKABLE void setAutoRegisterResultSTD(QString txt) {
        m_autoRegisterResultSTD = txt;
        emit autoRegisterResultSTDChanged(m_autoRegisterResultSTD);
    }

    int m_autoRegisterSTD;
    Q_INVOKABLE int getAutoRegisterSTD() { return m_autoRegisterSTD; }
    Q_INVOKABLE void setAutoRegisterSTD(int val)
    {
        m_autoRegisterSTD = val;
        emit autoRegisterSTDChanged(m_autoRegisterSTD);
    }

    int m_autoRegisterCorr;
    Q_INVOKABLE int getAutoRegisterCorr() { return m_autoRegisterCorr; }
    Q_INVOKABLE void setAutoRegisterCorr(int val)
    {
        m_autoRegisterCorr = val;
        emit autoRegisterCorrChanged(m_autoRegisterCorr);
    }

    Q_INVOKABLE void setAutoRegisterSizeXYZ(int x, int y, int z)
    {
        if (x >= 0) m_autoRegisterSize.x = x;
        if (y >= 0) m_autoRegisterSize.y = y;
        if (z >= 0) m_autoRegisterSize.z = z;
    }

    Q_INVOKABLE int getAutoRegisterSizeXYZ(int i)
    {
        if (i == 0) return m_autoRegisterSize.x;
        if (i == 1) return m_autoRegisterSize.y;
        if (i == 2) return m_autoRegisterSize.z;
    }

    Q_INVOKABLE void setAutoRegisterSearchSpaceXYZ(int x, int y, int z)
    {
        if (x >= 0) m_autoRegisterSearchSpace.x = x;
        if (y >= 0) m_autoRegisterSearchSpace.y = y;
        if (z >= 0) m_autoRegisterSearchSpace.z = z;
    }

    Q_INVOKABLE int getAutoRegisterSearchSpaceXYZ(int i)
    {
        if (i == 0) return m_autoRegisterSearchSpace.x;
        if (i == 1) return m_autoRegisterSearchSpace.y;
        if (i == 2) return m_autoRegisterSearchSpace.z;
    }

    Q_INVOKABLE void setAutoPopulateSpacingXYZ(int x, int y, int z)
    {
        if (x >= 0) m_autoPopulateSpacing.x = x;
        if (y >= 0) m_autoPopulateSpacing.y = y;
        if (z >= 0) m_autoPopulateSpacing.z = z;
    }

    Q_INVOKABLE int getAutoPopulateSpacingXYZ(int i)
    {
        if (i == 0) return m_autoPopulateSpacing.x;
        if (i == 1) return m_autoPopulateSpacing.y;
        if (i == 2) return m_autoPopulateSpacing.z;
    }

    Q_INVOKABLE QStringList availableROITransformModes() const {
        return { "Transform ROIs", "Translate ROIs" };
    }


// Most of these slots will be considered "commands" logged in the history
public slots:
//    void reBroadcastLandmarkSelectionChanged() { emit landmarkSelectionChanged(); }
    void selectRoiFromSlice(const QString& sessionKey, const QString& roiKey);
    void selectRoiInSlice(const QString& sessionKey, const QString& roiKey);
    void linkRoi(const QString& sessionKey, const QString& roiKey);
    void linkRoiSrc(const QString& sessionKey, const QString& roiKey);
    void linkRoiDest(const QString& sessionKey, const QString& roiKey);
    void unlinkRoi(const QString& sessionKey, const QString& roiKey);
    void clearLinkState();
    bool createNewRoi(const QString& sessionKey, const QString& roiKey);
    bool renameRoi(const QString& sessionKey, const QString& roiKey, const QString& newName);
    bool modifyRoi(const QString& sessionKey, const QString& roiKey);
    bool splitRoi(const QString& sessionKey, const QString& roiKey, unsigned int maxZ);
    bool mergeRois(const QString& sessionKey, const QList<RoiDataBase::roi_t>&);
    bool deleteRoi(const QString& sessionKey, const QString& roiKey);
    void deleteFlaggedRois(const QString& sessionKey);
    void deleteAllFlaggedRois();
    void filterHighPassRadius(const QString& sessionKey, const int minRadius);
    void filterHighPassVolume(const QString& sessionKey, const int minVolume);
    void flagFiltered(const QString& sessionKey);
    void unflagAll(const QString& sessionKey);
    void toggleFlag(const QString& sessionKey, const QString& roiKey);
    void sortByLink();
    void sortByName(const int column);

    // for sparse annotation mode
    void setSizeC(int c){
        m_c = c;
        for (unsigned int c =0 ; c < m_c; c++){
            channelColors.push_back(PREDEFINED_CHANNEL_COLORMAP_SPARSE.at(c));
        }
        channelListChanged();
    }
    void setConfirmedMaskColor(QColor color) {
        m_confirmedColor = color;
        emit confirmedColorChanged(m_confirmedColor);
    }
    void setUnconfirmedMaskColor(QColor color) {
        m_unconfirmedColor = color;
        emit unconfirmedColorChanged(m_unconfirmedColor);
    }
    // In Vivo Link edit mode: add and delete from candidate list
    void addCandidate(const QString& sessionKey, const QString& roiKey) {
        if(dynamic_cast<ProjectAnnotationSparse*>(getProject())->roiData()->addToCandidateList(sessionKey, roiKey)){
            dynamic_cast<ProjectAnnotationSparse*>(getProject())->roiDataModel()->resetModel();
            dynamic_cast<ProjectAnnotationSparse*>(getProject())->saveCandidateList(sessionKey);
        }
    };
    void deleteCandidate(const QString& sessionKey, const QString& roiKey) {
        if (dynamic_cast<ProjectAnnotationSparse*>(getProject())->roiData()->deleteFromCandidateList(sessionKey, roiKey)){
            dynamic_cast<ProjectAnnotationSparse*>(getProject())->roiDataModel()->resetModel();
            dynamic_cast<ProjectAnnotationSparse*>(getProject())->saveCandidateList(sessionKey);
        }
    };

signals:
    void windowTitleChanged(QString title);
    void refSessionChanged();
    void fixedSessionChanged();
    void dirtyChanged(bool flag);
    void lastCmdChanged();
    void linkInProgressChanged();
    void refExistsChanged();
    void roiSelected(QString sessionKey, QString roiKey);
    void roiSelectedIndex(QString sessionKey, int roiIndex);
    void linkedRoiSelectedIndex(int roiIndex);
    void roiConfirmedSelected(int roiIndex);
    void roiConfirmedDirty();
    void roiModified();
    void roiRenamed();
    void linkDone();
    void syncNavChanged(int z, int t, int zoom, float offsetX, float offsetY, qreal mouseX, qreal mouseY, bool zoombutton);
    void recreateTable();
    void removedSession(QString name);
    void flagsChanged();
    void roiDeleted();
    void projectNameChanged();
    void loadingNewProject();
    void newModel();
    void newLandmarkModel();
    void searchableSourcesChanged();
    void sessionNamesChanged();
    void searchResultsChanged();
    void addingLandmark();
    void landmarkCompleted();
    void transformModeChanged();
    void availableTransformModesChanged();
    void roiTransformModeChanged();
    void projectModeChanged();
    void channelColorChanged();
    void landmarkSelectionChanged();
    void landmarkPredicted(int n);
    void landmarkAutoRegistered(int n);

    // sparse annotation mode
    void updateAllSliceImages();
    void updateAllSliceOverlays();
    void showCandidatesChanged();
    void selectedRoiViaMouse(const float mouseX, const float mouseY, const int action);
    void mouseClicked(const float mouseX, const float mouseY);
    void unSelectAllRois();
    void roiEditModeChanged();
    void channelChanged(const int channel);
    void sessionChanged(const int seesion);
    void confirmedColorChanged(const QColor confirmedColor);
    void unconfirmedColorChanged(const QColor unconfirmedColor);
    void brightnessesChanged();
    void contrastsChanged();
    void channelListChanged();
    // for synchronize
    void changeAllSliceIndex(const int indexZ);
    void gotoZ(const int z);
    void nextZ();
    void prevZ();
    void updateZNavMasterSlider(const int z);
    void setAllZoomFactors(const int zoomFactor);
    void setAllMouseX(const qreal x);
    void setAllMouseY(const qreal y);
    void zoomFactorChanged(int zoomFactor);
    void rectChanged(qreal x, qreal y, qreal w, qreal z);
    void adjustOffset(const int x, const int y);

    void autoRegisterSizeChanged(VoxelCoordinate);
    void autoRegisterSearchSpaceChanged(VoxelCoordinate);
    void autoPopulateSpacingChanged(VoxelCoordinate);
    void autoRegisterResultChanged(QString txt);
    void autoRegisterResultSTDChanged(QString txt);
    void autoRegisterSTDChanged(int);
    void autoRegisterCorrChanged(int);

private:
    ApplicationData();
    ~ApplicationData();

    void deleteSearchResults();
    void commandIssued(QStringList details, bool isModification = true);
    void commandFailure(QString msg);

    void initDefaultPropertyValues();

    static int searchResultsCount(QQmlListProperty<SearchResult>*);
    static SearchResult* searchResultAt(QQmlListProperty<SearchResult>*, int);

    QString formatWindowTitle(const QString& projectName) {
        return projectName.leftRef(100) + (projectName.isEmpty() ? "": " - ") + QCoreApplication::applicationName() + " " + QCoreApplication::applicationVersion();
    }

    Project* m_project;

    QString m_windowTitle;
    RoiAddress m_currLinkSource;
    RoiAddress m_currLinkDest;
    QVector<QStringList> m_commandHistory;
    int m_maxHistory = 100;
    QString m_lastCmd;
    bool m_hideNextCommandUpdate = false;
    bool m_dirty = false;
    uchar m_iBase = 1; // set to 1 for 1-based coordinates instead of 0-based
    std::vector<std::shared_ptr<SearchResult>> m_searchResults;
    std::vector<QColor> preChannelColors0;
    std::vector<QColor> preChannelColors1;

    // for sparse annotation mode
    QString m_roiEditMode = "In Vivo Link";
    bool m_showCandidates = false;
    QColor m_confirmedColor = QColor(0, 255, 0).rgba();
    QColor m_unconfirmedColor = QColor(255, 0, 0).rgba();
    QList<qreal> m_brightnesses;
    QList<qreal> m_contrasts;
    unsigned int m_c = 0;
    std::vector<QColor> channelColors;
    // for synchronize
    qreal m_x = 0;
    qreal m_y = 0;
    qreal m_w = 0;
    qreal m_h = 0;
    qreal m_mouseX = 0;
    qreal m_mouseY = 0;
    int m_indexZ = 1;
    int m_zoomFactor = 100;
    bool  m_zoomButton = false;
    QHash<QString, qreal> m_zoomSettings;
};
#endif // APPLICATIONDATA_HPP
