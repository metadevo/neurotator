/// @author Sam Kenyon <sam@metadevo.com>
#include <QDebug>
#include <QPainter>
#include <QUrl>
#include <QGuiApplication>
#include <QCursor>
#include <QFileDialog>
#include <QListView>
#include <QTreeView>

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
#include "RoiDataDense.hpp"
#include "HyperstackTiff.hpp"
#include "WatershedImagePipeline.hpp"

#include "ProjectAnnotationDense.hpp"
#include "ProjectAnnotationSparse.hpp"
#include "ProjectRegistration.hpp"

ApplicationData::ApplicationData()
{
    if (AppSettings::instance().find("ibase")) {
        m_iBase = static_cast<uchar>(AppSettings::instance().get("ibase").toInt());
    } else {
        AppSettings::instance().set("ibase", m_iBase);
    }
    qInfo() << "index base: " << m_iBase;

    if (AppSettings::instance().find("command_history")) {
        m_maxHistory = AppSettings::instance().get("command_history").toInt();
    } else {
        AppSettings::instance().set("command_history", m_maxHistory);
    }

//    for test purposes
//    m_searchResults.append(new SearchResult("42", "foobar", false));
//    m_searchResults.append(new SearchResult("422", "foobar2", false));
//    m_searchResults.append(new SearchResult("4222", "foobar3", false));

    initDefaultPropertyValues();
}

ApplicationData::~ApplicationData()
{
}

void ApplicationData::initDefaultPropertyValues()
{
    //auto_reg
    m_autoRegisterSize = {96, 96, 4};
    m_autoRegisterSearchSpace = {16, 16, 4};
    m_autoPopulateSpacing = {0, 0, 0};
    m_autoRegisterResult = "0, 0, 0";
    m_autoRegisterSTD  = 0;
    m_autoRegisterCorr = 0;
}

//void ApplicationData::deleteSearchResults()
//{
//    foreach(SearchResult* s, m_searchResults) {
//        delete s;
//        s = nullptr;
//    }
//    m_searchResults.clear();
//}

void ApplicationData::setWindowTitle(const QString& title)
{
    m_windowTitle = title;
    emit windowTitleChanged(title);
}

void ApplicationData::setProjectName(const QString& name)
{
    m_project->setName(name);
    setWindowTitle(formatWindowTitle(name));
    commandIssued(QStringList{"Changed Project Name to", name}, false);
    emit projectNameChanged();
}

void ApplicationData::setProject(Project* project)
{
    m_project = project;
    //engine.rootContext()->setContextProperty("confirmedModel", blankProject->confirmedModel());
    setWindowTitle(formatWindowTitle(project->name()));
    emit projectNameChanged();
    emit projectModeChanged();

}

void ApplicationData::setRefSession(const QString& sessionName)
{
    if (!DENSE_PROJECT(m_project))
            return;
    if (sessionName != refSession()) {
        qInfo() << "Setting ref session.";
        DENSE_PROJECT(m_project)->roiData()->setRefSession(sessionName);
        DENSE_PROJECT(m_project)->roiData()->refreshRefLinks();

        DENSE_PROJECT(m_project)->confirmedModel()->resortColumnNames(sessionName);
        DENSE_PROJECT(m_project)->resortModels(sessionName);
        DENSE_PROJECT(m_project)->resetModels(true);

        emit refExistsChanged();
        emit refSessionChanged();

        emit roiModified();

        commandIssued(QStringList{"Set Ref Session.", sessionName}, false);
    }
}

void ApplicationData::setFixedSession(const QString& sessionName)
{
    if REGISTRATION_PROJECT(m_project) {
        if (sessionName != fixedSession()) {
            qInfo() << "Setting fixed session.";
            REGISTRATION_PROJECT(m_project)->setFixedSessionName(sessionName);

            emit fixedSessionChanged();

            commandIssued(QStringList{"Set Fixed Session.", sessionName}, false);
        }
    }
}

void ApplicationData::sortByLink()
{
    if (!DENSE_PROJECT(m_project))
        return;
    DENSE_PROJECT(m_project)->resortConfirmedModel(SortChoice::LINKS);
    emit linkedRoiSelectedIndex(0);

    emit roiModified();
    commandIssued(QStringList{"Sort by link"}, false);
}

void ApplicationData::sortByName(const int column)
{
    if (!DENSE_PROJECT(m_project))
        return;
    DENSE_PROJECT(m_project)->resortConfirmedModelByName(column);
    emit linkedRoiSelectedIndex(0);

    emit roiModified();
    commandIssued(QStringList{"Sort by ROI name"}, false);
}

void ApplicationData::unloadProject() {

    qDebug() << "Closing open projects...";
    if (m_project) {
        // this informs QML that these stack names are no longer valid
        QStringList names = m_project->stackNames();
        for (auto name : names) {
            emit removedSession(name);
        }

       delete m_project;
       m_project = nullptr;
       setWindowTitle(formatWindowTitle(""));
    }
}

/// SliceWindow and SliceOverlay objects referring to the current project should be destroyed from QML,
/// preferably before calling this.
void ApplicationData::loadProject(QUrl url)
{
    try
    {
        qDebug() << "Loading project...";
        emit loadingNewProject();

        unloadProject();

        Project *project = Project::makeProjectFromUrl(url);
        setProject(project);

        if (project->referenceName() != "") {
            setRefSession(project->referenceName());
        }

        emit newModel();
        emit newLandmarkModel();
        emit searchableSourcesChanged();
        emit sessionNamesChanged();
        emit transformModeChanged();
        emit roiTransformModeChanged();

        // in case it hasn't been added yet to recents list in app settings
        AppSettings::instance().setRecentProject(project->name(), url.toLocalFile());

        emit fixedSessionChanged();

        commandIssued(QStringList{"Loaded Project", url.fileName()}, false);
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
        commandFailure("Load project (" + url.toLocalFile() + ") failed");
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        commandFailure("Load project (" + url.toLocalFile() + ") failed");
    }
}

void ApplicationData::createNewProject(QString projectType, QUrl url) {
    if (m_project) {
        qWarning() << "Can't create a new Project if a project is already open";
        return;
    }
    Project *project = Project::makeProject(projectType);
    setProject(project);
    saveProjectAs(url);
}

void ApplicationData::saveProjectAs(QUrl url)
{
    if (!m_project) {
        qWarning() << "Can't save a non-existent project.";
        return;
    }
    m_project->saveProjectAs(url);
    commandIssued(QStringList{"Saved Project", url.fileName()}, false);
}

void ApplicationData::loadStack(QUrl url)
{
    m_project->loadStack(url);
    emit searchableSourcesChanged();
    emit sessionNamesChanged();

    commandIssued(QStringList{"Loaded Stack", url.fileName()}, false);
}

void ApplicationData::loadStacks(QList<QUrl> urls)
{

    SPARSE_PROJECT(m_project)->loadStacks(urls);
    emit searchableSourcesChanged();
    emit sessionNamesChanged();

    for (int i = 0; i < urls.length(); i++){
        commandIssued(QStringList{"Loaded Stack", urls[i].fileName()}, false);
    }
}

/// Unloads a stack as well as associated ROI data
void ApplicationData::unloadStack(const QString& name)
{
    clearLinkState();

    if ( DENSE_PROJECT(m_project) && (name == refSession())) {
        DENSE_PROJECT(m_project)->roiData()->clearRefSession();
        DENSE_PROJECT(m_project)->confirmedModel()->resortColumnNames(name);
        DENSE_PROJECT(m_project)->resortModels(name);
        DENSE_PROJECT(m_project)->resetModels(true);
        emit refExistsChanged();
    }

    m_project->unloadStack(name);

    if (DENSE_PROJECT(m_project)) {
        DENSE_PROJECT(m_project)->roiData()->refreshRefLinks();
        emit roiModified(); //signal overlays to update
    }
    if(!m_project->isSparseAnnotation()){
        emit removedSession(name);
    }
    emit searchableSourcesChanged();
    emit sessionNamesChanged();

    commandIssued(QStringList{"Unloaded Session", name}, false);
}

void ApplicationData::loadRoiData(QUrl url)
{
    m_project->loadRoiData(url);
    emit searchableSourcesChanged();
    commandIssued(QStringList{"Loaded ROI file", url.fileName()}, false);
}

QList<QUrl> ApplicationData::openSparseRoiFileDialog()
{
    QFileDialog dialog;

    dialog.setDirectoryUrl(getRecentProjectDir());
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::DontUseNativeDialog, true);
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.setViewMode(QFileDialog::Detail);

    QListView *l = dialog.findChild<QListView*>("listView");
    if (l) {
         l->setSelectionMode(QAbstractItemView::MultiSelection);
     }
    QTreeView *t = dialog.findChild<QTreeView*>();
     if (t) {
       t->setSelectionMode(QAbstractItemView::MultiSelection);
    }

    QList<QUrl> selected_directories;
    if (dialog.exec()){
        selected_directories = dialog.selectedUrls();
    }

    setRecentImportDir(dialog.directoryUrl());

    return selected_directories;
}

void ApplicationData::unloadRoiData(QString name)
{
    m_project->unloadRoiData(name);
    //emit searchableSourcesChanged();
    //commandIssued(QStringList{"unloaded ROI file", url.fileName()}, false);
}

QStringList ApplicationData::getSearchableSources()
{
    if (!DENSE_PROJECT(m_project))
        return QStringList();
    QStringList sources = DENSE_PROJECT(m_project)->confirmedModel()->columnNames();
    sources.push_front("All");
    return sources;
}

QStringList ApplicationData::getSessionNames()
{
    return m_project->stackNames();
}

void ApplicationData::searchRois(const QString& sessionKey, QString namePattern)
{
    try
    {
        if (!DENSE_PROJECT(m_project))
            return;
        //m_searchResults.clear();
        std::vector<std::shared_ptr<SearchResult>> temp = m_searchResults;
        m_searchResults = DENSE_PROJECT(m_project)->roiData()->searchRois(sessionKey, namePattern);

        QStringList stackNames = m_project->stackNames();
        for (auto it : m_searchResults) {
            if (stackNames.contains(it->session)) {
                it->hasStack = true;
            }
        }

        emit searchResultsChanged();
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
    }
}

void ApplicationData::updateTransforms()
{
    if (REGISTRATION_PROJECT(m_project))
        REGISTRATION_PROJECT(m_project)->updateTransforms();
}

/// @param selectInConfirmed: whether to auto select this in the confirmed table
bool ApplicationData::selectRoi(const QString& sessionKey, const QString& roiKey, bool selectInConfirmed)
{
    try
    {
        if (!m_project) {
            qWarning() << "Project is null!";
        }
        if (roiKey == "?") {
            return false;
        }
        if (!DENSE_PROJECT(m_project))
            return false;
        auto roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
        if (roi) {
            DENSE_PROJECT(m_project)->roiData()->setSelected(roi);

            if (selectInConfirmed && roi->linkedName() != "") {
                //int index = m_project->roiData()->linkedRefIndexAt(sessionKey, roi);
                int index = DENSE_PROJECT(m_project)->confirmedModel()->linkedRefIndexAt(sessionKey, roi);
                if (index != -1) {
                    emit roiConfirmedSelected(index);
                }
            }

            commandIssued(QStringList{"Selected ROI", sessionKey, roiKey}, false);

            // notify the slice widget(s) if any
            emit roiSelected(sessionKey, roiKey);

            return true;
        } else {
            return false;
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
}

/// Like selectRoi() but skips the widget signal
void ApplicationData::selectRoiFromSlice(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        if (!m_project) {
            qWarning() << "Project is null!";
        }
        if (!DENSE_PROJECT(m_project))
            return;
        bool emitted = false;
        auto roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
        if (roi)
        {
            DENSE_PROJECT(m_project)->roiData()->setSelected(roi);

            if (sessionKey == DENSE_PROJECT(m_project)->roiData()->refSessionName()) {
                qInfo() << "Searching confirmed model : roiIndexAt (" << roiKey << ")";
                int index = DENSE_PROJECT(m_project)->confirmedModel()->roiIndexAt(sessionKey, roiKey);
                if (index != -1) {
                    emit roiConfirmedSelected(index); // for confirmed table
                    emitted = true;
                }
            } else if (roi->linkedName() != "") {
                qInfo() << "Searching confirmed model : linkedRefIndexAt (" << roiKey << ") roi->linkedSession: " << roi->linkedSession();
                int index = DENSE_PROJECT(m_project)->confirmedModel()->linkedRefIndexAt(sessionKey, roi);
                if (index != -1) {
                    emit roiConfirmedSelected(index); // for confirmed table
                    emitted = true;
                }
            }

            if (!emitted) {
                // try unconfirmed table
                qInfo() << "Searching unconfirmed model (" << roiKey << ")";
                auto model = DENSE_PROJECT(m_project)->unconfirmedModelByName(sessionKey);
                if (model) {
                    int index = model->roiIndexAt(sessionKey, roiKey);
                    if (index != -1) {
                        emit roiSelectedIndex(sessionKey, index);
                        emitted = true;
                    }
                }
            }
        }
        if (!emitted) {
            commandFailure("Unknown key");
        } else {
            commandIssued(QStringList{"Selected ROI", sessionKey, roiKey}, false);
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
        return;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return;
    }
}

void ApplicationData::selectRoiInSlice(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        if (!DENSE_PROJECT(m_project))
            return;
        auto roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
        if (roi) {
            DENSE_PROJECT(m_project)->roiData()->setSelected(roi);

            commandIssued(QStringList{"Selected ROI", sessionKey, roiKey}, false);

            // notify the slice widget(s) if any
            emit roiSelected(sessionKey, roiKey);
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
    }
}

void ApplicationData::linkRoi(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        if (m_currLinkSource.roiKey == "") {
            linkRoiSrc(sessionKey, roiKey);
        } else {
            linkRoiDest(sessionKey, roiKey);
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
        return;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return;
    }
}

void ApplicationData::linkRoiSrc(const QString& sessionKey, const QString& roiKey)
{
    qInfo() << "Link ROI src: " << sessionKey << " : " << roiKey;
    m_currLinkSource.sessionKey = sessionKey;
    m_currLinkSource.roiKey = roiKey;
    emit linkInProgressChanged();
}

void ApplicationData::linkRoiDest(const QString& sessionKey, const QString& roiKey)
{
    if (!DENSE_PROJECT(m_project))
        return;
    qInfo() << "Link ROI dest: " << sessionKey << " : " << roiKey;

    if (m_currLinkSource.roiKey == "") {
        qWarning() << "Link source is undefined.";
        clearLinkState();
        emit linkDone();
        commandFailure("Error: Link source is undefined.");
        return;
    }
    if (m_currLinkSource.sessionKey == sessionKey) {
        clearLinkState();
        emit linkDone();
        commandFailure("Cannot link to self.");
        return;
    }
    if (DENSE_PROJECT(m_project)->roiData()->refSessionName() == "") {
        clearLinkState();
        emit linkDone();
        commandFailure("Cannot link without reference session.");
        return;
    }

    m_currLinkDest.sessionKey = sessionKey;
    m_currLinkDest.roiKey = roiKey;

    int ret = DENSE_PROJECT(m_project)->roiData()->modifyRoiLink(m_currLinkSource, m_currLinkDest);
    if (ret == 1) { // change link
        DENSE_PROJECT(m_project)->resetModels(true);
        auto roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
        int index = DENSE_PROJECT(m_project)->confirmedModel()->linkedRefIndexAt(sessionKey, roi); // use model in case it's been sorted
        if (index < 0) {
            qWarning() << "Index DNE!";
        } else {
            emit linkedRoiSelectedIndex(index);
        }
        emit roiModified();
        commandIssued(QStringList{"Changed Link", m_currLinkSource.sessionKey, m_currLinkSource.roiKey});
    } else if (ret == 0) { // new link
        DENSE_PROJECT(m_project)->resetModels(true);
        auto roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
        int index = DENSE_PROJECT(m_project)->confirmedModel()->linkedRefIndexAt(sessionKey, roi); // use model in case it's been sorted
        if (index < 0) {
            qWarning() << "Index DNE!";
        } else {
            emit linkedRoiSelectedIndex(index);
        }
        emit roiModified();
        commandIssued(QStringList{"Linked", m_currLinkSource.sessionKey, m_currLinkSource.roiKey});
    } else {
        commandFailure("Could not link.");
    }
    emit linkDone();
    clearLinkState();
}

void ApplicationData::unlinkRoi(const QString& sessionKey, const QString& roiKey)
{
    try
    {
        if (!DENSE_PROJECT(m_project))
            return;
        qDebug() << "Unlink ROI: " << sessionKey << " : " << roiKey;

        if (DENSE_PROJECT(m_project)->roiData()->refSessionName() == sessionKey) {
            clearLinkState();
            emit linkDone();
            return;
        }

        m_currLinkSource.sessionKey = sessionKey;
        m_currLinkSource.roiKey = roiKey;
        if (DENSE_PROJECT(m_project)->roiData()->removeRoiLink(m_currLinkSource))
        {
            DENSE_PROJECT(m_project)->resetModels(true);
            emit roiModified();
            emit linkDone();
            clearLinkState();
            commandIssued(QStringList{"Unlinked", sessionKey, roiKey});
        } else {
            commandFailure("Nothing to unlink.");
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
        return;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return;
    }
}

void ApplicationData::clearLinkState()
{
    m_currLinkSource.sessionKey = "";
    m_currLinkSource.roiKey = "";
    m_currLinkDest.sessionKey = "";
    m_currLinkDest.roiKey = "";
    emit linkDone();
    emit linkInProgressChanged();
}

bool ApplicationData::createNewRoi(const QString& sessionKey, const QString& roiKey)
{
    if (!DENSE_PROJECT(m_project))
        return false;
    std::shared_ptr<RoiFields> newRoi = std::make_shared<RoiFieldsDense>();
    newRoi->name = roiKey;
    if (DENSE_PROJECT(m_project)->roiData()->addNewRoi(sessionKey, newRoi->name, newRoi)) {
        emit roiConfirmedDirty();
        emit roiModified();
        commandIssued(QStringList{"Created new ROI", sessionKey, roiKey}, true);
        return true;
    } else {
        return false;
    }
}

/// @return false if the new name is already used
bool ApplicationData::renameRoi(const QString& sessionKey, const QString& roiKey, const QString& newName)
{
    if (!DENSE_PROJECT(m_project))
        return false;
    if (roiKey != "")
    {
        auto roi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, newName);
        if (roi) {
            commandFailure("Name already used.");
            return false;
        }

        qDebug() << "Rename ROI";
        clearLinkState();
        if (DENSE_PROJECT(m_project)->roiData()->renameRoi(sessionKey, roiKey, newName)) {
            DENSE_PROJECT(m_project)->resetModels(true); // update model and notify users, namely ROI tables
            emit roiConfirmedDirty(); // force confirmed table to update
            emit roiRenamed();  // notify slice overlay and slice window
            commandIssued(QStringList{"Renamed ROI", sessionKey, roiKey, " to ", newName}, true);

            m_hideNextCommandUpdate = true;
            selectRoi(sessionKey, newName, true);
            m_hideNextCommandUpdate = true;
            selectRoiFromSlice(sessionKey, newName);
        } else {
            commandFailure("Rename failed.");
        }
    }
    else
    {
        commandFailure("Empty key.");
    }
    return true;
}

bool ApplicationData::splitRoi(const QString& sessionKey, const QString& roiKey, unsigned int maxZ)
{
    if (!DENSE_PROJECT(m_project))
        return false;
    qInfo() << "Splitting...";
    std::shared_ptr<RoiFields> newRoi = std::make_shared<RoiFieldsDense>();
    newRoi->name = roiKey + "_split";

    qDebug() << "Adding new ROI " << newRoi->name << " as part of a Split cmd";
    if (!DENSE_PROJECT(m_project)->roiData()->addNewRoi(sessionKey, newRoi->name, newRoi)) {
        qWarning() << "Split ROI error: Create new ROI failed.";
        commandFailure("Split ROI error: Create new ROI failed.");
        return false;
    }

    auto oldRoi = DENSE_PROJECT(m_project)->roiData()->fieldsAt(sessionKey, roiKey);
    if (oldRoi) {
        QVector<QVector<VoxelCoordinate>> newMaskCoords;
        newMaskCoords.push_back(QVector<VoxelCoordinate>());
        newMaskCoords.push_back(QVector<VoxelCoordinate>());

        WatershedImagePipeline pipeline;
        ///@todo go through all z
        unsigned int currZ = 0;
        for (; currZ < maxZ; ++currZ)
        {
            QImage& src = std::dynamic_pointer_cast<RoiFieldsDense>(oldRoi)->masks[currZ].mask;
            const int pad = 10;
            const int doublePad = pad * 2;
            QImage padded(src.width() + doublePad, src.height() + doublePad, QImage::Format_RGB32);
            padded.fill(0);
            QPainter p(&padded);
            p.drawImage(QPoint(pad, pad), src);
            p.end();

            ImagePipeline::segments_t segments = pipeline.run(padded);

            if (segments.size() == 0) {
                QString msg = "Zero segments found at z " + QString::number(currZ);
                qWarning() << msg;
            } else {
                if (segments.size() == 1) {
                    QString msg = "Only 1 segment. Duplicating.";
                    qWarning() << msg;
                    // duplicate segment
                    segments.push_back(segments[0]);
                }
                for (int i = 0; i < 2; ++i) {
                    for (auto v : segments[i]) {
                        VoxelCoordinate vUpdated;
                        // these coordinates are relative to the input image so we need to make them slice-relative
                        vUpdated.x = v.x + std::dynamic_pointer_cast<RoiFieldsDense>(oldRoi)->masks[currZ].extent.x() - pad;
                        vUpdated.y = v.y + std::dynamic_pointer_cast<RoiFieldsDense>(oldRoi)->masks[currZ].extent.y() - pad;
                        vUpdated.z = currZ;
                        newMaskCoords[i].push_back(vUpdated);
                        qDebug() << "vUpdated:" << vUpdated.x << ", " << vUpdated.y << "," << vUpdated.z;
                    }
                }
            }
        }

        // clear old masks
        std::dynamic_pointer_cast<RoiFieldsDense>(oldRoi)->masks.clear();
        /// @todo could this get confused going assuming that the old is at 1 and the new is at 0?
        std::dynamic_pointer_cast<RoiFieldsDense>(newRoi)->setMask(newMaskCoords[0]);
        std::dynamic_pointer_cast<RoiFieldsDense>(oldRoi)->setMask(newMaskCoords[1]);

        // This will [re]generate the extents and QImage masks from the coordinate data
        DENSE_PROJECT(m_project)->roiData()->refreshRoi(sessionKey, std::dynamic_pointer_cast<RoiFieldsDense>(newRoi));
        DENSE_PROJECT(m_project)->roiData()->refreshRoi(sessionKey, std::dynamic_pointer_cast<RoiFieldsDense>(oldRoi));

    } else {
        // should be impossible
        qWarning() << "Old ROI not found.";
        commandFailure("Split ROI error: Old ROI not found.");
        return false;
    }

    DENSE_PROJECT(m_project)->resetModels(true);
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Split ROI", sessionKey, roiKey}, true);
    return true;
}


bool ApplicationData::mergeRois(const QString& sessionKey, const QList<RoiDataBase::roi_t>& rois)
{
    if (!DENSE_PROJECT(m_project))
        return false;
    qInfo() << "Merging...";
    DENSE_PROJECT(m_project)->roiData()->mergeToFirst(sessionKey, rois);

    // delete all but the first roi
    for (auto roi : rois.mid(1)) {
        DENSE_PROJECT(m_project)->roiData()->deleteRoi(sessionKey, roi->name);
    }

    DENSE_PROJECT(m_project)->resetModels(true);
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Merge ROIs"}, true);
    return true;
}

bool ApplicationData::deleteRoi(const QString& sessionKey, const QString& roiKey)
{
    if (!DENSE_PROJECT(m_project))
        return false;
    if (roiKey == "") {
        return false;
    }
    qDebug() << "Delete ROI";
    clearLinkState();
    if (DENSE_PROJECT(m_project)->roiData()->deleteRoi(sessionKey, roiKey)) {
        DENSE_PROJECT(m_project)->resetModels(true);
        emit roiConfirmedDirty();
        emit roiDeleted();
        commandIssued(QStringList{"Deleted ROI", sessionKey, roiKey}, true);
        return true;
    } else {
        return false;
    }
}

void ApplicationData::deleteFlaggedRois(const QString& sessionKey)
{
    if (!DENSE_PROJECT(m_project))
        return;
    qDebug() << "Delete Flagged ROIs in Session";
    clearLinkState();
    unsigned int delCount = DENSE_PROJECT(m_project)->roiData()->deleteFlagged(sessionKey);
    if (delCount > 0)
    {
        DENSE_PROJECT(m_project)->resetModels(true);
        emit roiConfirmedDirty();
        emit roiDeleted();
        commandIssued(QStringList{"Deleted ", QString::number(delCount) + " ROIs", sessionKey}, true);
    }
}

void ApplicationData::deleteAllFlaggedRois()
{
    if (!DENSE_PROJECT(m_project))
        return;
    qDebug() << "Delete Flagged ROIs in All Sessions";
    clearLinkState();
    unsigned int delCount = DENSE_PROJECT(m_project)->roiData()->deleteFlagged();
    if (delCount > 0)
    {
        DENSE_PROJECT(m_project)->resetModels(true);
        emit roiConfirmedDirty();
        emit roiDeleted();
        commandIssued(QStringList{"Deleted ", QString::number(delCount) + " ROIs"}, true);
    }
}

bool ApplicationData::modifyRoi(const QString&, const QString& roiKey)
{
    commandIssued(QStringList{"Modified ROI", roiKey}, true);
    ///@todo
    return true;
}

void ApplicationData::filterHighPassRadius(const QString& sessionKey, const int minRadius)
{
    if (!DENSE_PROJECT(m_project))
        return;
    unsigned int filteredOut = DENSE_PROJECT(m_project)->roiData()->filterHighPassRadius(sessionKey, minRadius);
    DENSE_PROJECT(m_project)->resetModels();
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Filtered out",  QString::number(filteredOut) + " ROIs"}, false);
}


void ApplicationData::filterHighPassVolume(const QString& sessionKey, const int minVolume)
{
    if (!DENSE_PROJECT(m_project))
        return;
    unsigned int filteredOut = DENSE_PROJECT(m_project)->roiData()->filterHighPassVolume(sessionKey, minVolume);
    DENSE_PROJECT(m_project)->resetModels();
    emit roiConfirmedDirty();
    emit roiModified();
    commandIssued(QStringList{"Filtered out",  QString::number(filteredOut) + " ROIs"}, false);
}

void ApplicationData::flagFiltered(const QString& sessionKey)
{
    if (!DENSE_PROJECT(m_project))
        return;
    unsigned int flagged = DENSE_PROJECT(m_project)->roiData()->flagFiltered(sessionKey);
    DENSE_PROJECT(m_project)->resetModels();
    emit roiModified();
    emit roiConfirmedDirty();
    emit flagsChanged();
    commandIssued(QStringList{"Flagged",  QString::number(flagged) + " ROIs"}, true);
}

void ApplicationData::unflagAll(const QString& sessionKey)
{
    if (!DENSE_PROJECT(m_project))
        return;
    unsigned int unflagged = DENSE_PROJECT(m_project)->roiData()->unflagAll(sessionKey);
    emit roiConfirmedDirty();
    emit flagsChanged();
    commandIssued(QStringList{"Unflagged",  QString::number(unflagged) + " ROIs"}, false);
}

void ApplicationData::toggleFlag(const QString& sessionKey, const QString& roiKey)
{
    if (!DENSE_PROJECT(m_project))
        return;
    if (roiKey != "") {
        bool state = DENSE_PROJECT(m_project)->roiData()->toggleFlag(sessionKey, roiKey);
        DENSE_PROJECT(m_project)->resetModels();
        emit linkedRoiSelectedIndex(0);
        emit roiModified();
        emit flagsChanged();
        commandIssued(QStringList{state ? "Flagged" : "Unflagged", sessionKey, roiKey}, false);
    }
}

void ApplicationData::commandIssued(QStringList details, bool isModification /*=true*/)
{   
    if (m_hideNextCommandUpdate) {
        m_hideNextCommandUpdate = false;
        return;
    }
    m_commandHistory.push_back(details);

    if (m_commandHistory.size() > m_maxHistory) {
        m_commandHistory.pop_front();
    }
    if (details.size() == 0) {
        return;
    }
    m_lastCmd = details[0];
    if (details.size() == 2) {
        m_lastCmd += " " + details[1];
    }
    if (details.size() >= 3) {
        m_lastCmd += " " + details[1] + "[" + details[2] + "]";
        for (int i = 3; i < details.size(); ++i) {
            m_lastCmd += details[i];
        }
    }
    //for (QString s : details) {
    //    m_lastCmd = m_lastCmd + " " + s;
    //}
    qInfo() << "User cmd: " << m_lastCmd;
    emit lastCmdChanged();
    m_dirty = m_dirty || isModification; // flag indicates if we need to save to disk
    if (isModification) {
        emit dirtyChanged(m_dirty);
    }
    ///@todo record in command system so we can support undo/redo
    qDebug() << "End of cmd processing";
}

void ApplicationData::commandFailure(QString msg)
{
    qInfo() << msg;
    m_lastCmd = msg;
    emit lastCmdChanged();
}


void ApplicationData::saveAllRoiFiles()
{
    if (m_project) {
        m_project->saveAllRoiFiles();
        commandIssued(QStringList{"Saved ROI files."}, false);
        m_dirty = false;
        emit dirtyChanged(m_dirty);
    }
}

QQmlListProperty<SearchResult> ApplicationData::getSearchResults()
{
    return QQmlListProperty<SearchResult>(this, this,
                                          &ApplicationData::searchResultsCount,
                                          &ApplicationData::searchResultAt);
}

int ApplicationData::searchResultsCount() const
{
    return m_searchResults.size();
}

SearchResult* ApplicationData::searchResultAt(int index) const
{
    if (index < (int)m_searchResults.size()) {
        return m_searchResults.at(index).get();
    } else {
        return nullptr;
    }
}

int ApplicationData::searchResultsCount(QQmlListProperty<SearchResult>* list)
{
    return reinterpret_cast<ApplicationData*>(list->data)->searchResultsCount();
}

SearchResult* ApplicationData::searchResultAt(QQmlListProperty<SearchResult>* list, int i)
{
    return reinterpret_cast<ApplicationData*>(list->data)->searchResultAt(i);
}

void ApplicationData::renderTransform()
{
    QGuiApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    if (dynamic_cast<ProjectRegistration*>(m_project))
        dynamic_cast<ProjectRegistration*>(m_project)->renderTransform();
    QGuiApplication::restoreOverrideCursor();
}

void ApplicationData::autoRegisterSelectedLandmarks()
{
    if (dynamic_cast<ProjectRegistration*>(m_project))
        dynamic_cast<ProjectRegistration*>(m_project)->autoRegisterSelectedLandMarks();
}


void ApplicationData::populateLandmarks()
{
    if (dynamic_cast<ProjectRegistration*>(m_project))
        dynamic_cast<ProjectRegistration*>(m_project)->populateLandmarks();
}

void ApplicationData::sortLandmarksByCorrelation()
{
    if (dynamic_cast<ProjectRegistration*>(m_project))
        dynamic_cast<ProjectRegistration*>(m_project)->sortLandmarksByCorrelation();
}

float  ApplicationData::getAutoRegisterCorrelation(int row)
{
    if (dynamic_cast<ProjectRegistration*>(m_project))
        return dynamic_cast<ProjectRegistration*>(m_project)->getAutoregisterCorrelation(row);
    return 0.0;
}
