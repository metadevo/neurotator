/// @author Sam Kenyon <sam@metadevo.com>
#include <algorithm>
#include <QDebug>
#include <QPainter>
#include <QUuid>

#include <vtkAbstractTransform.h>

#include "ApplicationData.hpp"
#include "RoiDataDense.hpp"
#include "RoiDataSparse.hpp"
#include "SliceImage.hpp"
#include "SliceOverlay.hpp"

SliceOverlay::SliceOverlay(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_appdata(ApplicationData::instance()),
    m_confirmedPen(QColor(0, 255, 0, 220), 1),
    m_unconfirmedPen(QColor(255, 0, 0, 220), 1),
    m_confirmedColor((m_appdata.getProject()->isSparseAnnotation()) ? m_appdata.getConfirmedMaskColor().rgba() : QColor(0, 255, 0).rgba()),
    m_unconfirmedColor((m_appdata.getProject()->isSparseAnnotation()) ? m_appdata.getUnconfirmedMaskColor().rgba() :QColor(255, 0, 0).rgba()),
    m_refColor(QColor(255, 255, 255).rgba()),
    m_confirmedCentroidPen(QColor(0, 255, 0, 128), 3),
    m_unconfirmedCentroidPen(QColor(255, 0, 0, 128), 3),
    m_defaultSelectPen(QPen(QColor(255, 255, 255, 220), 2, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin)),
    m_editSelectPen(QPen(QColor(255, 255, 0, 150), 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin)),
    m_selectPen{m_defaultSelectPen},
    m_linkingPen(QPen(QColor(0, 255, 255, 220), 3, Qt::DashDotDotLine, Qt::RoundCap, Qt::RoundJoin)),
    m_lightPen(QColor(255, 255, 255, 150)),
    m_labelPen(QColor(255, 255, 255, 200)),
    m_landmarkPen(QColor(255, 0, 0, 220), 1),
    m_currentLandmarkPen(QColor(255, 255, 0, 150), 2),
    m_maskColorTable{QColor(0, 0, 0, 0).rgba(), m_unconfirmedColor},
    m_labelFont(QFont("Sans Serif", 8)),
    m_currentLandmark(-1),
    // for sparse annotation mode
    m_sparseRoisColor(QColor(255, 255, 0).rgba())
{
    try
    {
        m_iBase = ApplicationData::instance().getIbase();
        m_iBaseF = static_cast<float>(ApplicationData::instance().getIbase());
        m_z = m_iBase;

        if (m_appdata.getProject()->isSparseAnnotation()){
            m_roiDataSparse = SPARSE_PROJECT(m_appdata.getProject())->roiData();
            m_showMaskOutlines = true; // show ROI mask outlines by default in sparse annotation mode
        }else{
            m_roiData = DENSE_PROJECT(m_appdata.getProject())->roiData();
        }

        QFontMetrics fm(m_labelFont);
        m_labelHeight = fm.height() * m_maxLabelLines;

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiSelected(QString, QString)),
                         this, SLOT(roiSelectionRequest(QString, QString)));

        QObject::connect(&ApplicationData::instance(), SIGNAL(linkDone()),
                         this, SLOT(resetLinking()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiModified()),
                         this, SLOT(update()));
        QObject::connect(&ApplicationData::instance(), SIGNAL(roiModified()),
                         this, SIGNAL(roiSelectionChanged()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiRenamed()),
                         this, SLOT(refresh()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(roiDeleted()),
                         this, SLOT(refresh()));

        QObject::connect(&ApplicationData::instance(),
                         SIGNAL(searchableSourcesChanged()),
                         this,
                         SLOT(refresh()));

        QObject::connect(&ApplicationData::instance(), SIGNAL(flagsChanged()),
                         this, SIGNAL(roiSelectionChanged()));

        // for sparse annotation mode
        if (dynamic_cast<ProjectAnnotationSparse*>(m_appdata.getProject())) {
            QObject::connect(&ApplicationData::instance(), SIGNAL(showCandidatesChanged()),
                             this, SLOT(update()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(selectedRoiViaMouse(const float, const float, const int)),
                             this, SLOT(selectRoiViaMouse(const float, const float, const int)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(mouseClicked(const float, const float)),
                             this, SLOT(mouseClick(const float, const float)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(unSelectAllRois()),
                             this, SLOT(unselectAll()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(channelChanged(const int)),
                             this, SLOT(setChannel(const int)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(sessionChanged(const int)),
                             this, SLOT(setSession(const int)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(updateAllSliceOverlays()),
                             this, SLOT(update()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(roiEditModeChanged()),
                             this, SLOT(update()));
            QObject::connect(&ApplicationData::instance(), SIGNAL(confirmedColorChanged(const QColor)),
                             this, SLOT(setConfirmedMaskColor(QColor)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(unconfirmedColorChanged(const QColor)),
                             this, SLOT(setUnconfirmedMaskColor(QColor)));
            QObject::connect(&ApplicationData::instance(), SIGNAL(changeAllSliceIndex(const int)),
                             this, SLOT(changeSliceIndex(const int)));
        }

        if (dynamic_cast<ProjectRegistration*>(m_appdata.getProject())) {
            auto *landmarks = &dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel();
            QObject::connect(landmarks, SIGNAL(landmarksChanged()),
                             this, SLOT(refresh()));
            QObject::connect(landmarks, SIGNAL(transformsChanged()),
                             this, SLOT(refresh()));
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
    }
}

int SliceOverlay::setPreRotation(const float val) {
    if (REGISTRATION_PROJECT(m_appdata.getProject())) {
        if (m_image) {
            if (REGISTRATION_PROJECT(m_appdata.getProject())->getNumLandmarks() == 0 ) {
                m_image->setPreRotation(static_cast<int>(val));
            }
            return m_image->getPreRotation();
        }
    }
    return 0;
}
void SliceOverlay::paint(QPainter *painter)
{
    painter->setBrush(Qt::NoBrush);
    painter->setRenderHint(QPainter::Antialiasing);
    //painter->setRenderHint(QPainter::SmoothPixmapTransform);

    // if this is registration window
    if(m_sessionName == ".transformed.")
        m_showLandmarks = true;

    if (dynamic_cast<ProjectRegistration*>(m_appdata.getProject()) && m_showLandmarks) {
        painter->setFont(m_labelFont);
        // if this is registration window
        if(m_sessionName == ".transformed."){
            // go through all sessions and add landmarks of each session to registration window
            for (const auto &session : m_appdata.getProject()->stackNames()) {
                bool fixedSession = session == m_appdata.fixedSession();

                auto xform = fixedSession ? nullptr :
                                            dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->getTransform(session);
                if (xform) xform = xform->GetInverse();

                auto landmarks = dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().getLandmarks(session);

                for (int i = 0; i < static_cast<int>(landmarks.size()); i++) {
                    QPen LandmarksPen(fixedSession?QColor(255, 255, 255, 220):QColor(255, 0, 255, 220));
                    painter->setPen(i == m_currentLandmark ? m_currentLandmarkPen : LandmarksPen);
                    drawLandmark(painter, landmarks[i], xform,
                                 static_cast<int>(m_appdata.getProject()->getImageSizeY(session)),
                                 static_cast<int>(m_appdata.getProject()->getImageSizeY(m_appdata.fixedSession())),
                                 fixedSession ? false : true
                                                );
                }
            }
        }
        else {
            auto xform = m_image->getTransform();
            auto landmarks = dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().getLandmarks(m_sessionName);

            for (int i = 0; i < static_cast<int>(landmarks.size()); i++) {
                painter->setPen(i == m_currentLandmark ? m_currentLandmarkPen : m_landmarkPen);
                drawLandmark(painter, landmarks[i], xform, m_image->getMaxY(), m_image->getMaxY(), false);
            }
        }
    }

    //    painter->resetTransform();
    //    painter->scale(m_scaling, m_scaling);
    //    painter->translate(-m_iBaseF + m_textureOffsetX, -m_iBaseF + m_textureOffsetY);

    if (m_showLabels) {
        painter->setFont(m_labelFont);
    }

    // won't show any masks or indicators if m_visibleRois is empty
    for (QList<RoiDataBase::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        bool hidden = !m_showConfirmed && (*it)->confirmed();
        hidden = hidden || (!m_showFullyLinked && (*it)->fullyLinked());
        hidden = hidden || (!m_showFiltered && (*it)->filtered());
        hidden = hidden || (!m_showFlagged && (*it)->flagged());

        // for sparse annotation mode
        if (m_appdata.getProject()->isSparseAnnotation()){
            hidden = hidden || (m_appdata.getShowCandidates() &&  !(*it)->inVivo() &&
                                !std::dynamic_pointer_cast<RoiFieldsSparse>(*it)->isCandidate() &&
                                std::dynamic_pointer_cast<RoiFieldsSparse>(*it)->numCandidates() != 0);
        }

        if (m_showMasks && !hidden) {
            if (m_appdata.getProject()->isSparseAnnotation()){
                if((*it)->inVivo()){
                    m_maskColorTable[1] = QColor(0, 255, 255).rgba();// sparse inVivo color: light blue
                }else if (m_appdata.getRoiEditMode() == "In Vivo Link"){
                    m_maskColorTable[1] = m_sparseRoisColor;
                }else if (m_appdata.getRoiEditMode() == "Decode"){
                    if (std::dynamic_pointer_cast<RoiFieldsSparse>(*it)->confirmed(m_session, m_channel)) {
                        m_maskColorTable[1] = m_confirmedColor;
                    } else {
                        m_maskColorTable[1] = m_unconfirmedColor;
                    }
                }
            } else{
                if ((*it)->confirmed()) {
                    m_maskColorTable[1] = m_confirmedColor;
                } else {
                    m_maskColorTable[1] = m_unconfirmedColor;
                }
            }
            if (m_showMaskOutlines) {
                (*it)->getMaskByZ(m_z).maskOutline.setColorTable(m_maskColorTable);
                painter->drawImage(worldToViewCoords((*it)->getMaskByZ(m_z).extent), (*it)->getMaskByZ(m_z).maskOutline );
            } else {
                (*it)->getMaskByZ(m_z).mask.setColorTable(m_maskColorTable);
                painter->drawImage(worldToViewCoords((*it)->getMaskByZ(m_z).extent), (*it)->getMaskByZ(m_z).mask );
            }
        }
        if (m_showCentroids) {
            //            if ((*it)->confirmed) {
            //                painter->setPen(m_confirmedCentroidPen);
            //                drawCrosshairs(painter, *it);
            //                painter->setPen(m_lightPen);
            //                drawCrosshairs(painter, *it);
            //            } else {
            //                painter->setPen(m_unconfirmedCentroidPen);
            //                drawCrosshairs(painter, *it);
            //                painter->setPen(m_lightPen);
            //                drawCrosshairs(painter, *it);
            //            }
            painter->setPen(m_lightPen);
            drawCrosshairs(painter, *it);
        }
    }

    //return;

    if (m_showRef)
    {
        for (QList<RoiDataBase::roi_t>::iterator it = m_refRois.begin(); it != m_refRois.end(); ++it)
        {
            m_maskColorTable[1] = m_refColor;
            if (m_showMaskOutlines) {
                (*it)->getMaskByZ(m_z).maskOutline.setColorTable(m_maskColorTable);
                painter->drawImage(worldToViewCoords((*it)->getMaskByZ(m_z).extent), (*it)->getMaskByZ(m_z).maskOutline);
            } else {
                (*it)->getMaskByZ(m_z).mask.setColorTable(m_maskColorTable);
                painter->drawImage(worldToViewCoords((*it)->getMaskByZ(m_z).extent), (*it)->getMaskByZ(m_z).mask);
            }
        }
    }

    //    // overlays that need to stay at constant size
    //    painter->resetTransform();
    //    for (QList<RoiData::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    //    {
    //        bool hidden = !m_showConfirmed && (*it)->confirmed;
    //        hidden = hidden || (!m_showFullyLinked && (*it)->fullyLinked);
    //        hidden = hidden || (!m_showFiltered && (*it)->filtered);
    //        hidden = hidden || (!m_showFlagged && (*it)->flagged);
    //        if (m_showMasks && !hidden) {
    //            if (m_showLabels) {
    //                painter->setPen(m_selectPen);
    //                QString labelText((*it)->name + "\nradius: ");
    //                labelText += QString::number(RoiData::radius((*it)->masks[m_z].extent));
    //                QPoint ul = worldToViewCoords((*it)->masks[m_z].extent.x(), (*it)->masks[m_z].extent.bottom());
    //                painter->drawText(QRect(ul.x(), ul.y(), m_maxLabelWidth, m_labelHeight), 0, labelText);
    //            }
    //        }
    //    }
    if (m_linking) {
        painter->setPen(m_linkingPen);
    } else {
        painter->setPen(m_selectPen);
    }
    for (QHash<QString, RoiDataBase::roi_t>::iterator j = m_selectedRois.begin(); j != m_selectedRois.end(); ++j)
    {
        painter->drawRoundedRect(worldToViewCoords((*j)->getMaskByZ(m_z).extent), 2, 2);
    }


}

void SliceOverlay::paintMask(QPainter *painter, RoiDataBase::roi_t roi)
{
    if (roi->confirmed()) {
        painter->setPen(m_confirmedPen);
    } else {
        painter->setPen(m_unconfirmedPen);
    }
    for (auto it : std::dynamic_pointer_cast<RoiFieldsDense>(roi)->mask) {
        if (it.z == (int)m_z) {
            painter->drawPoint(it.x, it.y);
        }
    }
}

void SliceOverlay::paintMaskOutline(QPainter *painter, RoiDataBase::roi_t roi)
{
    if (roi->confirmed()) {
        painter->setPen(m_confirmedPen);
    } else {
        painter->setPen(m_unconfirmedPen);
    }

}

void SliceOverlay::drawEllipse(QPainter *painter, RoiDataBase::roi_t roi)
{
    painter->drawEllipse(roi->getMaskByZ(m_z).extent);
}

const int LANDMARK_RADIUS = 10;

void SliceOverlay::drawLandmark(QPainter *painter, const Landmark &landmark,
                                vtkAbstractTransform* xform, int maxY_moving, int maxY_fixed, bool angle)
{
    double location[] = { static_cast<double>(landmark.location.x),
                          static_cast<double>(landmark.location.y),
                          static_cast<double>(landmark.location.z) };
    if (xform) {
        // Flip Y, transform, then unflip.
        location[1] = (maxY_moving-1) - location[1];
        xform->TransformPoint(location, location);
        location[1] = (maxY_fixed-1) - location[1];
    }

    // Round Z coordinate to match image slicing behavior.
    if (static_cast<int>(location[2] + 0.5) != static_cast<int>(m_z))
        return;

    QPoint p = worldToViewCoords(location[0], location[1]);
    painter->drawLine(p.x() - LANDMARK_RADIUS, p.y() - (angle?LANDMARK_RADIUS:0),
                      p.x() + LANDMARK_RADIUS, p.y() + (angle?LANDMARK_RADIUS:0));
    painter->drawLine(p.x() + (angle?LANDMARK_RADIUS:0), p.y() - LANDMARK_RADIUS,
                      p.x() - (angle?LANDMARK_RADIUS:0), p.y() + LANDMARK_RADIUS);
    if (m_showLandmarkLabels) {
        painter->drawText(QRect(p.x() + 10 , p.y() + (angle?1:-1)*10, m_maxLabelWidth, m_labelHeight),
                          0, landmark.label);
    }
}

void SliceOverlay::drawCrosshairs(QPainter *painter, RoiDataBase::roi_t roi)
{
    int w = 10; //roi->extents.width();
    int h = 10; //roi->extents.height();

    QPoint p1(roi->getCentroid().x - w, roi->getCentroid().y);
    QPoint p2(roi->getCentroid().x + w, roi->getCentroid().y);
    painter->drawLine(worldToViewCoords(p1), worldToViewCoords(p2));

    QPoint p3(roi->getCentroid().x, roi->getCentroid().y - h);
    QPoint p4(roi->getCentroid().x, roi->getCentroid().y + h);
    painter->drawLine(worldToViewCoords(p3), worldToViewCoords(p4));
}

/// Like update() but also clears the current selection
void SliceOverlay::refresh()
{
    m_selectedRois.clear();
    emit roiSelectionChanged();

    if (m_showAllRois) {
        showAllRois();
    } else {
        m_visibleRois.clear();
    }
    update();
}

void SliceOverlay::roiSelectionRequest(QString sessionKey, QString roiKey)
{
    if (sessionKey == m_sessionName) {
        // get the data structure
        RoiDataBase::roi_t roi = m_roiData->fieldsAt(sessionKey, roiKey);
        if (!roi)
        {
            qDebug() << "ROI " << roiKey << " does not exist in this session.";
            return;
        }

        if ((int)m_z != roi->getCentroid().z) {
            changeSlice(roi->getCentroid().z);
        }

        m_selectedRois[roiKey] = roi;
        emit roiSelectionChanged();
    }
}

/// Select an ROI that is in the current slice
void SliceOverlay::selectRoiInSlice(QString roiKey)
{
    RoiDataBase::roi_t roi = m_roiData->fieldsAt(m_sessionName, roiKey);
    if (roi && ((int)m_z == roi->getCentroid().z)) {
        m_selectedRois[roiKey] = roi;
        emit roiSelectionChanged();
    } else {
        qWarning() << "ROI " << roiKey << " does not exist in this slice.";
    }
}

/// Show all ROI locations in this slice
void SliceOverlay::showAllRois()
{
    m_showAllRois = true;
    m_visibleRois.clear();
    if (m_appdata.getProject()->isSparseAnnotation()){
        m_visibleRois = m_roiDataSparse->roisAtSessionFilterZ(m_sessionName, m_z);
    }else{
        m_visibleRois = m_roiData->roisAtSessionFilterZ(m_sessionName, m_z);
    }
}

void SliceOverlay::hideAllRois()
{
    m_showAllRois = false;
    m_visibleRois.clear();
}

///@return true if the coordinate is inside a region of interest
RoiDataBase::roi_t SliceOverlay::coordInRoi(const float mouseX, const float mouseY)
{
    int x = static_cast<int>(mouseX);
    int y = static_cast<int>(mouseY);
    if (m_scaling*m_zoom != 1.0f) {
        QPoint p = viewToWorldCoords(mouseX, mouseY);
        x = p.x();
        y = p.y();
    }
    for (QList<RoiDataBase::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        int left = (*it)->getMaskByZ(m_z).extent.x();
        int right = (*it)->getMaskByZ(m_z).extent.right();
        int top = (*it)->getMaskByZ(m_z).extent.y();
        int bottom = (*it)->getMaskByZ(m_z).extent.bottom();
        if (x >= left && x <= right && y >= top && y <= bottom) {
            return (*it);
        }
    }

    return nullptr;
}

void SliceOverlay::highlightLandmark(int index)
{
    if (index != m_currentLandmark) {

        m_currentLandmark = index;

        // retrieve coordinates of a landmark
        auto landmarks = dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().getLandmarks(m_landmarkSessionName);
        for (int i = landmarks.size() - 1; i >= 0; i--) {
            VoxelCoordinate loc = landmarks[i].location;
            if (i == index) {
                m_loc[0] = loc.x;
                m_loc[1] = loc.y;
                m_loc[2] = loc.z;
                // qDebug() << m_currentLandmark << "VoxelCoordinate" << m_loc[0] << m_loc[1] << m_loc[2];
                break;
            }
        }
        // the following method could cause a crash
        //        m_loc[0] = landmarks[index].location.x;
        //        m_loc[1] = landmarks[index].location.y;
        //        m_loc[2] = landmarks[index].location.z;
    }
    update();
}

/// The action depends on what mode we're in
void SliceOverlay::mouseClick(const float mouseX, const float mouseY)
{
    switch (m_mode) {
    case EDITMASK_MODE:
        modifyRoiPixel(mouseX, mouseY);
        break;
    case ADD_LANDMARK_MODE:
        addLandmark(mouseX, mouseY);
        break;
    default:
        selectRoiViaMouse(mouseX, mouseY);
    }
}

/**
 * @brief Select an ROI via mouse if there's one at the coords
 * @param x
 * @param y
 * @param action: extra action to perform if we selected an ROI
 */
void SliceOverlay::selectRoiViaMouse(const float mouseX, const float mouseY, const int action)
{
    RoiDataBase::roi_t roi = coordInRoi(mouseX, mouseY);
    if (roi)
    {
        if (action != MULTISELECT_ACTION) {
            m_selectedRois.clear();
        }
        m_selectedRois[roi->name] = roi;

        ApplicationData::instance().selectRoiFromSlice(m_sessionName, roi->name);
        switch (action) {
        case LINK_ACTION:
            ApplicationData::instance().linkRoi(m_sessionName, roi->name);
            m_linking = ApplicationData::instance().isLinking();
            break;
        case UNLINK_ACTION:
            ApplicationData::instance().unlinkRoi(m_sessionName, roi->name);
            ApplicationData::instance().clearLinkState();
            m_linking = false;
            break;
        case SPARSE_CANDIDATE_ACTION:
            ApplicationData::instance().addCandidate(m_sessionName, roi->name);
            break;
        case SPARSE_CONFIRM_ACTION:
            std::dynamic_pointer_cast<RoiFieldsSparse>(roi)->updateConfirmed(m_session, m_channel, 1);
            dynamic_cast<ProjectAnnotationSparse*>(m_appdata.getProject())->saveDecodeList(m_sessionName);
            break;
        case SPARSE_UNCONFIRM_ACTION:
            std::dynamic_pointer_cast<RoiFieldsSparse>(roi)->updateConfirmed(m_session, m_channel, 0);
            dynamic_cast<ProjectAnnotationSparse*>(m_appdata.getProject())->saveDecodeList(m_sessionName);
            break;
        default:
            ApplicationData::instance().clearLinkState();
            m_linking = false;
        }

        update();
        emit roiSelectionChanged();
        emit linkingChanged(); // just for the GUI
        return;
    }

    // if no hits, then just unselect
    m_selectedRois.clear();
    emit roiSelectionChanged();
    update();
}

void SliceOverlay::modifyRoiPixel(const float mouseX, const float mouseY,
                                  const ModifyRoiAction)
{
    if (m_selectedRois.size() == 1) {
        ApplicationData::instance().clearLinkState();
        setIsLinking(false);

        ///@todo log as command in appdata
        QPoint p = viewToWorldCoords(mouseX, mouseY);

        // convert to mask-relative coords
        int mx = p.x() - m_selectedRois.values()[0]->getMaskByZ(m_z).extent.x() + 1;
        int my = p.y() - m_selectedRois.values()[0]->getMaskByZ(m_z).extent.y() + 1;
        m_roiData->modifyMask(m_selectedRois.values()[0]->getMaskByZ(m_z), mx, my);
    }

    ///@todo
    //m_appdata.modifyRoi(m_sessionName, roi);
}

void SliceOverlay::newRoi(const float mouseX, const float mouseY,
                          const ModifyRoiAction)
{
    if (!coordInRoi(mouseX, mouseY)) {
        QString newRoiName = QUuid::createUuid().toString();
        if (!m_appdata.createNewRoi(m_sessionName, newRoiName)) {
            qWarning() << "Failed to create new ROI.";
            return;
        }
    }
}

QPoint SliceOverlay::viewToWorldCoords(const float x, const float y)
{
    QPoint p;
    p.setX(static_cast<int>(x / (m_scaling*m_zoom) - m_textureOffsetX+m_x0));
    p.setY(static_cast<int>(y / (m_scaling*m_zoom) - m_textureOffsetY+m_y0));
    return p;
}

QRect SliceOverlay::viewToWorldCoords(const float x1, const float y1,
                                      const float x2, const float y2)
{
    return QRect(viewToWorldCoords(x1, y1), viewToWorldCoords(x2, y2));
}

QPoint SliceOverlay::worldToViewCoords(const float x, const float y)
{
    QPoint p;
    p.setX(static_cast<int>((x - m_x0 + m_textureOffsetX) * m_scaling * m_zoom));
    p.setY(static_cast<int>((y - m_y0 + m_textureOffsetY) * m_scaling * m_zoom));
    return p;
}

QPoint SliceOverlay::worldToViewCoords(const QPoint p)
{
    return worldToViewCoords(p.x(), p.y());
}

/// This also shifts upper-left by 1 pixel
QRect SliceOverlay::worldToViewCoords(const QRect rect)
{
    QRect outRect;
    outRect.setX(static_cast<int>((rect.x() - 1 - m_x0 + m_textureOffsetX) * m_scaling * m_zoom));
    outRect.setY(static_cast<int>((rect.y() - 1 - m_y0 + m_textureOffsetY) * m_scaling * m_zoom));
    outRect.setRight(static_cast<int>((rect.x() - 1 + rect.width() - m_x0 + m_textureOffsetX) * m_scaling * m_zoom));
    outRect.setBottom(static_cast<int>((rect.y() - 1 + rect.height() - m_y0 + m_textureOffsetY) * m_scaling * m_zoom));
    return outRect;
}

void SliceOverlay::unlinkSelected()
{
    if (m_selectedRois.size() < 1) {
        return;
    }

    ApplicationData::instance().unlinkRoi(m_sessionName,  m_selectedRois.keys().at(0));
}

void SliceOverlay::hoverRoi(const int x, const int y)
{
    for (QList<RoiDataBase::roi_t>::iterator it = m_visibleRois.begin(); it != m_visibleRois.end(); ++it)
    {
        int left = (*it)->getMaskByZ(m_z).extent.x();
        int right = (*it)->getMaskByZ(m_z).extent.right();
        int top = (*it)->getMaskByZ(m_z).extent.y();
        int bottom = (*it)->getMaskByZ(m_z).extent.bottom();
        if (x >= left && x <= right && y >= top && y <= bottom) {
            m_hoveredRoi = (*it);
        }
    }
}

void SliceOverlay::exitHoverRoi()
{
    m_hoveredRoi.reset();
}

///@param z: user index which is typically base 1 NOT 0 (base defined by member m_iBase)
void SliceOverlay::changeSlice(const int z)
{
    if (z < 0 || z == static_cast<int>(m_z)) {
        return;
    }
    // changing slices, so unselect anything selected in this slice
    m_selectedRois.clear();
    changeMode(SELECT_MODE);

    m_z = static_cast<unsigned int>(z);

    if (m_showAllRois) {
        showAllRois();
    } else {
        m_visibleRois.clear();
    }
}

///@param indexZ: 0-based index
void SliceOverlay::changeSliceIndex(const int indexZ)
{
    changeSlice(indexZ + m_iBase);
}

void SliceOverlay::toggleShowReference()
{
    m_showRef = !m_showRef;
    if (m_showRef) {
        m_refRois = m_roiData->roisAtSession(m_roiData->refSessionName());
    } else {
        m_refRois.clear();
    }
}

void SliceOverlay::setScaleAndOffset(const float scale, const float zoom, const float xOffset, const float yOffset, const float x0, const float y0)
{
    m_scaling = scale;
    m_zoom = zoom;
    m_textureOffsetX = xOffset;
    m_textureOffsetY = yOffset;
    m_x0 = x0;
    m_y0 = y0;

    //    if (m_zoomFactor > 100) {
    //        setClip(true);
    //    } else {
    //        setClip(false);
    //    }

    update();
}

void SliceOverlay::toggleEditMode()
{
    (m_mode == EDITMASK_MODE) ? changeMode(SELECT_MODE) : changeMode(EDITMASK_MODE);
    update();
}

void SliceOverlay::enableAddLandmarkMode()
{
    changeMode(ADD_LANDMARK_MODE);
    update();
}


void SliceOverlay::disableAddLandmarkMode()
{
    changeMode(SELECT_MODE);
    //    m_appdata.getProject()->landmarkModel().updateTransforms();
    update();
}

void SliceOverlay::changeMode(ActionMode mode)
{
    m_mode = mode;
    if (m_mode == EDITMASK_MODE) {
        m_selectPen = m_editSelectPen;
    } else {
        m_selectPen = m_defaultSelectPen;
    }
}

void SliceOverlay::mergeSelected()
{
    m_appdata.mergeRois(m_sessionName, m_selectedRois.values());
    m_visibleRois.clear();
    m_visibleRois = m_roiData->roisAtSessionFilterZ(m_sessionName, m_z);
    update();
}

int SliceOverlay::landmarkNear(const float viewX, const float viewY)
{
    if (dynamic_cast<ProjectRegistration*>(m_appdata.getProject()) && m_showLandmarks && m_sessionName != ".transformed.") {
        QRect area = viewToWorldCoords(viewX - LANDMARK_RADIUS,
                                       viewY - LANDMARK_RADIUS,
                                       viewX + LANDMARK_RADIUS,
                                       viewY + LANDMARK_RADIUS);

        auto xform = m_image->getTransform();
        if (xform != nullptr) {
            //            xform = xform->GetInverse();

            int ymax = m_image->getMaxY() - 1;
            double p1[] = { static_cast<double>(area.left()),
                            static_cast<double>(ymax - area.top()),
                            static_cast<double>(m_z) };
            double p2[] = { static_cast<double>(area.right()),
                            static_cast<double>(ymax - area.bottom()),
                            static_cast<double>(m_z) };

            xform->TransformPoint(p1, p1);
            xform->TransformPoint(p2, p2);
            p1[1] = ymax - p1[1];
            p2[1] = ymax - p2[1];

            area.setLeft(std::min(p1[0], p2[0]));
            area.setTop(std::min(p1[1], p2[1]));
            area.setRight(std::max(p1[0], p2[0]));
            area.setBottom(std::max(p1[1], p2[1]));
        }

        auto landmarks = dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().getLandmarks(m_sessionName);
        // Search in reverse of drawing order. In the case of overlapping
        // landmarks, This ensures that we highlight the last one to be drawn, so
        // the highlight is visible.
        for (int i = landmarks.size() - 1; i >= 0; i--) {
            VoxelCoordinate loc = landmarks[i].location;
            if (loc.z == (int)m_z && area.contains(loc.x, loc.y)) {
                return i;
            }
        }
    }
    return -1;
}

void SliceOverlay::moveLandmark(const int index, const float viewX, const float viewY)
{
    QPoint p = viewToWorldCoords(viewX, viewY);
    auto xform = m_image->getTransform();
    if (xform) {
        //        xform = xform->GetInverse();

        int ymax = m_image->getMaxY() - 1;
        double tmp[] = { static_cast<double>(p.x()),
                         static_cast<double>(ymax - p.y()),
                         static_cast<double>(m_z) };
        xform->TransformPoint(tmp, tmp);
        p.setX(tmp[0]);
        p.setY(ymax - tmp[1]);
    }

    auto landmarks = dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().getLandmarks(m_sessionName);
    dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().setXY(m_sessionName, landmarks[index],
                                                                                      p.x(), p.y());
}

void SliceOverlay::moveLandmarkZ(const int index, const int z)
{
    auto landmarks = dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().getLandmarks(m_sessionName);
    dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel().setZ(m_sessionName,
                                                                                     landmarks[index],
                                                                                     z);
}

int clamp(int x, int min, int max)
{
    return std::max(min, std::min(max, x));
}

void SliceOverlay::addLandmark(float mouseX, float mouseY)
{
    QPoint p = viewToWorldCoords(mouseX, mouseY);
    dynamic_cast<ProjectRegistration*>(m_appdata.getProject())->landmarkModel()
            .addLandmark(m_sessionName, p.x(), p.y(), m_z);

    update();                   // XXX
}
