/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls.Material 2.4

Rectangle
{
    id: horDivider
    height: 2

    Rectangle {
        id: separatorHilite
        height: 1
        color: Material.color(Material.Grey, Material.Shade900)
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }
    Rectangle {
        id: separatorBottom
        height: 1
        color: Material.color(Material.Grey, Material.Shade700)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: separatorHilite.bottom
    }
}
