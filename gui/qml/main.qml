/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.13
import QtQuick.Layouts 1.13
import QtQml 2.13
import com.bu 1.0

ApplicationWindow {
    id: appWindow
    visible: true
    Material.theme: Material.Dark
    Material.accent: Material.Orange

    // set initial size to 1050xScreenHeight
    width: 1050 //1920
    height: Screen.height //1080
    title: appdata.windowTitle

    // using a few vars as much as possible for pixel dimensions/spacing
    property real spaceunit: 4
    property real barsizeunit: 32
    property real barunit2: 38
    property real titlebarunit: 24
    property real smallButtonSize: 24
    property real labelSize: 8
    property real tableCellWidth: 125
    property real tableColWidth: 125
    property real tableCellHeight: 24
    property real tableCellFontSize: 8
    property int defaultRoiTableX: 10
    property int defaultRoiTableY: 220
    property int oldRoiX: defaultRoiTableX
    property int oldRoiY: defaultRoiTableY
    property int defaultLandmarkTableX: 50
    property int defaultLandmarkTableY: 260
    property int oldLandmarkX: defaultLandmarkTableX
    property int oldLandmarkY: defaultLandmarkTableY
    property int defaultRegistrationWindowX: 150
    property int defaultRegistrationWindowY: 260
    property int oldRegistrationX: defaultRegistrationWindowX
    property int oldRegistrationY: defaultRegistrationWindowY
    property int highestZ: 0
    property int toolTipDelay: 500
    property int toolTipTimeout: 5000
    property int toolTipFontSize: 10
    property int nextStackX: 200
    property int nextStackY: 10

    // common colors
    property color lightOrangeColor: "#FFCC80"

    // common icons
    property string stackIcon: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_filter_none_white_18dp.png"
    property string refStackIcon: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_collections_bookmark_white_18dp.png"
    property string linkIcon: "qrc:/third-party/material-design-icons-master/editor/1x_web/ic_insert_link_white_18dp.png"
    property string confirmedIcon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_check_white_18dp.png"
    property string flagIcon: "qrc:/third-party/material-design-icons-master/content/1x_web/ic_flag_white_18dp.png"
    property string filterIcon: "qrc:/third-party/material-design-icons-master/content/1x_web/ic_filter_list_white_18dp.png"
    property string searchIcon: "qrc:/third-party/material-design-icons-master/action/ios/ic_search_white_18pt.imageset/ic_search_white_18pt.png"

    //property var lastFocusObj
    property var stackComponent: null
    property var sparseGroupWindowComponent: null
    property var sparseGroupWindowObject: null
    property var roiObject: null
    property var roiComponent: null
    property var landmarkObject: null
    property var landmarkComponent: null
    property var registrationObjectExists: false
    property bool secondScreenOn: false
    property bool thirdScreenOn: false
    property bool fourthScreenOn: false
    property var menuMap: null

    // global app state variables
    //property bool refIsSet: false
    property bool linkOperation: false

    ListModel {
        id: recentProjectsModel
    }

    ListModel {
        id: stackWindowsList
    }

    //onActiveFocusItemChanged: console.log("activeFocusItem", activeFocusItem)

//    function handleDirty(flag) {
//        if (flag) {
//            saveButton.text = "Save ROIs*"
//        } else {
//            saveButton.text = "Save ROIs"
//        }
//        saveButton.font.bold = flag;
//    }

    function refreshRecentsMenu() {
        recentProjectsModel.clear();
        var recents = appdata.getRecentProjects();
        var recentNames = appdata.getRecentProjectNames();
        for (var i = 0; i < Math.min(recents.length, 10); ++i) {
            recentProjectsModel.append({"path": recents[i], "name": recentNames[i].toString()});
        }
    }

    Component.onCompleted: {
//        appdata.onDirtyChanged.connect(handleDirty);
        appdata.removedSession.connect(removeMenuItem);
        refreshRecentsMenu();

        if (!stackComponent) {
            console.log("making stack component");
            stackComponent = Qt.createComponent("SliceWindow.qml");
        }
        if (!landmarkComponent) {
            console.log("making landmark table component");
            landmarkComponent = Qt.createComponent("LandmarkTable.qml", appWindow);
        }
        if (!sparseGroupWindowComponent) {
            console.log("making sparseGroupWindow component");
            sparseGroupWindowComponent = Qt.createComponent("SparseGroupWindow.qml");
        }

    }

    // find and print activeFocusItem
    // onActiveFocusItemChanged: print("activeFocusItem", activeFocusItem)

    signal newProject(url projectUrl, string projectName, string projectType)

    onNewProject: {
       appdata.createNewProject(projectType, projectUrl);
       appdata.projectName = projectName;
       refreshRecentsMenu();
       setMenuItemsForActiveProject();
    }

    function setMenuItemsForActiveProject() {
        newProjectSubmenu.enabled = false
        openProjectButton.enabled = false
        recentProjectSubmenu.enabled = false
        saveProjectButton.enabled = true
        saveProjectAsButton.enabled = true
        closeProjectButton.enabled = true
        importRoiButton.enabled =  (appdata.numLoadedRoiFiles() >= appdata.maxRoiFilesAllowed()) ? false: true;
        if (appdata.projectMode == "registration") {
            showRegistrationWindowButton.enabled = true
            renderTransformButton.enabled = true
            importStackButton.enabled = (appdata.numLoadedStacks() >= 2) ? false: true
        } else {
            showRoiTableButton.enabled = true; //appdata.numLoadedRoiFiles() ? true: false;
            saveRoiButton.enabled = true
            if (appdata.projectMode == "dense annotation")
                importStackButton.enabled = true
        }

        showLandmarkTableButton.enabled = true
    }

    property var newProjectUrl: null;
    property var newProjectName: null;
    property var newProjectType: null

    function createNewProject(projectType) {
        newProjectType = projectType;
        projectNewFileDialog.setFolder(appdata.getRecentProjectDir().toString());
        projectNewFileDialog.open()
    }

    FileDialog {
        id: projectNewFileDialog
        title: "New " + newProjectType + " Project"
        selectExisting: false
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            newProjectUrl = projectNewFileDialog.fileUrl;
            newProjectName = newProjectUrl.toString().substring(newProjectUrl.toString().lastIndexOf('/')+1);
            newProject(newProjectUrl, newProjectName, newProjectType)
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }
    menuBar: MenuBar {
            Menu {
                title: qsTr("&File")
                Menu {
                    id: newProjectSubmenu
                    title: qsTr("&New Project")
                    enabled: true
                    Action {
                        text: qsTr("Dense Annotation")
                        onTriggered: {
                            createNewProject("Dense Annotation")
                        }
                    }
                    Action {
                        text: qsTr("Sparse Annotation")
                        onTriggered: {
                            createNewProject("Sparse Annotation")
                        }
                    }
                    Action {
                        text: qsTr("Registration")
                        onTriggered: {
                            createNewProject("Registration")
                        }
                    }
                }
                Action {
                    id: openProjectButton
                    text: qsTr("&Open Project")
                    onTriggered: {
                        _v2_projectOpenFileDialog.setFolder(appdata.getRecentProjectDir());
                        _v2_projectOpenFileDialog.open();
                    }
                }
                Menu {
                    id: recentProjectSubmenu
                    title: qsTr("Recent Projects")
                    enabled: recentProjectFilesInstantiator.count > 0
                    Instantiator {
                        id: recentProjectFilesInstantiator
                        model: recentProjectsModel
                        delegate: MenuItem {
                            text: model.name
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: model.path
                            }
//                            onTriggered: Qt.callLater(function() {
//                                    openProject(model.path);
//                                })
                            onTriggered: openProject(model.path)

                        }
                        onObjectAdded: recentProjectSubmenu.insertItem(index, object)
                        onObjectRemoved: recentProjectSubmenu.removeItem(object)
                    }
                    MenuSeparator { }
                    MenuItem {
                        text: "Clear Recents"
                        onTriggered: {
                            appdata.clearRecentProjects();
                            refreshRecentsMenu();
                        }
                    }
                }

                MenuSeparator { }
                Action {
                    id: saveProjectButton
                    enabled: false
                    text: qsTr("&Save Project")
                    onTriggered: {

                    }
                }
                Action {
                    id: saveProjectAsButton
                    enabled: false
                    text: qsTr("Save Project &As...")
                    onTriggered: {
                        projectSaveAsFileDialog.setFolder(appdata.getRecentProjectDir());
                        projectSaveAsFileDialog.open();
                    }
                }
                MenuSeparator {}
                Action {
                    id: closeProjectButton
                    enabled: false
                    text: qsTr("&Close Project")
                    onTriggered: closeProject()
                }
                MenuSeparator { }
                Action {
                    id: quitButton
                    text: qsTr("&Quit")
                    onTriggered: quitDialog.open();
                }
            }
//            Menu {
//                title: qsTr("&Edit")
//                Action { text: qsTr("Cu&t") }
//                Action { text: qsTr("&Copy") }
//                Action { text: qsTr("&Paste") }
//            }
            Menu {
                title: qsTr("Stacks")
                Action {
                    id: importStackButton
                    text: qsTr("Import Stack")
                    enabled: false
                    shortcut: "F2"
                    onTriggered: importSession()
                }
            }
            Menu {
                title: qsTr("&ROIs")
                Action {
                    id: importRoiButton
                    text: qsTr("Import ROIs")
                    shortcut: "F4"
                    enabled: false
                    onTriggered: {
                        if (appdata.projectMode == "sparse annotation") {
                            var sparseRoiDirectories = appdata.openSparseRoiFileDialog()
                            for (var i = 0; i < sparseRoiDirectories.length; i++) {
                                loadSparseRoiDirectories(sparseRoiDirectories[i])
                            }
                        } else if (appdata.projectMode == "dense annotation") {
                            roiFileDialog.setFolder(appdata.getRecentImportDir());
                            roiFileDialog.setNameFilters(["ROI files (*.mat *.csv)", "All files (*)"]);
                            roiFileDialog.open();
                        }
                    }
                }
                Action {
                    id: saveRoiButton
                    text: qsTr("Save ROIs")
                    shortcut: "Ctrl+S"
                    enabled: false
                    onTriggered: {
                        appdata.saveAllRoiFiles();
                        console.log("Done saving!");
                    }
                }
                MenuSeparator { }
                Action {
                    id: showRoiTableButton
                    text: qsTr("ROI Table")
                    checkable: true
                    enabled: false
                    onTriggered: {
                        if (roiObject) {
                            if (checked) {
                                roiObject.x = defaultRoiTableX;
                                roiObject.y = defaultRoiTableY;
                                roiObject.z = ++highestZ;
                                roiObject.visible = true;
                            } else {
                                roiObject.visible = false;
                            }
                        }
                    }
                }
            }
            Menu {
                title: qsTr("&Registration")
                Action {
                    id: showLandmarkTableButton
                    text: qsTr("Landmark Table")
                    checkable: true
                    enabled: false
                    checked: false
                    onTriggered: toggleLandmarkTable()
                }
                MenuSeparator {}
                Action {
                    id: renderTransformButton
                    text: "Render Transform"
                    enabled: false
                    onTriggered: appdata.renderTransform()
                }
            }
            Menu {
                id: windowMenu
                title: qsTr("&Window")
                Action {
                    id: fullScreenButton
                    text: qsTr("&Full Screen")
                    shortcut: "F11"
                    checkable: true
                    checked: false
                    onToggled: toggleFullScreen()
                }
                Action {
                    id: secondScreenButton
                    text: qsTr("&Extend Screens")
                    shortcut: "F12"
                    enabled: true
                    onTriggered: extendScreens()
                 }
                MenuSeparator {}
                Action {
                    id: showRegistrationWindowButton
                    text: qsTr("Registration")
                    enabled: false
                    onTriggered: showRegistrationWindow()
                }
            }

//            Menu {
//                title: qsTr("&Help")
//                Action { text: qsTr("&About") }
//                Action {
//                    text: qsTr("&Shortcuts")
//                    onTriggered: helpDialog.open();
//                }
//            }
        }

    function extendScreens() {
        var screens = Qt.application.screens;
        console.log("screens.length:", screens.length);
        for (var i = 0; i < screens.length; ++i) {
            console.log("screen " + i + " ["+screens[i].name + "] has geometry (" +
                        screens[i].virtualX + ", " + screens[i].virtualY + ", " +
                        screens[i].width + "x" + screens[i].height + "), desktopAvailable:" +
                        screens[i].desktopAvailableWidth + "x" + screens[i].desktopAvailableHeight + ", appWindow(" +
                        appWindow.x + "," + appWindow.y + ", " + appWindow.width + "x" + appWindow.height+")");
        }
        if (screens.length > 1) {
            if (!secondScreenOn) {
                secondWindow.screen = Qt.application.screens[1];
                secondWindow.visibility = Window.Maximized;
                secondScreenOn = true;
            } else {
                secondWindow.visible = false;
                secondScreenOn = false;
            }
        }
        if (screens.length > 2) {
            if (!thirdScreenOn) {
                thirdWindow.screen = Qt.application.screens[2];
                thirdWindow.visibility = Window.Maximized;
                thirdScreenOn = true;
            } else {
                thirdWindow.visible = false;
                thirdScreenOn = false;
            }
        }
        if (screens.length > 3) {
            if (!fourthScreenOn) {
                fourthWindow.screen = Qt.application.screens[3];
                fourthWindow.visibility = Window.Maximized;
                fourthScreenOn = true;
            } else {
                fourthWindow.visible = false;
                fourthScreenOn = false;
            }
        }
    }

    function toggleFullScreen() {
        if (fullScreenButton.checked) {
            appWindow.visibility = Window.FullScreen;
        } else {
            appWindow.visibility = Window.Maximized;
            secondWindow.visibility = Window.Maximized;
        }
    }

    function toggleLandmarkTable() {
        if (showLandmarkTableButton.checked) {
            showLandmarkTable()
        } else {
            landmarkObject.visible = false
        }
    }

    MessageDialog {
        id: closeProjectMessageDialog
        icon: StandardIcon.Question
        title: "Close project"
        text: "Are you sure you want to close the current project?"
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: {
            if (appdata.projectMode == "registration") {
                destroyRegistrationWindow();
                destroyLandmarkTable();
            }
            else if (appdata.projectMode == "sparse annotation"){
                if (sparseGroupWindowObject) {
                    sparseGroupWindowObject.destroy();
                    sparseGroupWindowObject = null
                }
                destroyRoiTable();
            }else {
                destroyRoiTable();
            }
            destroyAllStackWindows();
            appdata.unloadProject();
            refreshRecentsMenu();
            newProjectSubmenu.enabled = true
            openProjectButton.enabled = true
            recentProjectSubmenu.enabled = true
            closeProjectButton.enabled = false
            saveProjectAsButton.enabled = false
            saveProjectButton.enabled = false
            importStackButton.enabled = false
            importRoiButton.enabled = false
            saveRoiButton.enabled = false
        }
        Component.onCompleted: visible = false
    }

    function closeProject() {
        closeProjectMessageDialog.visible = true
    }

    function openProject(projectUrl) {
//        if (appdata.dirty) {
//            appdata.saveAllRoiFiles();
//        }

        if (roiObject) {
            roiObject.destroy();
        }

        appdata.loadProject(projectUrl);

        if (appdata.projectMode === "dense annotation") {
            createRoiTable();
            if (roiObject) {
                roiObject.updateModels();
                roiObject.updateRef();
                roiObject.isLoading = false;
                roiObject.visible = true;
                showRoiTableButton.checked = true;
            }
        }
        else if (appdata.projectMode === "sparse annotation") {
            createRoiSparseTable();
            showRoiTableButton.checked = true;
        }
        else if (appdata.projectMode === "registration") {
            createLandmarkTable();
            createRegistrationWindow();
        }

        setMenuItemsForActiveProject();
        recreateSliceWindows();
        refreshRecentsMenu();
        recentProjectSubmenu.dismiss();
        console.log("Finished loading project.")
    }

    FileDialog {
        id: _v2_projectOpenFileDialog
        title: "Open Project"
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            openProject(_v2_projectOpenFileDialog.fileUrl); // signal
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

    MessageDialog {
        id: quitDialog
        title: "Quit Application"
        text: "Are you sure you want to quit?"
        icon: StandardIcon.Question
        visible: false
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: {
            console.log("User exited application.")
            Qt.quit()
        }
        Component.onCompleted: visible = false
    }


    Rectangle {
        id: window1Content
        anchors.fill: parent
        color: "transparent"
    }

    FileDialog {
        id: stackFileDialog
        title: "Import Stack"
        nameFilters: [ "Image stack files (*.tiff *.tif)", "All files (*)" ]
        onAccepted: {
            appdata.loadStack(stackFileDialog.fileUrl);
            createStackWindow(stackFileDialog.fileUrl);
            appdata.setRecentImportDir(stackFileDialog.folder)
            setMenuItemsForActiveProject();

        }
        onRejected: {
            console.log("Stack file selection canceled")
        }
    }

    FileDialog {
        id: roiFileDialog
        title: "Import ROI File"
        nameFilters: [ "Matlab files (*.mat)", "CSV files (*.csv)", "All files (*)" ]
        onAccepted: {
            if (roiObject) {
                //roiObject.visible = false; // don't allow UI during backend updating
                roiObject.isLoading = true;
            }
            appdata.loadRoiData(roiFileDialog.fileUrl);
            if (!roiObject) {
                createRoiTable();
            }
            if (roiObject) {
                roiObject.updateModels();
                roiObject.updateRef();
                roiObject.isLoading = false;
                roiObject.visible = true;
            }
            saveRoiButton.enabled=true
            appdata.setRecentImportDir(roiFileDialog.folder)
            if (appdata.numLoadedRoiFiles() >= appdata.maxRoiFilesAllowed()) {
                importRoiButton.enabled = false;
            }
            if (appdata.projectMode != "registration") {
                showRoiTableButton.enabled = true;
            }
        }
        onRejected: {
            console.log("ROI file selection canceled")
        }
    }


    FileDialog {
        id: saveFileDialog
        title: "Save ROI File As"
        selectExisting: false
        nameFilters: [ "Matlab files (*.mat)", "All files (*)" ]
        onAccepted: {
            console.log("saveFileDialog")
        }
        onRejected: {
            console.log("ROI file selection canceled")
        }
    }

    FileDialog {
        id: projectFileDialog
        title: "Open Project"
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            openProject(projectFileDialog.fileUrl);
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

    FileDialog {
        id: projectSaveAsFileDialog
        title: "Save Project As"
        selectExisting: false
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            appdata.saveProjectAs(projectSaveAsFileDialog.fileUrl);
            refreshRecentsMenu();
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

    function importSession() {
        stackFileDialog.setFolder(appdata.getRecentImportDir());
        stackFileDialog.open();
    }

    function removeMenuItem(name) {
        console.log("removeMenuItem:", name);
        windowMenu.removeItem(menuMap[name]);
    }

    function handleRoiDestroy(recreate) {
        console.log("handleRoiDestroy");
        oldRoiX = roiObject.x;
        oldRoiY = roiObject.y;
        roiObject.destroy();
        roiObejct = null;
        if (recreate) {
            recreateRoiTable();
            roiObject.visible = true;
        }
    }

    function handleMinimizeRoi() {
        showRoiTableButton.checked = false;
    }

    function createRoiSparseTable() {
        if (appdata.projectMode != "sparse annotation") {
            return;
        }
        console.log("[Re]creating ROI Table object for sparse annotation.");
        if (roiObject) {
            oldRoiX = roiObject.x;
            oldRoiY = roiObject.y;
            roiObject.destroy();
        }
        //if (!roiComponent) {
        roiComponent = Qt.createComponent("RoiSparseTable.qml", appWindow);
        console.log("ROI component created.");
        if (roiComponent.status === Component.Ready) {
            roiObject = roiComponent.createObject(appWindow, {"x": oldRoiX, "y": oldRoiY});

            // connecting of signals:
            roiObject.minimizing.connect(handleMinimizeRoi);

            roiObject.isLoading = false;
            console.log("ROI object constructed");
        }
    }

    function createRoiTable() {
        if (appdata.projectMode != "dense annotation") {
            return;
        }

        console.log("[Re]creating ROI Table object for dense annotation.");
        if (roiObject) {
            oldRoiX = roiObject.x;
            oldRoiY = roiObject.y;
            roiObject.destroy();
        }
        //if (!roiComponent) {
        roiComponent = Qt.createComponent("RoiTable.qml", appWindow);
        console.log("ROI component created.");
        //}
        if (roiComponent.status === Component.Ready) {
            roiObject = roiComponent.createObject(appWindow, {"x": oldRoiX, "y": oldRoiY});

            // connecting of signals:
            roiObject.minimizing.connect(handleMinimizeRoi);

            roiObject.isLoading = false;           
            console.log("ROI object constructed");
        }
    }

    function destroyRoiTable() {
        if (roiObject) {
            roiObject.destroy();
            roiObject = null
        }
        showRoiTableButton.checked = false;
        showRoiTableButton.enabled = false;
    }

    function handleMinimizeLandmark() {
        showLandmarkTableButton.checked = false;
    }

    function showLandmarkTable() {
        if (!landmarkObject) {
            createLandmarkTable();
        } else {
//            landmarkObject.x = defaultLandmarkTableX;
//            landmarkObject.y = defaultLandmarkTableY;
            landmarkObject.z = ++highestZ;
            landmarkObject.visible = true;
        }
    }

    function destroyLandmarkTable() {
        if (landmarkObject) {
            landmarkObject.destroy();
            landmarkObject = null
        }
        showLandmarkTableButton.checked = false;
        showLandmarkTableButton.enabled = false;
    }

    function createLandmarkTable() {
        if (appdata.projectMode != "registration") {
            return;
        }

        console.log("[Re]creating Landmark object");
//        if (landmarkObject) {
//            oldLandmarkX = landmarkObject.x;
//            oldLandmarkY = landmarkObject.y;
//            landmarkObject.destroy();
//        }
        console.log("Landmark component created.");
        if (landmarkComponent.status === Component.Ready) {
            landmarkObject = landmarkComponent.createObject(appWindow,
                                                            {"x": oldLandmarkX,
                                                             "y": oldLandmarkY});

            // connecting of signals:
            landmarkObject.minimizing.connect(handleMinimizeLandmark);
            landmarkObject.fixedSessionPicked.connect(recreateRegistrationWindow);
            landmarkObject.fixedSessionPicked.connect(showRegistrationWindow);

            landmarkObject.isLoading = false;
            showLandmarkTableButton.checked = true;
            console.log("Landmark object constructed");
        }
        else {
            console.log("Failed to construct landmark object", landmarkComponent.status,
                       landmarkComponent.errorString());
        }
    }

    function handleMinimizeRegistration() {
        showRegistrationWindowButton.visible = true;
    }

    function destroyRegistrationWindow(){
        if (appdata.projectMode != "registration")
            return;

        appdata.makeRegistrationStackCurrent();
        for (var i=stackWindowsList.count-1; i>=0; --i)
            if (stackWindowsList.get(i)["id"] === appdata.getCurrentStackWindow().toString()) {
                stackWindowsList.get(i)["object"].destroy()
                stackWindowsList.remove(i)
                registrationObjectExists = false;
                showRegistrationWindowButton.enabled = false;
                renderTransformButton.enabled = false
            }
    }

    function showRegistrationWindow() {
        if (registrationObjectExists) {
            appdata.makeRegistrationStackCurrent();
            reshowStackWindow(appdata.getCurrentStackWindow().toString());
//            recreateRegistrationWindow();
//        } else {
//            registrationObject.x = defaultRegistrationWindowX;
//            registrationObject.y = defaultRegistrationWindowY;
//            registrationObject.z = ++highestZ;
//            registrationObject.visible = true;
        }
        showRegistrationWindowButton.enabled = true;
    }

    function recreateRegistrationWindow() {
        destroyRegistrationWindow()
        createRegistrationWindow()
    }

    function createRegistrationWindow() {
        console.log("Creating Registration object");
        if (appdata.fixedSession == "") {
            /* Don't create registration window without a fixed session. */
            return;
        }
//        if (registrationObject)
//            return;
//        if (registrationObject) {
//            oldRegistrationX = registrationObject.x;
//            oldRegistrationY = registrationObject.y;
//            registrationObject.destroy();
//        } else {
//            oldRegistrationX = defaultRegistrationWindowX;
//            oldRegistrationY = defaultRegistrationWindowY;
//        }

        if (stackComponent.status === Component.Ready) {
            console.log("initializing reg window");
            appdata.makeRegistrationStackCurrent();
            var stackObject = stackComponent.createObject(appWindow,
                                                          {"x": defaultRegistrationWindowX,
                                                           "y": defaultRegistrationWindowY,
                                                           "isRegistration" : true,
                                                           "id": appdata.getCurrentStackWindow().toString()
                                                          });
            stackWindowsList.append({"id": appdata.getCurrentStackWindow().toString(), "object": stackObject});

            stackObject.screen = 1;
            // connecting of signals:
            stackObject.moveToScreen.connect(moveToOtherScreen);
            stackObject.minimizing.connect(handleMinimizeRegistration);
            registrationObjectExists = true;
        } else {
            console.log("Failed to construct stack object", stackComponent.status,
                       stackComponent.errorString());
        }

    }

    function destroyAllStackWindows() {
        for (var i=stackWindowsList.count-1; i>=0; --i) {
            destroyStackWindow(i)
        }
    }

    function destroyStackWindow(i) {
        stackWindowsList.get(i)["object"].destroy()
        stackWindowsList.remove(i)
    }

    function registrationWindowExists() {
        return registrationObjectExists;
    }

    function createStackWindow(stackUrl) {
        console.log("Creating SliceWindow object:", stackUrl);

        if (stackComponent.status === Component.Ready) {
            var stackObject = stackComponent.createObject(appWindow,
                                                          {"x": 200, "y": 10,
                                                          "id": appdata.getCurrentStackWindow().toString()});

            stackWindowsList.append({"id": appdata.getCurrentStackWindow().toString(), "object": stackObject});
            stackObject.screen = 1;
            // connecting of signals:
            stackObject.moveToScreen.connect(moveToOtherScreen);

            // add to main context menu
            var qmlStr = 'import QtQuick.Controls 2.4; MenuItem {
                              text: "' + stackObject.sessionName + '";
                              onTriggered: { reshowStackWindow("'+
                                appdata.getCurrentStackWindow().toString() +'"); }
                          }'
            var newMenuItem = Qt.createQmlObject(qmlStr, appWindow, "newMenuItem");
            windowMenu.addItem(newMenuItem);
            if (!menuMap) {
                menuMap = {};
            }
            menuMap[stackObject.sessionName] = newMenuItem;
        }
    }

    function reshowStackWindow(stackId) {
        appdata.setCurrentStackWindow(stackId);
        console.log("stack:"+stackId);
        for (var i=0; i < stackWindowsList.count; i++) {
            if (stackWindowsList.get(i)["id"] === stackId){
                stackWindowsList.get(i)["object"].visible = true
                stackWindowsList.get(i)["object"].z = ++highestZ
            }
        }
    }

    function recreateSliceWindows() {
        // recreate the slicewindows
        var stackUrls = appdata.getStackWindowUrls();
        ///@todo get coordinates
        for (var i = 0; i < stackUrls.length; ++i) {
            appdata.setCurrentStackWindow(i);
            createStackWindow(stackUrls[i]);
        }
    }

    function moveToOtherScreen(obj) {
        if (obj.screen === 1 && Qt.application.screens.length > 1) {
            obj.parent = window2Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 2;
        } else if (obj.screen === 2 && Qt.application.screens.length > 2) {
            obj.parent = window3Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 3;
        } else if (obj.screen === 3 && Qt.application.screens.length > 3) {
            obj.parent = window4Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 4;
        } else {
            obj.parent = window1Content;
            obj.x = 10;
            obj.y = 10;
            obj.screen = 1;
        }
    }

    property var secondWindow: Window {
        width: 1920
        height: 1080
        Material.theme: Material.Dark
        Material.accent: Material.Orange
        color: Material.background
        title: appdata.windowTitle
        flags: Qt.Window | Qt.WindowFullscreenButtonHint
        //property var seObject: null
        Rectangle {
            id: window2Content
            anchors.fill: parent
            color: "transparent"
        }
    }
    property var thirdWindow: Window {
        width: 1920
        height: 1080
        Material.theme: Material.Dark
        Material.accent: Material.Orange
        color: Material.background
        title: appdata.windowTitle
        flags: Qt.Window | Qt.WindowFullscreenButtonHint
        Rectangle {
            id: window3Content
            anchors.fill: parent
            color: "transparent"
        }
    }
    property var fourthWindow: Window {
        width: 1920
        height: 1080
        Material.theme: Material.Dark
        Material.accent: Material.Orange
        color: Material.background
        title: appdata.windowTitle
        flags: Qt.Window | Qt.WindowFullscreenButtonHint
        Rectangle {
            id: window4Content
            anchors.fill: parent
            color: "transparent"
        }
    }

    MessageDialog {
        id: errorDialog
        title: "Error"
        text: "Error message"
        icon: StandardIcon.Critical
        visible: false
        standardButtons: StandardButton.Close
        onRejected: {
            close();
        }
        Component.onCompleted: visible = false
    }

    function showErrorDialog(msg) {
        errorDialog.text = msg
        errorDialog.open()
    }

    MessageDialog {
        id: helpDialog
        title: "Keyboard Shortcuts"
        text: appdata.getSettingString("shortcuts")
        icon: StandardIcon.Information
        visible: false
        standardButtons: StandardButton.Close
        onRejected: {
            close();
        }
        Component.onCompleted: visible = false
    }

//    MessageDialog {
//        id: saveCurrentProjectDialog
//        title: "Unsaved Changes"
//        text: "Save current project first?"
//        icon: StandardIcon.Question
//        visible: false
//        standardButtons: StandardButton.Yes | StandardButton.No
//        onYes: {
//            console.log("User wants to save current project.")
//            appdata.saveAllRoiFiles();
//        }
//        Component.onCompleted: visible = false
//    }


    /*
        For Sparse Annotation
     */

    function loadSparseRoiDirectories(fileUrl){
        if (roiObject) {
            roiObject.visible = false; // don't allow UI during backend updating
            roiObject.isLoading = true;
        }
        appdata.loadRoiData(fileUrl);
        if (!roiObject)
            createRoiSparseTable();

        if (roiObject) {
//                roiObject.updateModels();
//                roiObject.updateRef();
            roiObject.isLoading = false;
            roiObject.visible = true;
        }
        saveRoiButton.enabled=true

        if (appdata.numLoadedRoiFiles() >= appdata.maxRoiFilesAllowed())
            importRoiButton.enabled = false;
        if (appdata.projectMode != "registration")
            showRoiTableButton.enabled = true;

    }

}

