import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import com.bu 1.0

Rectangle {
    id: landmarkTableCell
    property string name: "?"
    property string fixedSession: "?"
    property string movingSession: "?"
    property real regCorr : 0.0;
    property bool pressed: mouseArea.pressed
    property color normalColor: "transparent"
    property color hiliteColor: Material.color(Material.Grey, Material.Shade700)
    property color borderColor: Material.color(Material.Grey, Material.Shade600)
    property color borderHiliteColor: Material.color(Material.Grey, Material.Shade400)
    property color textColor: Material.color(Material.Grey, Material.Shade50)
    property color textHiliteColor: "white"
    property color textSelectedColor: "black"

    property bool selected: false

    signal buttonClicked(real x, real y)
    signal rightButtonClicked(real x, real y)
    signal buttonCtrlClicked(real x, real y)
    signal buttonShiftClicked(real x, real y)

    color: "transparent"
    border.color: borderColor
    width: parent.width //2 * tableCellWidth + 50
    height: tableCellHeight
    border.width: 1
    anchors.leftMargin: spaceunit
    anchors.rightMargin: spaceunit
    anchors.topMargin: spaceunit
    anchors.bottomMargin: spaceunit


    onSelectedChanged: {
        if (!selected) {
            innerBG.color = normalColor;
            border.color = borderColor;
            label.color = textColor;
            buttonPressedEffect.visible = false;
        } else {
            label.color = textSelectedColor;
            buttonPressedEffect.visible = true;
        }
    }

    Rectangle {
        id: innerBG
        anchors.fill: parent
        anchors.margins: border.width
        color: normalColor
     }
    RectangularGlow {
        id: buttonPressedEffect
        visible: false
        anchors.fill: landmarkTableCell
        glowRadius: 8
        spread: 0
        color: "orange"
    }

    Text {
        id: label
        text: parent.name
        color: textColor
        font.bold: false
        anchors.left: parent.left
        anchors.leftMargin: spaceunit //+ smallButtonSize
        anchors.rightMargin: spaceunit
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width/6
        clip: true
        font.pointSize: tableCellFontSize
    }

    Text {
        id: fixedLabel
        text: parent.fixedSession
        color: textColor
        font.bold: false
        anchors.left: label.right
        anchors.leftMargin: spaceunit
        anchors.rightMargin: spaceunit
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width/4
        clip: true
        font.pointSize: tableCellFontSize
    }

    Text {
        id: movingLabel
        text: parent.movingSession
        color: textColor
        font.bold: false
        anchors.left: fixedLabel.right
        anchors.leftMargin: spaceunit
        anchors.rightMargin: spaceunit
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width/4
        clip: true
        font.pointSize: tableCellFontSize
    }

    Text {
        id: regCorrLabel
        text: parent.regCorr.toFixed(5)
        color: textColor
        font.bold: false
        anchors.left: movingLabel.right
        anchors.leftMargin: spaceunit
        anchors.rightMargin: spaceunit
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width/4
        clip: true
        font.pointSize: tableCellFontSize
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        propagateComposedEvents: true
        //preventStealing: true
        hoverEnabled: true
        cursorShape: appdata.linkInProgress ? Qt.CrossCursor : Qt.ArrowCursor

        onEntered: {
            if (!selected) {
                innerBG.color = hiliteColor;
                parent.border.color = borderHiliteColor;
                label.color = textHiliteColor;
            }
        }
        onExited: {
            if (!selected) {
                innerBG.color = normalColor;
                parent.border.color = borderColor;
                label.color = textColor;
            }
        }
        onPressAndHold: mouse.accepted = false
        onClicked: {
            if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.ControlModifier)) {
                landmarkTableCell.buttonCtrlClicked(mouse.x, mouse.y);
            } else if ((mouse.button === Qt.LeftButton) && (mouse.modifiers & Qt.ShiftModifier)) {
                landmarkTableCell.buttonShiftClicked(mouse.x, mouse.y);
            } else if (mouse.button === Qt.RightButton) {
                landmarkTableCell.rightButtonClicked(mouse.x, mouse.y);
            } else if (mouse.button === Qt.LeftButton) {
                landmarkTableCell.buttonClicked(mouse.x, mouse.y);
            }
        }
    }
}

