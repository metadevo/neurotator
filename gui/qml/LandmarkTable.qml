import QtQuick 2.11
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import com.bu 1.0

Rectangle {
    id: landmarkTable
    property string label
    property bool isLoading: true
    property bool isFocus: true

    property color borderColor: "light gray"
    property color prevColor: borderColor
    property color syncColor: "red"
    property color focusColor: "lightblue"
    property int   prevZ: highestZ

    property LandmarkDataModel landmarkModel: appdata.landmarkModel
    property string referenceName: appdata.refSession

    property real minWidth: 300
    property real minHeight: 650

    onWidthChanged: if (width < minWidth) width = minWidth
    onHeightChanged: if (height < minWidth) height = minWidth

    width: 400
    height: 900
    z: highestZ
    color: Material.color(Material.Grey, Material.Shade800)
    radius: 3
    // this is for the glow/focus effect
    border.width: 2
    border.color: focusColor

    signal minimizing();
    /* Emitted only when the user chooses a fixed session (not on programmatic changes). */
    signal fixedSessionPicked();

    MouseArea {
        id: dragArea
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        propagateComposedEvents: true

        // don't catch hover over the whole rectangle as Qt doesn't propagate hover down the z stack

        drag.target: landmarkTable
        drag.axis: Drag.XAndYAxis
        onPressed: {
            prevColor = parent.border.color;
            parent.border.color = focusColor;
            parent.anchors.centerIn = undefined;
            landmarkTable.z = ++highestZ;
        }
        onReleased: {
            parent.border.color = prevColor;
            parent.isFocus = true;
            parent.focus = true;
        }
        onPressAndHold: mouse.accepted = false
        onClicked: {
            if (mouse.button & Qt.RightButton) {
                tableMenu.popup();
            }
        }
    }

    Rectangle
    {
        id: landmarkTableContent
        color: parent.color
        border.width: 1
        border.color: Material.color(Material.Grey, Material.Shade900)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.border.width
        anchors.rightMargin: parent.border.width
        anchors.topMargin: parent.border.width
        anchors.bottomMargin: parent.border.width

        Shortcut {
            sequence: StandardKey.Delete
            onActivated: landmarkModel.removeSelected()
        }

        Rectangle {
            id: titlebar
            height: titlebarunit
            color: landmarkTableContent.color
            anchors.left: parent.left
            anchors.leftMargin: parent.border.width
            anchors.right: parent.right
            anchors.rightMargin: parent.border.width
            anchors.top: parent.top
            anchors.topMargin: parent.border.width

            Text {
                id: titleText
                maximumLineCount: 1
                clip: true
                width: parent.width - closeButton.width - spaceunit
                anchors.left: titlebar.left
                anchors.leftMargin: spaceunit
                anchors.verticalCenter: titlebar.verticalCenter
                text: "Landmarks"
                color: "white"
                font.bold: true
                font.pointSize: 10
            }

            SmallButton {
                id: closeButton
                icon: "qrc:/gui/icons/minimize_white_18dp.png"
                anchors.right: titlebar.right
                anchors.centerIn: titlebar.Center
                onButtonClicked: {
                    landmarkTable.minimizing();
                    //landmarkTable.destroy();
                    landmarkTable.visible = false;
                }
            }
        }
        HorDivider {
            id: hordiv
            anchors.left: landmarkTableContent.left
            anchors.right: landmarkTableContent.right
            anchors.top: titlebar.bottom
            anchors.leftMargin: spaceunit
            anchors.rightMargin: spaceunit
        }

        ColumnLayout {
            id: toolArea
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: hordiv.bottom
            anchors.bottom: parent.bottom
            anchors.leftMargin: parent.border.width + spaceunit
            anchors.rightMargin: parent.border.width + spaceunit
            anchors.topMargin: spaceunit
            anchors.bottomMargin: parent.border.width + spaceunit

            ComboBox {
                id: selectFixed
                Layout.minimumWidth: 200

                font.pointSize: 10
                model: appdata.sessionNames
                displayText: appdata.fixedSession || "Set Fixed Session"

                function updateText() {
                    displayText = appdata.fixedSession
                }

                Component.onCompleted: {
                    appdata.fixedSessionChanged.connect(updateText);
                }

                onActivated: {
                    appdata.fixedSession = currentText;
                    landmarkTable.fixedSessionPicked();
                }
            }

            ComboBox {
                id: transformMode
                Layout.minimumWidth: 200

                font.pointSize: 10
                model: appdata.availableTransformModes
                displayText: appdata.transformMode

                onActivated: {
                    appdata.transformMode = currentText;
                }
            }

            ComboBox {
                id: roiMode
                Layout.minimumWidth: 200

                font.pointSize: 10
                model: appdata.availableROITransformModes()
                displayText: appdata.roiTransformMode

                onActivated: {
                    appdata.roiTransformMode = currentText;
                    appdata.updateTransforms();
                }
            }

            RowLayout {

                SmallButton {
                    id: landmarkPredictButton
                    icon: "qrc:/third-party/material-design-icons-master/auto_fix_18.png"
                    color: landmarkPredictButton.down ? "#98fb98": "#dc143c"
                    onDownChanged: {
                        appdata.landmarkPredictionDown = landmarkPredictButton.down
                    }

                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Landmark Prediction")
                    }
                }

                SmallButton {
                    id: autoRegisterOnButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_explore_white_18dp.png"
                    color: autoRegisterOnButton.down ? "#98fb98": "#dc143c"

                    onDownChanged: {
                        appdata.autoRegisterOnDown = autoRegisterOnButton.down
                    }

                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Auto Registration On/Off")
                    }
                }

                SmallButton {
                    id: applyAutoRegisterButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_explore_white_18dp.png"
                    onButtonClicked: {
                        appdata.autoRegisterSelectedLandmarks()
                        autoRegResult.text = appdata.getAutoRegisterResult()                        
                    }

                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Auto-Register Selected Landmarks")
                    }
                }

                SmallButton {
                    id: populateLandmarksButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_view_module_white_18dp.png"
                    onButtonClicked: {
                        appdata.populateLandmarks()
                    }

                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Populate Grid Landmarks")
                    }
                }

                SmallButton {
                    id: sortLandmarksButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_list_white_18dp.png"
                    onButtonClicked: {
                        appdata.sortLandmarksByCorrelation()

                    }

                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Sort Landmarks by Correlation ")
                    }
                }

                SmallButton {
                    id: deleteButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_delete_white_18dp.png"
                    onButtonClicked: {
                        landmarkModel.removeSelected()
                    }

                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Remove Selected Landmarks")
                    }
                }
            }

            HorDivider {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
            }

            //autoreg
            RowLayout
            {
                id: autoRegisterAdjustSizePanel
                height: barunit2
                visible: isRegistration
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                anchors.bottomMargin: spaceunit
                clip: true

                Text {
                    id: autoRegSizeLabel
                    text: "size:"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

               TextField
               {
                   id:autoRegSizeX
                   validator: IntValidator {bottom: 4; top: 256}
                   text:appdata.getAutoRegisterSizeXYZ(0)
                   width: parent.width
                   onAccepted:
                   {
                       appdata.setAutoRegisterSizeXYZ(autoRegSizeX.text, -1, -1)
                   }

                   horizontalAlignment: TextInput.AlignHCenter
                   implicitWidth: 100
               }

               TextField
               {
                   id:autoRegSizeY
                   validator: IntValidator {bottom: 4; top: 256}
                   text:appdata.getAutoRegisterSizeXYZ(1)
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                       appdata.setAutoRegisterSizeXYZ(-1, autoRegSizeY.text, -1)
                   }
               }

               TextField
               {
                   id:autoRegSizeZ
                   validator: IntValidator {bottom: 1; top: 64}
                   text:appdata.getAutoRegisterSizeXYZ(2)
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoRegisterSizeXYZ(-1, -1, autoRegSizeZ.text)
                   }
               }
            }

            RowLayout
            {
                id: autoRegisterAdjustSearchSpace
                height: barunit2
                visible: isRegistration
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                anchors.bottomMargin: spaceunit
                clip: true

                Text {
                    id: autoRegSearchSizeLabel
                    text: "search:"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

               TextField
               {
                   id:autoRegSearchSizeX
                   validator: IntValidator {bottom: 0; top: 256}
                   text:appdata.getAutoRegisterSearchSpaceXYZ(0)
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoRegisterSearchSpaceXYZ(autoRegSearchSizeX.text, -1, -1)
                   }
               }

               TextField
               {
                   id:autoRegSearchSizeY
                   validator: IntValidator {bottom: 0; top: 256}
                   text:appdata.getAutoRegisterSearchSpaceXYZ(1)
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoRegisterSearchSpaceXYZ(-1, autoRegSearchSizeY.text, -1)
                   }
               }

               TextField
               {
                   id:autoRegSearchSizeZ
                   text:appdata.getAutoRegisterSearchSpaceXYZ(2)
                   validator: IntValidator {bottom: 0; top: 256}
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoRegisterSearchSpaceXYZ(-1, -1, autoRegSearchSizeZ.text)
                   }
               }
            }

            RowLayout
            {
                id: autopopulateSpacing
                height: barunit2
                visible: isRegistration
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                anchors.bottomMargin: spaceunit
                clip: true

                Text {
                    id: autopopulateSpacingLabel
                    text: "space:"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

               TextField
               {
                   id:autopopulateSpacingX
                   validator: IntValidator {bottom: 0; top: 256}
                   text:appdata.getAutoPopulateSpacingXYZ(0)
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoPopulateSpacingXYZ(autopopulateSpacingX.text, -1, -1)
                   }
               }

               TextField
               {
                   id:autopopulateSpacingY
                   validator: IntValidator {bottom: 0; top: 256}
                   text:appdata.getAutoPopulateSpacingXYZ(1)
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoPopulateSpacingXYZ(-1, autopopulateSpacingY.text, -1)
                   }
               }

               TextField
               {
                   id:autopopulateSpacingZ
                   text:appdata.getAutoPopulateSpacingXYZ(2)
                   validator: IntValidator {bottom: 0; top: 256}
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   onAccepted:
                   {
                        appdata.setAutoPopulateSpacingXYZ(-1, -1, autopopulateSpacingZ.text)
                   }
               }
            }

            Row
            {
                id: autoRegisterAdjustSTDPanel
                height: barunit2
                visible: isRegistration
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                anchors.bottomMargin: spaceunit
                clip: true

                Text {
                    id: autoRegisterAdjustSTDLabel
                    text: "STD/m:"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

                TextField
                {
                    id:autoRegisterAdjustSTD
                    validator: IntValidator {bottom: 0; top: 200}
                    text:appdata.getAutoRegisterSTD()
                    implicitWidth: 100
                    horizontalAlignment: TextInput.AlignHCenter
                    onAccepted:
                    {
                        appdata.setAutoRegisterSTD(autoRegisterAdjustSTD.text)
                    }
                }


                Text
                {
                    id: autoRegisterAdjustCorrLabel
                    text: "Corr:"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

                TextField
                {
                    id:autoRegisterAdjustCorr
                    validator: IntValidator {bottom: 0; top: 101}
                    text:appdata.getAutoRegisterCorr()
                    implicitWidth: 100
                    horizontalAlignment: TextInput.AlignHCenter
                    Layout.leftMargin : 0
                    onAccepted:
                    {
                        appdata.setAutoRegisterCorr(autoRegisterAdjustCorr.text)
                    }
                }

            }

            HorDivider {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
            }

            Row
            {
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                Text
                {
                    id: autoRegisterCorrResultLabel
                    text: "offset:"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

                TextField
                {
                    id:autoRegResult
                    text:appdata.getAutoRegisterResult(2)
                    validator: IntValidator {bottom: 1; top: 64}
                    implicitWidth: 100
                    horizontalAlignment: TextInput.AlignHCenter
                    readOnly: true
                }

                Text
                {
                    id: autoRegisterSTDResultLabel
                    text: "std/m :"
                    maximumLineCount: 1
                    clip: true
                    Layout.alignment: Qt.AlignVCenter
                    color: Material.color(Material.Grey)
                    font.pointSize: autoRegSizeLabel
                    Layout.preferredWidth: parent.width/6
                }

               TextField
               {
                   id:autoRegResultSTD
                   //validator: IntValidator {bottom: 0; top: 256}
                   text:appdata.getAutoRegisterResultSTD()
                   implicitWidth: 100
                   horizontalAlignment: TextInput.AlignHCenter
                   readOnly: true
                   onAccepted:
                   {
                        //appdata.setAutoRegisterSearchSpaceXYZ(0, autoRegSearchSizeY.text, 0)
                   }
               }
            }
/*
            RowLayout
            {
                id: autoRegisterResultPanel
                height: barunit2
                visible: isRegistration
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                anchors.bottomMargin: spaceunit
                clip: true
                TextField {
                    id: autoRegResult
                    text: appdata.getAutoRegisterResult()
                    horizontalAlignment: TextInput.AlignHCenter
                    readOnly: true                   
                }

            }
*/
            RowLayout
            {
                spacing: 5
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                anchors.bottomMargin: spaceunit

                Image
                {
                    id: autoRegisterCapsF
                    source: "image://subvolumecaps/fixed"
                    cache: false
                }

                /*
                Image {
                    id: autoRegisterCapsM
                    source: "image://subvolumecaps/moving"
                    cache: false
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            autoRegisterCapsM.source = "image://subvolumecaps/m"+Math.random()
                            parent.sourceChanged(parent.source);
                            parent.update()
                        }
                    }
                }
                */
            }

            function updateAutoRegisteration(indx)
            {
                autoRegResult.text = appdata.getAutoRegisterResult()
                autoRegResultSTD.text = appdata.getAutoRegisterResultSTD()
                autoRegisterCapsF.source = "image://subvolumecaps/f" + Math.random()
                autoRegisterCapsF.sourceChanged(autoRegisterCapsF.source);
                autoRegisterCapsF.update()
                var myTableCell = confirmedList.contentItem.children[(indx == 0) ? 0 : indx+1] //weird
                myTableCell.updateCell();
            }

            Component.onCompleted:
            {
                appdata.landmarkAutoRegistered.connect(updateAutoRegisteration)
            }


            HorDivider {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
            }

            Rectangle {
                id: landmarkTableView

                color: "transparent"
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignLeft
                Layout.minimumWidth: 180
                Layout.minimumHeight: 150
                Layout.preferredWidth: parent.width
                Layout.leftMargin: spaceunit
                Layout.rightMargin: spaceunit
                border.width: 1
                border.color: Material.color(Material.Grey, Material.Shade900)

                ListView {
                    id: confirmedList
                    anchors.fill: parent
                    anchors.margins: 1
                    model: landmarkModel
                    highlightFollowsCurrentItem: true
                    highlightMoveDuration: 500
                    clip: true
                    //contentWidth: 2 * tableCellWidth + 50
                    flickableDirection: Flickable.AutoFlickDirection;
                    keyNavigationEnabled: true
                    focus: true
                    boundsBehavior: Flickable.StopAtBounds
                    ScrollBar.horizontal: ScrollBar { id: hbar; active: vbar.active }
                    ScrollBar.vertical: ScrollBar { id: vbar; active: hbar.active }
                    headerPositioning: ListView.OverlayHeader

                    Layout.fillWidth: true
                    delegate: LandmarkTableCell {
                        id: tableCell
                        name: model.label
                        fixedSession: model.fixedSession
                        movingSession: model.movingSession
                        regCorr : appdata.getAutoRegisterCorrelation(index)
                        selected: landmarkModel.isSelected(index)
                        Layout.fillWidth: true

                        function updateCell()
                        {
                            regCorr = appdata.getAutoRegisterCorrelation(index)
                            name = model.label
                            fixedSession = model.fixedSession
                            movingSession = model.movingSession
                        }

                        onButtonClicked: {
                            landmarkModel.setSelection(index);
                        }
                        onButtonCtrlClicked: {
                            landmarkModel.toggleSelection(index);
                        }

                        function updateSelected() {
                            selected = landmarkModel.isSelected(index)
                        }
                        Component.onCompleted: {
                            landmarkModel.selectionChanged.connect(updateSelected)
                            landmarkModel.orderChanged.connect(updateCell)
                        }
                        Component.onDestruction: {
                            // This seems redundant but it prevents crashes in the QML
                            // engine after removing landmarks.
//                            landmarkModel.selectionChanged.disconnect(updateSelected)
                        }
                    }
                }
            }

            Button {
                id: renderButton
                text: qsTr("Render Transform")
                onClicked: {
                    appdata.renderTransform();
                }
            }
        }
    }

    BusyIndicator {
        id: busy
        visible: false
        running: isLoading
        anchors.fill: parent
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // resizing grips

    // left side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: -2
        width: spaceunit + 2
        MouseArea {
            id: leftGrip
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
                //maximumX: //screen max x
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = landmarkTable.width - delta;
                    var newX = landmarkTable.x + delta;
                    if (newWidth < landmarkTable.minWidth || newX < 0) {
                        return;
                    }
                    landmarkTable.x = newX;
                    landmarkTable.width = newWidth;
                    confirmedList.update();
                }
            }
        }
    }

    // right side resize grip
    Rectangle {
        color: "transparent"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: -2
        width: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
                //maximumX: //screen max x
            }
            onPositionChanged: {
                if (drag.active) {
                    //var delta = Math.max(mouseX, mouseY)
                    var delta = mouseX;
                    var newWidth = landmarkTable.width + delta;
                    if (newWidth < landmarkTable.minWidth) {
                        return;
                    }
                    landmarkTable.width = newWidth;
                    confirmedList.update();
                }
            }
        }
    }

    // top side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
                //minimumY:
                //maximumY:
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = landmarkTable.height - delta;
                    var newY = landmarkTable.y + delta;
                    if (newHeight < landmarkTable.minHeight || newY < 0) {
                        return;
                    }
                    landmarkTable.y = newY;
                    landmarkTable.height = newHeight;
                    confirmedList.update();
                }
            }
        }
    }

    // bottom side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
                //minimumY:
                //maximumY:
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = landmarkTable.height + delta;
                    if (newHeight < landmarkTable.minHeight) {
                        return;
                    }
                    landmarkTable.height = newHeight;
                    confirmedList.update();
                    toolArea.update();
                }
            }
        }
    }
}
