import QtQuick 2.0
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.4
import QtGraphicalEffects 1.0
import QtQuick.Extras 1.4
import com.bu 1.0

Rectangle {
    x: 0
    y: 0
    width: 640
    height: 70
    color: "transparent"
    property bool sparseGroup: false
    property alias viewLabel: viewLabel
    property alias channelSelector: channelSelector

    Loader{
        anchors.left: parent.left
        anchors.right: parent.right

        sourceComponent: (sparseGroup) ? null : horDividerComponent
    }

    Component{
        id: horDividerComponent
        HorDivider {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: spaceunit
            anchors.rightMargin: spaceunit
        }
    }


    Text {
        id:viewLabel
        x: 20
        y: 28
        color: "#ffffff"
        text: qsTr("Channel")
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize: 12
    }

    function updateChannelDropdown() {
        channelSelector.update();
        updateSelectedChannelDetails();
    }

    function updateSelectedChannelDetails() {
        if(sparseGroup){
            if(firstWindow != null){
                updateColorPickerButton(firstWindow.sliceEdit.sliceImage.getColorForChannel(channelSelector.currentText))
                updateIntensityRangeText(firstWindow.sliceEdit.sliceImage.getScalarRangeForChannel(channelSelector.currentText))
                updateWindowLevelSlider(firstWindow.sliceEdit.sliceImage.getWindowLevelForChannel(channelSelector.currentText))
                updateChannelState(firstWindow.sliceEdit.sliceImage.getChannelState(channelSelector.currentText))
            }
        }else{
            updateColorPickerButton(sliceEdit.sliceImage.getColorForChannel(channelSelector.currentText))
            updateIntensityRangeText(sliceEdit.sliceImage.getScalarRangeForChannel(channelSelector.currentText))
            updateWindowLevelSlider(sliceEdit.sliceImage.getWindowLevelForChannel(channelSelector.currentText))
            updateChannelState(sliceEdit.sliceImage.getChannelState(channelSelector.currentText))
        }
    }

    ComboBox {
        id: channelSelector
        y: 22
        width: 65
        height: 26
        anchors.left: viewLabel.right
        anchors.leftMargin: 15
        textRole: qsTr("")
        anchors.verticalCenter: parent.verticalCenter
        font.bold: true
        displayText: "-1"
        font.pointSize: 12
        flat: true
        editable: true
        model: (sparseGroup) ? appdata.channelList : sliceEdit.sliceImage.channelList
        onCurrentTextChanged: {
            if(sparseGroup){
                for (let i = 0; i < sparseSliceWindowsList.count; i++) {
                    if(sparseSliceWindowsList.get(i)["object"].cList.includes(channelSelector.currentIndex)){
                        sparseSliceWindowsList.get(i)["object"].colorEditor.channelSelector.currentIndex = sparseSliceWindowsList.get(i)["object"].cList.indexOf(channelSelector.currentIndex);
                    }
                }
                if(viewLabel.text == "Session"){
                    appdata.sessionChanged(channelSelector.currentIndex);
                }else{
                    appdata.channelChanged(channelSelector.currentIndex);
                }
                appdata.updateAllSliceOverlays()
                //toggleChannel.enabled = !((firstWindow.sliceEdit.sliceImage.numActiveChannels() <= 1) && firstWindow.sliceEdit.sliceImage.getChannelState(channelSelector.currentText));
            }else{
                toggleChannel.enabled = !((sliceEdit.sliceImage.numActiveChannels() <= 1) && sliceEdit.sliceImage.getChannelState(channelSelector.currentText));
            }
            updateSelectedChannelDetails()
        }
        Component.onCompleted: {
//            __style.textColor = "white"
                if(!sparseGroup) updateSelectedChannelDetails()
        }
    }

    function updateChannelState(bState) {
        toggleChannel.checked = bState
    }

    function updateColorPickerButton(color) {
        colorPickerButton.Material.background = color
    }

    function updateIntensityRangeText(values) {
        intensityMin.text = values[0]
        intensityMax.text = values[1]
        windowLevelSlider.from = values[0]
        windowLevelSlider.to = values[1]

    }

    function updateWindowLevelSlider(values) {
        if (values[0] < windowLevelSlider.from)
            windowLevelSlider.from = values[0]
        windowLevelSlider.first.value = values[0]
        if (values[1] > windowLevelSlider.to)
            windowLevelSlider.from = values[1]
        windowLevelSlider.second.value = values[1]
    }

    function initializeWindowLevelSlider(rangeValues,sliderValues) {
        windowLevelSlider.from = rangeValues[0]
        windowLevelSlider.to = rangeValues[1]
        windowLevelSlider.first.value = sliderValues[0]
        windowLevelSlider.second.value = sliderValues[1]
    }


    Button {
        id: colorPickerButton
        y: 22
        width: 37
        height: 26
        text: ""
        anchors.left: channelSelector.right
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        font.capitalization: Font.Capitalize
        font.bold: false
        enabled: !isRegistration
        display: AbstractButton.IconOnly
        transformOrigin: Item.Center
        checkable: false
        onClicked: (sparseGroup) ? firstWindow.sliceEdit.sliceImage.showColorSelector(channelSelector.currentText) : sliceEdit.sliceImage.showColorSelector(channelSelector.currentText)
        Material.background: "#fc4056"
    }

    RangeSlider {
        id: windowLevelSlider
        y: 22
        width: 175
        height: 28
        anchors.left: intensityMin.right
        anchors.leftMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        second.value: 0.75
        first.value: 0.25
        first.onMoved: {
            if(sparseGroup){
                for (var i=0; i < sparseSliceWindowsList.count; i++) {
                    if(sparseSliceWindowsList.get(i)["object"].cList.includes(channelSelector.currentIndex)){
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setWindowLevelForChannel(sparseSliceWindowsList.get(i)["object"].cList.indexOf(channelSelector.currentIndex), first.value, second.value)
                        sparseSliceWindowsList.get(i)["object"].redrawNav()
                    }

                }
            }else{
                sliceEdit.sliceImage.setWindowLevelForChannel(channelSelector.currentText, first.value, second.value)
                sliceWindow.redrawNav()
            }
        }
        second.onMoved: {

            if(sparseGroup){
                for (var i=0; i < sparseSliceWindowsList.count; i++) {
                    if(sparseSliceWindowsList.get(i)["object"].cList.includes(channelSelector.currentIndex)){
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setWindowLevelForChannel(sparseSliceWindowsList.get(i)["object"].cList.indexOf(channelSelector.currentIndex), first.value, second.value)
                        sparseSliceWindowsList.get(i)["object"].redrawNav()
                    }
                }
            }else{
                sliceEdit.sliceImage.setWindowLevelForChannel(channelSelector.currentText, first.value, second.value)
                sliceWindow.redrawNav()
            }
        }
    }

    Text {
        id: intensityMin
        y: 27
        width: 30
        color: "#ffffff"
        text: qsTr("0")
        anchors.left: toggleChannel.right
        anchors.leftMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 12
    }

    Text {
        id: intensityMax
        y: 18
        width: 30
        color: "#ffffff"
        text: qsTr("0")
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: windowLevelSlider.right
        anchors.leftMargin: 5
        font.pixelSize: 12
        horizontalAlignment: Text.AlignRight
    }

    Switch {
        id: toggleChannel
        y: 40
        width: 60
        height: 30
        text: qsTr("")
        scale: 0.8
        anchors.left: colorPickerButton.right
        anchors.leftMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        checked: true
        onClicked: {
            if(sparseGroup){
                for (var i=0; i < sparseSliceWindowsList.count; i++) {
                    if(sparseSliceWindowsList.get(i)["object"].cList.includes(channelSelector.currentIndex)){
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.toggleChannelOnOff(sparseSliceWindowsList.get(i)["object"].cList.indexOf(channelSelector.currentIndex), toggleChannel.checked);
                    }
                }
            }else{
                sliceEdit.sliceImage.toggleChannelOnOff(channelSelector.currentText, toggleChannel.checked)
            }
        }
        display: AbstractButton.IconOnly
        enabled: !isRegistration
    }
}

/*##^## Designer {
    D{i:3;anchors_x:99}D{i:4;anchors_x:193}D{i:5;anchors_x:267}D{i:6;anchors_x:245}D{i:7;anchors_x:245}
D{i:8;anchors_x:0}
}
 ##^##*/
