/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtQuick.Controls 2.4

SpinBox {
    id: smallSpinBox
    value: 50
    editable: true
    width: 70
    height: 18

    background: Rectangle {
        implicitWidth: 70
        implicitHeight: 18
        width: smallSpinBox.width
        height: smallSpinBox.height
        border.color: Material.color(Material.Grey, Material.Shade600)
    }

    up.indicator: Rectangle {
        x: smallSpinBox.width - width
        height: smallSpinBox.height
        implicitWidth: 10
        implicitHeight: 10
        color: smallSpinBox.up.pressed ? Material.color(Material.Grey, Material.Shade900) : Material.color(Material.Grey, Material.Shade800)
        border.color: enabled ? "#bdbebe" : Material.color(Material.Grey, Material.Shade700)

        Text {
            text: "+"
            font.pixelSize: 8
            color: "#ffcc80"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    down.indicator: Rectangle {
        x: 0
        height: smallSpinBox.height
        implicitWidth: 10
        implicitHeight: 10
        color: smallSpinBox.down.pressed ? Material.color(Material.Grey, Material.Shade900) : Material.color(Material.Grey, Material.Shade800)
        border.color: enabled ? "#bdbebe" : Material.color(Material.Grey, Material.Shade700)

        Text {
            text: "-"
            font.pixelSize: 8
            color: "#ffcc80"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    contentItem: TextInput {
        z: 2
        text: smallSpinBox.textFromValue(smallSpinBox.value, smallSpinBox.locale)
        height: smallSpinBox.height
        //bottomPadding: 0
        //topPadding: 0
        font.pointSize: 8
        color: "#111111"
        selectionColor: "#21be2b"
        selectedTextColor: "#ffffff"
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        readOnly: !smallSpinBox.editable
        validator: smallSpinBox.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        selectByMouse: true
    }
}
