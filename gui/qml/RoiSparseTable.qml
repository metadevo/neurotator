import QtQuick 2.13
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.13
import QtQuick.Controls.Material 2.13
import QtQuick.Layouts 1.3
import com.bu 1.0

Rectangle {
    id: roiSparseTable
    property string label
    property bool isFocus: false
    property bool isLoading: true
    property bool needsRecreate: false

    property color borderColor: "light gray"
    property color prevColor: borderColor
    property color syncColor: "red"
    property color focusColor: "lightblue"

    property RoiDataSparseModel roiModel: appdata.sparseRoiModel

    property int prevZ: highestZ
    property int currentRow: 0
    property real gammaVal: 1.0
    property real minWidth: 250
    property real minHeight: 600
    property string viewMode: "Iso-Channel View"

    signal destroying(bool recreate);
    signal minimizing();

    onWidthChanged: if (width < minWidth) width = minWidth
    onHeightChanged: if (height < minWidth) height = minWidth

    width: 400
    height: 710
    z: highestZ
    color: Material.color(Material.Grey, Material.Shade800)
    radius: 3
    // this is for the glow/focus effect
    border.width: 2
    border.color: borderColor

    Keys.onUpPressed: {
        if(currentRow > 0){
            roiModel.setSelection(currentRow - 1);
            createSliceGroupWindow(getRoiName(currentRow - 1));
            currentRow--;
        }
    }

    Keys.onDownPressed: {
        if(currentRow < roiModel.rowCount()){
            roiModel.setSelection(currentRow + 1);
            createSliceGroupWindow(getRoiName(currentRow + 1));
            currentRow++;
        }
    }

    Component.onCompleted: {
        focus = true; // for up and down key inputs
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        acceptedButtons: Qt.AllButtons
        hoverEnabled: true
        propagateComposedEvents: true
        // don't catch hover over the whole rectangle as Qt doesn't propagate hover down the z stack
        drag.target: roiSparseTable
        drag.axis: Drag.XAndYAxis
        onEntered: parent.focus = true;
        onPressed: {
            prevColor = parent.border.color;
            parent.border.color = focusColor;
            roiSparseTable.z = ++highestZ;
        }
        onReleased: {
            parent.border.color = prevColor;
        }

    }

    Rectangle
    {
        id: roiSparseTableContent
        color: parent.color
        border.width: 1
        border.color: Material.color(Material.Grey, Material.Shade900)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.border.width
        anchors.rightMargin: parent.border.width
        anchors.topMargin: parent.border.width
        anchors.bottomMargin: parent.border.width

        Rectangle {
            id: titlebar
            height: titlebarunit
            color: parent.color
            anchors.left: parent.left
            anchors.leftMargin: parent.border.width
            anchors.right: parent.right
            anchors.rightMargin: parent.border.width
            anchors.top: parent.top
            anchors.topMargin: parent.border.width

            Text {
                id: titleText
                maximumLineCount: 1
                clip: true
                width: contentWidth
                anchors.left: titlebar.left
                anchors.leftMargin: spaceunit
                anchors.verticalCenter: titlebar.verticalCenter
                text: "Sparse ROIs"
                color: "white"
                font.bold: true
                font.pointSize: 10
            }

            SmallButton {
                id: viewModeButton
                height: parent.height
                anchors.right: openFindButton.left
                anchors.centerIn: titlebar.Center
                customWidth: 120
                text: viewMode
                onButtonClicked: {
                    (viewMode === "Iso-Channel View") ? viewMode = "Iso-Session View" : viewMode = "Iso-Channel View";
                    if(sparseGroupWindowObject != null) {
                        sparseGroupWindowObject.unloadSparseStacks()
                        sparseGroupWindowObject.createSparseSliceWindows(getRoiName(currentRow), viewMode)
                    }
                }

                ToolTip {
                    visible: parent.hovered
                    delay: toolTipDelay
                    timeout: toolTipTimeout
                    font.pointSize: toolTipFontSize
                    text: (viewMode === "Iso-Channel View") ? qsTr("Switch to Iso-Session View") : qsTr("Switch to Iso-Channel View")
                }
            }

            SmallButton {
                id: openFindButton
                icon: searchIcon
                anchors.right: closeButton.left
                anchors.centerIn: titlebar.Center
                onButtonClicked: {
                    findRoi.open();
                }
                ToolTip {
                    visible: parent.hovered
                    delay: toolTipDelay
                    timeout: toolTipTimeout
                    font.pointSize: toolTipFontSize
                    text: qsTr("Find...")
                }
            }

            SmallButton {
                id: closeButton
                icon: "qrc:/gui/icons/minimize_white_18dp.png"
                anchors.right: titlebar.right
                anchors.centerIn: titlebar.Center
                onButtonClicked: {
                    roiSparseTable.minimizing();
                    roiSparseTable.visible = false;
                }
            }
        }
        HorDivider {
            id: hordiv
            anchors.left: roiSparseTableContent.left
            anchors.right: roiSparseTableContent.right
            anchors.top: titlebar.bottom
            anchors.leftMargin: spaceunit
            anchors.rightMargin: spaceunit
        }

        ColumnLayout {
            id: toolArea
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: hordiv.bottom
            anchors.bottom: parent.bottom
            anchors.leftMargin: parent.border.width + spaceunit
            anchors.rightMargin: parent.border.width + spaceunit
            anchors.topMargin: spaceunit
            anchors.bottomMargin: parent.border.width + spaceunit

            Text {
                id: unconfirmedLabel
                text: "Loaded ROIs"
                color: Material.color(Material.Grey)
                font.pointSize: labelSize
                clip: true
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: roiSparseTableContent.border.width + spaceunit
                Layout.topMargin: spaceunit
                Layout.bottomMargin: spaceunit
            }

            Rectangle {
                id: roiSparseTableView
                color: "transparent"
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: tableColWidth
                Layout.minimumHeight: 150
                Layout.leftMargin: spaceunit
                Layout.rightMargin: spaceunit
                border.width: 1
                border.color: Material.color(Material.Grey, Material.Shade900)

                TableView {
                    id: tableView
                    clip: true
                    model: roiModel
                    anchors.fill: parent
                    columnWidthProvider: function (column) { return ( column === 0 ? 2 : 1 ) * (tableView.width/3); }
                    rowHeightProvider: function (column) { return 32; }
                    topMargin: columnsHeader.implicitHeight

                    Row {
                        id: columnsHeader
                        y: tableView.contentY
                        z: 2
                        onWidthChanged: tableView.forceLayout()
                        Repeater {
                            model: tableView.columns > 0 ? tableView.columns : 1
                            Label {
                                width: tableView.columnWidthProvider(modelData)
                                height: tableCellHeight
                                text: roiModel.headerData(modelData, Qt.Horizontal)
                                color: "black"
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                background: Rectangle {
                                    color: Material.color(Material.Grey, Material.Shade500)
                                    border.color: Material.color(Material.Grey, Material.Shade900)
                                    border.width: 1
                                }
                            }
                        }
                    }

                    delegate: Rectangle {
                        color: normalColor// { (row % 2 == 0) ? "#222222" : "#303030"}
                        width: tableView.columnWidthProvider(model.column)
                        height: tableCellHeight
                        border.width: 1
                        border.color: borderNormalColor
                        property color normalColor: (row % 2 == 0) ? "#222222" : "#303030"
                        property color hiliteColor: Material.color(Material.Grey, Material.Shade700)
                        property color borderNormalColor: Material.color(Material.Grey, Material.Shade900)
                        property color borderHiliteColor: Material.color(Material.Grey, Material.Shade400)
                        property color textColor: Material.color(Material.Grey, Material.Shade50)
                        property color textSelectedColor: "black"
                        property bool selected: roiModel.isSelected(index)
                        property bool hovered: false
                        onSelectedChanged: {
                            if (selected) {
                                border.color = borderHiliteColor;
                                roiLabel.color = textSelectedColor;
                                buttonPressedEffect.visible = true;
                            } else {
                                if(!hovered){
                                    innerBG.color = normalColor;
                                    border.color = borderNormalColor;
                                }
                                roiLabel.color = textColor;
                                buttonPressedEffect.visible = false;
                            }
                        }
                        onHoveredChanged: {
                            if (hovered) {
                                    innerBG.color = hiliteColor;
                                    border.color = borderHiliteColor;
                            } else if (!selected) {
                                    innerBG.color = normalColor
                                    border.color = borderNormalColor;
                            }
                        }
                        Component.onCompleted: {
                            roiModel.selectionChanged.connect(updateSelected)
                            roiModel.hoveredChanged.connect(updateHovered)
                        }
                        Component.onDestruction: {
                            roiModel.selectionChanged.disconnect(updateSelected)
                            roiModel.hoveredChanged.disconnect(updateHovered)
                        }
                        function updateSelected() {selected = roiModel.isSelected(index); }
                        function updateHovered() {hovered = roiModel.isHovered(index); }

                        Rectangle {
                            id: innerBG
                            anchors.fill: parent
                            anchors.margins: border.width
                            color: parent.color
                         }

                        RectangularGlow {
                            id: buttonPressedEffect
                            visible: false
                            anchors.fill: parent
                            glowRadius: 8
                            spread: 0
                            color: "orange"
                        }

                        Text {
                            id: roiLabel
                            color: textColor
                            text: display
                            anchors.centerIn: parent
                            horizontalAlignment: Text.AlignLeft
                        }


                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            acceptedButtons: Qt.LeftButton | Qt.RightButton
                            onEntered: if (!selected) roiModel.setHovered(index);
                            onExited: if (!selected) roiModel.emptyHovered();
                            onPressAndHold: mouse.accepted = false
                            onClicked: {
                                var start = new Date().getTime();

                                currentRow = model.row
                                if(mouse.button === Qt.LeftButton) {
                                    var start_createslicegroupwindow = new Date().getTime();
                                    createSliceGroupWindow(getRoiName(model.row));
                                    var end_createslicegroupwindow = new Date().getTime();
                                    console.log("Elapsed time (createSliceGroupWindow)"+ (end_createslicegroupwindow - start_createslicegroupwindow) + " ms")
                                    roiModel.setSelection(index);
                                }
                                var end = new Date().getTime();
                                console.log("Elapsed time (finished loading all stacks and contructing windows)"+ (end - start) + " ms")

                                if (mouse.button === Qt.RightButton) {
                                    contextMenu.popup()
                                }
                            }
                        }

                        Menu {
                            id: contextMenu
                            width: { // auto resize the menu width to fit text content
                                var result = 0;
                                var padding = 0;
                                for (var i = 0; i < count; ++i) {
                                    var item = itemAt(i);
                                    if(item.objectName !== "contextMenuDiv" ){
                                    result = Math.max(item.contentItem.implicitWidth, result);
                                    padding = Math.max(item.padding, padding)
                                    }
                                }
                                return result + padding * 2;
                            }
                            MenuItem {
                                text: "Delete " + getRoiName(model.row)
                                onTriggered:{
                                    appdata.unloadRoiData(getRoiName(model.row));
                                }
                            }
                        }
                    }
                }
            }

            Rectangle {
                color: Material.color(Material.Grey, Material.ShadeA100)
                border.width: 1
                border.color: Material.color(Material.Grey, Material.Shade800)
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumWidth: 100
                Layout.minimumHeight: 10
                Layout.leftMargin: spaceunit
                Layout.rightMargin: spaceunit
                clip: true

                Text {
                    id: lastCmdText
                    text: appdata.cmdText;
                    color: Material.color(Material.Grey)
                    font.pointSize: labelSize
                    clip: true
                    anchors.left: parent.left
                    anchors.topMargin: spaceunit
                    anchors.leftMargin: roiSparseTableContent.border.width + spaceunit
                }
            }
        }
    }

    FindRoi {
        id: findRoi
        visible: false
        x: parent.width
    }

    BusyIndicator {
        id: busy
        visible: false
        running: isLoading
        anchors.fill: parent
    }

    function getRoiName(index){
        return roiModel.data(roiModel.index(index, 0))
    }

    function createSliceGroupWindow(sessionName){
        if(sparseGroupWindowObject == null){
            // create sparse group window
            if (sparseGroupWindowComponent.status === Component.Ready) {

                sparseGroupWindowObject = sparseGroupWindowComponent.createObject
                        (appWindow,{x: roiSparseTable.x + roiSparseTable.width + 20, y: 50})
                sparseGroupWindowObject.createSparseSliceWindows(sessionName, viewMode)

                // Error Handling
                if (sparseGroupWindowObject == null) {
                    console.log("Error creating sparseGroupWindowObject");
                }
            } else if (sparseGroupWindowComponent.status === Component.Error) {
                console.log("Error loading sparseGroupWindowComponent:", sparseGroupWindowComponent.errorString());
            }

        }else if (sessionName !== sparseGroupWindowObject.sessionName){
            var start_a = new Date().getTime();
            sparseGroupWindowObject.unloadSparseStacks()
            var end_a = new Date().getTime();
            console.log("Elapsed time (sparseGroupWindowObject.unloadSparseStacks())"+ (end_a - start_a) + " ms")

            var start_b = new Date().getTime();
            sparseGroupWindowObject.createSparseSliceWindows(sessionName, viewMode)
            var end_b = new Date().getTime();
            console.log("Elapsed time (sparseGroupWindowObject.createSparseSliceWindows)"+ (end_b - start_b) + " ms")
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // resizing grips

    // left side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: -2
        width: spaceunit + 2
        MouseArea {
            id: leftGrip
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
                //maximumX: //screen max x
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = roiSparseTable.width - delta;
                    var newX = roiSparseTable.x + delta;
                    if (newWidth < roiSparseTable.minWidth || newX < 0) {
                        return;
                    }
                    roiSparseTable.x = newX;
                    roiSparseTable.width = newWidth;
                    confirmedList.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    // right side resize grip
    Rectangle {
        color: "transparent"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: -2
        width: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
                //maximumX: //screen max x
            }
            onPositionChanged: {
                if (drag.active) {
                    //var delta = Math.max(mouseX, mouseY)
                    var delta = mouseX;
                    var newWidth = roiSparseTable.width + delta;
                    if (newWidth < roiSparseTable.minWidth) {
                        return;
                    }
                    roiSparseTable.width = newWidth;
                }
            }
        }
    }

    // top side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
                //minimumY:
                //maximumY:
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = roiSparseTable.height - delta;
                    var newY = roiSparseTable.y + delta;
                    if (newHeight < roiSparseTable.minHeight || newY < 0) {
                        return;
                    }
                    roiSparseTable.y = newY;
                    roiSparseTable.height = newHeight;
                    confirmedList.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    // bottom side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
                //minimumY:
                //maximumY:
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = roiSparseTable.height + delta;
                    if (newHeight < roiSparseTable.minHeight) {
                        return;
                    }
                    roiSparseTable.height = newHeight;
                    confirmedList.update();
                    toolArea.update();
                    ///@todo update the unconfirmed lists too
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
}
