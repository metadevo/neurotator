/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtQuick.Controls 2.4

Slider {
    id: tinySlider
    value: 0.5
    topPadding: 1

    property color slideColor: "red"

    background: Rectangle {
        x: tinySlider.leftPadding
        y: tinySlider.topPadding + tinySlider.availableHeight / 2 - height / 2
        implicitWidth: 54
        implicitHeight: 4
        width: tinySlider.availableWidth
        height: implicitHeight
        radius: 2
        color: Material.color(Material.Grey, Material.ShadeA100)

        Rectangle {
            width: tinySlider.visualPosition * parent.width
            height: parent.height
            color: slideColor
            radius: 2
        }
    }

    handle: Rectangle {
        x: tinySlider.leftPadding + tinySlider.visualPosition * (tinySlider.availableWidth - width)
        y: tinySlider.topPadding + tinySlider.availableHeight / 2 - height / 2
        implicitWidth: 4
        implicitHeight: 4
        radius: 0
        color: tinySlider.pressed ? "#f0f0f0" : "#f6f6f6"
        border.color: "#bdbebf"
    }
}
