/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import com.bu 1.0
import QtQuick.Window 2.11

Rectangle {
    id: sliceWindow

    property string sessionName: ""
    property bool isSparseAnnotation: false
    property bool isRegistration: false
    property bool isFocus: false
    property bool isSynced: false
    property url stackPath: ""
    property int screen: 1
    property int id: -1
    property real ratio: sliceEdit.height/sliceEdit.width
    property color borderColor: "light gray"
    property color prevColor: borderColor
    property color syncColor: "red"
    property color focusColor: "lightblue"

    property int prevZ: highestZ
    // pixels needed beyond sliceEdit width
    property int guiWidth: 112
    // pixels needed beyond sliceEdit height
    property int guiHeight: 160 + (isRegistration ? barunit2 : 0) + 70 - 30

    property var cList: []

    property alias sliceEdit: sliceEdit
    property alias colorEditor: colorEditor

    color: Material.color(Material.Grey, Material.Shade800)
    z: highestZ
    width: isSparseAnnotation ? sliceEdit.width + spaceunit * 3 : sliceEdit.width + guiWidth;
    height: isSparseAnnotation ? spaceunit + sliceEdit.height + spaceunit * 2 + imageAdjustPanel2.height
                               : sliceEdit.height + guiHeight;
    radius: 3
    // this is for the glow/focus effect
    border.width: 2
    border.color: borderColor

    signal moveToScreen(var obj);
    signal minimizing();

    Component.onCompleted: {
        sliceWindow.z = ++highestZ;
        if (sliceEdit.width < sliceEdit.sliceImage.minWidth) {
            sliceEdit.sliceImage.setScaledWidth(sliceEdit.sliceImage.minWidth);
            sliceEdit.redrawSlice();
        }
        if (sliceEdit.height < sliceEdit.sliceImage.minHeight) {
            sliceEdit.sliceImage.setScaledHeight(sliceEdit.sliceImage.minHeight);
            sliceEdit.redrawSlice();
        }

        Screen.onWidthChanged.connect(checkWindowPosition);
        Screen.onHeightChanged.connect(checkWindowPosition);

        appdata.syncNavChanged.connect(synchronizeNav);
        appdata.loadingNewProject.connect(sliceWindow.destroy);
        appdata.addingLandmark.connect(enableAddLandmarkMode);
        appdata.landmarkCompleted.connect(clearAddLandmarkMode);
        appdata.landmarkSelectionChanged.connect(centerSelected);
        appdata.landmarkPredicted.connect(centerOnPredictedLandmark);
        sliceEdit.sliceImage.preRotationChanged.connect(updatePreRotationDial);
        sliceEdit.sliceImage.navChanged.connect(redrawNavAndFocus);
        sliceEdit.sliceImage.scaleChanged.connect(sliceEdit.sliceOverlay.setScaleAndOffset)
        sliceEdit.sliceImage.changeStackWindowPosition.connect(changeWindowPosition);
        sliceEdit.sliceImage.maxZChanged.connect(redrawNav);
        sliceEdit.sliceImage.systemChangedChannelList.connect(colorEditor.updateChannelDropdown);
        sliceEdit.sliceImage.channelToggled.connect(redrawNav);
        sliceEdit.sliceImage.userChangedChannelColor.connect(colorEditor.updateColorPickerButton);
        sliceEdit.sliceImage.userChangedChannelColor.connect(redrawNav);

        sliceEdit.sliceImage.initializeWindowLevelUi.connect(colorEditor.initializeWindowLevelSlider);
//        sliceEdit.sliceImage.systemChangedChannelWindowLevel.connect(colorEditor.updateSelectedChannelDetails);
//        sliceEdit.sliceImage.systemChangedChannelWindowLevel.connect(redrawNav);

        sliceEdit.sliceOverlay.setLandmarkSessionName(sliceEdit.sliceImage.getName());
        if (isRegistration) {
            sliceEdit.sliceImage.enableRegistrationMode();
            sliceEdit.sliceOverlay.toggleLandmarks();
            sliceEdit.sliceOverlay.setSessionName(".transformed.");
        }
        else if (isSparseAnnotation){
            sliceEdit.sliceOverlay.setSessionName(parent.sessionName)
            sliceEdit.brightnessContrastEffect.brightness = (typeof appdata.brightnesses[id] == 'undefined') ? 0 : appdata.brightnesses[id]
            sliceEdit.brightnessContrastEffect.contrast = (typeof appdata.contrasts[id] == 'undefined') ? 0 : appdata.contrasts[id]
        }else {
            sliceEdit.sliceOverlay.setSessionName(sliceEdit.sliceImage.getName());
        }

        //minInSlider.value = sliceEdit.sliceImage.getMaxColorValueF();
        isFocus = true;
        if(!isSparseAnnotation) focus = true;
        sessionName = sliceEdit.sliceImage.getName();

        // If this was a project loaded from a file the user probably changed the viewing configuration
        sliceEdit.sliceImage.loadWindowConfig();
        redrawNav(); // redraw the slice and overlay and make sure the labels, etc. on the gui are updated
    }

    Component.onDestruction: {
        appdata.syncNavChanged.disconnect(synchronizeNav);
        console.log(sessionName + " destroyed")
    }

    function synchronizeNav(z, t, zoom, offsetX, offsetY, mX, mY, zoomButton) {
        if (!sliceEdit.sliceImage) {
            return;
        }

        if (!sliceEdit.sliceImage.getSynced()) {
            console.log("ignoring sync signal");
            return;
        }
        sliceEdit.sliceImage.setSynced(false);
        if (z !== sliceEdit.sliceImage.getIndexZ()) {
            sliceEdit.sliceImage.gotoZ(z + 1);
            //redrawNav();
        }
        if (t !== sliceEdit.sliceImage.getIndexTime()) {
            sliceEdit.sliceImage.gotoTime(t + 1);
            //redrawNav();
        }
        if (zoom !== sliceEdit.sliceImage.zoomFactor) {
            sliceEdit.sliceImage.zoomFactor = zoom;
            //redrawNav();
        }

        if (mX !== sliceEdit.sliceImage.mouseX) {
            sliceEdit.sliceImage.mouseX = mX
        }

        if (mY !== sliceEdit.sliceImage.mouseY) {
            sliceEdit.sliceImage.mouseY = mY
        }

        if (zoomButton !== sliceEdit.sliceImage.zoomButton) {
            sliceEdit.sliceImage.zoomButton = zoomButton
        }

        sliceEdit.sliceImage.setOffsets(offsetX, offsetY);
        redrawNav();
        sliceEdit.sliceImage.setSynced(true);
    }

    function redrawNavAndFocus() {
        sliceWindow.z = ++highestZ;
        redrawNav();
    }

    function redrawNav() {
        sliceEdit.sliceImage.update();
        sliceEdit.sliceOverlay.changeSliceIndex(sliceEdit.sliceImage.getIndexZ());
        sliceEdit.sliceOverlay.update();
        positionText.text = sliceEdit.sliceImage.getIndexStr();
        zNavSlider.value = sliceEdit.sliceImage.getIndexZ() + 1
        tNavSlider.value = sliceEdit.sliceImage.getIndexTime() + 1
    }

    function addLandmark(x, y) {
        sliceEdit.sliceOverlay.addLandmark(x, y)
    }

    function clearModes() {
        editButton.clearEditMode();
        clearAddLandmarkMode();
    }

    function enableAddLandmarkMode() {
        sliceEdit.overlayMouseArea.cursorShape = Qt.CrossCursor;
        sliceEdit.sliceOverlay.enableAddLandmarkMode();
    }

    function clearAddLandmarkMode() {
        sliceEdit.overlayMouseArea.cursorShape = Qt.ArrowCursor;
        sliceEdit.sliceOverlay.disableAddLandmarkMode();
    }

    // move SliceWindow to the position it was lastly opened
    function changeWindowPosition(newX, newY) {
            x = newX;
            y = newY;
            checkWindowPosition();
    }

    function checkWindowPosition(){
        if (x >= Screen.width || y >= Screen.height || x < 0 ||y <0){
            x = 200;
            y = 10;
        }else{
            return;
        }
    }

    function updatePreRotationDial(val) {
        layerRotateDial.value = val;
    }

    function centerOnPredictedLandmark(n) {
//        console.log("Centering on predicted", n)
        sliceEdit.sliceOverlay.highlightLandmark(n);
        sliceEdit.sliceImage.gotoZ(sliceEdit.sliceOverlay.getLandmarkZ());
        redrawNav();
    }

    // pass coords of selected landmark or ROI for centering in zoomed slice
    function centerSelected(){

        if(!isRegistration){
            sliceEdit.sliceOverlay.highlightLandmark(appdata.landmarkModel.clickedIndex());
            sliceEdit.sliceImage.gotoZ(sliceEdit.sliceOverlay.getLandmarkZ());
            sliceEdit.sliceImage.loadCoords(sliceEdit.sliceOverlay.getLandmarkX(),
                                            sliceEdit.sliceOverlay.getLandmarkY(),
                                            sliceEdit.sliceOverlay.getLandmarkZ());
            sliceEdit.sliceImage.centerSelected = true;

        }else{

            sliceEdit.sliceOverlay.highlightLandmark(appdata.landmarkModel.clickedIndex());
            sliceEdit.sliceImage.gotoZ(sliceEdit.sliceOverlay.getLandmarkZ());
        }
        clearModes();
        redrawNav();
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        propagateComposedEvents: true
        acceptedButtons: Qt.AllButtons

        // don't catch hover over the whole rectangle as Qt doesn't propagate hover down the z stack

        drag.target: parent
        drag.axis: Drag.XAndYAxis
        onPressed: {
            // hack because QML for some reason won't send button info via onPositionChanged
            if (mouse.button & Qt.MiddleButton) {
                sliceEdit.overlayMouseArea.middleButton = true;
            } else {
                sliceEdit.overlayMouseArea.middleButton = false;
            }
            if (mouse.button & Qt.RightButton) {
                sliceEdit.overlayMouseArea.rightButton = true;
            } else {
                sliceEdit.overlayMouseArea.rightButton = false;
            }

            prevColor = parent.border.color;
            parent.border.color = focusColor;
            parent.anchors.centerIn = undefined;
            sliceWindow.z = ++highestZ;
        }
        onReleased: {
            parent.border.color = prevColor;
            parent.isFocus = true;
            if(!isSparseAnnotation)  parent.focus = true;
        }
        onPressAndHold: mouse.accepted = false
        onPositionChanged: {
            // QML won't propagate positionChanged, so we do it manually
            sliceEdit.overlayMouseArea.positionChanged(mouse);
            sliceEdit.sliceImage.windowPositionChanged(sliceWindow.x, sliceWindow.y);
        }
        onClicked: {
            if (mouse.button & Qt.RightButton) {
                mainIconMenu.popup();
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // layout

    Rectangle
    {
        id: sliceWindowContent
        color: parent.color
        border.width: 1
        border.color: Material.color(Material.Grey, Material.Shade900)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.border.width
        anchors.rightMargin: parent.border.width
        anchors.topMargin: parent.border.width
        anchors.bottomMargin: parent.border.width

        Column {
            anchors.fill: parent

            Rectangle {
                id: titlebar
                visible: !isSparseAnnotation
                height: titlebarunit
                width: parent.width
                color: "transparent"
                Image {
                    id: iconImg
                    source: { sliceEdit.sliceImage.isRef ? refStackIcon : stackIcon }
                    anchors.left: titlebar.left
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: titlebar.verticalCenter
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.AllButtons
                        propagateComposedEvents: true
                        onClicked: {
                            mainIconMenu.popup();
                        }
                    }
                }
                Menu {
                    id: mainIconMenu
                    Menu {
                        title: "Flags"
                        MenuItem {
                            text: "Delete Flagged"
                            onTriggered: {
                                appdata.deleteFlaggedRois(sliceEdit.sliceImage.getName());
                            }
                        }
                    }
                    MenuItem {
                        text: "Export Image..."
                        onTriggered: {
                            saveWindowFileDialog.fullWindow = true;
                            saveWindowFileDialog.open();
                        }
                    }
                    MenuItem {
                        text: "Export Image (Slice)..."
                        onTriggered: {
                            saveWindowFileDialog.fullWindow = false;
                            saveWindowFileDialog.open();
                        }
                    }
                }
                FileDialog {
                    id: saveWindowFileDialog
                    title: "Save Image As"
                    selectExisting: false
                    nameFilters: [ "PNG files (*.png)", "JPEG Files (*.jpg)", "All files (*)" ]
                    property bool fullWindow: true
                    onAccepted: {
                        //console.log("fileUrl:", fileUrl);
                        var urlNoProtocol = (fileUrl.toString()).replace('file:///', '');
                        console.log("urlNoProtocol:", urlNoProtocol);
                        if (fullWindow) {
                            sliceWindow.grabToImage(function(result) {
                                                      result.saveToFile(urlNoProtocol);
                                                  });
                        } else {
                            sliceEdit.saveToImage(urlNoProtocol);
                        }
                    }
                    onRejected: {
                        console.log("saveImageFileDialog file selection canceled");
                    }
                }

                Text {
                    id: titleText
                    maximumLineCount: 1
                    clip: true
                    //width: parent.width - closeButton.width - spaceunit
                    anchors.left: iconImg.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: titlebar.verticalCenter
                    text: isRegistration ? "Registration" : sliceEdit.sliceImage.getName()
                    color: "white"
                    font.bold: true
                    font.pointSize: 10
                }
                Text {
                    id: titleRoiText
                    maximumLineCount: 1
                    clip: true
                    anchors.left: titleText.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: titlebar.verticalCenter
                    text: sliceEdit.sliceOverlay.roiSelectionPretty
                    color: "white"
                    font.pointSize: 10
                }
                Text {
                    id: dimsText
                    maximumLineCount: 1
                    clip: true
                    //width: parent.width - closeButton.width - spaceunit
                    anchors.right: moveScreenButton.left
                    anchors.rightMargin: spaceunit
                    anchors.verticalCenter: titlebar.verticalCenter
                    text: sliceEdit.sliceImage.getMaxX() + "x" + sliceEdit.sliceImage.getMaxY() + "x" + sliceEdit.sliceImage.getMaxZ()
                    color: "light gray"
                    font.pointSize: 8
                }

                SmallButton {
                    id: moveScreenButton
                    icon: "qrc:/third-party/material-design-icons-master/hardware/1x_web/ic_desktop_windows_white_18dp.png"
                    anchors.right: minimizeButton.left
                    anchors.centerIn: titlebar.Center
                    onButtonClicked: {
                        moveToScreen(sliceWindow);
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Move to Next Screen")
                    }
                }
                SmallButton {
                    id: minimizeButton
                    icon: "qrc:/gui/icons/minimize_white_18dp.png"
                    anchors.right: closeButton.left
                    anchors.centerIn: titlebar.Center
                    onButtonClicked: {
                        sliceWindow.visible = false;
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Minimize")
                    }
                }
                SmallButton {
                    id: closeButton
                    icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_close_white_18dp.png"
                    anchors.right: titlebar.right
                    anchors.centerIn: titlebar.Center
                    onButtonClicked: {
                        if (!isRegistration) {
                            if (appWindow.registrationWindowExists())
                                appWindow.showErrorDialog("You can't delete a stack when a registration is in process. Close the registration window if you want to proceed.");
                            else {
                                appWindow.destroyStackWindow(id);
                                appdata.unloadStack(sliceEdit.sliceImage.getName());
                                appWindow.setMenuItemsForActiveProject();
                            }
                        } else {
                            appWindow.destroyRegistrationWindow();
                        }

                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Close Session (removes from project)")
                    }
                }
            }

            HorDivider {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
                visible: !isSparseAnnotation
            }

            Row {
                height: sliceEdit.height + spaceunit * 2
                width: parent.width
                anchors.left: if(isSparseAnnotation) parent.left
                anchors.leftMargin: if(isSparseAnnotation) spaceunit
                Rectangle {
                    id: navSide
                    width: 32
                    height: sliceEdit.height
                    color: "transparent"
                    visible: !isSparseAnnotation
                    //anchors.left: parent.left
                    //anchors.leftMargin: parent.border.width
                    //anchors.top: titlebar.bottom
                    //anchors.topMargin: spaceunit

                    Slider {
                        id: zNavSlider
                        orientation: Qt.Vertical
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: spaceunit
                        height: sliceEdit.sliceImage.height - prevZButton.height - playZButton.height - nextZButton.height - spaceunit * 3
                        value: 1
                        from: 1
                        to: sliceEdit.sliceImage.maxZ
                        live: true
                        stepSize: 1
                        snapMode: Slider.SnapAlways
                        enabled: { to > 1; }
                        onPressedChanged: {
                            if (!pressed) {
                                // just in case it didn't have time, make sure the resting slice is shown
                                redrawNav();
                            }
                        }
                        onMoved: {
                            //if (!pressed) {
                                sliceEdit.sliceImage.gotoZ(zNavSlider.value);
                                clearModes();
                                redrawNav();
                            //}
                        }
                    }
                    SmallButton {
                        id: nextZButton
                        anchors.top: zNavSlider.bottom
                        anchors.topMargin: spaceunit
                        anchors.horizontalCenter: parent.horizontalCenter
                        icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_expand_less_white_18dp.png"
                        onButtonClicked: {
                            sliceEdit.sliceImage.nextZ();
                            clearModes();
                            redrawNav();
                        }
                        ToolTip {
                            visible: parent.hovered
                            delay: toolTipDelay
                            timeout: toolTipTimeout
                            font.pointSize: toolTipFontSize
                            text: qsTr("Next Z")
                        }
                    }
                    SmallButton {
                        id: playZButton
                        anchors.top: nextZButton.bottom
                        anchors.topMargin: spaceunit
                        anchors.horizontalCenter: parent.horizontalCenter
                        icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_arrow_drop_up_white_18dp.png"
                        onButtonClicked: {
                            playZtimer.running = !playZtimer.running;
                        }
                        Timer {
                            id: playZtimer
                            interval: 42;
                            running: false;
                            repeat: true
                            onTriggered: {
                                if (sliceEdit.sliceImage.atMaxZ()) {
                                    sliceEdit.sliceImage.gotoZ(1);
                                } else {
                                    sliceEdit.sliceImage.nextZ();
                                }
                                clearModes();
                                redrawNav();
                            }
                        }
                        ToolTip {
                            visible: parent.hovered
                            delay: toolTipDelay
                            timeout: toolTipTimeout
                            font.pointSize: toolTipFontSize
                            text: qsTr("Play Z start/stop")
                        }
                    }
                    SmallButton {
                        id: prevZButton
                        anchors.top: playZButton.bottom
                        anchors.topMargin: spaceunit
                        anchors.horizontalCenter: parent.horizontalCenter
                        icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_expand_more_white_18dp.png"
                        onButtonClicked: {
                            sliceEdit.sliceImage.prevZ();
                            clearModes();
                            redrawNav();
                         }
                        ToolTip {
                            visible: parent.hovered
                            delay: toolTipDelay
                            timeout: toolTipTimeout
                            font.pointSize: toolTipFontSize
                            text: qsTr("Previous Z")
                        }
                    }
                }

                ///////////////////////////////////////////////////////////////////////////
                SliceEdit {
                    id: sliceEdit
                    anchors.top: parent.top
                    anchors.topMargin: spaceunit
                }
                ///////////////////////////////////////////////////////////////////////////

                // right side of sliceEdit
                Rectangle {
                    id: toolPanel
                    width: 73
                    height: sliceEdit.height + spaceunit * 2
                    anchors.leftMargin: 1
                    anchors.topMargin: spaceunit
                    anchors.bottomMargin: spaceunit
                    visible: !isSparseAnnotation
                    color: "transparent"

                    ColumnLayout {
                        spacing: 0

                        Text {
                            id: layersLabel
                            maximumLineCount: 1
                            clip: true
                            //anchors.horizontalCenter: parent.horizontalCenter
                            Layout.alignment: Qt.AlignHCenter
                            Layout.bottomMargin: 2
                            text: "Layers"
                            color: Material.color(Material.Grey)
                            font.pointSize: labelSize
                            //font.letterSpacing: 2
                            style: Text.Sunken; styleColor: Material.color(Material.Grey, Material.Shade800)
                        }
                        SmallButton {
                            id: layerRotateButton
                            text: qsTr("Rotate")
                            visible: !isRegistration
                            togglable: true
                            toggled: false
                            alwaysShowBorder: true
                            customWidth: toolPanel.width - spaceunit
                            onButtonClicked: {
                                sliceEdit.sliceOverlay.toggleShowRotation();
                                sliceEdit.sliceOverlay.update();
                            }
                        }
                        Button {
                               id: layerRotateResetButton
                               text: "Reset"
                               flat: true
                               Layout.preferredHeight: 30
                               font.pointSize: labelSize
                               visible: layerRotateButton.toggled && !isRegistration
                               onClicked: {
                                    layerRotateDial.value = sliceEdit.sliceOverlay.setPreRotation(0);
                                    redrawNav();
                               }
                        }
                        Dial {
                            id: layerRotateDial
                            visible: layerRotateButton.toggled && !isRegistration
                            value: 0
                            stepSize: 1
                            from: -180
                            to: 180
                            wrap: false
                            snapMode: Dial.SnapAlways
                            background: Rectangle {
                                x: layerRotateButton.width / 2 - width / 2 + 3
//                                y: layerRotateDial.height / 2 - height / 2 - 5
//                                anchors.top: layerRotateResetButton.bottom
                                width: layerRotateButton.width * 0.9
                                height: width
                                color: "transparent"
                                radius: width / 2
                                border.color: layerRotateDial.pressed ? "#17a81a" : "#21be2b"
                                opacity: layerRotateDial.enabled ? 1 : 0.3
                            }
                            onMoved: {
                                value = sliceEdit.sliceOverlay.setPreRotation(value);
                                redrawNav();
                            }

                        }
                        SmallButton {
                            id: layerRefButton
                            text: qsTr("Ref")
                            visible: !isRegistration
                            togglable: true
                            toggled: false
                            alwaysShowBorder: true
                            customWidth: toolPanel.width - spaceunit
                            onButtonClicked: {
                                console.log("Ref toggle");
                                sliceEdit.sliceOverlay.toggleShowReference();
                                sliceEdit.sliceOverlay.update();
                            }
                        }
                        TinyColorPicker {
                            id: refColorPicker
                            title: "Reference"
                            Layout.leftMargin: spaceunit
                            visible: layerRefButton.toggled && !isRegistration
                            currRed: sliceEdit.sliceOverlay.getRefMaskColor().r
                            currGreen: sliceEdit.sliceOverlay.getRefMaskColor().g
                            currBlue: sliceEdit.sliceOverlay.getRefMaskColor().b
                            currAlpha: sliceEdit.sliceOverlay.getRefMaskColor().a
                            onColorChanged: {
                                sliceEdit.sliceOverlay.setRefMaskColor(rgba);
                                sliceEdit.sliceOverlay.update();
                            }
                        }
                        SmallButton {
                            id: layerMasksButton
                            visible: true
                            togglable: true
                            toggled: true
                            alwaysShowBorder: true
                            customWidth: toolPanel.width - spaceunit
                            text: qsTr("Masks")
                            onButtonClicked: {
                                console.log("Mask toggle");
                                //down = !down;
                                sliceEdit.sliceOverlay.toggleShowMasks();
                                sliceEdit.sliceOverlay.update();
                            }
                        }

                        TinyColorPicker {
                            id: unconfirmedColorPicker
                            title: "Unconfirmed"
                            Layout.leftMargin: spaceunit
                            visible: !layerMasksButton.down && appdata.isProjectAnnotation()
                            currRed: sliceEdit.sliceOverlay.getUnconfirmedMaskColor().r
                            currGreen: sliceEdit.sliceOverlay.getUnconfirmedMaskColor().g
                            currBlue: sliceEdit.sliceOverlay.getUnconfirmedMaskColor().b
                            currAlpha: sliceEdit.sliceOverlay.getUnconfirmedMaskColor().a
                            onColorChanged: {
                                sliceEdit.sliceOverlay.setUnconfirmedMaskColor(rgba);
                                sliceEdit.sliceOverlay.update();
                            }
                        }
                        TinyColorPicker {
                            id: confirmedColorPicker
                            title: "Confirmed"
                            visible: !layerMasksButton.down && appdata.isProjectAnnotation()
                            currRed: sliceEdit.sliceOverlay.getConfirmedMaskColor().r
                            currGreen: sliceEdit.sliceOverlay.getConfirmedMaskColor().g
                            currBlue: sliceEdit.sliceOverlay.getConfirmedMaskColor().b
                            currAlpha: sliceEdit.sliceOverlay.getConfirmedMaskColor().a
                            onColorChanged: {
                                sliceEdit.sliceOverlay.setConfirmedMaskColor(rgba);
                                sliceEdit.sliceOverlay.update();
                            }
                        }
                        Rectangle {
                            Layout.topMargin: spaceunit
                            Layout.bottomMargin: spaceunit
                            height: toggleOutlinesButton.height
                            color: "transparent"
                            visible: !layerMasksButton.down

                            Row {
                                anchors.fill: parent

                                SmallButton {
                                    id: toggleOutlinesButton
                                    togglable: true
                                    icon: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_panorama_fish_eye_white_18dp.png"
                                    onButtonClicked: {
                                        sliceEdit.sliceOverlay.toggleShowMaskOutlines();
                                        sliceEdit.sliceOverlay.update();
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Outlining")
                                    }
                                }
                                SmallButton {
                                    id: showLabelsButton
                                    togglable: true
                                    icon : "qrc:/third-party/material-design-icons-master/action/1x_web/ic_label_outline_white_18dp.png"
                                    onButtonClicked: {
                                        sliceEdit.sliceOverlay.toggleShowLabels();
                                        sliceEdit.sliceOverlay.update();
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Labels")
                                    }
                                }
                                SmallButton {
                                    id: showCentroidsButton
                                    togglable: true
                                    icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_filter_tilt_shift_white_18dp.png"
                                    onButtonClicked: {
                                        sliceEdit.sliceOverlay.toggleShowCentroids();
                                        sliceEdit.sliceOverlay.update();
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Centroid Crosshairs")
                                    }
                                }
                            }
                        }

                        Rectangle {
                            Layout.topMargin: spaceunit
                            Layout.bottomMargin: spaceunit
                            height: toggleOutlinesButton.height
                            color: "transparent"
                            visible: !layerMasksButton.down

                            Row {
                                anchors.fill: parent

                                SmallButton {
                                    id: toggleLinkedButton
                                    togglable: true
                                    toggled: true
                                    icon: linkIcon
                                    anchors.leftMargin: spaceunit
                                    onButtonClicked: {
                                        sliceEdit.sliceOverlay.toggleShowConfirmed();
                                        sliceEdit.sliceOverlay.update();
                                        if (!toggled) {
                                            toggleFullyLinkedButton.forceToggle(false);
                                        }
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Linked")
                                    }
                                }
                                SmallButton {
                                    id: toggleFullyLinkedButton
                                    togglable: true
                                    toggled: true
                                    icon: confirmedIcon
                                    visible: sliceEdit.sliceImage.isRef
                                    anchors.leftMargin: spaceunit
                                    onButtonClicked: {
                                        if (toggled) {
                                            toggleLinkedButton.forceToggle(true);
                                        }
                                        sliceEdit.sliceOverlay.toggleShowFullyLinked();
                                        sliceEdit.sliceOverlay.update();
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Fully-Linked")
                                    }
                                }
                            }
                        }
                        Rectangle {
                            id: filterFlagButtons
                            Layout.topMargin: spaceunit
                            Layout.bottomMargin: spaceunit
                            height: toggleOutlinesButton.height
                            color: "transparent"
                            visible: !layerMasksButton.down

                            Row {
                                anchors.fill: parent

                                SmallButton {
                                    id: toggleFilteredButton
                                    togglable: true
                                    toggled: false
                                    icon: filterIcon
                                    anchors.leftMargin: spaceunit
                                    onButtonClicked: {
                                        sliceEdit.sliceOverlay.toggleShowFiltered();
                                        sliceEdit.sliceOverlay.update();
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Filters")
                                    }
                                }
                                SmallButton {
                                    id: toggleFlaggedButton
                                    togglable: true
                                    toggled: true
                                    icon: flagIcon
                                    anchors.leftMargin: spaceunit
                                    onButtonClicked: {
                                        sliceEdit.sliceOverlay.toggleShowFlagged();
                                        sliceEdit.sliceOverlay.update();
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Flagged")
                                    }
                                }
                            }
                        }

                        Rectangle {
                            Layout.topMargin: 2
                            Layout.bottomMargin: spaceunit
                            Layout.leftMargin: spaceunit
                            Layout.rightMargin: 2
                            height: smallButtonSize * 3 + spaceunit * 2
                            width: toolPanel.width - spaceunit - 2
                            color: Material.color(Material.Grey, Material.Shade900)
                            border.width: 1
                            border.color: Material.color(Material.Grey, Material.Shade700)
                            visible: toggleFilteredButton.toggled

                            Column {
                                id: clfilter
                                anchors.fill: parent

                                Text {
                                    maximumLineCount: 1
                                    clip: true
                                    anchors.left: parent.left
                                    anchors.leftMargin: 2
                                    anchors.bottomMargin: 2
                                    text: "Radius"
                                    color: Material.color(Material.Grey)
                                    font.pointSize: labelSize
                                }

                                SmallSpinBox {
                                    id: radiusInput
                                    value: 1
                                    from: 0
                                    to: 1000
                                    anchors.left: parent.left
                                    anchors.leftMargin: 2
                                    height: 30
                                    width: toolPanel.width - spaceunit * 3
//                                    onValueModified: {
//                                        appdata.filterHighPassRadius(sliceEdit.sliceImage.getName(), value);
//                                    }
                                }

                                SmallButton {
                                    id: flagFilteredButton
                                    //icon: flagIcon
                                    text: "Flag"
                                    customWidth: smallButtonSize * 2
                                    anchors.left: parent.left
                                    anchors.leftMargin: 2
                                    onButtonClicked: {
                                        console.log("Flag Filtered clicked");
                                        appdata.flagFiltered(sliceEdit.sliceImage.getName());
                                    }
                                    ToolTip {
                                        visible: parent.hovered
                                        delay: toolTipDelay
                                        timeout: toolTipTimeout
                                        font.pointSize: toolTipFontSize
                                        text: qsTr("Flag all filtered ROIs")
                                    }
                                }
                            }
                        }

//                        SmallButton {
//                            id: layerRButton
//                            text: qsTr("R")
//                            togglable: true
//                            toggled: true
//                            alwaysShowBorder: true
//                            customWidth: toolPanel.width - spaceunit
//                            onButtonClicked: {
//                                if (toggled) {
//                                    sliceEdit.rgbEffect.redChannel = 1.0;
//                                } else {
//                                    sliceEdit.rgbEffect.redChannel = 0.0;
//                                }
//                                sliceEdit.rgbChannelCompensate();
//                            }
//                        }
//                        SmallButton {
//                            id: layerGButton
//                            text: qsTr("G")
//                            togglable: true
//                            toggled: true
//                            alwaysShowBorder: true
//                            customWidth: toolPanel.width - spaceunit
//                            onButtonClicked: {
//                                if (toggled) {
//                                    sliceEdit.rgbEffect.greenChannel = 1.0;
//                                } else {
//                                    sliceEdit.rgbEffect.greenChannel = 0.0;
//                                }
//                                sliceEdit.rgbChannelCompensate();
//                            }
//                        }
//                        SmallButton {
//                            id: layerBButton
//                            text: qsTr("B")
//                            togglable: true
//                            toggled: true
//                            alwaysShowBorder: true
//                            customWidth: toolPanel.width - spaceunit
//                            onButtonClicked: {
//                                if (toggled) {
//                                    sliceEdit.rgbEffect.blueChannel = 1.0;
//                                } else {
//                                    sliceEdit.rgbEffect.blueChannel = 0.0;
//                                }
//                                sliceEdit.rgbChannelCompensate();
//                            }
//                        }

                    }

                    Rectangle {
                        width: parent.width
                        color: "transparent"
                        visible: !isRegistration && appdata.projectMode == "registration"
                        anchors.bottom: parent.bottom
                        height: 2 * (smallButtonSize + spaceunit) + landmarksLabel.height + spaceunit * 2

                        Text {
                            id: landmarksLabel
                            maximumLineCount: 1
                            clip: true
                            text: "Landmarks"
                            color: Material.color(Material.Grey)
                            font.pointSize: labelSize
                            style: Text.Sunken; styleColor: Material.color(Material.Grey, Material.Shade800)
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        Row {
                            id: landmarkTools1
                            anchors.top: landmarksLabel.bottom
                            anchors.topMargin: spaceunit

                            SmallButton {
                                id: addLandmarkButton
                                icon : "qrc:/third-party/material-design-icons-master/content/1x_web/ic_add_white_18dp.png"

                                onButtonClicked: {
                                    appdata.broadcastAddingLandmark();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Add Landmark")
                                }
                            }

                            SmallButton {
                                id: showLandmarksButton
                                togglable: true
                                toggled: true
                                icon : "qrc:/third-party/material-design-icons-master/editor/1x_web/ic_border_clear_white_18dp.png"

                                onToggledChanged: {
                                    sliceEdit.sliceOverlay.toggleLandmarks();
                                    sliceEdit.sliceOverlay.update();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Show Landmarks")
                                }
                            }

                            SmallButton {
                                id: showLandmarkLabelsButton
                                togglable: true
                                toggled: true
                                icon : "qrc:/third-party/material-design-icons-master/action/1x_web/ic_label_outline_white_18dp.png"
                                onButtonClicked: {
                                    sliceEdit.sliceOverlay.toggleLandmarkLabels();
                                    sliceEdit.sliceOverlay.update();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Show Landmark Labels")
                                }
                            }
                        }
                        Row {
                            anchors.top: landmarkTools1.bottom
                            anchors.topMargin: spaceunit
                            visible: false /* Disabled for now */

                            SmallButton {
                                id: enableRegistrationButton
                                togglable: true
                                toggled: false
                                icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_crop_rotate_white_18dp.png"
                                onToggledChanged: {
                                    sliceEdit.sliceImage.registrationEnabled = toggled;
                                    sliceEdit.sliceImage.update();
                                    sliceEdit.sliceOverlay.update();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Enable Registration")
                                }
                            }
                        }
                    }

                    /// tools--lower part
                    Rectangle {
                        id: roiToolPanel
                        color: "transparent"
                        visible: !isRegistration && appdata.isProjectAnnotation()
                        width: parent.width
                        height: 2 * (smallButtonSize + spaceunit) + roiLabel.height + spaceunit * 2

                        anchors.left: parent.left
                        //anchors.right: sliceEditContent.right
                        anchors.bottom: parent.bottom
                        //anchors.leftMargin: spaceunit
                        //anchors.rightMargin: spaceunit
                        //anchors.topMargin: spaceunit
                        anchors.bottomMargin: spaceunit

                        border.width: 0

                        Text {
                            id: roiLabel
                            maximumLineCount: 1
                            clip: true
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.top
                            anchors.topMargin: spaceunit
                            text: "Tools"
                            color: Material.color(Material.Grey)
                            font.pointSize: labelSize
                            style: Text.Sunken; styleColor: Material.color(Material.Grey, Material.Shade800)
                        }

                        SmallButton {
                            id: editButton
                            greyedOut: !sliceEdit.sliceOverlay.selectionExists
                            togglable: true
                            anchors.left: parent.left
                            anchors.top: roiLabel.bottom
                            anchors.leftMargin: 1
                            anchors.topMargin: spaceunit
                            icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_edit_white_18dp.png"
                            onToggledChanged: {
                                console.log("edit changed", toggled);
                                //appdata.clearLinkState(); // cancel link op if user started one
                                sliceEdit.sliceOverlay.toggleEditMode();
                                if (toggled) {
                                    addLandmarkButton.forceToggle(false);
                                    sliceEdit.overlayMouseArea.cursorShape = Qt.DragMoveCursor;
                                } else {
                                    sliceEdit.overlayMouseArea.cursorShape = Qt.ArrowCursor;
                                }
                            }
                            function clearEditMode() {
                                editButton.forceToggle(false);
                                sliceEdit.overlayMouseArea.cursorShape = Qt.ArrowCursor;
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Edit Mask")
                            }
                        }
                        SmallButton {
                            id: mergeButton
                            greyedOut: !sliceEdit.sliceOverlay.multiSelectionExists
                            anchors.left: editButton.right
                            anchors.top: roiLabel.bottom
                            anchors.topMargin: spaceunit
                            icon : "qrc:/third-party/material-design-icons-master/editor/1x_web/ic_merge_type_white_18dp.png"
                            onButtonClicked: {
                                sliceEdit.sliceOverlay.mergeSelected();
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Merge")
                            }
                        }
                        SmallButton {
                            id: splitButton
                            greyedOut: !sliceEdit.sliceOverlay.selectionExists
                            anchors.left: parent.left
                            anchors.top: editButton.bottom
                            anchors.leftMargin: 0
                            anchors.topMargin: 2
                            icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_broken_image_white_18dp.png"
                            onButtonClicked: {
                                //console.log("sliceEdit.sliceImage.getIndexZ():" + sliceEdit.sliceImage.getIndexZ());
                                sliceEdit.state = "split";
                                appdata.splitRoi(sliceEdit.sliceImage.getName(), sliceEdit.sliceOverlay.roiSelection, sliceEdit.sliceImage.getMaxZ());
                                redrawNav();
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Split")
                            }
                        }
                        SmallButton {
                            id: flagButton
                            greyedOut: !sliceEdit.sliceOverlay.selectionExists
                            icon: flagIcon
                            anchors.left: splitButton.right
                            anchors.top: mergeButton.bottom
                            anchors.leftMargin: 0
                            anchors.topMargin: 2
                            onButtonClicked: {
                                appdata.toggleFlag(sliceEdit.sliceImage.getName(), sliceEdit.sliceOverlay.roiSelection);
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Flag/Unflag")
                            }
                        }
                        SmallButton {
                            id: deleteButton
                            greyedOut: !sliceEdit.sliceOverlay.selectionExists
                            anchors.left: flagButton.right
                            anchors.top: mergeButton.bottom
                            anchors.leftMargin: 0
                            anchors.topMargin: 2
                            icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_cancel_white_18dp.png"
                            onButtonClicked: {
                                appdata.deleteRoi(sliceEdit.sliceImage.getName(), sliceEdit.sliceOverlay.roiSelection);
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Delete Mask")
                            }
                        }
                    }

                }
            }

            Rectangle {
                id: navLower
                visible: !isSparseAnnotation
                color: "transparent"
                width: parent.width
                height: 32

                Text {
                    id: positionText
                    width: 125
                    maximumLineCount: 1
                    clip: true
                    anchors.left: parent.left
                    anchors.leftMargin: spaceunit + 2
                    anchors.verticalCenter: parent.verticalCenter
                    text: sliceEdit.sliceImage.getIndexStr()
                    color: "light gray"
                    font.pointSize: labelSize
                    font.letterSpacing: 2
                }
                SmallButton {
                    id: zoomLabelButton
                    customWidth: 50
                    text: sliceEdit.sliceImage.zoomFactor + "%"
                    anchors.left: positionText.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: parent.verticalCenter
                    onButtonClicked: {
                        sliceEdit.sliceImage.resetZoom();
                        sliceEdit.redrawNav();
                    }
                }
                SmallButton {
                    id: zoomOutButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_zoom_out_white_18dp.png"
                    anchors.left: zoomLabelButton.right
                    anchors.verticalCenter: parent.verticalCenter
                    onButtonClicked: {
                        sliceEdit.sliceImage.zoomButton = true;
                        sliceEdit.sliceImage.zoomFactor -= 25;
                        sliceEdit.redrawSlice();
                    }
                }
                SmallButton {
                    id: zoomInButton
                    icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_zoom_in_white_18dp.png"
                    anchors.left: zoomOutButton.right
                    anchors.verticalCenter: parent.verticalCenter
                    onButtonClicked: {
                        sliceEdit.sliceImage.zoomButton = true;
                        sliceEdit.sliceImage.zoomFactor += 25;
                        sliceEdit.redrawSlice();
                    }
                }
                SmallButton {
                    id: prevTButton
                    icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_navigate_before_white_18dp.png"
                    anchors.left: zoomInButton.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: parent.verticalCenter
                    greyedOut: true
                    onButtonClicked: {
                        sliceEdit.sliceImage.prevTime();
                        redrawNav();
                        //sliceEdit.sliceImage.update();
                        //tNavSlider.value = sliceEdit.sliceImage.getIndexTime() + 1
                        //positionText.text = sliceEdit.sliceImage.getIndexStr();
                    }
                }
                SmallButton {
                    id: playTButton
                    anchors.left: prevTButton.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: parent.verticalCenter
                    icon : "qrc:/third-party/material-design-icons-master/av/drawable-mdpi/ic_play_arrow_white_18dp.png"
                    greyedOut: true
                }
                SmallButton {
                    id: nextTButton
                    anchors.left: playTButton.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: parent.verticalCenter
                    icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_navigate_next_white_18dp.png"
                    greyedOut: true
                    onButtonClicked: {
                        sliceEdit.sliceImage.nextTime();
                        redrawNav();
                        //sliceEdit.sliceImage.update();
                        //tNavSlider.value = sliceEdit.sliceImage.getIndexTime() + 1
                        //positionText.text = sliceEdit.sliceImage.getIndexStr();
                    }
                }
                Slider {
                    id: tNavSlider
                    orientation: Qt.Horizontal
                    anchors.left: nextTButton.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width - smallButtonSize * 6 - positionText.width - zoomLabelButton.width - syncSwitch.width - spaceunit * 10
                    value: 1
                    from: 1
                    to: sliceEdit.sliceImage.getMaxT()
                    live: false
                    stepSize: 1
                    snapMode: Slider.SnapAlways
                    enabled: { to > 1; }
                }
                Switch {
                    id: syncSwitch
                    text: "Sync"
                    visible: !isSparseAnnotation
                    anchors.left: tNavSlider.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: parent.verticalCenter
                    contentItem: Text {
                        text: syncSwitch.text
                        font.pointSize: labelSize
                        opacity: enabled ? 1.0 : 0.3
                        color: "light gray"
                        elide: Text.ElideRight
                        rightPadding: syncSwitch.indicator.width + syncSwitch.spacing
                        verticalAlignment: Text.AlignVCenter
                    }
                    indicator: Rectangle {
                        implicitWidth: 48
                        implicitHeight: 24
                        x: syncSwitch.width - width - syncSwitch.padding
                        y: parent.height / 2 - height / 2
                        radius: 12
                        color: syncSwitch.down ? "light gray" : "gray"
                        border.color: syncSwitch.down ? "gray" : "dark gray"

                        Rectangle {
                            x: syncSwitch.checked ? parent.width - width : 0
                            width: 24
                            height: 24
                            radius: 12
                            opacity: 0.8
                            color: syncSwitch.down ? (syncSwitch.checked ? "red" : "orange") : (syncSwitch.checked ? "red" : "light gray")
                            border.color: syncSwitch.checked ? "pink" : "dark gray"
                        }
                    }
                    onToggled: {
                        if (syncSwitch.checked) {
                            sliceWindow.border.color = syncColor
                            isSynced = true;
                            sliceEdit.sliceImage.setSynced(true);
                        } else {
                            sliceWindow.border.color = focusColor
                            isSynced = false;
                            sliceEdit.sliceImage.setSynced(false);
                        }
                    }
                }
                SmallButton {
                    id: foldFxPanelButton
                    icon: "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_unfold_less_white_18dp.png"
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.rightMargin: spaceunit
                    onButtonClicked: {
                        fxPanel.visible = !fxPanel.visible;
                        if (!fxPanel.visible) {
                            guiHeight -= fxPanel.height;
                            icon = "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_unfold_more_white_18dp.png";
                        } else {
                            icon = "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_unfold_less_white_18dp.png";
                            guiHeight += fxPanel.height;
                        }
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Hide/Show Effects Panel")
                    }
                }
            }

            ColorEditor {
                id: colorEditor
                visible: !isSparseAnnotation
                width: parent.width
            }

            Rectangle {
                id: fxPanel
                width: parent.width
                height: 86 + (isRegistration ? barunit2 : 0)
                color: "transparent"

                HorDivider {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.leftMargin: spaceunit
                    anchors.rightMargin: spaceunit
                }

                ScrollView {
                    id: fxScrollView
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: spaceunit * 2
                    anchors.rightMargin: spaceunit * 2
                    anchors.bottomMargin: 1
                    contentWidth: ColorEditor.width-20
                    contentHeight: barunit2 * 2
                    clip: true
                    //focusPolicy: Qt.WheelFocus
                    Component.onCompleted: contentItem.interactive = false
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.AllButtons
                        propagateComposedEvents: true
                        onClicked: {
                            if (mouse.button & Qt.RightButton) {
                                fxPanelContextMenu.popup();
                            }
                        }
                    }

                    RowLayout {
                        id: regAdjustPanel
                        height: barunit2
                        visible: isRegistration
                        spacing: 0
                        anchors.leftMargin: spaceunit
                        anchors.rightMargin: spaceunit
                        anchors.bottomMargin: spaceunit
                        clip: true

                        Text {
                            id: fadeLabel
                            text: "Fade"
                            maximumLineCount: 1
                            clip: true
                            Layout.alignment: Qt.AlignVCenter
                            color: Material.color(Material.Grey)
                            font.pointSize: labelSize
                            MouseArea {
                                anchors.fill: fadeLabel
                                propagateComposedEvents: true
                                onDoubleClicked: {
                                    fadeSlider.value = 0.0;
                                    fadeSlider.onMoved();
                                    mouse.accepted = false;
                                }
                                onPressAndHold: mouse.accepted = false
                                onPressed: mouse.accepted = false
                                onReleased: mouse.accepted = false
                            }
                        }
                        Slider {
                            id: fadeSlider
                            orientation: Qt.Horizontal
                            Layout.alignment: Qt.AlignVCenter
                            value: sliceEdit.sliceImage.fadeRegistered
                            from: 0.0
                            to: 1.0
                            stepSize: 0.05
                            snapMode: Slider.SnapAlways
                            onMoved: {
                                sliceEdit.sliceImage.fadeRegistered = fadeSlider.value;
                                sliceEdit.sliceImage.update();
                            }
                        }
                    }


//                    RowLayout {
//                        id: imageAdjustPanel
//                        width: ColorEditor.width
//                        height: barunit2
//                        spacing: 0
//                        anchors.top: isRegistration ? regAdjustPanel.bottom : parent.top
//                        anchors.leftMargin: spaceunit
//                        anchors.rightMargin: spaceunit
//                        clip: true

////                        SmallButton {
////                            id: histEqButton
////                            customWidth: 50
////                            Layout.alignment: Qt.AlignVCenter
////                            text: "HistEq"
////                            togglable: true
////                            toggled: false
////                            onButtonClicked: {
////                                sliceEdit.sliceImage.histEq = histEqButton.toggled;
////                                if (histEqButton.toggled) {
//////                                    minInSlider.value = 0.0;
//////                                    minInSlider.onMoved();
//////                                    maxInSlider.value = 1.0;
//////                                    maxInSlider.onMoved();
////                                }
////                                sliceEdit.sliceImage.update();
////                            }
////                        }
//                    }
//                    Menu {
//                        id: fxPanelContextMenu
//                        MenuItem {
//                            text: "Defaults"
//                            onTriggered: {
//                                console.log("Reset effects");
//                                histEqButton.forceToggle(false);
//                                sliceEdit.sliceImage.histEq = false;
//                                sliceEdit.sliceImage.update();
//                                brightSlider.value = 0.0;
//                                brightSlider.onMoved();
//                                contrastSlider.value = 0.0;
//                                contrastSlider.onMoved();
////                                desaturateSlider.value = 1.0;
////                                desaturateSlider.onMoved();
//                            }
//                        }
//                    }
                    RowLayout {
                        id: imageAdjustPanel2
                        anchors.top: isRegistration ? regAdjustPanel.bottom : parent.top
                        width: isSparseAnnotation ? fxScrollView.width - 20 : ColorEditor.width-20
                        height: if(!isSparseAnnotation) barunit2
                        spacing: 0
                        anchors.leftMargin: spaceunit
                        anchors.rightMargin: spaceunit
                        //anchors.topMargin: spaceunit
                        anchors.bottomMargin: spaceunit
                        clip: true

                        Text {
                            id: brightLabel
                            text: "Brightness"
                            maximumLineCount: 1
                            clip: true
                            Layout.alignment: Qt.AlignVCenter
                            color: Material.color(Material.Grey)
                            font.pointSize: labelSize
                            MouseArea {
                                anchors.fill: brightLabel
                                propagateComposedEvents: true
                                onDoubleClicked: {
                                    brightSlider.value = 0.0;
                                    brightSlider.onMoved();
                                    mouse.accepted = false;
                                }
                                onPressAndHold: mouse.accepted = false
                                onPressed: mouse.accepted = false
                                onReleased: mouse.accepted = false
                            }
                        }
                        Slider {
                            id: brightSlider
                            orientation: Qt.Horizontal
//                            anchors.left: brightLabel.right
                            Layout.alignment: Qt.AlignVCenter
                            Layout.minimumWidth: 100
                            Layout.preferredWidth: if (isSparseAnnotation) (parent.width - brightLabel.width - contrastLabel.width)/2
                            value: isSparseAnnotation ? appdata.brightnesses[id] : 0
                            from: -1.0
                            to: 1.0
                            stepSize: 0.01
                            snapMode: Slider.SnapAlways
                            onMoved: {
                                sliceEdit.brightnessContrastEffect.brightness = value
                                appdata.brightnesses[id] = value
                            }
                        }
                        Text {
                            id: contrastLabel
                            text: "Contrast"
                            maximumLineCount: 1
                            clip: true
//                            anchors.left: brightSlider.right
                            Layout.alignment: Qt.AlignVCenter
                            color: Material.color(Material.Grey)
                            font.pointSize: labelSize
                            MouseArea {
                                anchors.fill: contrastLabel
                                propagateComposedEvents: true
                                onDoubleClicked: {
                                    contrastSlider.value = 0.0;
                                    contrastSlider.onMoved();
                                    mouse.accepted = false;
                                }
                                onPressAndHold: mouse.accepted = false
                                onPressed: mouse.accepted = false
                                onReleased: mouse.accepted = false
                            }
                        }
                        Slider {
                            id: contrastSlider
                            orientation: Qt.Horizontal
//                            anchors.left: contrastLabel.right
                            Layout.alignment: Qt.AlignVCenter
                            Layout.minimumWidth: 100
                            Layout.preferredWidth: if (isSparseAnnotation) (parent.width - brightLabel.width - contrastLabel.width)/2
                            value: isSparseAnnotation ? appdata.contrasts[id] : 0
                            from: -1.0
                            to: 1.0
                            stepSize: 0.01
                            snapMode: Slider.SnapAlways
                            onMoved: {
                                sliceEdit.brightnessContrastEffect.contrast = value
                                appdata.contrasts[id] = value
                            }
                        }
//                        Text {
//                            id: desaturateLabel
//                            text: "Desaturate"
//                            maximumLineCount: 1
//                            clip: true
//                            anchors.left: contrastSlider.right
//                            Layout.alignment: Qt.AlignVCenter
//                            color: Material.color(Material.Grey)
//                            font.pointSize: labelSize
//                            MouseArea {
//                                anchors.fill: desaturateLabel
//                                propagateComposedEvents: true
//                                onDoubleClicked: {
//                                    desaturateSlider.value = 1.0;
//                                    desaturateSlider.onMoved();
//                                    mouse.accepted = false;
//                                }
//                                onPressAndHold: mouse.accepted = false
//                                onPressed: mouse.accepted = false
//                                onReleased: mouse.accepted = false
//                            }
//                        }
//                        Slider {
//                            id: desaturateSlider
//                            orientation: Qt.Horizontal
//                            Layout.alignment: Qt.AlignVCenter
//                            Layout.minimumWidth: 150
//                            anchors.left: desaturateLabel.right
//                            from: 1.0
//                            to: 0.0
//                            stepSize: 0.1
//                            value: 1.0
//                            snapMode: Slider.SnapAlways
//                            onMoved: {
//                                sliceEdit.rgbEffect.greyAmount = value;
//                                sliceEdit.colorizeEffect.visible = false;
//                            }
//                        }
                    }
                }
            }
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////
    // resizing grips

    // left side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: -2
        width: spaceunit + 2
        enabled: !isSparseAnnotation
        MouseArea {
            id: leftGrip
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = sliceEdit.sliceImage.width - delta;
                    var newX = sliceWindow.x + delta;
                    if (newWidth < sliceEdit.sliceImage.minWidth || newX < 0) {
                        return;
                    }
                    sliceWindow.x = newX;
                    //sliceEdit.sliceImage.width = newWidth;
                    sliceEdit.sliceImage.setScaledWidth(newWidth);
                    sliceEdit.redrawSlice();
                }
            }
        }
    }

    // right side resize grip
    Rectangle {
        color: "transparent"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: -2
        width: spaceunit + 2
        enabled: !isSparseAnnotation
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = sliceEdit.sliceImage.width + delta;
                    if (newWidth < sliceEdit.sliceImage.minWidth) {
                        return;
                    }
                    //sliceEdit.sliceImage.width = newWidth;
                    sliceEdit.sliceImage.setScaledWidth(newWidth);
                    sliceEdit.redrawSlice();
                }
            }
        }
    }

    // top side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: -2
        height: spaceunit + 2
        enabled: !isSparseAnnotation
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = sliceEdit.sliceImage.height - delta;
                    var newY = sliceWindow.y + delta;
                    if (newHeight < sliceEdit.sliceImage.minHeight || newY < 0) {
                        return;
                    }
                    sliceWindow.y = newY;
                    //sliceEdit.sliceImage.height = newHeight;
                    sliceEdit.sliceImage.setScaledHeight(newHeight);
                    sliceEdit.redrawSlice();
                }
            }
        }
    }

    // bottom side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -2
        height: spaceunit + 2
        enabled: !isSparseAnnotation
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = sliceEdit.sliceImage.height + delta;
                    if (newHeight < sliceEdit.sliceImage.minHeight) {
                        return;
                    }
                    //sliceEdit.sliceImage.height = newHeight;
                    sliceEdit.sliceImage.setScaledHeight(newHeight);
                    sliceEdit.redrawSlice();
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
}
