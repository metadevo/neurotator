/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3

Rectangle {
    id: projectWizard
    //visible: false
    anchors.fill: parent
    //color: Material.color(Material.Grey, Material.Shade700)
    gradient: Gradient {
        GradientStop { position: 1.0; color: Material.color(Material.Grey, Material.Shade500) }
        GradientStop { position: 0.0; color: Material.color(Material.Grey, Material.ShadeA100) }
    }

    signal newProject(url projectUrl, string projectName, bool registration)

    Text {
        text: "Neurotator"
        font.bold: true
        font.pointSize: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        color: lightOrangeColor
    }

    Pane {
        width: 1000
        height: 800

        Material.elevation: 6

        anchors.centerIn: parent

        ColumnLayout {
            anchors.fill: parent
            RowLayout {
                Layout.fillWidth: true


                Rectangle {
                    //spacer
                    color: "transparent"
                    width: 100
                }
                Row {
                    spacing: 8
                    Column {
                        spacing: 2
                        Label {
                            text: qsTr("New Project")
                            color: lightOrangeColor
                        }
                        Item {
                            id: inputItem
                            width: 500
                            height: 30

                            Rectangle {
                                anchors.fill: parent
                                color: Material.color(Material.Grey, Material.Shade900)
                                border.color: projectNameInput.text === "" ? "red" : Material.color(Material.Grey, Material.Shade700)
                            }

                            TextInput {
                                id: projectNameInput
                                text: "name"
                                width: inputItem.width
                                anchors.margins: 2
                                anchors.verticalCenter: parent.verticalCenter
                                padding: 2
                                color: "white"
                                font.pointSize: 12
                                clip: true
                                selectByMouse: true
                                onEditingFinished: {
                                    projectNameLabel.visible = true;
                                    appdata.projectName = text;
                                    refreshRecentsMenu();
                                }
                            }
                        }
                        Item {
                            //Layout.preferredWidth: 144
                            width: 500
                            height: smallButtonSize
                            property bool userChosen: false

                            Rectangle {
                                anchors.fill: parent
                                anchors.rightMargin: smallButtonSize
                                color: Material.color(Material.Grey, Material.Shade900)
                                border.color: Material.color(Material.Grey, Material.Shade700)
                            }

                            TextInput {
                                id: projectUrlInput
                                text: appdata.getRecentProjectDir() + "/" + projectNameInput.text + ".json"
                                readOnly: true
                                anchors.margins: 2
                                anchors.verticalCenter: parent.verticalCenter
                                padding: 2
                                color: "light gray"
                                font.pointSize: 10
                                clip: true
                                selectByMouse: true
                                onEditingFinished: {
                                    projectNameLabel.visible = true;
                                    appdata.projectName = text;
                                    refreshRecentsMenu();
                                }
                            }
                            SmallButton {
                                id: changePathButton
                                anchors.right: parent.right
                                anchors.rightMargin: 0

                                icon: "qrc:/third-party/material-design-icons-master/file/1x_web/ic_folder_open_white_18dp.png"
                                onButtonClicked: {
                                    projectSaveFileDialog.setFolder(appdata.getRecentProjectDir());
                                    projectSaveFileDialog.open();
                                }
                            }
                        }
                    }
                    Column {
                        RadioButton {
                            id: registrationButton
                            text: qsTr("Registration")
                            checked: true
                        }
                        RadioButton {
                            text: qsTr("Annotation")
                        }
                    }
                    Button {
                        id: newButton
                        text: qsTr("Create")
//                        anchors.topMargin: 20
//                        anchors.bottom: parent.bottom
                        anchors.verticalCenter: parent.verticalCenter

                        onClicked: {
                            //fileDialog.open();
                            var projectUrl = "NewProject.json";
                            projectWizard.newProject(projectUrlInput.text, projectNameInput.text,
                                                     registrationButton.checked)
                        }
                    }
                }
            }
        }
    }

    FileDialog {
        id: projectSaveFileDialog
        title: "New Project"
        selectExisting: false
        nameFilters: [ "Project files (*.json)", "All files (*)" ]
        onAccepted: {
            appdata.setRecentProjectDir(projectSaveFileDialog.folder);
            projectUrlInput.text = projectSaveFileDialog.fileUrl;
        }
        onRejected: {
            console.log("Project file selection canceled")
        }
    }

}
