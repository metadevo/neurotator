import QtQuick 2.11
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.3
import com.bu 1.0

Rectangle {
    id: sparseGroupWindow
    property string sessionName: ""
    property string roiEditMode: "In Vivo Link"
    property var stacksMatrix: []
    property var brightnesses: []
    property var contrasts: []
    property var firstWindow: null
    property int numStacks: 1
    property int gridCols: 1
    property int subWidth: 0
    property int zThick: 0
    property color borderColor: "light gray"
    property color prevColor: borderColor
    property color syncColor: "red"
    property color focusColor: "lightblue"

    color: Material.color(Material.Grey, Material.Shade800)
    z: highestZ
    radius: 3
    border.width: 2
    border.color: borderColor
    Component.onCompleted: {
        appdata.updateZNavMasterSlider.connect(updateZNavMasterSlider);
    }
    Component.onDestruction: {
        appdata.updateZNavMasterSlider.disconnect(updateZNavMasterSlider);
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        propagateComposedEvents: true
        acceptedButtons: Qt.AllButtons

        // don't catch hover over the whole rectangle as Qt doesn't propagate hover down the z stack
        drag.target: parent
        drag.axis: Drag.XAndYAxis
        onPressed: {
            prevColor = parent.border.color;
            parent.border.color = focusColor;
            sparseGroupWindow.z = ++highestZ;
        }
        onReleased: {
            parent.border.color = prevColor;
        }
        onPressAndHold: mouse.accepted = false
    }

    ListModel {
        id: sparseSliceWindowsList
    }

    Rectangle
    {
        id: sparseGroupWindowContent
        color: parent.color
        border.width: 1
        border.color: Material.color(Material.Grey, Material.Shade900)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.border.width
        anchors.rightMargin: parent.border.width
        anchors.topMargin: parent.border.width
        anchors.bottomMargin: parent.border.width

        Column {
            anchors.fill: parent

            Rectangle {
                id: titlebar
                height: titlebarunit
                width: parent.width
                color: "transparent"

                Text {
                    id: titleText
                    maximumLineCount: 1
                    clip: true
                    width: contentWidth
                    anchors.left: titlebar.left
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: titlebar.verticalCenter
                    text: sessionName
                    color: "white"
                    font.bold: true
                    font.pointSize: 10
                }
                Text {
                    id: titleRoiText
                    maximumLineCount: 1
                    clip: true
                    anchors.left: titleText.right
                    anchors.leftMargin: spaceunit
                    anchors.verticalCenter: titlebar.verticalCenter
                    text: firstWindow.sliceEdit.sliceOverlay.roiSelectionPretty
                    color: "white"
                    font.pointSize: 10
                }

                SmallButton {
                    id: moveScreenButton
                    icon: "qrc:/third-party/material-design-icons-master/hardware/1x_web/ic_desktop_windows_white_18dp.png"
                    anchors.right: minimizeButton.left
                    anchors.centerIn: titlebar.Center
                    onButtonClicked: {
                        //moveToScreen(sliceWindow);
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Move to Next Screen")
                    }
                }
                SmallButton {
                    id: minimizeButton
                    icon: "qrc:/gui/icons/minimize_white_18dp.png"
                    anchors.right: titlebar.right
                    anchors.centerIn: titlebar.Center
                    onButtonClicked: {
                        //sliceWindow.visible = false;
                    }
                    ToolTip {
                        visible: parent.hovered
                        delay: toolTipDelay
                        timeout: toolTipTimeout
                        font.pointSize: toolTipFontSize
                        text: qsTr("Minimize")
                    }
                }
            }

            HorDivider {
                id: sparseGroupWindowDivider
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: spaceunit
                anchors.rightMargin: spaceunit
            }

            Rectangle {
                id: horizontalMasterControls
                width: parent.width
                height: 80
                color: "transparent"
                Rectangle {
                    id: horitzontalControlButtons
                    anchors.left: parent.left
                    anchors.top: parent.top
                    width: parent.width
                    height: 40
                    color: "transparent"
                    SmallButton {
                        id: editingModeButton
                        anchors.left: parent.left
                        anchors.leftMargin: spaceunit * 3
                        anchors.verticalCenter: parent.verticalCenter
                        customWidth: 80
                        text: roiEditMode
                        onButtonClicked: {
                            (roiEditMode === "In Vivo Link") ? roiEditMode = "Decode" : roiEditMode = "In Vivo Link";
                            appdata.roiEditMode = roiEditMode;
                        }

                        ToolTip {
                            visible: parent.hovered
                            delay: toolTipDelay
                            timeout: toolTipTimeout
                            font.pointSize: toolTipFontSize
                            text: (roiEditMode === "In Vivo Link") ? qsTr("Switch to Decode mode") : qsTr("Switch to In Vivo Link mode")
                        }
                    }
                    SpinBox {
                        id: rowSetter
                        anchors.left: editingModeButton.right
                        anchors.leftMargin: spaceunit
                        anchors.verticalCenter: parent.verticalCenter
                        width: 120
                        from: 1
                        to: 3//items.length - 1
                        onValueModified: {
                            if(value > numStacks){ value = numStacks; }
                            sparseGroupGrid.rows = value
                            gridCols = Math.ceil(numStacks / sparseGroupGrid.rows)
                            sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height;
                            sparseGroupGrid.width = gridCols * firstWindow.width;
                        }
                    }
                    SmallButton {
                        id: zoomLabelButton
                        customWidth: 50
                        text: appdata.zoomFactor + "%"
                        anchors.left: rowSetter.right
                        anchors.verticalCenter: parent.verticalCenter
                        onButtonClicked: {
                            appdata.zoomFactor = 100;
                            appdata.updateAllSliceImages();
                        }
                    }
                    SmallButton {
                        id: zoomOutButton
                        icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_zoom_out_white_18dp.png"
                        anchors.left: zoomLabelButton.right
                        anchors.verticalCenter: parent.verticalCenter
                        onButtonClicked: {
                            for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.zoomButton = true
                            }
                            appdata.zoomFactor -= 25;
                            appdata.updateAllSliceImages();
                        }
                    }
                    SmallButton {
                        id: zoomInButton
                        icon: "qrc:/third-party/material-design-icons-master/action/1x_web/ic_zoom_in_white_18dp.png"
                        anchors.left: zoomOutButton.right
                        anchors.verticalCenter: parent.verticalCenter
                        onButtonClicked: {
                            for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.zoomButton = true
                            }
                            appdata.zoomFactor += 25;
                            appdata.updateAllSliceImages();
                        }
                    }
                }


                ColorEditor {
                    id: colorEditor
                    anchors.left: parent.left
                    anchors.top: horitzontalControlButtons.bottom
                    width: parent.width
                    height: 40
                    sparseGroup: true

                }

            }

            Row{
                id: sliceRow
                width: parent.width
                Rectangle {
                    id: verticalMasterControls
                    width: 73 * 2
                    anchors.top: parent.top
                    color: "transparent"
                    onHeightChanged: zNavMasterSlider.height = verticalMasterControls.height -
                                     (prevZButton.height + playZButton.height + nextZButton.height +
                                     thickUpButton.height + thickLabelButton.height + thickDownButton.height +
                                     projectMaxButton.height + projectMinButton.height + spaceunit * 10)

                    Rectangle {
                        id: navSide
                        width: 73
                        height: parent.height
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        color: "transparent"
                        Slider {
                            id: zNavMasterSlider
                            orientation: Qt.Vertical
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.top
                            anchors.topMargin: spaceunit
                            value: 1
                            from: 1
                            to: 1
                            live: true
                            stepSize: 1
                            snapMode: Slider.SnapAlways
                            enabled: { to > 1; }
                            // just in case it didn't have time, make sure the resting slice is shown
                            onPressedChanged: {
                                if (!pressed) {
                                    appdata.updateAllSliceImages();
                                    appdata.changeAllSliceIndex(appdata.getIndexZ());
                                    appdata.updateAllSliceOverlays();
                                    zNavMasterSlider.value = appdata.getIndexZ() + 1;
                                }
                            }
                            onMoved: {
                                appdata.gotoZ(zNavMasterSlider.value);
                                appdata.updateAllSliceImages();
                                appdata.changeAllSliceIndex(appdata.getIndexZ());
                                appdata.updateAllSliceOverlays();
                                zNavMasterSlider.value = appdata.getIndexZ() + 1;
                            }
                        }
                        SmallButton {
                            id: nextZButton
                            anchors.top: zNavMasterSlider.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_expand_less_white_18dp.png"
                            onButtonClicked: {
                                appdata.nextZ();
                                appdata.updateAllSliceImages();
                                appdata.changeAllSliceIndex(appdata.getIndexZ());
                                appdata.updateAllSliceOverlays();
                                zNavMasterSlider.value = appdata.getIndexZ() + 1;
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Next Z")
                            }
                        }
                        SmallButton {
                            id: playZButton
                            anchors.top: nextZButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_arrow_drop_up_white_18dp.png"
                            onButtonClicked: {
                                playZtimer.running = !playZtimer.running;
                            }
                            Timer {
                                id: playZtimer
                                interval: 42;
                                running: false;
                                repeat: true
                                onTriggered: {
                                    if (firstWindow.sliceEdit.sliceImage.atMaxZ()) {
                                        appdata.gotoZ(1);
                                    } else {
                                        appdata.nextZ();
                                    }
                                    appdata.updateAllSliceImages();
                                    appdata.changeAllSliceIndex(appdata.getIndexZ());
                                    appdata.updateAllSliceOverlays();
                                    zNavMasterSlider.value = appdata.getIndexZ() + 1;
                                }
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Play Z start/stop")
                            }
                        }
                        SmallButton {
                            id: prevZButton
                            anchors.top: playZButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_expand_more_white_18dp.png"
                            onButtonClicked: {
                                appdata.prevZ();
                                appdata.updateAllSliceImages();
                                appdata.changeAllSliceIndex(appdata.getIndexZ());
                                appdata.updateAllSliceOverlays();
                                zNavMasterSlider.value = appdata.getIndexZ() + 1;
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Previous Z")
                            }
                        }
                        SmallButton {
                            id: thickUpButton
                            anchors.top: prevZButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_expand_less_white_18dp.png"
                            onButtonClicked: if(zThick < firstWindow.sliceEdit.sliceImage.maxZ / 2) zThick += 1
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Increase thickness")
                            }
                        }
                        SmallButton {
                            id: thickLabelButton
                            anchors.top: thickUpButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            text: "±" + zThick
                            onTextChanged: {
                                for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                    sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setZThick(zThick)
                                    sparseSliceWindowsList.get(i)["object"].redrawNav()
                                }
                            }
                            onButtonClicked: zThick = 0
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Z Thickness")
                            }
                        }
                        SmallButton {
                            id: thickDownButton
                            anchors.top: thickLabelButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_expand_more_white_18dp.png"
                            onButtonClicked: if(zThick > 0) zThick -= 1
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Decrease thickness")
                            }
                        }
                        SmallButton {
                            id: projectMaxButton
                            anchors.top: thickDownButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width - 2 * spaceunit
                            text: "max"
                            togglable: true
                            onButtonClicked: {
                                thickUpButton.visible = !projectMaxButton.toggled
                                thickLabelButton.visible = !projectMaxButton.toggled
                                thickDownButton.visible = !projectMaxButton.toggled
                                projectMinButton.visible = !projectMaxButton.toggled
                                if(projectMaxButton.toggled){
                                    for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("max")
                                        sparseSliceWindowsList.get(i)["object"].redrawNav()
                                    }
                                }else{
                                    for (i = 0; i < sparseSliceWindowsList.count; i++) {
                                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("")
                                        sparseSliceWindowsList.get(i)["object"].redrawNav()
                                    }
                                }
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Project from current z to max z")
                            }
                        }
                        SmallButton {
                            id: projectMinButton
                            anchors.top: projectMaxButton.bottom
                            anchors.topMargin: spaceunit
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width - 2 * spaceunit
                            text: "min"
                            togglable: true
                            onButtonClicked:{
                                thickUpButton.visible = !projectMinButton.toggled
                                thickLabelButton.visible = !projectMinButton.toggled
                                thickDownButton.visible = !projectMinButton.toggled
                                projectMaxButton.visible = !projectMinButton.toggled
                                if(projectMinButton.toggled){
                                    for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("min")
                                        sparseSliceWindowsList.get(i)["object"].redrawNav()
                                    }
                                }else{
                                    for (i = 0; i < sparseSliceWindowsList.count; i++) {
                                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("")
                                        sparseSliceWindowsList.get(i)["object"].redrawNav()
                                    }
                                }
                            }
                            ToolTip {
                                visible: parent.hovered
                                delay: toolTipDelay
                                timeout: toolTipTimeout
                                font.pointSize: toolTipFontSize
                                text: qsTr("Project from current z to min z")
                            }
                        }
                    }
                    Rectangle {
                        id: toolPanel
                        width: 73
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.left: navSide.right
                        anchors.right: parent.right
                        anchors.topMargin: spaceunit
                        anchors.bottomMargin: spaceunit
                        color: "transparent"

                        ColumnLayout {
                            spacing: 0
                            Text {
                                id: layersLabel
                                maximumLineCount: 1
                                clip: true
                                Layout.alignment: Qt.AlignHCenter
                                Layout.bottomMargin: 2
                                text: "Layers"
                                color: Material.color(Material.Grey)
                                font.pointSize: labelSize
                                style: Text.Sunken; styleColor: Material.color(Material.Grey, Material.Shade800)
                            }

                            SmallButton {
                                id: layerRotateButton
                                text: qsTr("Rotate")
                                togglable: true
                                toggled: false
                                alwaysShowBorder: true
                                customWidth: toolPanel.width - spaceunit
                                onButtonClicked: {
                                    //for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                    //    sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceOverlay.toggleShowRotation();
                                    //    sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceOverlay.update();
                                    //}
                                }
                            }
                            Button {
                                id: layerRotateResetButton
                                text: "Reset"
                                flat: true
                                Layout.preferredHeight: 30
                                font.pointSize: labelSize
                                visible: layerRotateButton.toggled
                                onClicked: {
                                    //layerRotateDial.value = sliceEdit.sliceOverlay.setPreRotation(0);
                                    //redrawNav();
                                }
                            }
                            Dial {
                                id: layerRotateDial
                                visible: layerRotateButton.toggled
                                value: 0
                                stepSize: 1
                                from: -180
                                to: 180
                                wrap: false
                                snapMode: Dial.SnapAlways
                                background: Rectangle {
                                    x: layerRotateButton.width / 2 - width / 2 + 3
                                    width: layerRotateButton.width * 0.9
                                    height: width
                                    color: "transparent"
                                    radius: width / 2
                                    border.color: layerRotateDial.pressed ? "#17a81a" : "#21be2b"
                                    opacity: layerRotateDial.enabled ? 1 : 0.3
                                }
                                onMoved: {
                                    //value = sliceEdit.sliceOverlay.setPreRotation(value);
                                    //redrawNav();
                                }
                            }

                            SmallButton {
                                id: layerRefButton
                                text: qsTr("Ref")
                                togglable: true
                                toggled: false
                                alwaysShowBorder: true
                                customWidth: toolPanel.width - spaceunit
                                onButtonClicked: {
                                    console.log("Ref toggle");
                                    //sliceEdit.sliceOverlay.toggleShowReference();
                                    //sliceEdit.sliceOverlay.update();
                                }
                            }
                            TinyColorPicker {
                                id: refColorPicker
                                title: "Reference"
                                Layout.leftMargin: spaceunit
                                visible: layerRefButton.toggled
                                //currRed: sliceEdit.sliceOverlay.getRefMaskColor().r
                                //currGreen: sliceEdit.sliceOverlay.getRefMaskColor().g
                                //currBlue: sliceEdit.sliceOverlay.getRefMaskColor().b
                                //currAlpha: sliceEdit.sliceOverlay.getRefMaskColor().a
                                onColorChanged: {
                                    //sliceEdit.sliceOverlay.setRefMaskColor(rgba);
                                    //sliceEdit.sliceOverlay.update();
                                }
                            }

                            SmallButton {
                                id: layerMasksButton
                                visible: true
                                togglable: true
                                toggled: true
                                alwaysShowBorder: true
                                customWidth: toolPanel.width - spaceunit
                                text: qsTr("Masks")
                                onButtonClicked: {
                                    console.log("Mask toggle");
                                    //sliceEdit.sliceOverlay.toggleShowMasks();
                                    //sliceEdit.sliceOverlay.update();
                                }
                            }

                            TinyColorPicker {
                                id: unconfirmedColorPicker
                                title: "Unconfirmed"
                                Layout.leftMargin: spaceunit
                                visible: !layerMasksButton.down && appdata.isProjectAnnotation()
                                currRed: appdata.getUnconfirmedMaskColor().r
                                currGreen: appdata.getUnconfirmedMaskColor().g
                                currBlue: appdata.getUnconfirmedMaskColor().b
                                currAlpha: appdata.getUnconfirmedMaskColor().a
                                onColorChanged: {
                                    appdata.setUnconfirmedMaskColor(rgba);
                                    appdata.updateAllSliceOverlays();
                                }
                            }
                            TinyColorPicker {
                                id: confirmedColorPicker
                                title: "Confirmed"
                                Layout.leftMargin: spaceunit
                                visible: !layerMasksButton.down && appdata.isProjectAnnotation()
                                currRed: appdata.getConfirmedMaskColor().r
                                currGreen: appdata.getConfirmedMaskColor().g
                                currBlue: appdata.getConfirmedMaskColor().b
                                currAlpha: appdata.getConfirmedMaskColor().a
                                onColorChanged: {
                                    appdata.setConfirmedMaskColor(rgba);
                                    appdata.updateAllSliceOverlays();
                                }
                            }
                            Rectangle {
                                Layout.topMargin: spaceunit
                                Layout.bottomMargin: spaceunit
                                height: toggleOutlinesButton.height
                                color: "transparent"
                                visible: !layerMasksButton.down

                                Row {
                                    anchors.fill: parent

                                    SmallButton {
                                        id: toggleOutlinesButton
                                        icon: "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_panorama_fish_eye_white_18dp.png"
                                        onButtonClicked: {
                                            for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                                                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceOverlay.toggleShowMaskOutlines()
                                                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceOverlay.update()
                                            }
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Outlining")
                                        }
                                    }
                                    SmallButton {
                                        id: showLabelsButton
                                        togglable: true
                                        icon : "qrc:/third-party/material-design-icons-master/action/1x_web/ic_label_outline_white_18dp.png"
                                        onButtonClicked: {
                                            //sliceEdit.sliceOverlay.toggleShowLabels();
                                            //sliceEdit.sliceOverlay.update();
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Labels")
                                        }
                                    }
                                    SmallButton {
                                        id: showCentroidsButton
                                        togglable: true
                                        icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_filter_tilt_shift_white_18dp.png"
                                        onButtonClicked: {
                                            //sliceEdit.sliceOverlay.toggleShowCentroids();
                                            //sliceEdit.sliceOverlay.update();
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Centroid Crosshairs")
                                        }
                                    }
                                }
                            }

                            Rectangle {
                                Layout.topMargin: spaceunit
                                Layout.bottomMargin: spaceunit
                                height: toggleOutlinesButton.height
                                color: "transparent"
                                visible: !layerMasksButton.down

                                Row {
                                    anchors.fill: parent

                                    SmallButton {
                                        id: toggleLinkedButton
                                        greyedOut: roiEditMode === "Decode"
                                        togglable: true
                                        toggled: appdata.showCandidates
                                        icon: linkIcon
                                        anchors.leftMargin: spaceunit
                                        onButtonClicked: {
                                            appdata.showCandidates = toggleLinkedButton.toggled;
                                            //sliceEdit.sliceOverlay.toggleShowConfirmed();
                                            //sliceEdit.sliceOverlay.update();
                                            if (!toggled) {
                                                toggleFullyLinkedButton.forceToggle(false);
                                            }
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Linked")
                                        }
                                    }
                                    SmallButton {
                                        id: toggleFullyLinkedButton
                                        togglable: true
                                        toggled: true
                                        icon: confirmedIcon
                                        //visible: sliceEdit.sliceImage.isRef
                                        anchors.leftMargin: spaceunit
                                        onButtonClicked: {
                                            if (toggled) {
                                                toggleLinkedButton.forceToggle(true);
                                            }
                                            //sliceEdit.sliceOverlay.toggleShowFullyLinked();
                                            //sliceEdit.sliceOverlay.update();
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Fully-Linked")
                                        }
                                    }
                                }
                            }
                            Rectangle {
                                id: filterFlagButtons
                                Layout.topMargin: spaceunit
                                Layout.bottomMargin: spaceunit
                                height: toggleOutlinesButton.height
                                color: "transparent"
                                visible: !layerMasksButton.down

                                Row {
                                    anchors.fill: parent

                                    SmallButton {
                                        id: toggleFilteredButton
                                        togglable: true
                                        toggled: false
                                        icon: filterIcon
                                        anchors.leftMargin: spaceunit
                                        onButtonClicked: {
                                            //sliceEdit.sliceOverlay.toggleShowFiltered();
                                            //sliceEdit.sliceOverlay.update();
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Filters")
                                        }
                                    }
                                    SmallButton {
                                        id: toggleFlaggedButton
                                        togglable: true
                                        toggled: true
                                        icon: flagIcon
                                        anchors.leftMargin: spaceunit
                                        onButtonClicked: {
                                            //sliceEdit.sliceOverlay.toggleShowFlagged();
                                            //sliceEdit.sliceOverlay.update();
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Flagged")
                                        }
                                    }
                                }
                            }

                            Rectangle {
                                Layout.topMargin: 2
                                Layout.bottomMargin: spaceunit
                                Layout.leftMargin: spaceunit
                                Layout.rightMargin: 2
                                height: smallButtonSize * 3 + spaceunit * 2
                                width: toolPanel.width - spaceunit - 2
                                color: Material.color(Material.Grey, Material.Shade900)
                                border.width: 1
                                border.color: Material.color(Material.Grey, Material.Shade700)
                                visible: toggleFilteredButton.toggled

                                Column {
                                    id: clfilter
                                    anchors.fill: parent

                                    Text {
                                        maximumLineCount: 1
                                        clip: true
                                        anchors.left: parent.left
                                        anchors.leftMargin: 2
                                        anchors.bottomMargin: 2
                                        text: "Radius"
                                        color: Material.color(Material.Grey)
                                        font.pointSize: labelSize
                                    }

                                    SmallSpinBox {
                                        id: radiusInput
                                        value: 1
                                        from: 0
                                        to: 1000
                                        anchors.left: parent.left
                                        anchors.leftMargin: 2
                                        height: 30
                                        width: toolPanel.width - spaceunit * 3
                                    }

                                    SmallButton {
                                        id: flagFilteredButton
                                        //icon: flagIcon
                                        text: "Flag"
                                        customWidth: smallButtonSize * 2
                                        anchors.left: parent.left
                                        anchors.leftMargin: 2
                                        onButtonClicked: {
                                            console.log("Flag Filtered clicked");
                                            //appdata.flagFiltered(sliceEdit.sliceImage.getName());
                                        }
                                        ToolTip {
                                            visible: parent.hovered
                                            delay: toolTipDelay
                                            timeout: toolTipTimeout
                                            font.pointSize: toolTipFontSize
                                            text: qsTr("Flag all filtered ROIs")
                                        }
                                    }
                                }
                            }
                        }


                        /// tools--lower part
                        Rectangle {
                            id: roiToolPanel
                            color: "transparent"
                            visible: !isRegistration && appdata.isProjectAnnotation()
                            width: parent.width
                            height: 2 * (smallButtonSize + spaceunit) + roiLabel.height + spaceunit * 2

                            anchors.left: parent.left
                            //anchors.right: sliceEditContent.right
                            anchors.bottom: parent.bottom
                            //anchors.leftMargin: spaceunit
                            //anchors.rightMargin: spaceunit
                            //anchors.topMargin: spaceunit
                            anchors.bottomMargin: spaceunit

                            border.width: 0

                            Text {
                                id: roiLabel
                                maximumLineCount: 1
                                clip: true
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.top: parent.top
                                anchors.topMargin: spaceunit
                                text: "Tools"
                                color: Material.color(Material.Grey)
                                font.pointSize: labelSize
                                style: Text.Sunken; styleColor: Material.color(Material.Grey, Material.Shade800)
                            }

                            SmallButton {
                                id: editButton
                                //greyedOut: !sliceEdit.sliceOverlay.selectionExists
                                togglable: true
                                anchors.left: parent.left
                                anchors.top: roiLabel.bottom
                                anchors.leftMargin: 1
                                anchors.topMargin: spaceunit
                                icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_edit_white_18dp.png"
                                onToggledChanged: {
                                    console.log("edit changed", toggled);
                                    //appdata.clearLinkState(); // cancel link op if user started one
                                    //sliceEdit.sliceOverlay.toggleEditMode();
                                    if (toggled) {
                                        //addLandmarkButton.forceToggle(false);
                                        //sliceEdit.overlayMouseArea.cursorShape = Qt.DragMoveCursor;
                                    } else {
                                        //sliceEdit.overlayMouseArea.cursorShape = Qt.ArrowCursor;
                                    }
                                }
                                function clearEditMode() {
                                    //editButton.forceToggle(false);
                                    //sliceEdit.overlayMouseArea.cursorShape = Qt.ArrowCursor;
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Edit Mask")
                                }
                            }
                            SmallButton {
                                id: mergeButton
                                //greyedOut: !sliceEdit.sliceOverlay.multiSelectionExists
                                anchors.left: editButton.right
                                anchors.top: roiLabel.bottom
                                anchors.topMargin: spaceunit
                                icon : "qrc:/third-party/material-design-icons-master/editor/1x_web/ic_merge_type_white_18dp.png"
                                onButtonClicked: {
                                    //  sliceEdit.sliceOverlay.mergeSelected();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Merge")
                                }
                            }
                            SmallButton {
                                id: splitButton
                                greyedOut: !sliceEdit.sliceOverlay.selectionExists
                                anchors.left: parent.left
                                anchors.top: editButton.bottom
                                anchors.leftMargin: 0
                                anchors.topMargin: 2
                                icon : "qrc:/third-party/material-design-icons-master/image/drawable-mdpi/ic_broken_image_white_18dp.png"
                                onButtonClicked: {
                                    //console.log("sliceEdit.sliceImage.getIndexZ():" + sliceEdit.sliceImage.getIndexZ());
                                    //   sliceEdit.state = "split";
                                    //   appdata.splitRoi(sliceEdit.sliceImage.getName(), sliceEdit.sliceOverlay.roiSelection, sliceEdit.sliceImage.getMaxZ());
                                    //   redrawNav();
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Split")
                                }
                            }
                            SmallButton {
                                id: flagButton
                                //greyedOut: !sliceEdit.sliceOverlay.selectionExists
                                icon: flagIcon
                                anchors.left: splitButton.right
                                anchors.top: mergeButton.bottom
                                anchors.leftMargin: 0
                                anchors.topMargin: 2
                                onButtonClicked: {
                                    //    appdata.toggleFlag(sliceEdit.sliceImage.getName(), sliceEdit.sliceOverlay.roiSelection);
                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Flag/Unflag")
                                }
                            }
                            SmallButton {
                                id: deleteButton
                                greyedOut: !firstWindow.sliceEdit.sliceOverlay.candidate || roiEditMode === "Decode"
                                anchors.left: flagButton.right
                                anchors.top: mergeButton.bottom
                                anchors.leftMargin: 0
                                anchors.topMargin: 2
                                icon : "qrc:/third-party/material-design-icons-master/navigation/1x_web/ic_cancel_white_18dp.png"
                                onButtonClicked: {
                                    appdata.deleteCandidate(firstWindow.sliceEdit.sliceOverlay.getSessionName(), firstWindow.sliceEdit.sliceOverlay.roiSelection);
                                    appdata.unSelectAllRois();

                                }
                                ToolTip {
                                    visible: parent.hovered
                                    delay: toolTipDelay
                                    timeout: toolTipTimeout
                                    font.pointSize: toolTipFontSize
                                    text: qsTr("Delete Mask")
                                }
                            }
                        }
                    }
                }

                Grid {
                    id: sparseGroupGrid
                    onWidthChanged: sparseGroupWindow.width =
                                    verticalMasterControls.width + sparseGroupGrid.width + 2 * sparseGroupWindow.border.width
                    onHeightChanged: {
                        verticalMasterControls.height = firstWindow.height
                        sparseGroupWindow.height = titlebarunit + horizontalMasterControls.height + sparseGroupGrid.height + sparseGroupWindowDivider.height + sparseGroupWindow.border.width * 2

                    }
                    rows:1
                }
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // resizing grips

    // left side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: -2
        width: spaceunit + 2
        MouseArea {
            id: leftGrip
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = firstWindow.sliceEdit.sliceImage.width - delta/gridCols
                    var newX = sparseGroupWindow.x + delta;
                    if (newWidth < firstWindow.sliceEdit.sliceImage.minWidth * 0.7
                            || newX < 0) {
                        return;
                    }
                    sparseGroupWindow.x = newX;
                    for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setScaledWidth(newWidth);
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.redrawSlice()
                    }
                    sparseGroupGrid.width = gridCols * firstWindow.width
                    sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height
                    subWidth = firstWindow.width
                }
            }
        }
    }

    // right side resize grip
    Rectangle {
        color: "transparent"
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: -2
        width: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeHorCursor
            drag {
                target: parent
                axis:  Drag.XAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseX;
                    var newWidth = firstWindow.sliceEdit.sliceImage.width + delta/gridCols;
                    if (newWidth < firstWindow.sliceEdit.sliceImage.minWidth * 0.7) {
                        return;
                    }
                    for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setScaledWidth(newWidth);
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.redrawSlice()
                    }
                    sparseGroupGrid.width = gridCols * firstWindow.width
                    sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height
                    subWidth = firstWindow.width
                }
            }
        }
    }

    // top side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = firstWindow.sliceEdit.sliceImage.height - delta/sparseGroupGrid.rows
                    var newY = sparseGroupWindow.y + delta;
                    if (newHeight < firstWindow.ratio * firstWindow.sliceEdit.sliceImage.minWidth * 0.7 || newY < 0) {
                        return;
                    }
                    sparseGroupWindow.y = newY;
                    for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setScaledHeight(newHeight);
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.redrawSlice()
                    }
                    sparseGroupGrid.width = gridCols * firstWindow.width
                    sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height
                    subWidth = firstWindow.width
                }
            }
        }
    }

    // bottom side resize grip
    Rectangle {
        color: "transparent"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -2
        height: spaceunit + 2
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            cursorShape: Qt.SizeVerCursor
            drag {
                target: parent
                axis:  Drag.YAxis
            }
            onPositionChanged: {
                if (drag.active) {
                    var delta = mouseY;
                    var newHeight = firstWindow.sliceEdit.sliceImage.height + delta/sparseGroupGrid.rows
                    if (newHeight < firstWindow.ratio * firstWindow.sliceEdit.sliceImage.minWidth * 0.7) {
                        return;
                    }
                    for (var i = 0; i < sparseSliceWindowsList.count; i++) {
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setScaledHeight(newHeight);
                        sparseSliceWindowsList.get(i)["object"].sliceEdit.redrawSlice()
                    }
                    sparseGroupGrid.width = gridCols * firstWindow.width
                    sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height
                    subWidth = firstWindow.width
                }
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////

    function unloadSparseStacks(){
        if(sparseSliceWindowsList.count != 0){
            for (var i = sparseSliceWindowsList.count - 1; i >=0 ; --i) {
                sparseSliceWindowsList.get(i)["object"].destroy(0);
                sparseSliceWindowsList.remove(i)
            }
        }
        appdata.unloadStack("all stack");

    }

    function createSparseSliceWindows(currentSession, viewMode){
        sessionName = currentSession
        var sessionsContainingChannels = appdata.getSparseStackUrlMatrix(sessionName)
        var channelsContainingSessions = []
        for(var i = 0; i < sessionsContainingChannels[0].length; i++){
            var sessions = []
            for (var j = 0; j < sessionsContainingChannels.length; j++){
                sessions.push(sessionsContainingChannels[j][i])
            }
            channelsContainingSessions.push(sessions)
        }

        if (viewMode === "Iso-Channel View"){
            colorEditor.viewLabel.text = qsTr("Session")
            stacksMatrix = channelsContainingSessions
        } else {
            colorEditor.viewLabel.text = qsTr("Channel")
            stacksMatrix = sessionsContainingChannels
        }

        numStacks = stacksMatrix.length

        // find the slice window with the largest number of channel
        // to prevent crash when selected empty channel in certain slices
        var sizeC = 0;
        var window = 0;
        for(let i = 0; i < stacksMatrix.length; i++){
            let l = 0;
            for(let j = 0; j < stacksMatrix[i].length; j++){
                if(stacksMatrix[i][j] !== ""){
                    l++;
                }
            }
            if(l > sizeC){
                window = i;
                sizeC = l;
            }
        }

        appdata.setSizeC(sizeC);
        rowSetter.to = numStacks
        gridCols = Math.ceil(numStacks / sparseGroupGrid.rows);

        var start_n = new Date().getTime();
        for (let i = 0; i < numStacks; i++){
            console.log("iterating over stack " + i)
            var start_c = new Date().getTime();
            createSparseSliceWindowObject(i,stacksMatrix[i], viewMode)
            var end_c = new Date().getTime();
            console.log("Elapsed time (createSparseSliceWindowObject)"+ (end_c - start_c) + " ms")

            if (i === window) {
                firstWindow = sparseSliceWindowsList.get(i)["object"]
                sparseGroupGrid.width = gridCols * firstWindow.width;
                sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height;
                verticalMasterControls.height = firstWindow.height;
                zNavMasterSlider.to = firstWindow.sliceEdit.sliceImage.maxZ;
            }
        }
        var end_n = new Date().getTime();
        console.log("Elapsed time (createspareSliceWindowObjects for all stacks)"+ (end_n - start_n) + " ms")

        // go to z of centroid invivo
        zNavMasterSlider.value = appdata.getCentroidInvivoZ(firstWindow.sliceEdit.sliceOverlay.getSessionName());
        appdata.gotoZ(zNavMasterSlider.value);
        appdata.updateAllSliceImages();
        appdata.changeAllSliceIndex(appdata.getIndexZ());
        appdata.updateAllSliceOverlays();
        zNavMasterSlider.value = appdata.getIndexZ() + 1;

        // remember zThick
        if(projectMaxButton.toggled){
            for (i = 0; i < sparseSliceWindowsList.count; i++) {
                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("max")
                sparseSliceWindowsList.get(i)["object"].redrawNav()
            }
        } else if (projectMinButton.toggled){
            for (i = 0; i < sparseSliceWindowsList.count; i++) {
                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("min")
                sparseSliceWindowsList.get(i)["object"].redrawNav()
            }
        } else {
            for (i = 0; i < sparseSliceWindowsList.count; i++) {
                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setProjection("")
                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setZThick(zThick)
                sparseSliceWindowsList.get(i)["object"].redrawNav()
            }
        }

        // remember the sizes of subview displays
        if (subWidth != 0){
            for (i = 0; i < sparseSliceWindowsList.count; i++) {
                sparseSliceWindowsList.get(i)["object"].sliceEdit.sliceImage.setScaledWidth(subWidth);
                sparseSliceWindowsList.get(i)["object"].sliceEdit.redrawSlice();
            }
            sparseGroupGrid.width = gridCols * firstWindow.width;
            sparseGroupGrid.height = sparseGroupGrid.rows * firstWindow.height;
        }
    }

    function createSparseSliceWindowObject(index, stackUrls, viewMode) {
        var start = new Date().getTime();
        appdata.loadStacks(stackUrls)
        var end = new Date().getTime();
        console.log("Elapsed time (loaded all stacks for a slice window)"+ (end - start) + " ms")

        let cList = [];
        for(let i = 0; i < stackUrls.length; i++){
            if(stackUrls[i] !== ""){
                cList.push(i);
            }
        }
        if (stackComponent.status === Component.Ready) {
            var sparseSliceWindowObject = stackComponent.createObject(sparseGroupWindow,
                                                                      {"id": appdata.getCurrentStackWindow().toString(), "isSparseAnnotation":true});

            if(viewMode === "Iso-Channel View"){
                sparseSliceWindowObject.sliceEdit.sliceOverlay.setChannel(index);
            } else if (viewMode === "Iso-Session View"){
                sparseSliceWindowObject.sliceEdit.sliceOverlay.setSession(index);
            }
            sparseSliceWindowsList.append({"id": index, "object": sparseSliceWindowObject});
            sparseSliceWindowObject.parent = sparseGroupGrid;
            sparseSliceWindowObject.cList = cList;
            sparseSliceWindowObject.sliceEdit.sliceImage.setChannelColorsSparse(cList);
            colorEditor.updateSelectedChannelDetails();
        }
    }

    // for sub SliceWindows to update SparseGroupWindow
    function updateZNavMasterSlider(z) {zNavMasterSlider.value = z;}
    function updateMasterZoom(zoom) {zoomLabelButton.text = zoom + "%";}

}
