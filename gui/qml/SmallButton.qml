/// @author Sam Kenyon <sam@metadevo.com>
import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtGraphicalEffects 1.0

Rectangle {
    id: smallButton

    property string text: ""
    property string icon: ""
    property string pressedIcon: ""
    property real customWidth: smallButtonSize

    property bool pressed: mouseArea.pressed
    property color normalColor:  Material.color(Material.Grey, Material.Shade800)
    property color hiliteColor: Material.color(Material.Grey, Material.Shade700)
    property color pressedColor: Material.color(Material.Grey, Material.Shade900)
    property color borderColor: "transparent"
    property color borderHiliteColor: Material.color(Material.Grey, Material.Shade400)
    property color textColor: Material.color(Material.Grey, Material.Shade50)
    property color textHiliteColor: "white"
    property color prevColor: normalColor
    property color prevBorderColor: borderColor
    property color prevTextColor: textColor
    property bool glowing: false
    property bool hovered: false;
    property bool greyedOut: false
    property bool togglable: false
    property bool toggled: false
    property bool alwaysShowBorder: false
    property bool down: false
    property bool textLeftAligned: false

    //onTextChanged: { label.text = smallButton.text; console.log("text changed");}

    signal buttonClicked(real x, real y)

    color: "transparent"
    border.color: borderColor
    width: customWidth
    height: smallButtonSize
    border.width: 1
    anchors.leftMargin: spaceunit
    anchors.rightMargin: spaceunit
    anchors.topMargin: spaceunit
    anchors.bottomMargin: spaceunit

    Component.onCompleted: {
//        if (text != "") {
//            console.log("width:", label.width);
//            width = label.width + smallButtonSize + spaceunit * 2;
//        }
        if (toggled) {
            innerBG.color = pressedColor;
            border.color = hiliteColor;
            label.color = textHiliteColor;
            buttonPressedEffect.visible = glowing;
        }
        if (alwaysShowBorder) {
           borderColor = hiliteColor;
        }
    }

    function forceToggle(flag) {
        toggled = flag;
        if (!toggled) {
            innerBG.color = normalColor;
            border.color = borderColor;
            label.color = textColor;
            buttonPressedEffect.visible = false;
        } else {
            innerBG.color = pressedColor;
            border.color = borderHiliteColor;
            label.color = textHiliteColor;
            buttonPressedEffect.visible = glowing;
        }
    }

    Rectangle {
        id: innerBG
        color: normalColor
        anchors.fill: parent
        anchors.margins: border.width
     }
    RectangularGlow {
        id: buttonPressedEffect
        visible: false
        anchors.fill: smallButton
        glowRadius: 8
        spread: 0
        color: "orange"
    }

    Image {
        id: buttonIcon
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        visible: { icon != ""; }
        source: icon
        smooth: true
    }
    BrightnessContrast {
        id: buttonIconGreyedOut
        anchors.fill: buttonIcon
        source: buttonIcon
        brightness: -0.5
        visible: greyedOut
    }
//    Image {
//        id: buttonIconPressed
//        anchors.verticalCenter: parent.verticalCenter
//        anchors.horizontalCenter: parent.horizontalCenter
//        visible: { pressedIcon != ""; }
//        source: pressedIcon
//    }

    Text {
        id: label
        text: parent.text
        color: textColor
        font.bold: true
        //anchors.left: { ("icon != ") ? buttonIcon.right : parent.left; }
        //anchors.left: textLeftAligned ? parent.left : undefined
        width: textLeftAligned ? parent.width : undefined
        //x: textLeftAligned ? 5 : 0
        Binding on x {
            when: textLeftAligned
            value: 5
        }
        anchors.horizontalCenter: textLeftAligned ? undefined : parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        elide: Text.ElideLeft
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        propagateComposedEvents: true
        //preventStealing: true
        hoverEnabled: true
        onEntered: {
            hovered = true;
            if (!greyedOut) {
                innerBG.color = hiliteColor;
                parent.border.color = borderHiliteColor;
                label.color = textHiliteColor;
                prevColor = innerBG.color;
                prevBorderColor = parent.border.color;
                prevTextColor = label.color;
            }
        }
        onExited: {
            hovered = false;
            if (!toggled) {
                innerBG.color = normalColor;
                parent.border.color = borderColor;
                label.color = textColor;
                buttonPressedEffect.visible = false;
            } else {
                innerBG.color = pressedColor;
                parent.border.color = hiliteColor;
                label.color = textHiliteColor;
                buttonPressedEffect.visible = glowing;
            }
        }
        onPressed: {
            if (!greyedOut) {
                innerBG.color = pressedColor;
                parent.border.color = hiliteColor;
                label.color = textHiliteColor;
                buttonPressedEffect.visible = glowing;
            }
        }
        onReleased: {
            if (!greyedOut) {
                if (containsMouse) {
                    if (!toggled) {
                        innerBG.color = pressedColor;
                        parent.border.color = hiliteColor;
                        label.color = textHiliteColor;
                        buttonPressedEffect.visible = glowing;
                    } else {
                        innerBG.color = prevColor;
                        parent.border.color = prevBorderColor;
                        label.color = prevTextColor;
                        buttonPressedEffect.visible = false;
                    }
                } else {
                    if (!toggled) {
                        innerBG.color = normalColor;
                        parent.border.color = borderColor;
                        label.color = textColor;
                        buttonPressedEffect.visible = false;
                    } else {
                        innerBG.color = pressedColor;
                        parent.border.color = hiliteColor;
                        label.color = textHiliteColor;
                        buttonPressedEffect.visible = glowing;
                    }
                }
            }
        }
        onPressAndHold: mouse.accepted = false
        onClicked: {
            if (!greyedOut) {
                down = !down;
                if (togglable) {
                    toggled = !toggled;
                }
                smallButton.buttonClicked(mouse.x, mouse.y);
            }
        }
    }
}
