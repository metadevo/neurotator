/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SLICEOVERLAY_HPP
#define SLICEOVERLAY_HPP

#include <QObject>
#include <QQuickPaintedItem>
#include <QPen>

#include "ApplicationData.hpp"

class SliceImage;

enum SelectRoiAction
{
    CLEAR_ACTION = 0,
    LINK_ACTION = 1,
    UNLINK_ACTION = 2,
    MULTISELECT_ACTION = 3,
    SPARSE_CANDIDATE_ACTION = 4,
    SPARSE_CONFIRM_ACTION = 5,
    SPARSE_UNCONFIRM_ACTION = 6
};

enum ModifyRoiAction
{
    TOGGLE_PIXEL = 0,
    SET_PIXEL = 1,
    CLEAR_PIXEL = 2,
};

enum ActionMode
{
    SELECT_MODE = 0,
    EDITMASK_MODE = 1,
    ADD_LANDMARK_MODE = 2
};

/// A widget to use in QML, intended to be drawn over a SliceImage
class SliceOverlay : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(bool newMask READ isMakingNewMask NOTIFY newMaskChanged)
    Q_PROPERTY(QString roiSelectionPretty READ getRoiSelectionPretty NOTIFY roiSelectionChanged)
    Q_PROPERTY(QString roiSelection READ getRoiSelection NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool selectionExists READ isSelection NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool multiSelectionExists READ isMultiSelection NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool selectionFlagged READ isSelectionFlagged NOTIFY roiSelectionChanged)
    Q_PROPERTY(bool linking READ isLinking NOTIFY linkingChanged)
    Q_PROPERTY(bool candidate READ isCandidate NOTIFY roiSelectionChanged)

public:
    explicit SliceOverlay(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);

    Q_INVOKABLE QString getSessionName() {return m_sessionName; }
    Q_INVOKABLE void setSessionName(QString sessionName) { m_sessionName = sessionName; }
    Q_INVOKABLE void setLandmarkSessionName(QString landmarkSessionName) { m_landmarkSessionName = landmarkSessionName;} // for jumping to selected landmark
    //Q_INVOKABLE RoiData::roi_t roiAtMouse(const int x, const int y);
    Q_INVOKABLE QColor getConfirmedMaskColor() { return m_confirmedPen.color(); }
    Q_INVOKABLE QColor getUnconfirmedMaskColor() { return m_unconfirmedPen.color(); }
    Q_INVOKABLE QColor getRefMaskColor() { return m_refColor; }
    Q_INVOKABLE void initNewMask() { m_makingNewMask = true; }
    // Index of landmark near position, or -1 for now
    Q_INVOKABLE int landmarkNear(const float viewX, const float viewY);
    // Show this landmark in a different color
    Q_INVOKABLE void highlightLandmark(int index);
    Q_INVOKABLE void setImage(SliceImage *image) { m_image = image; }
    Q_INVOKABLE int getLandmarkX() { return m_loc[0]; }
    Q_INVOKABLE int getLandmarkY() { return m_loc[1]; }
    Q_INVOKABLE int getLandmarkZ() { return m_loc[2]; }
    // for sparse annotation mode
    Q_INVOKABLE bool isCandidate() { return (isSelection()) ? std::dynamic_pointer_cast<RoiFieldsSparse>(m_selectedRois.values()[0])->isCandidate() : false; }

    bool isMakingNewMask() { return m_makingNewMask; }
    QString getRoiSelectionPretty() { return (isSelection() ? ("[" + m_selectedRois.values()[0]->name + "]") : ""); }
    QString getRoiSelection() { return isSelection() ? m_selectedRois.values()[0]->name : ""; }
    bool isSelection() { return (m_selectedRois.size() == 1); }
    bool isMultiSelection() { return (m_selectedRois.size() > 1); }
    bool isSelectionFlagged() { return (isSelection() ?  m_selectedRois.values()[0]->flagged() : false); }
    bool isLinking() const { return m_linking; }

public slots:
    void refresh();
    void roiSelectionRequest(QString sessionKey, QString roiKey);
    void mouseClick(const float mouseX, const float mouseY);
    void selectRoiInSlice(QString roiKey);
    void selectRoiViaMouse(const float mouseX, const float mouseY, const int action = CLEAR_ACTION);
    void modifyRoiPixel(const float mouseX, const float mouseY, const ModifyRoiAction action = SET_PIXEL);
    void newRoi(const float mouseX, const float mouseY, const ModifyRoiAction action);
    void unselectAll() { m_selectedRois.clear(); roiSelectionChanged(); update();}
    void hoverRoi(const int x, const int y);
    void exitHoverRoi();
    void showAllRois();
    void hideAllRois();
    void toggleAllRois() { (m_showAllRois) ? hideAllRois() : showAllRois(); }
    void changeSlice(const int z);
    void changeSliceIndex(const int indexZ);
    void toggleShowCentroids() { m_showCentroids = !m_showCentroids; }
    void toggleShowMasks() { m_showMasks = !m_showMasks; }
    void toggleShowMaskOutlines() { m_showMaskOutlines = !m_showMaskOutlines; }
    void toggleShowConfirmed() { m_showConfirmed = !m_showConfirmed; }
    void toggleShowFullyLinked() { m_showFullyLinked = !m_showFullyLinked; }
    void toggleShowReference();
    void toggleShowRotation() { m_showRot = !m_showRot;}
    void toggleShowLabels() { m_showLabels = !m_showLabels; }
    void toggleShowFlagged() { m_showFlagged = !m_showFlagged; }
    void toggleShowFiltered() { m_showFiltered = !m_showFiltered; }
    void toggleLandmarks() { m_showLandmarks = !m_showLandmarks; }
    void toggleLandmarkLabels() { m_showLandmarkLabels = !m_showLandmarkLabels; }

    void resetLinking() { setIsLinking(false); update(); }
    void unlinkSelected();
    void setConfirmedMaskColor(QColor color) {
        m_confirmedPen.setColor(color);
        m_confirmedColor = color.rgba();
    }
    void setUnconfirmedMaskColor(QColor color) {
        m_unconfirmedPen.setColor(color);
        m_unconfirmedColor = color.rgba();
    }
    void setRefMaskColor(QColor color) {
        m_refColor = color.rgba();
    }
    int setPreRotation(const float val);
    void setScaleAndOffset(const float scale, const float zoom, const float xOffset, const float yOffset, const float x0, const float y0);
    void toggleEditMode();
    void enableAddLandmarkMode();
    void disableAddLandmarkMode();
    void mergeSelected();
    void moveLandmark(const int index, const float viewX, const float viewY);
    void moveLandmarkZ(const int index, const int z);

    // for sparse annotation mode
    void setChannel(const int channel) { m_channel = channel; }
    void setSession(const int session){ m_session = session; }

signals:
    void newMaskChanged();
    void roiSelectionChanged();
    void linkingChanged();

private:    
    void addLandmark(float mouseX, float mouseY);
    void drawLandmark(QPainter *painter, const Landmark &landmark,
                      vtkAbstractTransform* xform, int maxY_moving, int maxY_fixed, bool angle);

    void paintMask(QPainter *painter, RoiDataBase::roi_t roi);
    void paintMaskOutline(QPainter *painter, RoiDataBase::roi_t roi);
    void drawCrosshairs(QPainter *painter, RoiDataBase::roi_t roi);
    void drawEllipse(QPainter *painter, RoiDataBase::roi_t roi);
    QPoint viewToWorldCoords(const float x, const float y);
    QPoint worldToViewCoords(const float x, const float y);
    QPoint worldToViewCoords(const QPoint p);
    QRect viewToWorldCoords(const float x1, const float y1,
                            const float x2, const float y2);
    QRect worldToViewCoords(const QRect rect);

    RoiDataBase::roi_t coordInRoi(const float mouseX, const float mouseY);
    void changeMode(ActionMode mode);
    void setIsLinking(bool flag) {
        if (m_linking != flag) {
            m_linking = flag;
            emit linkingChanged();
        }
    }

    ApplicationData& m_appdata;
    QString m_sessionName;
    QString m_landmarkSessionName;
    QHash<QString, RoiDataBase::roi_t> m_selectedRois;
    QList<RoiDataBase::roi_t> m_visibleRois;
    QList<RoiDataBase::roi_t> m_refRois;
    RoiDataBase::roi_t m_hoveredRoi;
    ActionMode m_mode = SELECT_MODE;
    bool m_showAllRois = true;
    bool m_showCentroids = false;
    bool m_showMasks = true;
    bool m_showMaskOutlines = false;
    bool m_showConfirmed = true;
    bool m_showFullyLinked = true;
    bool m_showLabels = false;
    bool m_showFlagged = true;
    bool m_showFiltered = true;
    bool m_showRef = false;
    bool m_showRot = false;
    bool m_showLandmarks = true;
    bool m_showLandmarkLabels = true;
    bool m_linking = false;
    unsigned int m_z = 0;
    unsigned char m_iBase = 0;
    float m_iBaseF = 0;
    RoiDataDense* m_roiData; ///@todo should be smart pointer
    QPen m_confirmedPen;
    QPen m_unconfirmedPen;
    QRgb m_confirmedColor;
    QRgb m_unconfirmedColor;
    QRgb m_refColor;
    QPen m_confirmedCentroidPen;
    QPen m_unconfirmedCentroidPen;
    QPen m_defaultSelectPen;
    QPen m_editSelectPen;
    QPen m_selectPen;
    QPen m_linkingPen;
    QPen m_lightPen;
    QPen m_labelPen;
    QPen m_landmarkPen;
    QPen m_currentLandmarkPen;
    QVector<QRgb> m_maskColorTable;
    QFont m_labelFont;
    float m_scaling = 1.0;
    float m_zoom = 1.0;
    float m_textureOffsetX = 0;
    float m_textureOffsetY = 0;
    float m_x0;
    float m_y0;

    int m_maxLabelWidth = 150;
    int m_labelHeight = 12;
    const int m_maxLabelLines = 2;
    bool m_makingNewMask;
    int m_currentLandmark;
    SliceImage *m_image = nullptr;
    QList<int> m_loc= {0, 0, 0}; // coordinates of the clicked of landmark

    // for sparse annotation mode
    RoiDataSparse* m_roiDataSparse;
    QRgb m_sparseRoisColor;
    int m_session = 0;
    int m_channel = 0;

};

#endif // SLICEOVERLAY_HPP

