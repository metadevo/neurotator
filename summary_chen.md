# Neurotator Documentation 09/01/20

Covers major updates from Oct 2019 to Aug 2020 made by Chen Xin

## The general use case of Neurotation

The neurotator opens and imports a project by reading project configuration in a json file

With an opened project

* for registration mode, it can import 2 stack images from tiff files and landmarks data from project json file. For each project, 2 stack images can be opened and a registration stack is generated from blending the two stack images. Users can then examine and edit landmark data on opened images. Edited landmark data can be saved to json file.

* for dense annotation mode, it can import and open multiple stack images from tiff files and ROI (Region of Interests) data from mat file. Users can examine and edit ROI data on the opened images. Edited ROI data are saved to the mat file.

* for sparse annotation mode, it can import a list of ROI directories each of which contains M(number of channels) * N(number of sessions) stack and one ROI mat file. Users can examing all stacks acorss different channels and sessions and make changes to the ROI data. Edited ROI data are saved to the mat file of the ROI directory

NOTE: registration mode and dense annotation mode comes with the legacy codes while sparse annotation mode is newly implemented.


## Software Overall Structure

### C++ Classes

ApplicationData: created once you launch the software, maintains global variables

Project: created when importing and opening a project, maintains project variables and configurations

HyperstackTiff: imports and handles tiff image using VTK package

SliceImage: created for each tiff stack window, configures tiff image and displays the image on certain z index.

SliceOverlay: created for each tiff stack window, displays a layer on top of the slice image based on the data of ROI or landmarks

MatFile: contains functions to read and write data in mat file

RoiField: contains data for each ROI

RoiData: contains all RoiFields 

RoiModel: stores all ROI data into a table model that be can presented and updated on UI

### QML Components for UI

SliceWindow.qml displays image data from SliceImage and SliceOverlay and handles user interactions for data editing

SliceEdit.qml handles mouse cursor interaction on the image

## Image Zooming 

### Zooming by mouse scrolling to where the mouse is hovering 
### Zoom buttons zoom in/out towards the center of the displayed part of an image

Implemented mainly in SliceImage class. 

Zooming function is realized by croping the image based on a zoom factor and a zoom center
Image is cropped using coordinates of the zoomed part's upper left cornor (x, y), width w and height h

When zoomg using mouse scrolling toward where the mouse hovers
* We first read the coordinates of mouse cursor as the zoom center (centerX, centerY)
* Zoom factor is increased or decreased with mouse scrolling
* The cropping width w and height y is calculated based on the current zoom factor
* The zoomed part's upper left coordinates should be (centerX - w/2, centerY - h/2)

When zooming using zoom in/out button
* we set the zoom center to the center of the display
* change zoom factor based on zoom button clicks

To make sure the ROI or Landmarks position also zooms and changes with the zoomed image, 
the SliceImage needs to send zoom settings signal to ApplicationData 
Then SliceOverlay reads the zoom settings from ApplicationData and adjust the size and position of ROI or landmarks accordingly

## Landmark Selection in Registration Mode

### Clicking on a landmark from landmark table shows the z-plane of where the landmark exists

Get the cooridinates of a landmark when clicking on a landmark from the landmark table model
Trigger the signal containing the landmark coordinates in ApplicationData
Set z-index of SliceImage and SliceOverlay to the z coordinate of the landmark
If the image is zoomed, recrop the image to where the landmark is in the center based on the x, y coordinates of the landmark

## ROI Selection in Dense Annotation Mode

### Clicking on a ROI from ROI table shows the z-plane of where the ROI exists

This high level logic should be the same as Landmark Selection in Registration Mode
Differs in that ROI data read from RoiField or RoiData class.

## Configureing the Application Windows

### Set initial size and position of the Windows
### Set where subwindows should be tiled up
### Move a window to the next screen
### Enable items in menu bar

The above setting can be found and configured in main.qml

## Sparse Annotation Mode

### Import multiple directory

Differed from other modes, sparse annotation mode used QFileDialog to enable multiple file imports implemented in ApplicationData

### ROI Directory selection, adds and deletes

In sparse annotation mode, ROI Table component displays the list of imported directory and supports additional adds and deletes implemented in RoiSparseTable.qml 
Clicking on one directory creates a SparseGroupWindow component that displays the data of the clicked ROI directories.
If a previous SparseGroupWindow already exists, it destroy all sub slice windows and repopulate the SparseGroupWindow with new subwindows from the current selected SparseGroupWindow.
A button on Sparse ROI table switches ISO-Channel view or ISO-Session view by setting the current viewMode variable

### mat file and ROIs

For each ROI directory, there is a mat file containing 3 variables, Baxter_list, ROI_list, coverage
We created a RoiField object for each element in Baxter_list with a specific shape of mask,
but each ROIField object created carries the same data stored in ROI_list

### Extracting stacks of a ROI directory

For each ROI directory, there is 2 matrix of tiff filepaths called tiff_aligned and tiff_exvivo stored in the mat file under ROI_list variable
The stack matrices are read using MatFileSparseRoi from mat file and then loaded into ROIField objects
In ProjectAnnotationSparse, a function getStackUrlMatrix(QString directoryName) gets the stacks data from a ROIField and prepare a matrix of file path
If elements are empty in ROI_list.tiff_aligned, check for the files listed in the corresponding elements in ROI_list.tiff_exvivo. 
If exist, load that file into respective session/channel position (can be noted as “coarse aligned”)

### Displaying stacks of a ROI directory

One SparseGroupWindow.qml holds multiple SliceWindow.qml as sub windows.

Once we have all the file paths of stacks, HyperstackTiff using VTK reads and combines tiff stack of the same channel or the same session depending on the view mode Iso-Channel/Iso-Session
For each combined stacks, we create a SliceImage to display the image and create a SliceOverlay object based on the same RoiDataSparse
The view mode setting is set and passed from UI component RoiSparseTable.qml 
For each ROI directory, we arrange all slice windows in a grid container inside UI component SparseGroupWindow.qml where you can choose the number of row to decide how they are tiled up
When you resize sparse group window, each sub slice window also resizes accordingly. The resizing grip component in SparseGroupWindow.qml is connected to the resizing grip components of each sub SliceWindow.qml

### Global mouse-click on ROI

Clicking on a ROI in a subwindow selects the same ROI across all sub window

Mouse clicks on image are handled inside SliceEdit.qml, 
If a ROI is clicked, it triggers a signal in ApplicationData, which tells all sub windows to do the same
And also update all sub windows 

### Adding or removing candidate

Candidate list is stored in ROI_list variable in the mat file
As mentioned earlier in "mat file and ROIs", each ROIField object maintains the data of ROI_list and ROI_list contains data of candidate list
When we select an ROI, we make it candidate by adding it to the candidate list.

### Confirm/Unconfirm ROI

Whether a ROI is confirmed in certain channel and session is stored in a matrix where rows represents session and column represents channel
A confirm matrix is stored in Baxter_list for each ROIField.

### Saving in mat file

Whenever a ROI is added or removed from candidate list or whenever a ROI is confirmed or unconfirmed, the software reads and copys all data from the mat file into a MATLAB data structure
Then it updates that data structure with current data and overwrites the original mat file to complete saving
The above needs to be done using MATLAB API

### Synchronize contorls for all sub windows

Each time we use a control button or sliders, we trigger a signal in ApplicationData that is being sent to all SliceImages and SliceOverlay
In this way, controls in SparseGroupWindow.qml can control all SliceWindow.qml at the same time

An alternative way is that inside SparseGroupWindow we manually pass the control operation to each sub window
All sub windows are stored in a List of object inside SparseGroupWindow, we can loop through all windows and apply the control operations

### Remember settings when closing and reopening different ROI directories

Create variables in ApplicationData classes to store the current state of Sparse group window including which buttons are toggled, which colors are picked for confirmed and etc.
When switching to a new ROI directory, the previous sub windows are destoryed and new sub windows will be added into sparse group window
After all new subwindows are generated, set all status and controls based on the previous state of sparse group window stored in Application class