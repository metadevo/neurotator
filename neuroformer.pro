CONFIG += console widgets no_batch
CONFIG += c++14
QT += core gui quickcontrols2 widgets

QMAKE_CXXFLAGS_RELEASE  += -O2
QMAKE_CXXFLAGS_DEBUG  += -D_DEBUG

MATLAB_VERSION=v97
VTK_VER=8.90

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# Neurotator specific defines
DEFINES += MATLAB_SUPPORT

INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/gui

HEADERS += \
    include/MatFile.hpp \
    include/MatFileDenseRoi.hpp \
    include/MatFileSparseRoi.hpp \
    include/PersistenceInterface.hpp \
    include/Project.hpp \
    include/ProjectDense.hpp \
    include/ProjectAnnotationDense.hpp \
    include/ProjectAnnotationSparse.hpp \
    include/ProjectRegistration.hpp \
    include/AppSettings.hpp \
    include/HyperstackTiff.hpp \
    include/Hyperstack.hpp \
    include/Render.hpp \
    include/RoiDataModel.hpp \
    include/RoiDataSparseModel.hpp \
    include/RoiDataBase.hpp \
    include/RoiDataDense.hpp \
    include/RoiDataSparse.hpp \
    include/ImagePipeline.hpp \
    include/VtkHyperTiff.hpp \
    include/WatershedImagePipeline.hpp \
    include/SearchResults.hpp \
    include/LandmarkModel.hpp \
    gui/ApplicationData.hpp \

SOURCES += \
    neuroformer/src/main.cpp \
    src/Project.cpp \
    src/ProjectDense.cpp \
    src/ProjectAnnotationDense.cpp \
    src/ProjectAnnotationSparse.cpp \
    src/ProjectRegistration.cpp \
    src/MatFileDenseRoi.cpp \
    src/MatFileSparseRoi.cpp \
    gui/ApplicationData.cpp \
    src/HyperstackTiff.cpp \
    src/RegisterRigid3D.cpp \
    src/Hyperstack.cpp \
    src/AppSettings.cpp \
    src/Render.cpp \
    src/RoiDataModel.cpp \
    src/RoiDataSparseModel.cpp \
    src/RoiDataBase.cpp \
    src/RoiDataDense.cpp \
    src/RoiDataSparse.cpp \
    src/VtkHyperTiff.cpp \
    src/WatershedImagePipeline.cpp \
    src/LandmarkModel.cpp

contains(DEFINES, MATLAB_SUPPORT) {
    SOURCES += src/MatFile.cpp
}
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

contains(DEFINES, MATLAB_SUPPORT) {
    win32 {
      INCLUDEPATH += C:/"Program Files"/MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/extern/include
      LIBS += -LC:/"Program Files"/MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/extern/lib/win64/microsoft -llibmat -llibmx
      LIBS += -LC:/"Program Files"/MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/bin/win64
      DEPENDPATH += C:/"Program Files"MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/extern/include
    }
    macx {
      INCLUDEPATH += "/Applications/MATLAB/MATLAB_Runtime/$${MATLAB_VERSION}/extern/include"
      LIBS += -L"/Applications/MATLAB/MATLAB_Runtime/$${MATLAB_VERSION}/bin/maci64" -lmat -lmx
      DEPENDPATH += "/Applications/MATLAB/MATLAB_Runtime/$${MATLAB_VERSION}/extern/include"

      # Set DYLD_FRAMEWORK_PATH to include /Applications/MATLAB/MATLAB_Runtime/v97/bin/maci64
      # Set DYLD_LIBRARY_PATH to include /Applications/MATLAB/MATLAB_Runtime/v97/bin/maci64
    }
}

win32 {
    INCLUDEPATH += $$PWD/third-party/tiff/libtiff
    DEPENDPATH += $$PWD/third-party/tiff/libtiff
    LIBS += -L$$PWD/third-party/tiff/build
    CONFIG(release, debug|release): LIBS += -llibtiff
    else:CONFIG(debug, debug|release): LIBS += -llibtiffd
}
macx {
    INCLUDEPATH += /usr/local/include/vtk-$$VTK_VER/vtktiff/libtiff
    DEPENDPATH += /usr/local/include/vtk-$$VTK_VER/vtktiff
    LIBS +=  -L/usr/local/lib/ -lvtktiff-$$VTK_VER    -ljpeg

    # Set DYLD_FRAMEWORK_PATH to include /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/
    # Set DYLD_LIBRARY_PATH to include /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/
}


win32 {
    INCLUDEPATH += $$PWD/third-party/g3log/src
    INCLUDEPATH += $$PWD/third-party/g3log/build/include
    DEPENDPATH += $$PWD/third-party/g3log/src
    DEPENDPATH += $$PWD/third-party/g3log/build/include
    CONFIG(release, debug|release): LIBS += -L$$PWD/third-party/g3log/build -lg3logger
    else:CONFIG(debug, debug|release): LIBS += -L$$PWD/third-party/g3log/build -lg3loggerd
}
macx {
    INCLUDEPATH += /usr/local/include
    LIBS += -L/usr/local/lib/ -lg3logger
}

win32 {
    INCLUDEPATH += c:/opencv/build/include
    DEPENDPATH += c:/opencv/build/include
    CONFIG(release, debug|release): LIBS += -Lc:/opencv/build/x64/vc15/lib -lopencv_world450
    else:CONFIG(debug, debug|release): LIBS += -LC:/opencv/build/x64/vc15/lib -lopencv_world450d
    LIBS += -Lc:/opencv/build/x64/vc15/bin
}
macx {
    INCLUDEPATH += /usr/local/include/opencv4/
    LIBS += -lopencv_world
}

# vtk
win32 {
    INCLUDEPATH +=  "C:/VTK-$$VTK_VER/include/vtk-$$VTK_VER"
    LIBS += -L"C:/VTK-$$VTK_VER/lib"
    LIBS += -L"C:/VTK-$$VTK_VER/bin"
}
macx {
    INCLUDEPATH +=  "/usr/local/include/vtk-$$VTK_VER"
}

 LIBS += -lvtkCommonTransforms-$$VTK_VER -lvtkCommonCore-$$VTK_VER -lvtkRenderingQt-$$VTK_VER -lvtkCommonExecutionModel-$$VTK_VER \
         -lvtkCommonDataModel-$$VTK_VER -lvtkImagingGeneral-$$VTK_VER -lvtkImagingCore-$$VTK_VER -lvtkRenderingCore-$$VTK_VER \
         -lvtkIOImage-$$VTK_VER -lvtkFiltersCore-$$VTK_VER


