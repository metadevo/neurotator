# NEUROTATOR
A hyperdimensional cellular image visualizer and annotation editor.

## Table of contents

- [Build for Windows](#build-for-windows)
- [Build for Mac OSX](#build-for-mac-osx)

### Install

If you just want to run the application without rebuilding, there should be a prebuilt installer available for Windows.

---

# Build for Windows

This is all cross-platform, but so far it's only been built and run on Windows using MSVC (Visual Studio) build configuration. 

Theoretically, everything should build fine as well using gcc / MinGW. For other operating systems, you'd have to acquire the appropriate dependencies (MATLAB Runtime, Qt, OpenCV, VTK). In practice, this is quite difficult given the existing code status

## Install CMake
[Download](https://cmake.org/download/) and install the latest version of CMake.

## Install MS Visual Studio

[Download](https://visualstudio.microsoft.com/downloads/) and install the _free Community edition_ of Microsoft Visual Studio *2019*. Make sure to include the _Desktop
Development with C++_. 

We'll assume that the installation was made to _C:\Program Files (x86)\Microsoft Visual Studio\2019\Community_

## Install VTK
[Download VTK-8.90](https://vtk.org/download/) and unzip it. Move the inner VTK-8.2.0 directory to _C:\src\VTK-8.2.0_

* Launch CMake.
* Type _C:\src\VTK-8.90_ in the "Source code" text box.
* Type _C:\src\VTK-8.90-build_ in the "Where to build the binaries" text box.
* Click the Configure button
* Set VTK_GROUP_ENABLE_QT to YES to Qt5_DIR to Qt\5.13.0\clang_64\libcmake\Qt5
* Set the CMAKE_BUILD_TYPE to Release
* Set CMAKE_INSTALL_PREFIX to C:\vtk-8.90\
* Click the Configure button again and verify that all the QT variables have been properly detected/configured
* Click Generate, which will create your MSVC project files

Launch Microsoft Visual Studio C++ 2019 and open VTK.sln from _C:\src\vtk-8.90_ . Change the "Debug" build option to "Release" and select "x64" to build 64-bit libraries.
Right-click "ALL-BUILD" to build the VTK libraries. After that is finished, right click "INSTALL" and select Build to install the DLLs in C:\vtk\8.90.

Add _C:\VTK-8.90\bin_ to your system PATH.

## Install OpenCV

[Download](https://opencv.org/releases/) and install the latest version of precompiled OpenCV libraries. 

Unpack the compressed file to C:\src

Add _C:\src\opencv\build\x64\vc15\bin_ to your PATH.

## Install MATLAB Runtime

If you want to be able to load .mat files, you'll need Matlab's C API, which is made up of some DLLS and associated headers (and if using MSVC also import libraries). 

Even if you don't have a MATLAB license, you can [download](https://www.mathworks.com/products/compiler/matlab-runtime.html) and install the free runtime. 

Let's say the installation was made to _C:\Program Files\MATLAB_.

Add the _C:\Program Files\MATLAB\MATLAB Runtime\v97\bin\win64_ to your PATH.

## Compile TIFF library

Launch the x64 Command Prompt for VS 2019.

Type the following to build the TIFF library:
```
cd C:\src\neurotator\third-party\tiff
mkdir build
nmake /f Makefile.vc all DEBUG=1
copy libtiff\libtiff.dll build\libtiffd.dll
copy libtiff\libtiff.lib build\libtiffd.lib
nmake /f Makefile.vc clean
nmake /f Makefile.vc all
move libtiff\libtiff.dll build 
move libtiff\libtiff.lib build
cd tools
nmake /f Makefile.vc
```

Add the _C:\src\neurotator\third-party\tiff\build_ to your PATH.

## Compile G3Log library

Launch the x64 Command Prompt for VS 2017.

Type the following to build the G3Log library:
```
cd C:\src\neurotator\third-party\g3log
mkdir build
cd build
cmake -G "Visual Studio 16 2019" -A x64 -DUSE_G3LOG_UNIT_TEST=ON -DCMAKE_BUILD_TYPE=Release -DCHANGE_G3LOG_DEBUG_TO_DBUG=ON ..
msbuild g3log.sln /p:Configuration=Release /p:Platform=x64
move release\g3logger.dll .
move release\g3logger.lib .
cmake -G "Visual Studio 16 2019" -A x64 -DUSE_G3LOG_UNIT_TEST=ON -DCMAKE_BUILD_TYPE=Debug -DCHANGE_G3LOG_DEBUG_TO_DBUG=ON ..
msbuild g3log.sln /p:Configuration=Debug /p:Platform=x64
move debug\g3logger.dll .\g3loggerd.dll
move debug\g3logger.lib .\g3loggerd.lib
```

Add _C:\src\neurotator\third-party\g3log\build\_ to your PATH

## Install Qt

[Download](https://www.qt.io/download) the latest version of Qt, opting for the _Open Source_ edition. Include
* Qt 5.13.1: Tick the MSVC 2017 64 bit, Sources, and all the options starting with Qt
* Developer and Designer Tools: including Qt Creator, Debugging Tools, Qt 3D Studio 2.4 and Qt Installer Framework

We'll assume that the installation was made to _C:\Qt_

When you first Launch Qt Creator, select _Manage Kits_ and make sure there is a kit named _Desktop Qt 5.13.1 MSVC2017 64bit_. Under the C++ Compiler, make sure _Microsoft Visual C++ Compiler .. (amd64)_ is selected.
If not, create a custom ABI with the following options: amd64, x86, windows, msvc2017, pe, 64bit.

Add C:\Qt\5.13.1\msvc2017_64\bin to your PATH.

## Compile Neurotator

When you first Launch Qt Creator, open the project C:/src/neurotator/neurotator.pro
Click Projects, and check your Build settings. Make sure the "Enable Qt Quick Compiler" is enabled for the release configuration.

After you build neurotator.exe, you will want to organize all the necessary Qt libraries (DLLs) so you can ship them together.
Open a command prompt window, change directory to where you built neurotator.exe and run the following command:
```
C:\Qt\5.13.1\msvc2017_64\bin\windeployqt --release  --compiler-runtime --force  neurotator.exe --verbose=3 --qmldir=C:\Qt\5.13.1\msvc2017_64\qml
```


---

# Build for Mac OSX

The project also builds on Mac OSX.

Install the [Matlab Runtime](https://www.mathworks.com/products/compiler/matlab-runtime.html) and [Qt](https://www.qt.io/download) from the respective vendors.

Install [CMake](https://cmake.org/download/) and use it to configure, compile and install [VTK-8.2](https://vtk.org/download/) and [OpenCV](https://opencv.org/releases/), in that order, using the default installation target _/usr/local_. Be sure to enable the following options in CMake:

* For VTK 
	* Enable VTK\_GROUP\_QT
	* Set QT_DIR to /your-install/Qt/5.13.0/clang\_64/lib/cmake/Qt5
* For OpenCV
	*  Enable BUILD\_opencv\_world

As described above, compile g3log from _neurotator/third-party_ and install to /usr/local/ as well.


---

# NOTES

If changing the .pro file, you should re-run qmake (from Qt Creator: Build->Run QMake) to validate changes.
	
----

## BELOW IS UNCHECKED - TO BE REVIEWED

- [optional] If you want to use the debugger, install Windows SDK for your OS (https://developer.microsoft.com/en-us/windows/downloads/sdk-archive)

- [optional] Install MSYS if you want to use gcc compiler on Windows instead of VS
## Creating the installer (after building)

### Windows

	In a command prompt:
	
	Change dir to Qt location, and then in the target configuration, e.g. msvc2017_64, run the windeployqt utility. Replace [destination directory] with the path to an empty folder for it to copy all the necessary binary files to.
	
	C:\Qt\5.11.2\msvc2017_64\bin>windeployqt.exe --release --qmldir "[path to source code]\neurotator\gui\qml"  "[destination directory]"

	Then copy the remaining third-party DLLs from the neurotator source tree and the MSVC distributable dlls.