QT += qml quick widgets
CONFIG += c++14
QT += quickcontrols2

QMAKE_CXXFLAGS_RELEASE  += -O2
QMAKE_CXXFLAGS_DEBUG  += -D_DEBUG

MATLAB_VERSION=v99
VTK_VER=9.0

#QMAKE_CXXFLAGS+= -openmp
#QMAKE_LFLAGS +=  -openmp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
#10.0.18362.0
INCLUDEPATH += "C:\Program Files (x86)\Windows Kits\10\Include\10.0.16299.0\ucrt"
LIBS += -L"C:\Program Files (x86)\Windows Kits\10\Lib\10.0.16299.0\ucrt\x64"

# Neurotator specific defines
DEFINES += MATLAB_SUPPORT

INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/gui

HEADERS += \
    include/PersistenceInterface.hpp \
    include/Project.hpp \
    include/ProjectDense.hpp \
    include/ProjectRegistration.hpp \
    include/ProjectAnnotationDense.hpp \
    include/ProjectAnnotationSparse.hpp \
    include/AppSettings.hpp \
    include/HyperstackTiff.hpp \
    include/Hyperstack.hpp \
    include/RegisterRigid3D.h \
    include/Render.hpp \
    include/RoiDataDense.hpp \
    include/RoiDataModel.hpp \
    include/RoiDataSparse.hpp \
    include/RoiDataSparseModel.hpp \
    include/RoiDataBase.hpp \
    include/ImagePipeline.hpp \
    include/Voxels.hpp \
    include/VtkHyperTiff.hpp \
    include/WatershedImagePipeline.hpp \
    include/SearchResults.hpp \
    include/LandmarkModel.hpp \
    gui/ApplicationData.hpp \
    gui/SliceImage.hpp \
    gui/SliceOverlay.hpp \ 

SOURCES += \
    gui/main.cpp \
    src/Project.cpp \
    src/ProjectDense.cpp  \
    src/ProjectRegistration.cpp \
    src/ProjectAnnotationDense.cpp \
    src/ProjectAnnotationSparse.cpp \
    gui/ApplicationData.cpp \
    gui/SliceImage.cpp \
    src/HyperstackTiff.cpp \
    src/Hyperstack.cpp \
    src/AppSettings.cpp \
    src/RegisterRigid3D.cpp \
    src/Render.cpp \
    src/RoiDataDense.cpp \
    src/RoiDataModel.cpp \
    src/RoiDataBase.cpp \
    src/RoiDataSparse.cpp \
    src/RoiDataSparseModel.cpp \
    gui/SliceOverlay.cpp \
    src/VtkHyperTiff.cpp \
    src/WatershedImagePipeline.cpp \
    src/LandmarkModel.cpp \

contains(DEFINES, MATLAB_SUPPORT) {
    SOURCES +=     src/MatFile.cpp     src/MatFileDenseRoi.cpp src/MatFileSparseRoi.cpp
    HEADERS +=     include/MatFile.hpp include/MatFileDenseRoi.hpp include/MatFileSparseRoi.hpp
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

contains(DEFINES, MATLAB_SUPPORT) {
    win32 {
      INCLUDEPATH += C:/"Program Files"/MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/extern/include
      LIBS += -LC:/"Program Files"/MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/extern/lib/win64/microsoft -llibmat -llibmx
      LIBS += -LC:/"Program Files"/MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/bin/win64
      DEPENDPATH += C:/"Program Files"MATLAB/"MATLAB Runtime"/$${MATLAB_VERSION}/extern/include
    }
    macx {
      INCLUDEPATH += "/Applications/MATLAB/MATLAB_Runtime/$${MATLAB_VERSION}/extern/include"
      LIBS += -L"/Applications/MATLAB/MATLAB_Runtime/$${MATLAB_VERSION}/bin/maci64" -lmat -lmx
      DEPENDPATH += "/Applications/MATLAB/MATLAB_Runtime/$${MATLAB_VERSION}/extern/include"

      # Set DYLD_FRAMEWORK_PATH to include /Applications/MATLAB/MATLAB_Runtime/v97/bin/maci64
      # Set DYLD_LIBRARY_PATH to include /Applications/MATLAB/MATLAB_Runtime/v97/bin/maci64
    }
}

#message(Qt is installed in $$[QT_INSTALL_PREFIX])
#INCLUDEPATH += $$[QT_INSTALL_PREFIX]/../Src/qtimageformats/src/3rdparty/libtiff/libtiff
#DEPENDPATH += $$[QT_INSTALL_PREFIX]/../Src/qtimageformats/src/3rdparty/libtiff/libtiff
#LIBS += -L$$[QT_INSTALL_PREFIX]/plugins/imageformats
#CONFIG(release, debug|release): LIBS += -L$$[QT_INSTALL_PREFIX]/lib -lQt5Gui
#else:CONFIG(debug, debug|release): LIBS += -L$$[QT_INSTALL_PREFIX]/lib -lQt5Guid


win32 {
    INCLUDEPATH += $$PWD/third-party/tiff/libtiff
    DEPENDPATH += $$PWD/third-party/tiff/libtiff
    LIBS += -L$$PWD/third-party/tiff/build
    CONFIG(release, debug|release): LIBS += -llibtiff
    else:CONFIG(debug, debug|release): LIBS += -llibtiffd
}
macx {
    INCLUDEPATH += /usr/local/include/vtk-$$VTK_VER/vtktiff/libtiff
    DEPENDPATH += /usr/local/include/vtk-$$VTK_VER/vtktiff
    LIBS +=  -L/usr/local/lib/ -lvtktiff-$$VTK_VER    -ljpeg

    # Set DYLD_FRAMEWORK_PATH to include /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/
    # Set DYLD_LIBRARY_PATH to include /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/
}


win32 {
}
macx {
    INCLUDEPATH += /usr/local/include
}

win32 {
    INCLUDEPATH += c:/opencv/build/include
    DEPENDPATH += c:/opencv/build/include
    CONFIG(release, debug|release): LIBS += -Lc:/opencv/build/x64/vc15/lib -lopencv_world450
    else:CONFIG(debug, debug|release): LIBS += -LC:/opencv/build/x64/vc15/lib -lopencv_world450d
    LIBS += -Lc:/opencv/build/x64/vc15/bin
}
macx {
    INCLUDEPATH += /usr/local/include/opencv4/
    LIBS += -lopencv_world
}

# vtk
win32 {
    INCLUDEPATH +=  "C:/VTK-$$VTK_VER/include/vtk-$$VTK_VER"
    LIBS += -L"C:/VTK-$$VTK_VER/lib"
    LIBS += -L"C:/VTK-$$VTK_VER/bin"
}
macx {
    INCLUDEPATH +=  "/usr/local/include/vtk-$$VTK_VER"
}

 LIBS += -lvtkCommonTransforms-$$VTK_VER -lvtkCommonCore-$$VTK_VER -lvtkRenderingQt-$$VTK_VER -lvtkCommonExecutionModel-$$VTK_VER \
         -lvtkCommonDataModel-$$VTK_VER -lvtkImagingGeneral-$$VTK_VER -lvtkImagingCore-$$VTK_VER -lvtkRenderingCore-$$VTK_VER \
         -lvtkIOImage-$$VTK_VER -lvtkFiltersCore-$$VTK_VER


DISTFILES += \
    gui/qml/ColorEditor.qml \
    gui/qml/SparseGroupWindow.qml \
    gui/qml/main.qml \
    gui/qml/SliceEdit.qml \
    gui/qml/SliceImageFrame.qml \
    gui/qml/SmallButton.qml \
    gui/qml/HorDivider.qml \
    gui/qml/RoiTable.qml \
    gui/qml/RoiSparseTable.qml \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_black_18dp.png \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_black_24dp.png \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_white_18dp.png \
    third-party/material-design-icons-master/navigation/drawable-ldrtl-mdpi/ic_arrow_back_white_24dp.png \
    third-party/material-design-icons-master/action/1x_web/ic_explore_white_18dp.png \
    gui/qml/RoiTableCell.qml \
    gui/qml/RoiTableHeader.qml \
    gui/qml/TinyColorPicker.qml \
    gui/qml/TinySlider.qml \
    gui/qml/SmallSpinBox.qml \
    gui/qml/SmallMenu.qml \
    gui/qml/SliceWindow.qml \
    gui/qml/FindRoi.qml \
    gui/qml/ToolStatePanel.qml \
    gui/qml/LandmarkTable.qml \
    gui/qml/LandmarkTableCell.qml \

