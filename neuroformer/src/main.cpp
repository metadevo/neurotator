//
// Created by Guy Shechter on 11/18/19.
//

#include <QtCore>

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
#include "Project.hpp"
#include "ProjectRegistration.hpp"
#include "HyperstackTiff.hpp"
#include "RoiDataModel.hpp"
#include <vtkAbstractTransform.h>

int main(int argc, char **argv)
{
    qDebug("Neuroformer v2.1.6");
    MAKE_PROTOTYPE(registration, ProjectRegistration)

    if (argc != 2) {
        qCritical() << "Please provide json config file as command line argument" << endl;
        exit(1);
    }

    vtkSmartPointer <vtkAbstractTransform> xform = nullptr;

    qDebug() << "Loading: " << argv[1];

    QFile loadFile(argv[1]);
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Couldn't open config file: " << loadFile.errorString();
        exit(1);
    }
    QFileInfo configFileInfo(loadFile);

    QByteArray saveData = loadFile.readAll();
    QJsonDocument json(QJsonDocument::fromJson(saveData));

    QString projectFile(json["project_file"].toString());


    bool useRawImageStack=false;
    if (json.object().contains("useRawImageStack")) {
        useRawImageStack = json["useRawImageStack"].toBool();
    }

    QString inputImageName(json["image_stack"].toString());
    QString inputImageNameRaw;
    if (useRawImageStack) {
        qDebug() << "Running in RAW STACK MODE";
        inputImageNameRaw = json["image_stack_raw"].toString();
    } else {
        qDebug() << "Running in TIFF STACK MODE";
    }

    QJsonArray jsonScale = json["scale"].toArray();
    const int scale[3] = {jsonScale[0].toInt(), jsonScale[1].toInt(), jsonScale[2].toInt()};
    QUrl url = QUrl::fromLocalFile(projectFile);

    QJsonArray padToTargeSize;
    int *paddedSize = nullptr;
    if (json.object().contains("padToTargetSize")) {
        padToTargeSize = json["padToTargetSize"].toArray();
        qDebug() << "Output will be padded to size: " << padToTargeSize;
        paddedSize = new int(3);
        for (int i =0; i < 3; i++)
            paddedSize[i] = padToTargeSize[i].toInt();
    }

    qDebug() << "Trying to load transformation and stack info from " << projectFile;

    Project *project = Project::makeProjectFromUrl(url, true, !useRawImageStack);
    if (!project) {
        QFileInfo q(projectFile);
        qDebug() << "Looking locally for " << q.fileName();
        project = Project::makeProjectFromUrl(QUrl::fromLocalFile(configFileInfo.path() + "/" + q.fileName()), true, !useRawImageStack);
    }

    QJsonObject registrationHints = dynamic_cast<ProjectRegistration*>(project)->getRegistrationHints();


    for (auto n : dynamic_cast<ProjectRegistration*>(project)->stackNames()) {

        int preRotation = dynamic_cast<ProjectRegistration*>(project)->getStackPreRotation(n);
        dynamic_cast<ProjectRegistration*>(project)->cachePreRotation(n, preRotation);
    }
    dynamic_cast<ProjectRegistration*>(project)->updateTransforms();
    HyperstackSize outputSize;
    for (auto n : dynamic_cast<ProjectRegistration*>(project)->stackNames()) {
        qDebug() << "Iterating over stack: " << n;
        if (n != dynamic_cast<ProjectRegistration*>(project)->fixedSessionName()) {
            qDebug() << " -> Transforming " << n;


            xform = dynamic_cast<ProjectRegistration*>(project)->getTransform(n);

            if (registrationHints.empty()) {
                outputSize = dynamic_cast<ProjectRegistration*>(project)->getStack(dynamic_cast<ProjectRegistration*>(project)->fixedSessionName())->size();
            } else {
                outputSize.x = static_cast<unsigned int>(registrationHints["fixed"].toArray()[0].toInt());
                outputSize.y = static_cast<unsigned int>(registrationHints["fixed"].toArray()[1].toInt());
                outputSize.z = static_cast<unsigned int>(registrationHints["fixed"].toArray()[2].toInt());
            }
            qDebug() << "Output Size: " << outputSize.x << ", " << outputSize.y << ", " << outputSize.z;

            HyperstackTiff *inputTiff = new HyperstackTiff();

            qDebug() << "Loading source data: " << inputImageName;

            if (! inputTiff->loadStreamingFast(inputImageName, scale) ) {
                QFileInfo sourceFile(inputImageName);
                qDebug() << "Looking locally for " << sourceFile.fileName();
                inputImageName = QUrl::fromLocalFile(configFileInfo.path() + "/" + sourceFile.fileName()).toLocalFile();
                if (! inputTiff->loadStreamingFast(inputImageName, scale)) {
                    qCritical() << "Couldn't find input image";
                    return 0;
                }
            }

            qDebug() << "Source data loaded...";
            if (useRawImageStack) {
                QFileInfo raw(inputImageNameRaw);
                inputImageNameRaw = QUrl::fromLocalFile(configFileInfo.path() + "/" + raw.fileName()).toLocalFile();
                inputTiff->saveTransformedSlices(inputImageNameRaw, xform, outputSize, scale, paddedSize, useRawImageStack);
            } else {
                inputTiff->saveTransformedSlices(inputImageName, xform, outputSize, scale, paddedSize, useRawImageStack);
            }
        }
    }
    qDebug() << "Done";
    return 0;
}

