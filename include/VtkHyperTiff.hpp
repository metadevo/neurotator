#ifndef VTKHYPERTIFF_HPP
#define VTKHYPERTIFF_HPP

#include <vtkThreadedImageAlgorithm.h>

class vtkHyperTiffFilter:public vtkThreadedImageAlgorithm {
public:
    static vtkHyperTiffFilter *New();

#define _vtkSetMacro(name, type)                                                                    \
  virtual void Set##name(type _arg)                                                                \
  {                                                                                                \
    if (this->name != _arg)                                                                        \
    {                                                                                              \
      this->name = _arg;                                                                           \
      this->Modified();                                                                            \
    }                                                                                              \
  }
//
// Get built-in type.  Creates member Get"name"() (e.g., GetVisibility());
//
#define _vtkGetMacro(name, type)                                                                    \
  virtual type Get##name()                                                                         \
  {                                                                                                \
    return this->name;                                                                             \
  }


    vtkTypeMacro(vtkHyperTiffFilter, vtkThreadedImageAlgorithm)
    _vtkSetMacro(NumberOfChannels, vtkTypeInt8)

  //@{
    /**
     * Set the desired output scalar type to cast to
     */
    _vtkSetMacro(OutputScalarType, vtkTypeInt8)
    _vtkGetMacro(OutputScalarType, vtkTypeInt8)
    void SetOutputScalarTypeToDouble() { this->SetOutputScalarType(VTK_DOUBLE); }
    void SetOutputScalarTypeToFloat() { this->SetOutputScalarType(VTK_FLOAT); }
    void SetOutputScalarTypeToLong() { this->SetOutputScalarType(VTK_LONG); }
    void SetOutputScalarTypeToUnsignedLong() { this->SetOutputScalarType(VTK_UNSIGNED_LONG); }
    void SetOutputScalarTypeToInt() { this->SetOutputScalarType(VTK_INT); }
    void SetOutputScalarTypeToUnsignedInt() { this->SetOutputScalarType(VTK_UNSIGNED_INT); }
    void SetOutputScalarTypeToShort() { this->SetOutputScalarType(VTK_SHORT); }
    void SetOutputScalarTypeToUnsignedShort() { this->SetOutputScalarType(VTK_UNSIGNED_SHORT); }
    void SetOutputScalarTypeToChar() { this->SetOutputScalarType(VTK_CHAR); }
    void SetOutputScalarTypeToSignedChar() { this->SetOutputScalarType(VTK_SIGNED_CHAR); }
    void SetOutputScalarTypeToUnsignedChar() { this->SetOutputScalarType(VTK_UNSIGNED_CHAR); }
    //@}

protected:

    vtkHyperTiffFilter();
    ~vtkHyperTiffFilter() override {}
    vtkTypeInt8 NumberOfChannels;
    vtkTypeInt8 OutputScalarType;

    void ThreadedRequestData(vtkInformation *request,
                             vtkInformationVector **inputVector,
                             vtkInformationVector *outputVector,
                             vtkImageData ***inData,
                             vtkImageData **outData,
                             int outExt[6],
                             int id) override;
    int RequestInformation(vtkInformation *,
                           vtkInformationVector **,
                           vtkInformationVector *) override;
    int RequestUpdateExtent(vtkInformation *,
                            vtkInformationVector **,
                            vtkInformationVector *) override;
    void InternalRequestUpdateExtent(int *inExt, int *outExt);

private:
    vtkHyperTiffFilter(const vtkHyperTiffFilter &);  // Not implemented.
    void operator=(const vtkHyperTiffFilter &);  // Not implemented.
};

#endif // VTKHYPERTIFF_HPP
