/// @author Sam Kenyon <sam@metadevo.com>
#ifndef PERSISTENCEINTERFACE_H
#define PERSISTENCEINTERFACE_H

#include <memory>
#include <string>
#include <QString>

class PersistenceInterface;

class PersistenceFactory
{
public:
    PersistenceFactory() = delete;

    static std::shared_ptr<PersistenceInterface> create(const std::string& filepath);

};

class PersistenceInterface
{
public:
    PersistenceInterface() {}
    virtual ~PersistenceInterface() {}

    virtual bool load(const std::string& filepath) = 0;
    virtual bool save(const QString& sessionKey, const std::string& filepath) = 0;
};

#endif // PERSISTENCEINTERFACE_H
