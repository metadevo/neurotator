/// @author Sam Kenyon <sam@metadevo.com>
#ifndef SLICESTACKTIFF_HPP
#define SLICESTACKTIFF_HPP

#include <QImage>
#include <tiffio.h>

#include <vtkImageReslice.h>
#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <vtkExtractVOI.h>
#include <vtkTIFFReader.h>
#include <vtkWindowLevelLookupTable.h>
#include <vtkImageMapToColors.h>
#include <vtkImageBlend.h>
#include <vtkImageAppendComponents.h>
#include <vtkImageConstantPad.h>
#include <vtkImageMapToColors.h>
#include <vtkImageExtractComponents.h>
#include <vtkImageSlab.h>
#include "VtkHyperTiff.hpp"
#include "Hyperstack.hpp"

class vtkAbstractTransform;

struct windowLevel {
    int32 windowMin = INT_MAX, windowMax = -INT_MAX;
    int32 intensityMin = INT_MAX, intensityMax = -INT_MAX;
    bool active = true;
};

struct ImageJMetadata {
    bool imageJ = false;
    QString version;
    bool hyperstack = false;
    bool modeComposite = false;
    unsigned int images = 0;
    unsigned int slices = 0;
    unsigned int frames = 0;
    unsigned int channels = 0;
};

//typedef double VoxelType;
typedef unsigned char VoxelType;
struct VolumeBuffer
{
    VoxelType *  buffer;
    HyperstackSize size;

    VolumeBuffer(unsigned int sX, unsigned int sY, unsigned int sZ, unsigned int sT = 1, unsigned int c = 3)
    {
        size = {sX, sY, sZ, sT, c, 0, 0};
        buffer = new VoxelType[size.x*size.y*size.z*size.c];
    };

    VolumeBuffer(int size, int c = 3):VolumeBuffer(size, size, size, 1, c){};

    ~VolumeBuffer()
    {
        delete [] buffer;
    }

    inline VoxelType * ptr(int x = 0, int y = 0, int z = 0)
    {
        return buffer + (x + y*size.x + z*size.x*size.y)*size.c;
    };

    inline const VoxelType * c_ptr(int x = 0, int y = 0, int z = 0) const
    {
        return (const VoxelType *)(buffer + ((x + y*size.x + z*size.x*size.y)*size.c)*sizeof(VoxelType));
    };

    int dumpToFile(QString path, int len = 1)
    {
        return dumpToFile(path, buffer, size.x*size.y*size.c*len*sizeof(VoxelType));
    }

    static int dumpToFile(QString path, VoxelType* ptr, int size);

    enum ChannelReductionPolicy
    {
        AVERAGE,
        MAX_INTENSITY
    };
};

typedef vtkSmartPointer <vtkWindowLevelLookupTable> SmartColorLookupTablePtrs;

/// Handle stacks and hyperstacks stored in TIFF files including ImageJ TIFFs.
/// Indices are all zero-based in this class.

// NOTE: this class is in serious need of refactoring.
// Originally it kept track of the current index (z/time, although the time
// part was not used), and loaded individual slices on demand. Now all the
// slices are loaded up front into a single vtkImageData.

// Currently SliceImage keeps track of the current Z, and the index here is
// mostly ignored. But that may not be the best design. We either need to
// move all the state into SliceImage and remove the index entirely, or move
// it all back here.
class HyperstackTiff:public Hyperstack {
public:
    HyperstackTiff();
    virtual ~HyperstackTiff();

    virtual bool ready() const { return m_ready; }
    virtual bool loadStreaming(const QString filePath, const bool lazyLoad = false);
    virtual bool loadStreamingFast(const QString filePath, const int *scale);
    virtual bool loadStreamingMultipleTiffs(const QVector<QString> filePaths);
    virtual unsigned int numImages() const { return m_numImages; }
    virtual HyperstackSize size() const;
    virtual QString name() const { return m_name; }

    virtual vtkSmartPointer <vtkImageData> getImageData(int z, vtkAbstractTransform *xform,
                                                        HyperstackSize size = HyperstackSize());


    virtual vtkSmartPointer <vtkImageData> getImageData(vtkAbstractTransform *xform,
                                                        HyperstackSize size, const VoxelCoordinate& pOrigin, int zoff);

    virtual QImage imageBuffer(int z,
                               vtkAbstractTransform *xform = nullptr,
                               HyperstackSize size = HyperstackSize());

    virtual QImage imageBufferSparse(int zLower, int zUpper, HyperstackSize size = HyperstackSize());

    virtual bool saveTransformedSlices(const QString outputFileName,
                                 vtkSmartPointer<vtkAbstractTransform> xform = nullptr,
                                 HyperstackSize size = HyperstackSize(),
                                 const int *scale = nullptr, const int *paddedSize = nullptr,
                                 bool useRawImageStack = false);

    QColor maxColorValue() const { return QColor(m_maxColorValue); }
    HyperstackIndex index() const { return m_index; }
    void addClient() { m_clients++; }
    void removeClient() { m_clients--; }
    unsigned int clients() const { return m_clients; }
    void updateChannelColormap(std::vector <QColor> newColormap);
    virtual std::vector<int> getScalarRangeForChannel(unsigned int channelIndex);
    void updateWindowLevelForChannel(unsigned int c, std::vector<int> wl);
    virtual std::vector<int> getWindowLevelForChannel(unsigned int channelIndex);
    void toggleChannelState(unsigned int c, bool state);
    int numActiveChannel();
    bool getChannelState(unsigned int c) { return channelWindowLevel[c].active; }

    void retrieveSubVolume(VolumeBuffer &subVolume, const VoxelCoordinate& p0,
                           vtkAbstractTransform *xform, const HyperstackSize& cWorldSize, int channelPolicy = VolumeBuffer::MAX_INTENSITY);

private:
    QImage loadSlice(const HyperstackIndex index);
    void cleanup();
    static void cleanupBufferHandler(void *info);
    QImage loadRasterBuffer(TIFF *tif);
    bool loadMetadata(TIFF *tif);

    static unsigned int countPages(TIFF *tif);
    static ImageJMetadata readImageJMetadata(TIFF *tif);
    static int getMetadataInt(QString name, QStringList metatags);
    static QString getMetadataStr(QString name, QStringList metatags);
    static void isolateChannelRGB(uint32 *buffer, const RGBA_Channel channel, const unsigned int width, const unsigned int height);
    static uchar mergeBuffersRGB(uint32 **buffers, const unsigned int size);
    static void mergeBuffersRGBQuick(uint32 **buffers, const unsigned int size);
    static uchar mergeBuffersRGBA(uint32 **buffers, unsigned int pixelSize);

    bool m_initted = false;
    bool m_ready = false;
    unsigned int m_numImages = 0; // aka number of TIFF pages
    bool m_hyperstack = false;
    HyperstackSize m_size;
    TIFF *m_tif = nullptr;
    QString m_name;
    HyperstackIndex m_index;
    QRgb m_maxColorValue;
    uchar m_maxColor = 0;
    unsigned int m_maxHeapImages = 1;
    unsigned int m_clients = 0;
    uint16 m_photometric = 0;

    vtkSmartPointer <vtkImageReslice> m_slicer = nullptr;
    vtkSmartPointer <vtkTIFFReader> m_reader = nullptr;
    std::vector< vtkSmartPointer <vtkTIFFReader>> m_sparse_readers;
    std::vector< vtkSmartPointer <vtkImageSlab>> m_imageSlabs;
    vtkSmartPointer <vtkHyperTiffFilter> m_hyperTiff = nullptr;
    vtkSmartPointer <vtkImageConstantPad> m_paddedImage = nullptr;
    vtkSmartPointer <vtkImageAlgorithm> m_imageSrc = nullptr;

    std::vector <vtkSmartPointer <vtkHyperTiffFilter>> m_hyperTiffs;
    std::vector <vtkSmartPointer <vtkImageAlgorithm>> m_imageSrcs;

    std::vector <vtkSmartPointer<vtkImageMapToColors>> m_mapColors;
    std::vector <vtkSmartPointer<vtkImageExtractComponents>> m_extractors;

    std::vector <QColor> channelColormap;
    std::vector <windowLevel> channelWindowLevel;
    std::vector <SmartColorLookupTablePtrs> colorTransferFunctions;

    void initializeWindowLevel();
    void scanImageForIntensityRange();
    void updateColorTransferFunctions();
    size_t getFilesize(const char* filename);

};

#endif // SLICESTACKTIFF_HPP
