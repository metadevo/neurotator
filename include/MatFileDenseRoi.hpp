#ifndef MATFILEDENSEROI_HPP
#define MATFILEDENSEROI_HPP

#include "MatFile.hpp"
#include "RoiDataDense.hpp"

/// Load/Save Matlab data files
class MatFileDenseRoi : public MatFile
{
public:
    explicit MatFileDenseRoi(RoiDataDense *roiData);
    virtual ~MatFileDenseRoi();

protected:
    bool examine() override;
    bool loadCustomData() override;
    bool saveCustomData() override;

};

#endif // MATFILEDENSEROI_HPP

