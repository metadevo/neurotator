#ifndef ROIDATASPARSEMODEL_HPP
#define ROIDATASPARSEMODEL_HPP

#include <QAbstractTableModel>
#include <QStringList>
#include <QDebug>

#include "RoiDataSparse.hpp"


class RoiDataSparseModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    RoiDataSparseModel() {}
    RoiDataSparseModel(RoiDataSparse* roiData);
    virtual ~RoiDataSparseModel();
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    int columnCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    void beginReset() { beginResetModel(); }
    void endReset() { beginResetModel(); }
    void resetModel() {beginResetModel(); endResetModel();}

    bool rowChanged();

    Q_INVOKABLE void setSelection(int index) {
        m_selected.clear();
        if(index < rowCount()){
            m_selected.insert(index);
            m_selected.insert(index + rowCount());
        }else{
            m_selected.insert(index);
            m_selected.insert(index - rowCount());
        }
        emit selectionChanged();
    }

    Q_INVOKABLE bool isSelected(int index) const {
        return m_selected.find(index) != m_selected.end();
    }

    Q_INVOKABLE void setHovered(int index) {
        m_hovered.clear();
        if(index < rowCount()){
            m_hovered.insert(index);
            m_hovered.insert(index + rowCount());
        }else{
            m_hovered.insert(index);
            m_hovered.insert(index - rowCount());
        }
        emit hoveredChanged();
    }

    Q_INVOKABLE void emptyHovered(){
        m_hovered.clear();
        emit hoveredChanged();
    }

    Q_INVOKABLE bool isHovered(int index) const {
        return m_hovered.find(index) != m_hovered.end();
    }

signals:
    void selectionChanged();
    void hoveredChanged();

protected:
    RoiDataSparse* m_roiData;
    QSet<int> m_selected;
    QSet<int> m_hovered;
    QVariant dataAt(const int row, int col) const;

};

#endif
