/// @author Sam Kenyon <sam@metadevo.com>
#ifndef ROIDATADENSE_HPP
#define ROIDATADENSE_HPP

#include <memory>
#include <vector>
#include <QDebug>
#include <QHash>
#include <QString>
#include <QVector>

#include "Hyperstack.hpp"
#include "RoiDataBase.hpp"


class SearchResult;
class LandmarkModel;

const session_key_t TRANSFORMED_SESSION(".transformed.");

struct RoiAddress
{
    QString sessionKey;
    QString roiKey;
};


/// Data for a single 3D region of interest
class RoiFieldsDense : public RoiFields
{
public:
    bool confirmed() override { return b_confirmed; }
    void setConfirmed(bool x) override { b_confirmed = x; }

    bool selected() override { return b_selected; }
    void setSelected(bool x) override { b_selected = x;
                                      }
    bool flagged() override { return b_flagged; }
    void setFlagged(bool x) override { b_flagged = x; }

    bool filtered() override { return b_filtered; }
    void setFiltered(bool x) override { b_filtered = x; }

    bool fullyLinked() override { return b_fullyLinked; }
    void setFullyLinked(bool x) override { b_fullyLinked = x; }

    QString linkedName() override { return s_linkedName; }
    void setLinkedName(QString x) override { s_linkedName = x; }
    void clearLinkedName() override { s_linkedName.clear(); }

    QString linkedSession() override { return s_linkedSession; }
    void setLinkedSession(QString x) override { s_linkedSession = x; }
    void clearLinkedSession() override { s_linkedSession.clear(); }

    void clearLinkedIn() override { linkedIn.clear(); }
    bool linkedInEmpty() override { return linkedIn.empty(); }
    bool linkedInSize() override { return linkedIn.size(); }

    void addToMask(VoxelCoordinate v) override { mask.push_back(v); }
    void setMask(QVector<VoxelCoordinate> qv) override { mask = qv; }

    void setLinkedIn(QString s, std::shared_ptr<RoiFields> r) override { linkedIn[s] = std::dynamic_pointer_cast<RoiFieldsDense>(r); }
    void removeLinkedIn(QString s) override { linkedIn.remove(s); }
    bool emptyLinkedIn() override { return linkedIn.empty(); }
    int sizeLinkedIn() override { return linkedIn.size(); }
    bool linkedInContains(QString s) override{ return linkedIn.contains(s); }
    std::weak_ptr<RoiFields> getLinkedIn(QString s) override { return linkedIn[s]; }

    QHash<session_key_t, std::weak_ptr<RoiFieldsDense>>::iterator findLinkedIn(QString s) { return linkedIn.find(s); }
    QHash<session_key_t, std::weak_ptr<RoiFieldsDense>>::iterator endLinkedIn() { return linkedIn.end(); }


    RoiMask& getMaskByZ(const unsigned int z) override { return masks[z]; }
    QVector<VoxelCoordinate> mask;  // temporary, cleared after loading
    QHash<z_key_t, RoiMask> masks;

protected:
    QString s_linkedName;
    QString s_linkedSession;
    QStringList candidates;
    bool b_confirmed = false;
    bool b_flagged = false;
    QHash<session_key_t, std::weak_ptr<RoiFieldsDense>> linkedIn;
    bool b_fullyLinked = false; // for ref session, are all other sessions linked in
    bool b_selected = false;
    bool b_filtered = false;
};

struct LinkCount
{
    LinkCount(int i, int c) : index(i), count(c) {}
    int index = 0;
    int count = 0;
};

/// Manages regions of interest for multiple sessions
class RoiDataDense : public RoiDataBase
{
public:

    struct IndexedRoi
    {
        int index = 0;
        RoiDataDense::roi_t roi;
    };

    RoiDataDense();
    ~RoiDataDense();

    bool dummy() override { return true;}

    bool addNewRoi(roi_key_t name, roi_t fields) override;
    bool addNewRoi(session_key_t session, roi_key_t name, roi_t fields);
    bool renameRoi(session_key_t session, roi_key_t name, roi_key_t newName);
    bool deleteRoi(session_key_t session, roi_key_t roiKey);
    unsigned int deleteFlagged(const QString& sessionKey);
    unsigned int deleteFlagged();
    void setRefSession(const session_key_t& sessionKey);
    void clearRefSession();
    int modifyRoiLink(RoiAddress& src, RoiAddress& dest);
    bool removeRoiLink(RoiAddress& src);
    void refreshRefLinks();
    void setSelected(roi_t roi);
    bool modifyMask(RoiMask& mask, const int x, const int y);
    void mergeToFirst(session_key_t session, const QList<RoiDataBase::roi_t>& rois);
    void split(session_key_t session, roi_t src, roi_t dest);

    unsigned int filterHighPassRadius(const QString& sessionKey, const int minRadius);
    unsigned int filterHighPassVolume(const QString& sessionKey, const int minVolume);
    unsigned int flagFiltered(const QString& sessionKey);
    unsigned int unflagAll(const QString& sessionKey);
    void setFlag(const QString& sessionKey, const QString& roiKey, bool flag);
    bool toggleFlag(const QString& sessionKey, const QString& roiKey);

    session_key_t refSessionName() const { return m_refSession; }
    int refSessionSize() const;
    bool refSessionExists() const { return m_refSession != ""; }
    QStringList traverseRefNames(const int rowIndex, const QStringList colNames) const;
    roi_t fieldsAt(session_key_t sessionKey, roi_key_t roiKey) const;

    QList<roi_t> roisAtSessionFilterZ(session_key_t sessionKey, const z_key_t z) const;
    int roiIndexAt(session_key_t sessionKey, roi_key_t roiKey) const;
    int linkedRefIndexAt(const session_key_t sessionKey, roi_t roi) const;
    QList<QString> columnNamesUnordered();
    std::vector<LinkCount> refLinkCounts();
    std::vector<IndexedRoi> indexedRoisAtSession(session_key_t sessionKey) const;
    std::vector<std::shared_ptr<SearchResult>> searchRois(const QString& sessionKey, QString namePattern);

    void createMaskImages(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks);
    void createMaskOutlines(QHash<z_key_t, RoiMask>& masks);
    void maskImageToVector(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks, bool append = false);
    void refreshRoi(session_key_t session, std::shared_ptr<RoiFieldsDense> roi);
    // Transform ROIs from all moving sessions, storing result in
    // TRANSFORMED_SESSION. Return true if successful, false if there are no
    // ROIs for the moving sessions.
    bool transformRois(const QStringList &sessionNames,
                       const LandmarkModel &landmarks,
                       bool translateCentroids);

    static int radius(const QRect& rect) { return maxDim(rect) / 2; }
    static int maxDim(const QRect& rect) { return (rect.width() > rect.height()) ? rect.width() : rect.height(); }

private:    
    void removeRoiFromMaskTables(session_key_t session, roi_t roi);
    void refreshFullyLinked(roi_t roi);
    bool removeOldLink(const roi_key_t refRoiName, const session_key_t sessionName);

    session_key_t m_refSession;
    roi_t m_prevSelection = nullptr;

    // settings that affect data interpretation
    bool m_originTopLeft = true;
};

#endif // ROIDATA_HPP
