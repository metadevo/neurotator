#ifndef __LANDMARKMODEL_HPP
#define __LANDMARKMODEL_HPP

/*
  A model for representing landmark locations.

  The first column stores a label, and the remaining columns store locations
  for each session. Each landmark should have locations defined in exactly
  two sessions (usually the fixed session and one moving session). Landmarks
  with a location defined for one session are considered incomplete and are
  ignored when determining transforms.

  Most of the code here is fairly flexible and should handle incomplete
  landmarks, or landmarks which exist in three or more sessions. But these
  situations are confusing to the user and should be avoided.
*/

#include <QList>
#include <QObject>
#include <QString>
#include <QStandardItemModel>
#include <QJsonArray>
#include <map>
#include <set>

#include <vtkSmartPointer.h>
#include <vtkAbstractTransform.h>

#include "RoiDataDense.hpp"

class HyperstackTiff;

using session_key_t = QString;
using xform_ptr = vtkSmartPointer<vtkAbstractTransform>;
using stack_ptr_t = HyperstackTiff*;

struct Landmark {
    Landmark() = default;
    Landmark(int x, int y, int z, const QString &label, int row);

    VoxelCoordinate location;
    QString label;
    int row;
};

struct SessionData {
    int height;
    int width;
    xform_ptr xform;
    stack_ptr_t stack;
};

class LandmarkDataModel : public QStandardItemModel
{
    Q_OBJECT

public:
    Q_INVOKABLE void setSelection(int index) {
        m_selected.clear();
        landmarkIndex = index;
        m_selected.insert(index);
        emit selectionChanged();
    }
    Q_INVOKABLE void toggleSelection(int index) {
        auto found = m_selected.find(index);
        if (found == m_selected.end()) {
            m_selected.insert(index);
        }
        else {
            m_selected.erase(found);
        }
        emit selectionChanged();
    }
    Q_INVOKABLE bool isSelected(int index) const {
        return m_selected.find(index) != m_selected.end();
    }

    Q_INVOKABLE int clickedIndex (){
        return landmarkIndex;
    }

    Q_INVOKABLE void removeSelected();
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void updateOrder()
    {
        emit orderChanged();
    }

signals:
    void selectionChanged();
    void orderChanged();


private:
    int landmarkIndex = -1;
    std::set<int> m_selected;
};

class LandmarkModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString transformMode READ transformMode WRITE setTransformMode
               NOTIFY transformModeChanged)
    Q_PROPERTY(QStringList availableTransformModes READ availableTransformModes
               NOTIFY availableTransformModesChanged)

public:
    LandmarkModel();
    bool bFlipVTK = true;
    void addNewSession(const stack_ptr_t stack);
    void removeSession(const session_key_t &name);
    QList<Landmark> getLandmarks(const session_key_t &session) const;
    QJsonObject toJson() const;
    void fromJson(const QJsonObject &data);
    LandmarkDataModel* model() { return &m_model; }
    vtkSmartPointer<vtkAbstractTransform> getTransform(const session_key_t &session) const;
    stack_ptr_t getStackPtr(const session_key_t &session) const;

    const session_key_t &fixedSessionName() const { return m_fixedSession; }
    void setFixedSessionName(const session_key_t &name);
    void cachePreRotation(QString name, int pre_rotation);
    const QString &transformMode() const { return m_transformMode; }
    void setTransformMode(const QString &mode)  {
        m_transformMode = mode;
        emit transformModeChanged();
        updateTransforms();
    }
    QStringList availableTransformModes() const;
    int getNumLandmarks() { return m_model.rowCount();}

    float getCorrelation(int indx) const;

public slots:

    void addLandmark(const session_key_t &session, float x, float y, int z);
    void setXY(const session_key_t &session, const Landmark &landmark,
               float x, float y);
    void setZ(const session_key_t &session, const Landmark &landmark, float z);
    void updateTransforms();
    void transformPoints(QVector<VoxelCoordinate> &points,
                         session_key_t session) const;

    void predictLandmark(VoxelCoordinate &point, session_key_t landmark_ref_session, const session_key_t& session) const;

    void autoRegisterLandmark(int iRow, bool isGray = true);
    void autoRegisterSelected();

    //void addLandmarkAutoRegister(VoxelCoordinate & pos);
    void populateLandmarks();
    void sortLandmarksByCorrelation();

signals:
    void landmarksChanged();
    void transformsChanged();
    void availableTransformModesChanged();
    void transformModeChanged();
    void landmarkPredicted(int n);
    void landmarkAutoRegistered(int n);

private:

    bool m_populateLandmarksInProgress = false;

    bool autoRegisterLandmark(int iRow, const VoxelCoordinate& ref_point, const session_key_t& ref_session,
                              const session_key_t& session, bool isGray = true);
    void autoRegisterLandmarkUnitTest(VoxelCoordinate &point, session_key_t session, bool isGray = true);

    void setCorrelation(int indx, float corr);

    bool isLandmarkComplete() const;
    int columnForSession(const session_key_t &name) const;
    QVector3D vectorAt(int row, int column) const;
    bool isVectorAt(int row, int column) const;
    QString label(int row) const;
    xform_ptr createTransform(const session_key_t &name, int imageHeight, const int *upSample=nullptr) const;
    void updateSessionsForRow(int row);
    QHash<QString, int> preRotations;
    QList<session_key_t> m_sessions;
    std::map<session_key_t, SessionData> m_sessionData;
    LandmarkDataModel m_model;
    int m_nextIndex = 1;
    session_key_t m_fixedSession;
    QString m_transformMode = "Rigid Body";
};

#endif // __LANDMARKMODEL_HPP
