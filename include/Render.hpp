#ifndef __RENDER_HPP
#define __RENDER_HPP

#include <vector>
#include <QImage>

class HyperstackTiff;
class QString;
class vtkAbstractTransform;
struct HyperstackSize;

using stack_render_list = std::vector<std::pair<HyperstackTiff&, vtkAbstractTransform*>>;

// Render a slice of multiple stacks, and combine the results into one image.
QImage renderSlice(stack_render_list &stacks, int z, const HyperstackSize& size);

// Render multiple stacks and save to a TIFF image.
void renderToTiff(const QString &filePath, stack_render_list &stacks,
                  const HyperstackSize& size);

#endif // __RENDER_HPP
