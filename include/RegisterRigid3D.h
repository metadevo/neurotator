#ifndef REGISTERRIGID3D_H
#define REGISTERRIGID3D_H

#include <QQuickImageProvider>
#include "HyperstackTiff.hpp"

float registerRigid3D(const VolumeBuffer & ref_vb,
                      const VolumeBuffer & vb,
                      float &rSTDThreshold0,
                      float &rSTDThreshold1,
                      VoxelCoordinate & offset,
                      bool isTakeCaps = false);


class VolumeBufferCapsProvider : public QQuickImageProvider
{
public:
    static QImage
        caps;

    VolumeBufferCapsProvider():QQuickImageProvider(QQuickImageProvider::Image)
    {
        caps = QImage(128, 128, QImage::Format_Grayscale8);
    }

    static void setBitMap(unsigned char* data, QImage & img = caps);
    static void offsetCaps(unsigned char* data0, const QSize & size0,
                           unsigned char* data1, const QSize & size1,
                           int x0, int y0);

    ImageType imageType() const override { return ImageType::Image;}

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
};


#endif // REGISTERRIGID3D_H
