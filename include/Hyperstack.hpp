/// @author Sam Kenyon <sam@metadevo.com>
#ifndef HYPERSTACK_H
#define HYPERSTACK_H

#include <vector>
#include <QImage>
#include <QString>
#include <Voxels.hpp>

struct HyperstackFlags
{
    unsigned int stackId = 0;
    bool time = false;
    bool z = false;
    bool channel = false;
};

struct HyperstackIndex
{
    unsigned int time = 0;
    unsigned int z = 0;
    unsigned int channel = 0;
    //std::vector<HyperstackFlags> extra;
};

//struct HyperVoxelIndex
//{
//    unsigned int x = 0;
//    unsigned int y = 0;
//    unsigned int z = 0;
//    unsigned int t = 0;
//    unsigned int c = 0;
//};

struct HyperstackSize
{
    unsigned int x = 0;
    unsigned int y = 0;
    unsigned int z = 0;
    unsigned int t = 0;
    unsigned int c = 1; // Has to have at least one channel!
    unsigned int slicePixels = 0;
    unsigned int bpp = 0;
};

enum RGBA_Channel : unsigned int
{
    RED = 0,
    GREEN = 1,
    BLUE = 2,
    ALPHA = 3
};

/// Manages memory caches of images loaded from image stacks such as multi-page TIFFs.
/// This will have to be thread-aware, providing thread-safe accessors so a render thread
/// can request image buffers.
class Hyperstack
{
public:
    Hyperstack();
    virtual ~Hyperstack();
    Hyperstack(Hyperstack&) = delete;
    Hyperstack& operator=(Hyperstack&) = delete;

    // class interface
    virtual void setColor(std::vector<int> map)  { colormap = map; }
    virtual QString filePath() const { return m_filePath; }
    //bool load(const std::string& filePath);
//    virtual unsigned int numImages() const;
//    virtual HyperstackSize size() const;
//    virtual QString name() const;
//    virtual QImage imageBuffer();

protected:
    QString m_filePath;
    std::vector<int> colormap;
};


#endif // HYPERSTACK_H
