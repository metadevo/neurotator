#ifndef ROIDATABASE_HPP
#define ROIDATABASE_HPP

#include <memory>
#include <vector>
#include <QDebug>
#include <QHash>
#include <QString>
#include <QVector>
#include <QImage>

#include <Voxels.hpp>

using session_key_t = QString;
using roi_key_t = QString;
using z_key_t = int;

struct RoiFile {
    QString savePath;
    QString sessionKey;
};

struct RoiMask
{
    QRect extent;
    QImage mask;
    QImage maskOutline;
};

static RoiMask ROI_MASK_NOT_FOUND;

/// Data for a single 3D region of interest
class RoiFields
{
public:
    virtual ~RoiFields() {}
    QString name;

    virtual bool confirmed() { return false; }
    virtual void setConfirmed(bool) {}
    virtual bool selected() { return false; }
    virtual void setSelected(bool) {}
    virtual bool flagged() { return false; }
    virtual void setFlagged(bool) {}
    virtual bool filtered() { return false; }
    virtual void setFiltered(bool) {}
    virtual bool fullyLinked() { return false; }
    virtual void setFullyLinked(bool) {}

    virtual void clearLinkedIn() {}
    virtual bool linkedInEmpty() { return true; }
    virtual bool linkedInSize() { return 0; }

    virtual QString linkedName() { return ""; }
    virtual void setLinkedName(QString) {}
    virtual void clearLinkedName() {}

    virtual QString linkedSession() { return ""; }
    virtual void setLinkedSession(QString) {}
    virtual void clearLinkedSession() {}

    void setCentroid(VoxelCoordinate v) { centroid = v; }
    VoxelCoordinate getCentroid() { return centroid; }

    virtual void addToMask(VoxelCoordinate) {}
    virtual void setMask(QVector<VoxelCoordinate>) {}

    virtual void setLinkedIn(QString, std::shared_ptr<RoiFields>) {}
    virtual void removeLinkedIn(QString ) {}
    virtual bool emptyLinkedIn() { return true; }
    virtual int sizeLinkedIn() { return 0; }
    virtual bool linkedInContains(QString) { return false; }
    virtual std::weak_ptr<RoiFields> getLinkedIn(QString) { return std::weak_ptr<RoiFields>(); }

    virtual bool inVivo() { return false; }
    virtual void setInVivo(bool) {}

    virtual RoiMask& getMaskByZ(const unsigned int) { return ROI_MASK_NOT_FOUND; }

protected:
    VoxelCoordinate centroid;

};

class RoiDataBase
{
public:
    using roi_t = std::shared_ptr<RoiFields>;
    using session_t = QHash<roi_key_t, roi_t>;
    using maskTable_t = QHash<z_key_t, QList<roi_t>>;

    bool addNewSession(session_key_t name);
    bool removeSession(session_key_t name);

    int sessionCount() const { return m_sessions.size(); }
    QList<QString> sessionNames();
    QString sessionNameAt(int index);

    virtual bool addNewRoi(roi_key_t, roi_t) { return false; }
    QList<roi_t> roisAtSession(session_key_t sessionKey) const;

    virtual bool dummy() { return false;}
protected:
    session_key_t m_currSession;

    // for quick lookup of masks
    QHash<session_key_t, maskTable_t> m_maskTables;

    // the main container of ROI data for the whole application
    QHash<session_key_t, session_t> m_sessions;

};

#endif // ROIDATABASE_HPP
