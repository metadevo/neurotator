#ifndef MATFILESPARSEROI_HPP
#define MATFILESPARSEROI_HPP

#include <string>
#include <QFileInfo>
#include "MatFile.hpp"
#include "RoiDataSparse.hpp"

/// Load/Save Matlab data files
class MatFileSparseRoi : public MatFile
{
public:
    explicit MatFileSparseRoi(RoiDataSparse *roiData);
    virtual ~MatFileSparseRoi() override {}
    bool saveCandidateList(const QString& sessionKey, const std::string& filepath);
    bool saveDecodeList(const QString& sessionKey, const std::string& filepath);

private:
    bool examine() override;
    bool loadCustomData() override;
    bool saveCustomData() override { return false;}

    RoiDataSparse *m_roiDataSparse;
};

#endif // MATFILESPARSEROI_HPP
