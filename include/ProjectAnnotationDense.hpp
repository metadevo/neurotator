#ifndef PROJECT_ANNOTATION_DENSE_HPP
#define PROJECT_ANNOTATION_DENSE_HPP

#include "ProjectDense.hpp"

class ProjectAnnotationDense : public ProjectDense {
public:
    IMPLEMENT_CLONE(ProjectAnnotationDense)
    bool isDenseAnnotation() const override { return true; }
    QString mode() const override { return QString("dense annotation"); }
    bool isProjectAnnotation() override { return true; }

protected:

    void writeConfigProjectSpecific() const override;
};

#endif
