/// @author Sam Kenyon <sam@metadevo.com>
#ifndef WATERSHEDIMAGEPIPELINE_HPP
#define WATERSHEDIMAGEPIPELINE_HPP

#include <QImage>
#include <QVector>

#include "ImagePipeline.hpp"

class WatershedImagePipeline : public ImagePipeline
{
public:
    WatershedImagePipeline();
    virtual ~WatershedImagePipeline();

    virtual segments_t run(const QImage& input);
    virtual QVector<Intermediate> getIntermediates();

    segments_t runWithFile(const QString filepath);

private:
    QVector<Intermediate> m_intermediates;
};

#endif // WATERSHEDIMAGEPIPELINE_HPP
