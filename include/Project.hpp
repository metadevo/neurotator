/// @author Sam Kenyon <sam@metadevo.com>
#ifndef PROJECT_HPP
#define PROJECT_HPP

#include <limits>
#include <memory>
#include <vector>
#include <QHash>
#include <QQmlEngine>
#include <QObject>
#include <QJsonObject>
#include <QImage>
#include <QString>
#include <QUrl>
#include <QVector>
#include <QJsonDocument>
#include <QDir>

#include "Hyperstack.hpp"
#include "Render.hpp"
#include "RoiDataDense.hpp"
#include "RoiDataModel.hpp"

class HyperstackTiff; ///@todo use base class
class QFile;

using stack_index_t = unsigned long long;

const stack_index_t REG_WINDOW_INDEX = 999;
const unsigned int MAX_ROI_FILES_ALLOWED = 999;

const std::vector<std::vector<int>> colorMap {
  { 0, 255, 0 },
  { 255, 0, 0 },
  { 0, 0, 255 },
  {227, 35, 157},
  {59, 198, 182},
  //{225, 0, 225},
  //{0, 225, 225}
};

struct StackWindow {
    stack_index_t stackIndex = 0;
    HyperstackIndex coords;
    int zoomFactor = 100;
    float offsetX = 0.0;
    float offsetY = 0.0;
    float width = 0;
    int windowX = 200;
    int windowY = 10;
    int preRotation = 0;
    bool visible = true;
    QString name = "";
};

#define IMPLEMENT_CLONE(TYPE) Project* clone() const override { return new TYPE(); }
#define MAKE_PROTOTYPE(DESCRIPTION, TYPE) \
   Project* TYPE ## _myPrototype = Project::addPrototype(#DESCRIPTION, new TYPE());

class Project : public QObject
{
    Q_OBJECT
public:
    Project();
//    explicit Project( Project ref) {}
//    explicit Project(const QUrl fileUrl);
    virtual ~Project();

    virtual Project* clone() const = 0;
    static Project* makeProject(QString type);
    static Project* makeProjectFromUrl(QUrl fileUrl, bool b_lazyLoad = false, bool b_flipVTK = true);
    static Project* addPrototype(QString type, Project* p);
    static std::map<QString, Project*> protoTable;

    QUrl filePath() const { return m_filePath; }
    void setFilePath(const QUrl fileUrl) { m_filePath = fileUrl; }

    QString name() const { return m_name; }
    void setName(const QString name);

    Q_INVOKABLE virtual bool isRegistration() const { return false;}
    Q_INVOKABLE virtual bool isDenseAnnotation() const { return false;}
    Q_INVOKABLE virtual bool isSparseAnnotation() const { return false;}

    virtual QString mode() const { return QString(); }
    virtual bool isProjectAnnotation() { return false; }
    virtual bool isProjectRegistration() { return false; }

    void saveProjectAs(const QUrl fileUrl);

    virtual unsigned int maxRoiFilesAllowed() const { return MAX_ROI_FILES_ALLOWED; }
    unsigned int numLoadedRoiFiles() const { return static_cast<unsigned int>(m_roiFiles.size()); }
    void loadRoiData(const QUrl);
    virtual void unloadRoiData(const QString);
    virtual void saveAllRoiFiles() {}

    unsigned int numLoadedStacks() const {return m_stackWindows.size(); }
    virtual bool loadStack(const QUrl) { return false; }
    virtual bool loadStack(const QString&) { return false; }
    virtual void unloadStack(const QString&) {}

    void saveStackZ(const stack_index_t stack, const unsigned int z);
    void saveStackZoom(const stack_index_t stack, const int zoomFactor);
    void saveStackOffset(const stack_index_t stack, const float offsetX, const float offsetY);
    void saveStackWidth(const stack_index_t stack, const float width);
    void saveStackWindowPosition(const stack_index_t stack, const int x, const int y);
    void saveStackPreRotation(stack_index_t stack, const int preRotation);
    //void saveStackWindowHidden(const stack_index_t windowIndex);

    virtual int getImageSizeY(QString) { return 0; }

    virtual void setCurrentStackWindow(const stack_index_t) {}
    unsigned long long currentStackWindowIndex() { return m_currentStackWindow; }
    const StackWindow& getStackWindowConfig(stack_index_t stack) const;
    StackWindow& getStackWindowConfig(stack_index_t stack);
    int getStackPreRotation(QString name);

    virtual QStringList stackNames() { return QStringList(); }
    virtual QList<QUrl> stackUrls() { return QList<QUrl>(); }
    virtual QList<QUrl> stackWindowUrls() { return QList<QUrl>(); }

    QString referenceName() { return m_referenceName; }

    // Used by ProjectRegistration
    virtual bool flipVTK() { return bFlipVTK; }
    //

public slots:
    bool saveConfig() const;

protected:
    bool lazyLoad = false;
    bool readOnly = false;

    // Used by ProjectRegistration
    bool bFlipVTK = true;
    void setFlipVTK(bool val) { bFlipVTK = val; }
    //

    virtual void projectSpecificInit() {}

    void setLazyLoad() { lazyLoad = readOnly = true; }

    // PROJECT JSON
    QUrl m_filePath;
    QString m_name;
    mutable QJsonObject m_jsonObject;

    static QJsonDocument loadJsonDocumentFromFile(QString filePath);
    static QString readMode(const QJsonObject &json);

    bool loadConfig();
    void readMisc(const QJsonObject &json);
    virtual void writeConfigProjectSpecific() const {}
    virtual void readConfigProjectSpecific(const QJsonObject &) {}
    void write() const;
    void readWindowConfigs(const QJsonObject &json);
    void writeWindowConfig() const;
    void updateStacksConfig() const;
    //

    /// ROI
    QVector<RoiFile> m_roiFiles;
    virtual void loadRoiData(const QString&) {}
    virtual bool loadMatlabRoiFile(QString , QString ) { return false; }
    virtual bool loadCsvRoiFile(QString , QString ) { return false; }

    unsigned int m_maxClientsPerStack = 1;

    std::vector<StackWindow> m_stackWindows;
    StackWindow m_registrationWindow;
    stack_index_t m_currentStack = 0;
    stack_index_t m_currentStackWindow = 0;

    stack_index_t m_findResult = 0;

    //mutable QFile* m_saveFile = nullptr;

    bool m_openingProject = false;
    QString m_referenceName;
};


#endif // PROJECT_HPP
