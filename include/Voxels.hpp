#ifndef VOXELS_HPP
#define VOXELS_HPP


struct VoxelCoordinate
{
    int x = 0;
    int y = 0;
    int z = 0;
    //int t = 0;

    VoxelCoordinate() {}

    VoxelCoordinate(int x, int y, int z) :
        x(x), y(y), z(z) {}
};

#endif // VOXELS_HPP
