#ifndef PROJECT_REGISTRATION_HPP
#define PROJECT_REGISTRATION_HPP

#include "ProjectDense.hpp"
#include "LandmarkModel.hpp"

class ProjectRegistration : public ProjectDense {
public:
    ProjectRegistration();
    ~ProjectRegistration();
    IMPLEMENT_CLONE(ProjectRegistration)
    bool isRegistration() const override { return true; }
    QString mode() const override { return QString("registration"); }
    bool isProjectRegistration() override { return true; }

    LandmarkModel& landmarkModel() { return m_landmarks; }
    void setFixedSessionName(QString);
    QString fixedSessionName() const { return m_landmarks.fixedSessionName(); }
    QStringList availableTransformModes() { return m_landmarks.availableTransformModes(); }
    void setTransformMode(QString mode) { m_landmarks.setTransformMode(mode); }
    QString transformMode() { return m_landmarks.transformMode(); }
    void updateTransforms() { m_landmarks.bFlipVTK = bFlipVTK; m_landmarks.updateTransforms(); }
    int getNumLandmarks() { return m_landmarks.getNumLandmarks(); }
    void autoRegisterSelectedLandMarks(){m_landmarks.autoRegisterSelected();};
    void populateLandmarks(){m_landmarks.populateLandmarks();};
    void sortLandmarksByCorrelation(){m_landmarks.sortLandmarksByCorrelation();};

    float getAutoregisterCorrelation(int indx) {return m_landmarks.getCorrelation(indx); }

    void cachePreRotation(QString name, int pre_rotation) { m_landmarks.cachePreRotation(name, pre_rotation); }

    vtkSmartPointer<vtkAbstractTransform> getTransform(const QString &session) const { return m_landmarks.getTransform(session); }

    void setCurrentStackWindow(const stack_index_t index) override;

    QJsonObject getRegistrationHints();

    /// ROI Transformation
    const QString &roiTransformMode() const { return m_roiTransformMode;}
    void setRoiTransformMode(const QString &);

public slots:
    // Implemented in ProjectRegistration
    void renderTransform();
    void transformROIs();
    //
    stack_render_list renderList();

protected:

    // PROJECT JSON
    virtual void projectSpecificInit() override;
    QJsonObject m_registrationHints;
    QString m_roiTransformMode;

    QJsonObject registrationHints() const;
    void readRegistrationHints(const QJsonObject &json);
    void readConfigProjectSpecific(const QJsonObject &json) override;
    void writeConfigProjectSpecific() const override;
    void readLandmarks(const QJsonObject &json);
    void readRoiTransformMode(const QJsonObject &json);
    //

    QString movingSessionName() const;
    LandmarkModel m_landmarks;
    void addStackToLandmarks(stack_ptr_t stack) override;
    void removeStackFromLandmarks(QString) override;
    void disconnectLandmarkSignals() { m_landmarks.disconnect(); }


};

// Return stacks and their associated transforms for render.

#endif
