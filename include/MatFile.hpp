/// @author Sam Kenyon <sam@metadevo.com>
#ifndef MATFILE_HPP
#define MATFILE_HPP

#include <string>
#include <QVector>
#include "mat.h"

#include "PersistenceInterface.hpp"
#include "RoiDataBase.hpp"

typedef QVector<QVector<std::string>> StringMatrix;

class MatFile : public PersistenceInterface {
public:
    explicit MatFile(RoiDataBase*);
    virtual ~MatFile();

    bool load(const std::string& filepath);
    bool save(const QString& sessionKey, const std::string& filepath);

protected:
    bool open();
    bool close();

    virtual bool examine() { return false; }
    virtual bool loadCustomData() { return false; }
    virtual bool saveCustomData() { return false; }

    QVector<VoxelCoordinate> loadMask(const mxArray* array);
    VoxelCoordinate loadCoordinate(const mxArray* array);
    std::string loadString(const mxArray* array);
    VoxelCoordinate loadVoxelCoordinate(const mxArray *array, bool flipXY = false);
    QVector<int> loadVectorOfInts(const mxArray* array);
    QVector<QVector<int>> loadMatrixOfInts(const mxArray* array);
    QVector<VoxelCoordinate> loadVectorOfVoxels(const mxArray* array, bool flipXY = false);
    StringMatrix loadVectorOfStrings(const mxArray* array);

    unsigned long long examineArray(const mxArray* array, const unsigned int tabs = 0);

    RoiDataBase *m_roiData;

    QString m_sessionKey;
    std::string m_filepath;
    MATFile* m_mf = nullptr;
    std::string m_topArrayName;
    std::string m_defaultToplevel = "ROI_list";
    unsigned long long m_numElements = 0;
    bool m_toplevelIsField = false;
};

#endif
