#ifndef PROJECT_ANNOTATION_SPARSE_HPP
#define PROJECT_ANNOTATION_SPARSE_HPP

#include "Project.hpp"
#include "RoiDataSparse.hpp"
#include "RoiDataSparseModel.hpp"
#include "HyperstackTiff.hpp"

#define MAX_ROI_FILES_SPARSE 100

using stack_ptr_t = HyperstackTiff*;

class ProjectAnnotationSparse : public Project {
public:
    IMPLEMENT_CLONE(ProjectAnnotationSparse)
    ProjectAnnotationSparse();
    virtual ~ProjectAnnotationSparse();

    bool isSparseAnnotation() const override { return true; }
    QString mode() const override { return QString("sparse annotation"); }
    bool isProjectAnnotation() override { return true; }

    stack_ptr_t currentStack();
    stack_ptr_t getStack(const QString& sessionName) const;

    bool loadStacks(QList<QUrl> urls);
    void unloadStack(const QString& name) override;

    QStringList stackNames() override;
    QList<QUrl> stackUrls() override;
    QList<QUrl> stackWindowUrls() override;

    int getImageSizeY(QString imageName) override;
    virtual void setCurrentStackWindow(const stack_index_t index) override;

    //SPARSE ROI

    virtual void writeConfigProjectSpecific() const override ;
    virtual void readConfigProjectSpecific(const QJsonObject &json) override;
    virtual void saveAllRoiFiles() override ;
    virtual unsigned int maxRoiFilesAllowed() const override { return MAX_ROI_FILES_SPARSE; }

    void saveCandidateList(const QString& sessionKey);
    void saveDecodeList(const QString& sessionKey);

    RoiDataSparse* roiData() { return m_roiData; }
    RoiDataSparseModel* roiDataModel() { return m_roiDataModel; }
    QVariantList getStackUrlMatrix(QString directoryName);

protected:
    QString matlabRoiFilepath;

    virtual void loadRoiData(const QString&) override;
    virtual void unloadRoiData(const QString) override;
    bool loadSparseRoiDirectory(RoiFile);
    bool unloadSparseRoiDirectory(QString);
    void readRoiDirectories(const QJsonObject&);

    // SPARSE ROIs
    RoiDataSparse *m_roiData;
    RoiDataSparseModel *m_roiDataModel;

    std::vector<stack_ptr_t> m_stack;
    QVector<QList<stack_ptr_t>> m_sparseStacks;
    QVariantList m_currentStackUrls;

};

#endif
