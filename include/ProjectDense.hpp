#ifndef PROJECT_DENSE_HPP
#define PROJECT_DENSE_HPP

#include "Project.hpp"

using stack_ptr_t = HyperstackTiff*;

class ProjectDense : public Project {
public:
    IMPLEMENT_CLONE(ProjectDense)
    ProjectDense();
    ~ProjectDense();

    bool loadStack(const QUrl fileUrl) override;
    bool loadStack(const QString& pathStr) override;
    void unloadStack(const QString& name) override;

    stack_ptr_t currentStack();
    stack_ptr_t getStack(const QString& sessionName) const;

    QStringList stackNames();
    QList<QUrl> stackUrls();
    QList<QUrl> stackWindowUrls();

    int getImageSizeY(QString imageName) override;

    virtual void setCurrentStackWindow(const stack_index_t index) override;

    // DENSE ROIS

    RoiDataDense* roiData() { return m_roiData; }
    RoiDataModel* confirmedModel() { return m_confirmedModel; }
    RoiDataModel* unconfirmedModel(const int index);
    std::shared_ptr<RoiDataModel> unconfirmedModelByName(const QString sessionName) { return m_unconfirmedModelNameMap[sessionName]; }
    RoiDataModel* lastUnconfirmedModel() { return m_unconfirmedModels.last().get(); }
    void resortModels(const QString refSessionKey);
    void resetModels(bool regenConfirmed = true);
    void resortConfirmedModel(const SortChoice sort);
    void resortConfirmedModelByName(const int column);

    virtual void saveAllRoiFiles();
protected:

    // PROJECT JSON
    virtual void writeConfigProjectSpecific() const { writeStacks(); }
    virtual void readConfigProjectSpecific(const QJsonObject &json);
    //

    virtual void readRoiFiles(const QJsonObject &json);

    // DENSE STACKS
    std::vector<stack_ptr_t> m_stack;
    void readStacks(const QJsonObject &json);
    void writeStacks() const;
    bool findStack(const QString& filePath);
    //

    virtual void addStackToLandmarks(stack_ptr_t) {}
    virtual void removeStackFromLandmarks(QString) {}

    virtual void loadRoiData(const QString &) override;
    virtual bool loadMatlabRoiFile(QString pathStr, QString keyName) override;
    virtual bool loadCsvRoiFile(QString pathStr, QString keyName) override;

    // DENSE ROIs
    RoiDataDense *m_roiData;
    RoiDataModel *m_confirmedModel;
    QList<std::shared_ptr<RoiDataModel>> m_unconfirmedModels;
    QHash<QString, std::shared_ptr<RoiDataModel>> m_unconfirmedModelNameMap;
    void updateUnconfirmedNameMap();
    //
};

#endif
