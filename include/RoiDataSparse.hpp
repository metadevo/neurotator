#ifndef ROISPARSEDATA_HPP
#define ROISPARSEDATA_HPP

#include <memory>
#include <vector>
#include <QDebug>
#include <QHash>
#include <QString>
#include <QVector>

#include "RoiDataBase.hpp"

typedef QVector<QVector<std::string>> StringMatrix;
enum StackMatrix {tiff_aligned, tiff_exvivo};

/// Data for a sparse region of interest
class RoiFieldsSparse : public RoiFields
{
public:
    QVector<int> candidates() { return m_candidates; }
    void setCandidates(QVector<int> x) { m_candidates = x; }
    bool isCandidate() {return m_candidates.contains(RoiFields::name.toInt());}
    int numCandidates() { return m_candidates.size(); }

    QVector<VoxelCoordinate> roiOriginal() { return m_roiOriginal; }
    void setRoiOriginal(QVector<VoxelCoordinate> x) { m_roiOriginal = x; }

    VoxelCoordinate centroidOriginal() { return m_centroidOriginal; }
    void setCentroidOriginal(VoxelCoordinate x) { m_centroidOriginal = x; }

    QString tiffOriginal() { return m_tiffOriginal; }
    void setTiffOriginal(QString x) { m_tiffOriginal = x; }

    QVector<VoxelCoordinate> roiInvivo() { return m_roiInVivo; }
    void setRoiInvivo(QVector<VoxelCoordinate> x) { m_roiInVivo = x; }

    QVector<VoxelCoordinate> roiRoiInvivoUs() { return m_roiInVivoUs; }
    void setRoiInvivoUs(QVector<VoxelCoordinate> x) { m_roiInVivoUs = x; }

    VoxelCoordinate centroidInVivo() { return m_centroidInVivo; }
    void setCentroidInVivo(VoxelCoordinate x) { m_centroidInVivo = x; }

    VoxelCoordinate centroidInVivoUs() { return m_centroidInVivoUs; }
    void setCentroidInVivoUs(VoxelCoordinate x) { m_centroidInVivoUs = x; }

    QString tiffInVivo() { return m_tiffInVivo; }
    void setTiffInVivo(QString x) { m_tiffInVivo = x; }

    StringMatrix tiffAligned() { return m_tiffAligned; }
    void setTiffAligned(StringMatrix x) {
        m_tiffAligned = x;
        // initialize decode matrix
        if(name == "invivo"){
            QVector<int> decodeRow = QVector<int>(0);
            QVector<QVector<int>> decode = QVector<QVector<int>>(0);
            setDecode(decode);
        } else {
            QVector<int> decodeRow = QVector<int>(x.first().size(), 0);
            QVector<QVector<int>> decode = QVector<QVector<int>>(x.size(), decodeRow);
            setDecode(decode);
        }
    }
    StringMatrix tiffExVivo() { return m_tiffExVivo; }
    void setTiffExVivo(StringMatrix x) { m_tiffExVivo = x; }

    QString linkedSession() override { return s_linkedSession; }
    void setLinkedSession(QString x) override { s_linkedSession = x; }
    void clearLinkedSession() override { s_linkedSession.clear(); }

    QString linkedName() override { return s_linkedName; }
    void setLinkedName(QString x) override { s_linkedName = x; }
    void clearLinkedName() override { s_linkedName.clear(); }

    void setMask(QVector<VoxelCoordinate> x) override { mask = x;}

    bool inVivo() override { return m_inVivo; }
    void setInVivo(bool x) override { m_inVivo = x; }

    QVector<QVector<int>> decode() { return m_decode;}
    void setDecode(QVector<QVector<int>> x) { if(x.size() > 0) { m_decode = x; } }

    RoiMask& getMaskByZ(const unsigned int z) override { return masks[z]; }

    using RoiFields::confirmed;
    bool confirmed (int session, int channel) {
        return (m_decode.value(session).value(channel) == 1) ? true : false;
    }
    void updateConfirmed(int session, int channel, int confirm){
        QVector<int> channels = m_decode.value(session);
        channels.replace(channel, confirm);
        m_decode.replace(session, channels);
    }

    QVector<VoxelCoordinate> mask;  // temporary, cleared after loading
    QHash<z_key_t, RoiMask> masks;

protected:

    bool m_inVivo = false;
    QVector<VoxelCoordinate> m_roiOriginal;
    VoxelCoordinate m_centroidOriginal;
    QString m_tiffOriginal;
    bool flag = false;
    //QVector<int> confirmed;
    QVector<int> m_candidates;
    QString s_linkedName;
    QString s_linkedSession;
    QVector<VoxelCoordinate> m_roiInVivo, m_roiInVivoUs;
    VoxelCoordinate m_centroidInVivo, m_centroidInVivoUs;
    QString m_tiffInVivo;
    StringMatrix m_tiffExVivo;
    QVector<int> x, y, z;
//    QVector<QVector<PixelCoordinate>> shifts;
    StringMatrix m_tiffAligned;
    QVector<QVector<int>> m_decode;
};

/// Manages regions of interest for multiple sessions
class RoiDataSparse : public RoiDataBase
{
public:
//    using maskTable_t = QHash<z_key_t, QList<roi_t>>;

    RoiDataSparse();
    virtual ~RoiDataSparse() {}

    bool dummy() override { return true; }
    bool addNewRoi(roi_key_t name, roi_t fields) override;
    bool addNewRoi(session_key_t session, roi_key_t name, roi_t fields);
    QList<roi_t> roisAtSessionFilterZ(session_key_t sessionKey, const z_key_t z) const;

    void createMaskImages(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks);
    void createMaskOutlines(QHash<z_key_t, RoiMask>& masks);
    void refreshRoi(session_key_t session, std::shared_ptr<RoiFieldsSparse> roi);

    bool addToCandidateList(const QString& sessionKey, const QString& roiKey);
    bool deleteFromCandidateList(const QString& sessionKey, const QString& roiKey);
    QList<int> getCandidatelist(const QString& sessionKey);

    QList<QVector<QVector<int>>> getDecodeList(const QString& sessionKey);
    StringMatrix getStackMatrix(session_key_t, StackMatrix);    
    int getCentroidInvivoZ(const QString& sessionKey);

private:

    // for quick lookup of masks
//    QHash<session_key_t, maskTable_t> m_maskTables;

    // settings that affect data interpretation
    bool m_originTopLeft = true;
};

#endif
