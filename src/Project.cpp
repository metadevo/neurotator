/// @author Sam Kenyon <sam@metadevo.com>
#include <memory>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QUrl>
#include <QThread>
#include "algorithm"

#include "ApplicationData.hpp"
#include "AppSettings.hpp"
#include "Project.hpp"

std::map<QString, Project*> Project::protoTable;

Project::Project() :
        m_name("blank")
{
}

Project::~Project()
{
    qDebug() << "Destroying Project - Done.";
}

Project* Project::makeProject(QString projectType)
{
   std::map<QString, Project*>::iterator it;

   it = protoTable.find(projectType.toLower());
   if (it != protoTable.end())
       return it->second->clone();
   qCritical() << "prototype not found";
   return nullptr;
}

Project* Project::makeProjectFromUrl(QUrl fileUrl, bool b_lazyLoad, bool b_flipVTK)
{
   std::map<QString, Project*>::iterator it;

   QJsonDocument loadDoc(loadJsonDocumentFromFile(fileUrl.toLocalFile()));

   if (loadDoc.isEmpty())
       return nullptr;

   QString projectType = readMode(loadDoc.object());

   // For backward compatibility
   if (projectType.toLower() == "annotation")
       projectType = "dense annotation";

   it = protoTable.find(projectType.toLower());
   if (it != protoTable.end()) {
       Project *p = it->second->clone();
       if (b_lazyLoad)
           p->setLazyLoad();
       p->setFlipVTK(b_flipVTK);
       p->setFilePath(fileUrl);
       p->loadConfig();
       p->projectSpecificInit();
       return p;
   }
   qCritical() << "prototype not found";
   return nullptr;
}

Project* Project::addPrototype(QString type, Project* p)
{
   qDebug() << "adding prototype for " << type << endl;
   protoTable[type] = p;
   qDebug() << "done";
   return p;
}

void Project::setName(const QString name)
{
    m_name = name;
    saveConfig();
    AppSettings::instance().updateRecentProjectName(name, m_filePath.toLocalFile());
}

void Project::saveProjectAs(const QUrl fileUrl)
{
    try {
        m_filePath = fileUrl;
        saveConfig();
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
}

void Project::loadRoiData(const QUrl fileUrl)
{
    QString pathStr = fileUrl.toLocalFile();
    loadRoiData(pathStr);
}

void Project::unloadRoiData(QString name)
{
}


QJsonDocument Project::loadJsonDocumentFromFile(QString filePath) {
    qInfo() << "Loading project file...";
    QFile loadFile(filePath);
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning() << "Couldn't open config file: " << loadFile.errorString();
        return QJsonDocument();
    }
//    m_openingProject = true;
    QByteArray saveData = loadFile.readAll();

    return QJsonDocument::fromJson(saveData);
}

bool Project::loadConfig()
{
    try {
        QJsonDocument loadDoc(loadJsonDocumentFromFile(m_filePath.toLocalFile()));

        if (loadDoc.isEmpty())
            return false;

        m_openingProject = true;

        readMisc(loadDoc.object());
        readWindowConfigs(loadDoc.object());
        readConfigProjectSpecific(loadDoc.object());

        m_openingProject = false;
        qDebug() << "Loaded project config from " << m_filePath.toLocalFile();

        return true;
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
        return false;
    }
    catch (...) {
        qWarning() << "Unknown exception";
        return false;
    }
}

QString Project::readMode(const QJsonObject &json) {
    if (json.contains("mode"))
        return json["mode"].toString();
    return QString();
}

void Project::readMisc(const QJsonObject &json)
{
    qInfo() << "Reading JSON file...";
    if (json.contains("project_name") && json["project_name"].isString()) {
        m_name = json["project_name"].toString();
    }
}

void Project::readWindowConfigs(const QJsonObject &json)
{
    if (json.contains("stack_windows") && json["stacks"].isArray()) {
        m_stackWindows.clear();
        QJsonArray windowsArray = json["stack_windows"].toArray();
        unsigned int arraySize = static_cast<unsigned int>(windowsArray.size());
        for (unsigned int i = 0; i < arraySize; ++i) {
            StackWindow windowConfig;
            QJsonObject obj = windowsArray[i].toObject();
            windowConfig.stackIndex = obj["stackId"].toInt();
            windowConfig.coords.z = obj["z"].toInt();
            windowConfig.coords.time = obj["t"].toInt();
            windowConfig.preRotation = obj["preRotation"].toInt();
            windowConfig.zoomFactor = obj["zoomFactor"].toInt();
            windowConfig.offsetX = obj["offsetX"].toDouble();
            windowConfig.offsetY = obj["offsetY"].toDouble();
            windowConfig.width = obj["width"].toDouble();
            windowConfig.windowX = obj["windowX"].toInt();
            windowConfig.windowY = obj["windowY"].toInt();
            windowConfig.name = json["stacks"].toArray()[i].toString();
            m_stackWindows.push_back(windowConfig);
        }
    }
}

void Project::saveStackZ(stack_index_t stack, unsigned int z)
{
    getStackWindowConfig(stack).coords.z = z;
    saveConfig();
}

void Project::saveStackZoom(stack_index_t stack, const int zoomFactor)
{
    getStackWindowConfig(stack).zoomFactor = zoomFactor;
    saveConfig();
}

void Project::saveStackPreRotation(stack_index_t stack, const int preRotation)
{
    getStackWindowConfig(stack).preRotation = preRotation;
    saveConfig();
}

void Project::saveStackOffset(stack_index_t stack, const float offsetX, const float offsetY)
{
    getStackWindowConfig(stack).offsetX = offsetX;
    getStackWindowConfig(stack).offsetY = offsetY;
    saveConfig();
}

void Project::saveStackWidth(stack_index_t stack, const float width)
{
    getStackWindowConfig(stack).width = width;
    saveConfig();
}

void Project::saveStackWindowPosition(stack_index_t stack, const int x, const int y)
{
    getStackWindowConfig(stack).windowX = x;
    getStackWindowConfig(stack).windowY = y;
    saveConfig();
}

//void Project::saveStackWindowHidden(const stack_index_t windowIndex)
//{
//    ///@todo set a visible flag?
//    saveConfig();
//}

// rewrites entire json object
bool Project::saveConfig() const
{
    if (readOnly)
        return false;

    try {
        // create the json object
        m_jsonObject = QJsonObject(); // clear
        write();

        QFile saveFile(m_filePath.toLocalFile());

        if (!saveFile.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
            qWarning() << "Couldn't open file: " << saveFile.errorString();
            return false;
        }

        // save to file
        QJsonDocument saveDoc(m_jsonObject);
        saveFile.write(saveDoc.toJson());

        // in case it hasn't been added yet to recents list in app settings
        AppSettings::instance().setRecentProject(m_name, m_filePath.toLocalFile());

        return true;
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
        return false;
    }
    catch (...) {
        qWarning() << "Unknown exception";
        return false;
    }
}

void Project::write() const
{
    m_jsonObject["neurotator_ver"] = QCoreApplication::applicationVersion();
    m_jsonObject["project_name"] = m_name;
    m_jsonObject["mode"] = mode();

    writeConfigProjectSpecific();
    writeWindowConfig();

    QJsonArray roiFilesArray;
    for (auto roiFile : m_roiFiles) {
        roiFilesArray.append(roiFile.savePath);
    }
    m_jsonObject["roi_files"] = roiFilesArray;
}

void Project::writeWindowConfig() const
{
    QJsonArray windowsArray;
    for (auto w : m_stackWindows) {
        QJsonObject windowConfig;
        windowConfig["stackId"] = static_cast<int>(w.stackIndex);
        windowConfig["z"] = static_cast<int>(w.coords.z);
        windowConfig["t"] = static_cast<int>(w.coords.time);
        windowConfig["zoomFactor"] = w.zoomFactor;
        windowConfig["offsetX"] = w.offsetX;
        windowConfig["offsetY"] = w.offsetY;
        windowConfig["width"] = w.width;
        windowConfig["windowX"] = w.windowX;
        windowConfig["windowY"] = w.windowY;
        windowConfig["preRotation"] = w.preRotation;
        windowsArray.append(windowConfig);
    }
    m_jsonObject["stack_windows"] = windowsArray;
}

const StackWindow &Project::getStackWindowConfig(stack_index_t stack) const
{
    if (stack == REG_WINDOW_INDEX) {
        return m_registrationWindow;
    } else {
        return m_stackWindows.at(stack);
    }
}

StackWindow &Project::getStackWindowConfig(stack_index_t stack)
{
    if (stack == REG_WINDOW_INDEX) {
        return m_registrationWindow;
    } else {
        return m_stackWindows.at(stack);
    }
}

int Project::getStackPreRotation(QString name)
{
    for (auto x : m_stackWindows)
        if (x.name.contains(name))
            return x.preRotation;
    return 0;
}

