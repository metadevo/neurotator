
#include "MatFile.hpp"
#include <QDebug>

MatFile::MatFile(RoiDataBase *roiData) {
    m_roiData = roiData;
}

MatFile::~MatFile() {
//    qDebug() << "Destroying MatFile()";
    m_roiData = nullptr;
    close();
}

bool MatFile::open()
{
    try
    {
//        qInfo() << "Opening file " << m_filepath.c_str();
        m_mf = matOpen(m_filepath.c_str(), "r");
        if (m_mf == nullptr) {
            qWarning() << "Error opening file " << m_filepath.c_str();
            return false;
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
    return true;
}

bool MatFile::close()
{
    try
    {
        if (m_mf) {
            if (matClose(m_mf) != 0) {
                qWarning() << "Error closing file " << m_filepath.c_str();
                return false;
            }
            m_mf = nullptr;
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
    return true;
}

bool MatFile::load(const std::string& filepath)
{
    try
    {
        m_filepath = filepath;
        if (open()) {
            if(examine()) {
                return loadCustomData();
            }
        }
    }
    catch (const std::exception &e)
    {
        qWarning() << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
    return false;
}

bool MatFile::save(const QString& sessionKey, const std::string& filepath)
{
    try
    {
        m_sessionKey = sessionKey;
        m_filepath = filepath;
        qDebug() << "matlab Saving!";
        qInfo() << "Saving file " << m_filepath.c_str();
        m_mf = matOpen(m_filepath.c_str(), "w");
        if (m_mf == nullptr) {
            qWarning() << "Error opening file " << m_filepath.c_str();
            return false;
        }
        return saveCustomData();
    }
    catch (const std::exception &e)
    {
        qWarning() << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
}

VoxelCoordinate MatFile::loadCoordinate(const mxArray* array)
{
    VoxelCoordinate v;
    if ((array != nullptr) && (mxGetNumberOfElements(array) >= 3))
    {
        mxClassID classId = mxGetClassID(array);
        int x = 0, y = 0, z = 0;
        switch (classId) {
            case mxINT16_CLASS:
                {
                    mxInt16* numPtr = mxGetInt16s(array);
                    x = static_cast<int>(numPtr[0]);
                    y = static_cast<int>(numPtr[1]);
                    z = static_cast<int>(numPtr[2]);
                }
                break;
            case mxUINT16_CLASS:
                {
                    mxUint16* numPtr = mxGetUint16s(array);
                    x = static_cast<int>(numPtr[0]);
                    y = static_cast<int>(numPtr[1]);
                    z = static_cast<int>(numPtr[2]);
                }
                break;
            case mxSINGLE_CLASS:
                {
                    float* realPtr = mxGetSingles(array);
                    x = static_cast<int>(realPtr[0]);
                    y = static_cast<int>(realPtr[1]);
                    z = static_cast<int>(realPtr[2]);
                }
                break;
            case mxDOUBLE_CLASS:
                {
                    double* realPtr = mxGetDoubles(array);
                    x = static_cast<int>(realPtr[0]);
                    y = static_cast<int>(realPtr[1]);
                    z = static_cast<int>(realPtr[2]);
                }
                break;
            default: qWarning() << "Wrong number type."; break;
        }

        // For some reason y is first and x is second in the data
        v = VoxelCoordinate(y, x, z);
    } else {
        qWarning() << "Not enough elements.";
    }
    return v;
}

/// Tries to get/convert a string out of the given field
/// @returns A string which is empty if the field was empty (or there was an error)
std::string MatFile::loadString(const mxArray* array)
{
    if (array == nullptr) {
        qWarning() << "Error reading array.";
        return "";
    }
    std::string ret;
    mxClassID classId = mxGetClassID(array);
    switch (classId) {
        case mxCHAR_CLASS:
            {
                // Allocate enough memory to hold the converted string
                mwSize buflen = mxGetNumberOfElements(array) + 1;
                char* buf = static_cast<char*>(mxCalloc(buflen, sizeof(char)));

                // Copy the string data from array and place it into buf
                if (mxGetString(array, buf, buflen) != 0) {
                    qWarning() << "Could not convert string data.";
                } else {
                    ret = std::string(buf);
                }
            }
            break;
        case mxDOUBLE_CLASS:
            {
                double* numericName = mxGetDoubles(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxSINGLE_CLASS:
            {
                float* numericName = mxGetSingles(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT8_CLASS:
            {
                mxInt8* numericName = mxGetInt8s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxUINT8_CLASS:
            {
                mxUint8* numericName = mxGetUint8s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT16_CLASS:
            {
                mxInt16* numericName = mxGetInt16s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxUINT16_CLASS:
            {
                mxUint16* numericName = mxGetUint16s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT32_CLASS:
            {
                mxInt32* numericName = mxGetInt32s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxUINT32_CLASS:
            {
                mxUint32* numericName = mxGetUint32s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        case mxINT64_CLASS:
        {
            mxInt64* numericName = mxGetInt64s(array);
            if (numericName) {
                ret = QString::number(*numericName).toStdString();
            }
        }
        break;
        case mxUINT64_CLASS:
            {
                mxUint64* numericName = mxGetUint64s(array);
                if (numericName) {
                    ret = QString::number(*numericName).toStdString();
                }
            }
            break;
        default:
            qWarning() << "Unknown data type.";
            break;
    }
    return ret;
}


VoxelCoordinate  MatFile::loadVoxelCoordinate(const mxArray *array, bool flipXY) {
    VoxelCoordinate voxel;
    QVector<int> vi;
    vi = loadVectorOfInts(array);
    if ( vi.size() != 3 ) {
        qWarning() << "Array not of size 3.";
        return voxel;
    }
    if (flipXY) {
        voxel.x = vi[1];
        voxel.y = vi[0];
    } else {
        voxel.x = vi[0];
        voxel.y = vi[1];
    }
    voxel.z = vi[2];
    return voxel;
}

QVector<QVector<std::string>> MatFile::loadVectorOfStrings(const mxArray* array) {
    QVector<QVector<std::string>> matrixOfStrings;
    if (array != nullptr)
    {
        const mwSize* dims = mxGetDimensions(array);
        unsigned long long numDims = mxGetNumberOfDimensions(array);

        bool dimsOk = true;
        if (numDims != 2) {
            qWarning() << "Wrong dimensions.";
        }
        if ((mxGetNumberOfElements(array) != (dims[0] * dims[1]))) {
            qWarning() << "Wrong dimension size.";
        }
        if (!dimsOk) {
            return matrixOfStrings;
        }

        // for each string
        std::string ret;
        mxClassID classId = mxGetClassID(array);
        for (mwSize row = 0; row < dims[0] ; ++row) {
            QVector<std::string> rowOfStrings;
            for (mwSize col = 0; col < dims[1] ; ++col) {
                switch (classId) {
                    case mxCELL_CLASS:
                    {
                        const mxArray *cell_element_ptr;
                        cell_element_ptr = mxGetCell(array, row+dims[0]*col);
                        if (cell_element_ptr == nullptr) {
                            //qDebug() << "Empty Cell";
                            rowOfStrings.push_back("");
                        }
                        else {
                            //qDebug() << row << col << mxGetClassID(cell_element_ptr) << QString::fromStdString(loadString(cell_element_ptr));
                            rowOfStrings.push_back(loadString(cell_element_ptr));
                        }
                    }
                    break;
                default:
                    qWarning() << "Expecting a string, but received " << classId;
                }
            }
            matrixOfStrings.push_back(rowOfStrings);
        }

    }

    return matrixOfStrings;
}

QVector<int> MatFile::loadVectorOfInts(const mxArray* array)
{
    QVector<int> vint;
    if (array != nullptr)
    {
     const mwSize* dims = mxGetDimensions(array);
     unsigned long long numDims = mxGetNumberOfDimensions(array);

     bool dimsOk = true;
     if (numDims != 2) {
         qWarning() << "Wrong dimensions.";
     }
     if ((dims[0] != 1) || (mxGetNumberOfElements(array) != (dims[0] * dims[1]))) {
         qWarning() << "Wrong dimension size.";
     }
     if (dims[0] == 0) {
         qInfo() << "Mask is empty";
     }
     if (!dimsOk) {
         return vint;
     }

     // for each coordinate
     mxClassID classId = mxGetClassID(array);
     double* realPtr;
     mxUint16* u16Ptr;
     int x = 0;
     for (mwSize i = 0; i < (dims[0] * dims[1]); ++i)
     {
         switch (classId) {
             case mxUINT16_CLASS:
                 u16Ptr = mxGetUint16s(array);
                 x = static_cast<int>(u16Ptr[i]);
                 break;
             case mxDOUBLE_CLASS:
                 realPtr = mxGetDoubles(array);
                 x = static_cast<int>(realPtr[i]);
                 break;
             default: qWarning() << "Wrong number type."; break;
         }
         vint.push_back(x);
     }
    }
    else
    {
     qWarning() << "Array empty.";
    }
    return vint;
}

QVector<QVector<int>> MatFile::loadMatrixOfInts(const mxArray* array)
{
    QVector<QVector<int>> matrixOfInts;
    if (array != nullptr)
    {
        const mwSize* dims = mxGetDimensions(array);
        unsigned long long numDims = mxGetNumberOfDimensions(array);

        bool dimsOk = true;
        if (numDims != 2) {
            qWarning() << "Wrong dimensions.";
        }
        if (!dimsOk) {
            return matrixOfInts;
        }

        // for each coordinate
        mxClassID classId = mxGetClassID(array);
        double* realPtr;
        mxUint8* u8Ptr;
        mxUint16* u16Ptr;
        int x = 0;
        for (mwSize row = 0; row < dims[0]; ++row)
        {
            QVector<int> rowOfInts;
            for(mwSize col = 0; col < dims[1]; ++col){
                switch (classId) {
                case mxUINT8_CLASS:
                    u8Ptr = mxGetUint8s(array);
                    x = static_cast<int>(u8Ptr[dims[0] * col + row]);
                    break;
                case mxUINT16_CLASS:
                    u16Ptr = mxGetUint16s(array);
                    x = static_cast<int>(u16Ptr[dims[0] * col + row]);
                    break;
                case mxDOUBLE_CLASS:
                    realPtr = mxGetDoubles(array);
                    x = static_cast<int>(realPtr[dims[0] * col + row]);
                    break;
                default: qWarning() << "Wrong number type."; break;
                }
                rowOfInts.push_back(x);
            }
            matrixOfInts.push_back(rowOfInts);
        }
    }
    else
    {
        qWarning() << "Array empty.";
    }
    return matrixOfInts;
}

QVector<VoxelCoordinate> MatFile::loadVectorOfVoxels(const mxArray* array, bool flipXY)
{
    QVector<VoxelCoordinate> mask;
    if (array != nullptr)
    {
        const mwSize* dims = mxGetDimensions(array);
        unsigned long long numDims = mxGetNumberOfDimensions(array);

        bool dimsOk = true;
        if (numDims != 2) {
            qWarning() << "Wrong dimensions.";
        }
        if ((dims[1] != 3) || (mxGetNumberOfElements(array) != (dims[0] * dims[1]))) {
            qWarning() << "Wrong dimension size.";
        }
        if (dims[0] == 0) {
            qInfo() << "Mask is empty";
        }
        if (!dimsOk) {
            return mask;
        }

        // for each coordinate
        mxClassID classId = mxGetClassID(array);
        double* realPtr = mxGetPr(array);
        mxUint16* u16Ptr;
         int x = 0, y = 0, z = 0; // signed because we allow negative coords
         for (mwSize i = 0; i < dims[0]; ++i)
         {
             switch (classId) {
                 case mxUINT16_CLASS:
                     u16Ptr = mxGetUint16s(array);
                     x = static_cast<int>(u16Ptr[i]);
                     y = static_cast<int>(u16Ptr[dims[0] + i]);
                     z = static_cast<int>(u16Ptr[dims[0] * 2 + i]);
                     break;
                 case mxDOUBLE_CLASS:
                     realPtr = mxGetDoubles(array);
                     x = static_cast<int>(realPtr[i]);
                     y = static_cast<int>(realPtr[dims[0] + i]);
                     z = static_cast<int>(realPtr[dims[0] * 2 + i]);
 //                    if (x < 0 || y < 0 || z < 0) {
 //                        LOG(DBUG) << "Negative coordinate: (" << x << "," << y << "," << z << ")";
 //                    }
                     break;
                 default: qWarning() << "Wrong number type."; break;
             }

             // For some reason y is first and x is second in the data
             if (flipXY) {
                 VoxelCoordinate v(y, x, z);
                 mask.push_back(v);
             } else {
                 VoxelCoordinate v(x, y, z);
                 mask.push_back(v);
             }
         }
     }
     else
     {
        qWarning() << "Array empty.";
    }
    return mask;
}

///@return number of elements
unsigned long long MatFile::examineArray(const mxArray* array, unsigned int tabs)
{
    std::string tab(tabs, '\t');
    // Look at header
    //bool isGlobal = mxIsFromGlobalWS(array);
    unsigned long long numDims = mxGetNumberOfDimensions(array);
    const mwSize* dims = mxGetDimensions(array);
    unsigned long long numElements = mxGetNumberOfElements(array);
    mxClassID classId = mxGetClassID(array);
    const char* className = mxGetClassName(array);

    // Log information
    //qInfo() << "Was a " << (isGlobal ? "global" : "local") << " variable when saved.";
//    qInfo() << tab.c_str() << "Number of dimensions: " << numDims;
//    for (mwSize c = 0; c < numDims; ++c) {
//        qInfo() << tab.c_str() << "Dim [" << c << "]: " << dims[c];
//    }
//    qInfo() << tab.c_str() << "Total number of elements: " << numElements;
//    qInfo() << tab.c_str() << "Matlab class ID: "<< classId << " (" << className << ")";
//    if (mxIsSparse(array)) {
//        qInfo() << tab.c_str() << "Is sparse.";
//    }

    // If it's a struct, get some more info
    if (classId == mxSTRUCT_CLASS) {
        int number_of_fields = mxGetNumberOfFields(array);
//        qInfo() << tab.c_str() << "Number of fields in struct: " << number_of_fields;

//        qInfo() << "Fields (first element):";
        const char *field_name;
        for (int field_index = 0; field_index < number_of_fields; ++field_index) {
            field_name = mxGetFieldNameByNumber(array, field_index);
//            qInfo() << "\t" << tab.c_str() << field_name;
            mxArray* fieldArray = mxGetFieldByNumber(array, 0, field_index);
            if (fieldArray == nullptr) {
                qWarning() << "\t" << tab.c_str() << "Error reading array.";
            } else {
                if (field_name == m_defaultToplevel) {
                    numElements = mxGetNumberOfElements(fieldArray);
                    m_toplevelIsField = true;
//                    qInfo() << tab.c_str() << "Updated total number of elements: " << numElements;
                }
                examineArray(fieldArray, 2);
            }
            //NOTE: "Do NOT call mxDestroyArray on an mxArray returned by the mxGetFieldByNumber function."
        }
    }
    return numElements;
}


QVector<VoxelCoordinate> MatFile::loadMask(const mxArray* array)
{
    QVector<VoxelCoordinate> mask;
    if (array != nullptr)
    {
        const mwSize* dims = mxGetDimensions(array);
        unsigned long long numDims = mxGetNumberOfDimensions(array);

        bool dimsOk = true;
        if (numDims != 2) {
            qWarning() << "Wrong dimensions.";
        }
        if ((dims[1] != 3) || (mxGetNumberOfElements(array) != (dims[0] * dims[1]))) {
            qWarning() << "Wrong dimension size.";
        }
        if (dims[0] == 0) {
            qInfo() << "Mask is empty";
        }
        if (!dimsOk) {
            return mask;
        }

        // for each coordinate
        mxClassID classId = mxGetClassID(array);
        double* realPtr = mxGetPr(array);
        mxUint16* u16Ptr;
        int x = 0, y = 0, z = 0; // signed because we allow negative coords
        for (mwSize i = 0; i < dims[0]; ++i)
        {
            switch (classId) {
                case mxUINT16_CLASS:
                    u16Ptr = mxGetUint16s(array);
                    x = static_cast<int>(u16Ptr[i]);
                    y = static_cast<int>(u16Ptr[dims[0] + i]);
                    z = static_cast<int>(u16Ptr[dims[0] * 2 + i]);
                    break;
                case mxDOUBLE_CLASS:
                    realPtr = mxGetDoubles(array);
                    x = static_cast<int>(realPtr[i]);
                    y = static_cast<int>(realPtr[dims[0] + i]);
                    z = static_cast<int>(realPtr[dims[0] * 2 + i]);
//                    if (x < 0 || y < 0 || z < 0) {
//                        LOG(DBUG) << "Negative coordinate: (" << x << "," << y << "," << z << ")";
//                    }
                    break;
                default: qWarning() << "Wrong number type."; break;
            }

            // For some reason y is first and x is second in the data
            mask.push_back(VoxelCoordinate(y,x,z));
        }
    }
    else
    {
        qWarning() << "Array empty.";
    }
    return mask;
}
