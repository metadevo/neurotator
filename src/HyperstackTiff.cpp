/// @author Sam Kenyon <sam@metadevo.com>
#include <string>
#include <QFileInfo>
#include <QDebug>
#include <QDir>
#include <QElapsedTimer>

#include <tiffio.h>
#include <vtkImageImport.h>
#include <vtkAbstractTransform.h>
#include <vtkImageData.h>
#include <vtkImageReslice.h>
#include <vtkQImageToImageSource.h>
#include <vtkSmartPointer.h>
#include <vtkTIFFWriter.h>
#include <vtkImageFlip.h>
#include <vtkTransform.h>
#include <stdarg.h>
#include "HyperstackTiff.hpp"

#ifdef Q_OS_MACX
    #include <sys/mman.h>
    #include <unistd.h>
#endif
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <assert.h>

#define TIFFTAG_PHOTOMETRIC_RGB 2

int VolumeBuffer::dumpToFile(QString path, VoxelType* ptr, int size)
{
    QFile file(path);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out.writeBytes((char*)ptr, size);
    file.close();
    return 0;
}

HyperstackTiff::HyperstackTiff()
{
    //m_index.stackId = stackId;
    m_slicer = vtkSmartPointer<vtkImageReslice>::New();
    m_reader = vtkSmartPointer<vtkTIFFReader>::New();
    m_hyperTiff = vtkSmartPointer<vtkHyperTiffFilter>::New();
    m_paddedImage = vtkSmartPointer<vtkImageConstantPad>::New();
}

HyperstackTiff::~HyperstackTiff()
{
    cleanup();
}

void HyperstackTiff::cleanup()
{
    qDebug() << "Cleaning up hyperstackTiff...";
    try {
        if (m_tif != nullptr)
            TIFFClose(m_tif);
        m_tif = nullptr;
    }
    catch (const std::exception &e) {
        qWarning() << "Exception: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
    qDebug() << "Done.";
}

/// Function for passing in to QImage so we can safely release buffer
/// memory after all the QImage refs are destroyed.
void HyperstackTiff::cleanupBufferHandler(void *info)
{
    try {
        uint32 *buffer = reinterpret_cast<uint32 *>(info);
        if (buffer) {
            //qDebug() << "Destroying buffer...";
            _TIFFfree(buffer);
        }
    }
    catch (const std::exception &e) {
        qWarning() << "Exception: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
}

void HyperstackTiff::scanImageForIntensityRange()
{
    double *pixelVal;
    vtkSmartPointer <vtkImageExtractComponents> extractor = vtkSmartPointer<vtkImageExtractComponents>::New();
    qDebug()<< "before setinputconnection";
    extractor->SetInputConnection(m_imageSrc->GetOutputPort());
    qDebug()<< "after setinputconnection";
    for (unsigned int c = 0; c < m_size.c; c++) {
        extractor->SetComponents(static_cast<int>(c));
        extractor->Update();
        pixelVal = extractor->GetOutput()->GetScalarRange();
        if (channelWindowLevel[c].intensityMin > pixelVal[0])
            channelWindowLevel[c].intensityMin = static_cast<int32>(pixelVal[0]);
        if (channelWindowLevel[c].intensityMax < pixelVal[1])
            channelWindowLevel[c].intensityMax = static_cast<int32>(pixelVal[1]);
    }
}

void HyperstackTiff:: initializeWindowLevel()
{
   scanImageForIntensityRange();

    for (auto &i : channelWindowLevel) {
        i.windowMin = i.intensityMin;
        i.windowMax = i.intensityMax;
    }
    updateColorTransferFunctions();
}

std::vector<int> HyperstackTiff::getScalarRangeForChannel(unsigned int channelIndex)
{
    std::vector<int> range;
    if (m_size.c > (int) channelIndex) {
        range.push_back(channelWindowLevel[channelIndex].intensityMin);
        range.push_back(channelWindowLevel[channelIndex].intensityMax);
    } else {
        range.push_back(0);
        range.push_back(1);
    }
    return range;
}

void HyperstackTiff::updateWindowLevelForChannel(unsigned int c, std::vector<int> wl)
{
    channelWindowLevel[c].windowMin = wl[0];
    channelWindowLevel[c].windowMax = wl[1];
    updateColorTransferFunctions();
}

void HyperstackTiff::toggleChannelState(unsigned int c, bool state)
{
    channelWindowLevel[c].active = state;
    updateColorTransferFunctions();
}

int HyperstackTiff::numActiveChannel(){
    int activeChannel = 0;
    for(unsigned long i = 0; i < channelWindowLevel.size(); i++){
        if(channelWindowLevel[i].active == true){
            activeChannel++;
        }
    }
    return activeChannel;
}

std::vector<int> HyperstackTiff::getWindowLevelForChannel(unsigned int channelIndex)
{
    std::vector<int> range;
    if (m_size.c > channelIndex) {
        range.push_back(channelWindowLevel[channelIndex].windowMin);
        range.push_back(channelWindowLevel[channelIndex].windowMax);
    } else {
        range.push_back(0);
        range.push_back(1);
    }
    return range;
}

unsigned int HyperstackTiff::countPages(TIFF *tif)
{
    unsigned int dirCount = 0;
    if (tif) {
        do {
            dirCount++;
        } while (TIFFReadDirectory(tif));
    } else {
        qWarning() << "TIFF object is null!";
    }
    TIFFSetDirectory(tif, 0);
    return dirCount;
}

/// Load metadata from tags
bool HyperstackTiff::loadMetadata(TIFF *tif)
{
    if (!tif) {
        qWarning() << "TIFF object is null!";
        return false;
    }

    unsigned int width, height;
    uint16 bps, spp, compression;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bps);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &spp);
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &m_photometric);
    TIFFGetField(tif, TIFFTAG_COMPRESSION, &compression);


    m_size.x = width;
    m_size.y = height;
    m_size.slicePixels = width * height;

    qDebug() << "TIFF width (" << width << "), height (" << height << "), bps ("
             << bps << "), spp (" << spp << "), photometric(" << m_photometric << "), compression(" << compression
             << ").";

    ImageJMetadata metadata = readImageJMetadata(tif);
    if (metadata.imageJ) {
        m_numImages = metadata.images;
        m_size.c = metadata.channels;
        m_size.z = metadata.slices;
        m_size.t = metadata.frames;
    } else {
        qWarning() << "Unknown TIFF format.";
        qDebug() << "WARNING: Unknown TIFF format.";
    }
    m_size.bpp = bps;
    qDebug() << "numImages (" << m_numImages << "), m_size.c (" << m_size.c << "), m_size.z (" << m_size.z
             << "), m_size.t (" << m_size.t << ").";
    return true;
}

/// Reads ImageJ custom information (if there is any) out of the TIFF file
ImageJMetadata HyperstackTiff::readImageJMetadata(TIFF *tif)
{
    ImageJMetadata data;

    char *strData;
    int valid = TIFFGetField(tif, TIFFTAG_IMAGEDESCRIPTION, &strData);
    if (valid != 1) {
        qInfo() << "No Image Description tag found.";
        return data;
    }
    QString desc(strData);
    qDebug() << "TIFFTAG_IMAGEDESCRIPTION: " << desc;

    if (desc.startsWith("ImageJ")) {
        data.imageJ = true;
        qDebug() << "TIFF is ImageJ format.";
        QStringList metatags = desc.split("\n", QString::SkipEmptyParts);

        data.version = getMetadataStr("ImageJ", metatags);
        qDebug() << "data.version:" << data.version;
        data.hyperstack = "true" == getMetadataStr("hyperstack", metatags);
        qDebug() << "data.hyperstack:" << data.hyperstack;
        data.modeComposite = "composite" == getMetadataStr("mode", metatags);
        qDebug() << "data.modeComposite:" << data.modeComposite;

        data.images = static_cast<unsigned int>(getMetadataInt("images", metatags));
        data.slices = static_cast<unsigned int>(getMetadataInt("slices", metatags));
        data.frames = static_cast<unsigned int>(getMetadataInt("frames", metatags));
        data.channels = static_cast<unsigned int>(getMetadataInt("channels", metatags));
        if (data.channels == data.images)
            data.channels = 1;

        qDebug() << "images:" << data.images;
        qDebug() << "slices:" << data.slices;
        qDebug() << "frames:" << data.frames;
        qDebug() << "channels:" << data.channels;
    }
    return data;
}

/// Extracts an integer out of a string tokenized on an equals symbol, e.g. "foo=42"
int HyperstackTiff::getMetadataInt(QString name, QStringList metatags)
{
    int value = 0;
    QStringList matches = metatags.filter(name);
    if (matches.size() > 0) {
        QString chunk = matches[0];
        QStringList tokens = chunk.split('=', QString::SkipEmptyParts);

        if (tokens.size() == 2) {
            bool ok = false;
            value = tokens[1].toInt(&ok);
            if (!ok) {
                qDebug() << "ERROR: Metadata value not an integer (string: " << tokens[1] << ")";
                value = 1;
            }
        }
    }
    return value;
}

/// Extracts a string out of a string tokenized on an equals symbol
QString HyperstackTiff::getMetadataStr(QString name, QStringList metatags)
{
    QString value;
    QStringList matches = metatags.filter(name);
    if (matches.size() > 0) {
        QString chunk = matches[0];
        QStringList tokens = chunk.split('=', QString::SkipEmptyParts);
        if (tokens.size() == 2) {
            value = tokens[1];
        }
    }
    return value;
}

/**
 * @brief HyperstackTiff::isolateChannel
 * @param buffer: Image buffer to be modified. All channels except one (and alpha) will be zeroed.
 * @param channel: Which channel to leave. Currently this ignores the alpha channel and leaves it as is.
 * @param width
 * @param height
 */
void HyperstackTiff::isolateChannelRGB(uint32 *buffer, const RGBA_Channel channel, const unsigned int width,
                                       const unsigned int height)
{
    if (channel == RGBA_Channel::ALPHA) {
        return;
    }

    int i = 0;
    for (unsigned int y = 0; y < height; ++y) {
        for (unsigned int x = 0; x < width; ++x) {
            for (unsigned int c = 0; c < 3; ++c) {
                if (c == channel) {
                    buffer[i] = buffer[i];
                } else {
                    buffer[i] = 0;
                }
                ++i;
            }
        }
    }
}

/// merges three buffers as R, G, B into the first (the Red) buffer in ARGB order
uchar HyperstackTiff::mergeBuffersRGB(uint32 **buffers, const unsigned int pixelSize)
{
    uchar maxColorValue = 0;
    int a = 0xFF;
    for (unsigned int p = 0; p < pixelSize; ++p) {
        int r = TIFFGetR(buffers[0][p]);
        int g = TIFFGetG(buffers[1][p]);
        int b = TIFFGetB(buffers[2][p]);
        buffers[0][p] = qRgba(r, g, b, a);
        if (r > maxColorValue) {
            maxColorValue = r;
        }
        if (g > maxColorValue) {
            maxColorValue = g;
        }
        if (b > maxColorValue) {
            maxColorValue = b;
        }
    }
    return maxColorValue;
}

/// merges three buffers as R, G, B into the first (the Red) buffer in ARGB order
void HyperstackTiff::mergeBuffersRGBQuick(uint32 **buffers, const unsigned int pixelSize)
{
    int r, g, b, a = 0xFF;
    for (unsigned int p = 0; p < pixelSize; ++p) {
        r = TIFFGetR(buffers[0][p]);
        g = TIFFGetG(buffers[1][p]);
        b = TIFFGetB(buffers[2][p]);
        buffers[0][p] = qRgba(r, g, b, a);
    }
}

/// merges four buffers as R, G, B, A into the first (the Red) buffer in ARGB order
uchar HyperstackTiff::mergeBuffersRGBA(uint32 **buffers, unsigned int pixelSize)
{
    uchar maxColorValue = 0;
    for (unsigned int p = 0; p < pixelSize; ++p) {
        int r = TIFFGetR(buffers[0][p]);
        int g = TIFFGetG(buffers[1][p]);
        int b = TIFFGetB(buffers[2][p]);
        int a = TIFFGetA(buffers[3][p]);

        buffers[0][p] = qRgba(r, g, b, a);
        if (r > maxColorValue) {
            maxColorValue = r;
        }
        if (g > maxColorValue) {
            maxColorValue = g;
        }
        if (b > maxColorValue) {
            maxColorValue = b;
        }
    }
    return maxColorValue;
}


void HyperstackTiff::updateChannelColormap(std::vector <QColor> newColormap)
{
    channelColormap = newColormap;
    updateColorTransferFunctions();
}

void HyperstackTiff::updateColorTransferFunctions()
{
    if (channelColormap.size() < m_size.c)
        return;
    colorTransferFunctions.clear();
    for (unsigned int i = 0; i < m_size.c; i++) {
        double range = channelWindowLevel[i].windowMax - channelWindowLevel[i].windowMin;
        double level = (channelWindowLevel[i].windowMax + channelWindowLevel[i].windowMin) / 2.0;

        colorTransferFunctions.push_back(SmartColorLookupTablePtrs::New());

        if (channelWindowLevel[i].active == true) {
            colorTransferFunctions[i]->SetMaximumTableValue(channelColormap[i].red() / 255.0,
                                                            channelColormap[i].green() / 255.0,
                                                            channelColormap[i].blue() / 255.0, 1);
            colorTransferFunctions[i]->SetTableValue(255, channelColormap[i].red() / 255.0,
                                                     channelColormap[i].green() / 255.0,
                                                     channelColormap[i].blue() / 255.0, 1);
        } else {
            colorTransferFunctions[i]->SetMaximumTableValue(0, 0, 0, 1);
            colorTransferFunctions[i]->SetTableValue(255, 0, 0, 0, 1);
        }
        colorTransferFunctions[i]->SetMinimumTableValue(0, 0, 0, 0);
        colorTransferFunctions[i]->SetTableValue(0, 0, 0, 0, 0);

        colorTransferFunctions[i]->Build();
        colorTransferFunctions[i]->SetWindow(range);
        colorTransferFunctions[i]->SetLevel(level);
        colorTransferFunctions[i]->Build();

        m_mapColors[i]->SetLookupTable(colorTransferFunctions[i]);
    }
}

// The actual conversion code
static QImage vtkImageDataToQImageOriginal(vtkSmartPointer <vtkImageData> imageData)
{
    if (!imageData) { return QImage(); }

    /// \todo retrieve just the UpdateExtent
    int width = imageData->GetDimensions()[0];
    int height = imageData->GetDimensions()[1];
    QImage image(width, height, QImage::Format_RGB32);
    QRgb *rgbPtr =
            reinterpret_cast<QRgb *>( image.bits()) + width * (height - 1);
//            reinterpret_cast<QRgb *>( image.bits());
    unsigned char *colorsPtr = reinterpret_cast<unsigned char *>( imageData->GetScalarPointer());

    // Loop over the vtkImageData contents.
    for (int row = 0; row < height; row++) {
        for (int col = 0; col < width; col++) {
            // Swap the vtkImageData RGB values with an equivalent QColor
            *(rgbPtr++) = QColor(colorsPtr[0], colorsPtr[1], colorsPtr[2]).rgb();
            colorsPtr += imageData->GetNumberOfScalarComponents();
        }
        rgbPtr -= width * 2;
    }
    return image;
}

void handle_error(const char *kind, const char *fmt, va_list ap)
{
    char buf[512];
    vsnprintf(buf, sizeof(buf), fmt, ap);
    buf[sizeof(buf) - 1] = '\0';
    qWarning() << "TIFF Error: " << kind << " " << buf;
}


bool HyperstackTiff::loadStreaming(const QString filePath, const bool lazyLoading)
{
    QFileInfo fileInfo(filePath);

    TIFFSetErrorHandler(handle_error);

    m_ready = false;
    if (!fileInfo.exists()) {
        qDebug() << "ERROR: TIFF file does not exist!";
        return false;
    }
    m_tif = TIFFOpen(filePath.toStdString().c_str(), "r");
    if (!m_tif) {
        qDebug() << "ERROR: TIFF object null!";
        return false;
    }
    m_filePath = filePath;
    m_name = fileInfo.baseName();
    qDebug() << "Trying to load file " << m_name;

    loadMetadata(m_tif);
    if (m_numImages == 0) {
        m_numImages = countPages(m_tif);
    }
    qDebug() << "Image count for \"" << fileInfo.fileName() << "\":" << m_numImages;

    if (m_size.z == 0 && m_size.t == 0) {
        // assume this is a z stack unless user tells us otherwise
        m_size.z = m_numImages;
    }
    if (m_size.c == 0) {
        m_size.c = 1;
    }
    if (m_size.c == m_size.z && m_size.c > 20) {
        m_size.c = 1;
    }

    qDebug() << "m_size.z (" << m_size.z << ").";
    TIFFClose(m_tif);
    m_tif = nullptr;

    m_initted = true;
    m_ready = true;

    if (lazyLoading)
        return true;

    //Read the image
    m_reader->SetFileName(filePath.toUtf8().constData());
    if (m_photometric != TIFFTAG_PHOTOMETRIC_RGB) {
        m_reader->SetNumberOfScalarComponents(1);
        m_reader->SetDataExtent(0, static_cast<int>(m_size.x - 1), 0, static_cast<int>(m_size.y - 1), 0,
                                static_cast<int>(m_size.z) + static_cast<int>(m_size.c) - 2);
    }
    m_reader->SetDataSpacing(1, 1, 1);
    m_reader->SetDataOrigin(0.0, 0.0, 0.0);
    m_reader->IgnoreColorMapOn();
    m_reader->SetOrientationType(ORIENTATION_BOTLEFT);

    switch (m_size.bpp) {
        case 32:
            m_reader->SetDataScalarTypeToUnsignedInt();
            break;
        case 16:
            m_reader->SetDataScalarTypeToUnsignedShort();
            break;
        case 8:
            m_reader->SetDataScalarTypeToUnsignedChar();
            break;
        default:
            qWarning() << "Unrecognized channel_size " << m_size.bpp;
            m_reader->SetDataScalarTypeToUnsignedChar();
    }
    m_reader->UpdateWholeExtent();
    m_reader->UpdateInformation();

    switch (m_size.bpp) {
        case 32:
            m_reader->SetDataScalarTypeToUnsignedInt();
            break;
        case 16:
            m_reader->SetDataScalarTypeToUnsignedShort();
            break;
        case 8:
            m_reader->SetDataScalarTypeToUnsignedChar();
            break;
        default:
            qWarning() << "Unrecognized channel_size " << m_size.bpp;
            m_reader->SetDataScalarTypeToUnsignedChar();
    }

    if (m_photometric == TIFFTAG_PHOTOMETRIC_RGB) {
        m_size.c =3;
        m_imageSrc = m_reader;
    } else {
        m_hyperTiff->SetNumberOfChannels(static_cast<vtkTypeInt8>(m_size.c));
        m_hyperTiff->SetInputConnection(m_reader->GetOutputPort());
        m_hyperTiff->Update();
        m_imageSrc = m_hyperTiff;
    }

    m_paddedImage->SetInputConnection(m_imageSrc->GetOutputPort());
    m_paddedImage->SetConstant(0);

    for (unsigned int c = 0; c < m_size.c; c++) {
        m_mapColors.push_back(vtkSmartPointer<vtkImageMapToColors>::New());
        m_extractors.push_back(vtkSmartPointer<vtkImageExtractComponents>::New());
        m_extractors[c]->SetComponents(static_cast<int>(c));
    }
    channelWindowLevel = std::vector<windowLevel>(m_size.c);
    initializeWindowLevel();
    return true;
}


bool HyperstackTiff::loadStreamingFast(const QString filePath, const int *scale)
{
    TIFFSetErrorHandler(handle_error);

    m_ready = false;
    QFileInfo fileInfo(filePath);
    if (!fileInfo.exists()) {
        qDebug() << "ERROR: TIFF file does not exist!";
        return false;
    }
    m_tif = TIFFOpen(filePath.toStdString().c_str(), "r");
    if (!m_tif) {
        qDebug() << "ERROR: TIFF object null!";
        return false;
    }
    m_filePath = filePath;
    m_name = fileInfo.baseName();
    qDebug() << "Trying to load file " << m_name;

    loadMetadata(m_tif);
    if (m_numImages == 0) {
        m_numImages = countPages(m_tif);
    }
    qDebug() << "Image count for \"" << fileInfo.fileName() << "\":" << m_numImages;

    if (m_size.z == 0 && m_size.t == 0) {
        // assume this is a z stack unless user tells us otherwise
        m_size.z = m_numImages;
    }
    if (m_size.c == 0) {
        m_size.c = 1;
    }
    if (m_size.c == m_size.z && m_size.c > 20) {
        m_size.c = 1;
    }

    qDebug() << "m_size.z (" << m_size.z << ").";
    TIFFClose(m_tif);
    m_tif = nullptr;

    m_initted = true;
    m_ready = true;

    //Read the image
    m_reader->SetFileName(filePath.toUtf8().constData());


    if (m_photometric != TIFFTAG_PHOTOMETRIC_RGB) {
        m_reader->SetNumberOfScalarComponents(1);
        m_reader->SetDataExtent(0, static_cast<int>(m_size.x - 1), 0, static_cast<int>(m_size.y - 1), 0,
                                static_cast<int>(m_size.z) + static_cast<int>(m_size.c) - 2);
    }

    m_reader->SetSpacingSpecifiedFlag(true);
    m_reader->SetDataSpacing(1.0/scale[0], 1.0/scale[1], 1.0/scale[2]);
    m_reader->SetDataOrigin(0.0, 0.0, 0.0);
    m_reader->IgnoreColorMapOn();
    m_reader->SetOrientationType(ORIENTATION_BOTLEFT);

    switch (m_size.bpp) {
        case 32:
            m_reader->SetDataScalarTypeToUnsignedInt();
            break;
        case 16:
            m_reader->SetDataScalarTypeToUnsignedShort();
            break;
        case 8:
            m_reader->SetDataScalarTypeToUnsignedChar();
            break;
        default:
            qWarning() << "Unrecognized channel_size " << m_size.bpp;
            m_reader->SetDataScalarTypeToUnsignedChar();
    }

    if (m_photometric == TIFFTAG_PHOTOMETRIC_RGB) {
        m_size.c = 3;
        m_imageSrc = m_reader;
    }
    else {
        m_hyperTiff->SetNumberOfChannels(static_cast<vtkTypeInt8>(m_size.c));
        m_hyperTiff->SetInputConnection(m_reader->GetOutputPort());
        m_imageSrc = m_hyperTiff;
    }

    return true;
}

size_t HyperstackTiff::getFilesize(const char* filename) {
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

bool HyperstackTiff::saveTransformedSlices(const QString inputFileName,
                                     vtkSmartPointer <vtkAbstractTransform> xform,
                                     HyperstackSize fixedSize, const int *scale, const int *paddedSize,
                                     bool useRawImageStack)
{
#ifdef Q_OS_MACX
    void* mmappedData;
    size_t filesize;
    int fd;
    vtkSmartPointer<vtkImageImport> importer = vtkSmartPointer<vtkImageImport>::New();
#endif

    QFileInfo inputFile(inputFileName);
    QString outputDirectory(inputFile.dir().path() + "/" + inputFile.completeBaseName());
    QDir().mkdir(outputDirectory);
    for (int c =0; c < fixedSize.c; c++)
        QDir().mkdir(outputDirectory+"/"+QString::number(c).rightJustified(2, '0'));

    const int unitScale[3] = {1, 1, 1};
    if (scale == nullptr)
        scale = unitScale;

#ifdef Q_OS_MACX
    if (useRawImageStack) {
        filesize = getFilesize(inputFileName.toStdString().c_str());
        //Open file
        fd = open(inputFileName.toStdString().c_str(), O_RDONLY, 0);
        assert(fd != -1);
        mmappedData = mmap(NULL, filesize, PROT_READ, MAP_PRIVATE , fd, 0);
        assert(mmappedData != MAP_FAILED);

        importer->SetDataScalarTypeToUnsignedShort();
        importer->SetNumberOfScalarComponents(fixedSize.c);
        importer->SetDataExtent(0,this->m_size.x-1,0,this->m_size.y-1,0,this->m_size.z-1);
        importer->SetWholeExtent(0,this->m_size.x-1,0,this->m_size.y-1,0,this->m_size.z-1);
        importer->SetDataOrigin(0,0,0);
        importer->SetDataSpacing( 1.0/scale[0], 1.0/scale[1], 1.0/scale[2]);
        importer->SetImportVoidPointer(mmappedData);
        importer->Update();
    }
#endif

    cerr << "-------------- Save Transformed Slices ------------" << endl;
    qDebug() << "           Input  Size " << this->m_size.x << " " << this->m_size.y << " " << this->m_size.z;
    qDebug() << "Requesting Output Spacing " << 1.0/scale[0] << " " << 1.0/scale[1] << " " << 1.0/scale[2];
    if (paddedSize)
        qDebug() << "Requesting Output Size " << paddedSize[0] << " " << paddedSize[1] << " " << paddedSize[2];
    else
        qDebug() << "Requesting Output Size " << fixedSize.x*scale[0] << " " << fixedSize.y*scale[1] << " " << fixedSize.z*scale[2];

    vtkSmartPointer<vtkImageReslice> tmpSlicer = vtkSmartPointer<vtkImageReslice>::New();
    tmpSlicer->SetResliceTransform(xform);
    tmpSlicer->SetOutputDimensionality(2);
    tmpSlicer->SetOutputScalarType(VTK_UNSIGNED_SHORT);
    tmpSlicer->SetInterpolationModeToLinear();
#ifdef Q_OS_MACX
    if (useRawImageStack)
        tmpSlicer->SetInputConnection(importer->GetOutputPort());
    else
#endif
        tmpSlicer->SetInputConnection(m_imageSrc->GetOutputPort());

    vtkSmartPointer<vtkImageReslice> resizeToFixed = vtkSmartPointer<vtkImageReslice>::New();
    resizeToFixed->SetOutputDimensionality(2);
    resizeToFixed->SetInputConnection(tmpSlicer->GetOutputPort());

    vtkSmartPointer<vtkImageExtractComponents> m_extractor = vtkSmartPointer<vtkImageExtractComponents>::New();
    m_extractor->SetInputConnection(resizeToFixed->GetOutputPort());

    for (unsigned int z = 0; z < (paddedSize ? paddedSize[2] : fixedSize.z); z++) {

        qDebug() << "Working on z=" << z;
        tmpSlicer->SetResliceAxesOrigin(0,0,z);
        tmpSlicer->Update();
        if (paddedSize)
            resizeToFixed->SetOutputExtent(0, paddedSize[0]-1, 0, paddedSize[1]-1, 0, 0);
        else
            resizeToFixed->SetOutputExtent(0, (fixedSize.x*scale[0])-1, 0, (fixedSize.y*scale[1])-1, 0, 0);
        resizeToFixed->SetOutputOrigin(0,0,0);
        resizeToFixed->Update();

        qDebug() << "Saving z=" << z;

        for (unsigned int c =0; c < fixedSize.c; c++) {
            m_extractor->SetComponents(static_cast<int>(c));
            m_extractor->Update();

            if (z%1==0) {
                QString tmpOutputFileName(outputDirectory + "/" + QString::number(c).rightJustified(2, '0') + "/" + inputFile.completeBaseName() +
                                          "_xformed_" + QString::number(c).rightJustified(2, '0') + "_" + QString::number(z).rightJustified(5, '0') + ".tif");

                qDebug() << "Saving Hyperstack to " << tmpOutputFileName;

                vtkSmartPointer <vtkTIFFWriter> tiffWriter = vtkSmartPointer<vtkTIFFWriter>::New();
                tiffWriter->SetFileName(tmpOutputFileName.toStdString().c_str());
                tiffWriter->SetInputConnection(m_extractor->GetOutputPort());
                tiffWriter->Write();
            }
        }
    }
#ifdef Q_OS_MACX
    if (useRawImageStack) {
        int rc = munmap(mmappedData, filesize);
        assert(rc == 0);
        close(fd);
    }
#endif
    return true;
}

vtkSmartPointer <vtkImageData> HyperstackTiff::getImageData(vtkAbstractTransform *xform, HyperstackSize size, const VoxelCoordinate& pOrigin, int zoff)

{
    std::vector <vtkSmartPointer<vtkImageData>> im;
    vtkSmartPointer <vtkImageBlend> appendFilter = vtkSmartPointer<vtkImageBlend>::New();

    appendFilter->SetBlendModeToCompound();

    m_slicer->SetInputConnection(m_imageSrc->GetOutputPort());
    m_slicer->SetResliceTransform(xform);
    m_slicer->SetOutputOrigin(pOrigin.x, pOrigin.y, pOrigin.z);
    m_slicer->SetOutputExtent(0, static_cast<int>(size.x) - 1,
                              0, static_cast<int>(size.y) - 1,
                              zoff, zoff + size.z);

    const int extent[6] = {0, static_cast<int>(size.x) - 1,
                           0, static_cast<int>(size.y) - 1,
                           zoff, (int)(zoff+size.z)};

    bool firstChannel = true;
    for (unsigned int c = 0; c < m_size.c; c++)
    {
        im.push_back(vtkSmartPointer<vtkImageData>::New());
        if (channelWindowLevel[c].active == true) {
            m_extractors[c]->SetInputConnection(m_slicer->GetOutputPort());
            m_extractors[c]->UpdateExtent(extent);
            m_extractors[c]->Update();

            im[c]->SetDimensions(static_cast<int>(size.x), static_cast<int>(size.y), 1);
            im[c]->AllocateScalars(m_extractors[c]->GetOutput()->GetScalarType(), 1);
            im[c]->DeepCopy(m_extractors[c]->GetOutput());

            m_mapColors[c]->SetInputData(im[c]);
            m_mapColors[c]->Update();
            if (firstChannel) {
                firstChannel = false;
                appendFilter->SetInputConnection(0, m_mapColors[c]->GetOutputPort());
            }
            else
                appendFilter->AddInputConnection(0, m_mapColors[c]->GetOutputPort());
        }
    }
    appendFilter->Update();
    return appendFilter->GetOutput();

}

vtkSmartPointer <vtkImageData> HyperstackTiff::getImageData(int z, vtkAbstractTransform *xform, HyperstackSize size)
{
    std::vector <vtkSmartPointer<vtkImageData>> im;
    vtkSmartPointer <vtkImageBlend> appendFilter = vtkSmartPointer<vtkImageBlend>::New();

    appendFilter->SetBlendModeToCompound();
    vtkAlgorithm *imageInputPtr;
    // Extract result back to QImage

    if (xform) {

        if ((size.x > m_size.x) || (size.y > m_size.y) || (size.z > m_size.z)) {
            m_paddedImage->SetOutputWholeExtent(0, static_cast<int>(std::max(m_size.x, size.x) - 1),
                                                0, static_cast<int>(std::max(m_size.y, size.y) - 1),
                                                0, static_cast<int>(std::max(m_size.z, size.z) - 1));
            m_slicer->SetInputConnection(m_paddedImage->GetOutputPort());
        } else
            m_slicer->SetInputConnection(m_imageSrc->GetOutputPort());

        m_slicer->SetResliceTransform(xform);

        m_slicer->SetOutputOrigin(0,0,0);
        m_slicer->SetOutputExtent(0, static_cast<int>(size.x) - 1,
                                  0, static_cast<int>(size.y) - 1,
                                  z, z);

        imageInputPtr = m_slicer;
    } else {
        size.x = m_size.x;
        size.y = m_size.y;
        size.z = m_size.z;
        size.c = m_size.c;
        imageInputPtr = m_imageSrc;
    }

    const int extent[6] = {0, static_cast<int>(size.x) - 1, 0, static_cast<int>(size.y) - 1, z, z};
    bool firstChannel = true;
    for (unsigned int c = 0; c < m_size.c; c++) {
        im.push_back(vtkSmartPointer<vtkImageData>::New());
        if (channelWindowLevel[c].active == true) {
            m_extractors[c]->SetInputConnection(imageInputPtr->GetOutputPort());
            m_extractors[c]->UpdateExtent(extent);
            m_extractors[c]->Update();

            im[c]->SetDimensions(static_cast<int>(size.x), static_cast<int>(size.y), 1);
            im[c]->AllocateScalars(m_extractors[c]->GetOutput()->GetScalarType(), 1);
            im[c]->DeepCopy(m_extractors[c]->GetOutput());

            m_mapColors[c]->SetInputData(im[c]);
            m_mapColors[c]->Update();
            if (firstChannel) {
                firstChannel = false;
                appendFilter->SetInputConnection(0, m_mapColors[c]->GetOutputPort());
            }
            else
                appendFilter->AddInputConnection(0, m_mapColors[c]->GetOutputPort());
        }
    }
    appendFilter->Update();
    return appendFilter->GetOutput();
}

/// @return QImage object with current transform applied.
QImage HyperstackTiff::imageBuffer(int z, vtkAbstractTransform *xform,
                                   HyperstackSize size)
{
    vtkSmartPointer <vtkImageData> imageData = getImageData(z, xform, size);
    return vtkImageDataToQImageOriginal(imageData);
}

HyperstackSize HyperstackTiff::size() const
{
    return m_size;
}

void HyperstackTiff::retrieveSubVolume(VolumeBuffer &subVolume, const VoxelCoordinate &pCenter, vtkAbstractTransform *xform,
                                       const HyperstackSize& worldSize, int channelPolicy)
{
    VoxelCoordinate p0(pCenter.x - subVolume.size.x/2, pCenter.y - subVolume.size.y/2, pCenter.z - subVolume.size.z/2);

    //if border policy is translate
    {
    p0.x = qMax(p0.x, 0);
    p0.y = qMax(p0.y, 0);
    p0.z = qMax(p0.z, 0);

    p0.x = qMin(p0.x, (int)worldSize.x-1);
    p0.y = qMin(p0.y, (int)worldSize.y-1);
    p0.z = qMin(p0.z, (int)worldSize.z-1);
    }

    int nVolumeStep = (subVolume.size.c)*sizeof(VoxelType);

    vtkSmartPointer <vtkImageData> imageData = getImageData(xform, {subVolume.size.x, subVolume.size.y, subVolume.size.z, m_size.c, 0,0},
                                                            {p0.x, ((int)(worldSize.y - subVolume.size.y))-p0.y, p0.z}, 0);

    unsigned char *colorsPtr = reinterpret_cast<unsigned char *>( imageData->GetScalarPointer());
    int nStep = imageData->GetNumberOfScalarComponents();

    for(unsigned int iZ = 0; iZ < subVolume.size.z; iZ++)
    {
        for (unsigned int iY = 0; iY < subVolume.size.y; iY++)
        {
            VoxelType * pVol = subVolume.ptr(0, subVolume.size.y-iY-1, iZ);
            for (unsigned int iX = 0; iX < subVolume.size.x; iX++)
            {
                pVol[0] =(VoxelType) qMax(colorsPtr[0], qMax(colorsPtr[1], colorsPtr[2]));
                pVol += nVolumeStep;
                colorsPtr += nStep;
            }
        }
    }

}

/////////

bool HyperstackTiff::loadStreamingMultipleTiffs(const QVector<QString> filePaths) {

    // Record start time
    // auto start = std::chrono::high_resolution_clock::now();
    for (int c = 0; c < filePaths.size(); c++) {
        //auto start_set = std::chrono::high_resolution_clock::now();
        m_sparse_readers.push_back(vtkSmartPointer<vtkTIFFReader>::New());
        m_imageSlabs.push_back(vtkSmartPointer<vtkImageSlab>::New());

        QFileInfo fileInfo(filePaths[c]);

        TIFFSetErrorHandler(handle_error);

        m_ready = false;
        if (!fileInfo.exists()) {
            qDebug() << "ERROR: TIFF file does not exist!";
            return false;
        }
        m_tif = TIFFOpen(filePaths[c].toStdString().c_str(), "r");
        if (!m_tif) {
            qDebug() << "ERROR: TIFF object null!";
            return false;
        }
        m_filePath = filePaths[c];
        m_name = fileInfo.baseName();
        qDebug() << "Trying to load file " << m_name;

        loadMetadata(m_tif);
        if (m_numImages == 0) {
            m_numImages = countPages(m_tif);
        }
        qDebug() << "Image count for \"" << fileInfo.fileName() << "\":" << m_numImages;

        if (m_size.z == 0 && m_size.t == 0) {
            // assume this is a z stack unless user tells us otherwise
            m_size.z = m_numImages;
        }
        if (m_size.c == 0) {
            m_size.c = 1;
        }
        if (m_size.c == m_size.z && m_size.c > 20) {
            m_size.c = 1;
        }

        qDebug() << "m_size.z (" << m_size.z << ").";
        TIFFClose(m_tif);
        m_tif = nullptr;
        m_initted = true;
        m_ready = true;

        //Read the image
        m_sparse_readers[c]->SetFileName(filePaths[c].toUtf8().constData());
        if (m_photometric != TIFFTAG_PHOTOMETRIC_RGB) {
            m_sparse_readers[c]->SetNumberOfScalarComponents(1);
            m_sparse_readers[c]->SetDataExtent(0, static_cast<int>(m_size.x - 1),
                                               0, static_cast<int>(m_size.y - 1),
                                               0, static_cast<int>(m_size.z - 1));
        }

        m_sparse_readers[c]->SetDataSpacing(1, 1, 1);
        m_sparse_readers[c]->SetDataOrigin(0.0, 0.0, 0.0);
        m_sparse_readers[c]->IgnoreColorMapOn();
        m_sparse_readers[c]->SetOrientationType(ORIENTATION_BOTLEFT);

        // the above costs < 1ms
        switch (m_size.bpp) {
            case 32:
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedInt();
                break;
            case 16:
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedShort();
                break;
            case 8:
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedChar();
                break;
            default:
                qWarning() << "Unrecognized channel_size " << m_size.bpp;
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedChar();
        }

        //auto set = std::chrono::high_resolution_clock::now();
        //std::chrono::duration<double> elapsed1 = set - start_set;
        //qDebug() << "Elapsed time (tiff reader settings 1): " << elapsed1.count() << " s\n";

        m_sparse_readers[c]->UpdateWholeExtent();
        //auto update0 = std::chrono::high_resolution_clock::now();
        //std::chrono::duration<double> elapsed0 = update0 - set;
        //qDebug() << "Elapsed time (tiff reader update): " << elapsed0.count() << " s\n";
        m_sparse_readers[c]->UpdateInformation();

        //auto update = std::chrono::high_resolution_clock::now();
        //std::chrono::duration<double> elapsed2 = update - update0;
        //qDebug() << "Elapsed time (tiff reader update): " << elapsed2.count() << " s\n";

        switch (m_size.bpp) {
            case 32:
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedInt();
                break;
            case 16:
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedShort();
                break;
            case 8:
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedChar();
                break;
            default:
                qWarning() << "Unrecognized channel_size " << m_size.bpp;
                m_sparse_readers[c]->SetDataScalarTypeToUnsignedChar();
        }

        //auto set2 = std::chrono::high_resolution_clock::now();
        //std::chrono::duration<double> elapsed3 = set2 - update;
        //qDebug() << "Elapsed time (tiff reader settings 2): " << elapsed3.count() << " s\n";
        m_imageSlabs[c]->SetInputConnection(m_sparse_readers[c]->GetOutputPort());
        m_imageSlabs[c]->SetSliceRange(0,0);
        m_imageSlabs[c]->Update();
        //auto slab = std::chrono::high_resolution_clock::now();
        //std::chrono::duration<double> elapsed4 = slab - set2;
        //qDebug() << "Elapsed time (imageslab settings): " << elapsed4.count() << " s\n";
    }

    // Record end time
    //auto closedTiff = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double> elapsed = closedTiff - start;
    //qDebug() << "Elapsed time (tiff reader reads and settings): " << elapsed.count() << " s\n";

    m_size.c = static_cast<unsigned int>(filePaths.size());

    // Initializing the colormaps for later
    for (unsigned int c = 0; c < m_size.c; c++) {
        m_mapColors.push_back(vtkSmartPointer<vtkImageMapToColors>::New());
    }
    // Record end time
    //auto color = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double> elapsed5 = color - closedTiff;
    //qDebug() << "Elapsed time (Initializing the colormaps for late): " << elapsed5.count() << " s\n";

    channelWindowLevel = std::vector<windowLevel>(m_size.c);
    double *pixelVal;
    for (unsigned int c = 0; c < m_size.c; c++) {
        pixelVal = m_sparse_readers[c]->GetOutput()->GetScalarRange();
        if (channelWindowLevel[c].intensityMin > pixelVal[0])
            channelWindowLevel[c].intensityMin = static_cast<int32>(pixelVal[0]);
        if (channelWindowLevel[c].intensityMax < pixelVal[1])
            channelWindowLevel[c].intensityMax = static_cast<int32>(pixelVal[1]);
    }
    // Record end time
    //auto window = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double> elapsed6 = window - color;
    //qDebug() << "Elapsed time (chennal window level): " << elapsed6.count() << " s\n";

    for (auto &i : channelWindowLevel) {
        i.windowMin = i.intensityMin;
        i.windowMax = i.intensityMax;
    }
    updateColorTransferFunctions();
    // Record end time
    //auto update2 = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double> elapsed7 = update2 - window;
    //qDebug() << "Elapsed time (update color transfer function): " << elapsed7.count() << " s\n";

    return true;
}


/// @return QImage object with current transform applied.
QImage HyperstackTiff::imageBufferSparse(int zLower, int zUpper, HyperstackSize size)
{
    std::vector <vtkSmartPointer<vtkImageData>> im;
    vtkSmartPointer <vtkImageBlend> appendFilter = vtkSmartPointer<vtkImageBlend>::New();

    appendFilter->SetBlendModeToCompound();

    // Extract result back to QImage
    size.x = m_size.x;
    size.y = m_size.y;
    size.z = m_size.z;
    size.c = m_size.c;

    if(numActiveChannel() == 0){
        QImage img(size.x, size.y, QImage::Format_ARGB32_Premultiplied);
        img.fill(Qt::black);
        return img;
    } else {
        bool firstChannel = true;
        for (unsigned int c = 0; c < m_size.c; c++) {
            im.push_back(vtkSmartPointer<vtkImageData>::New());
            if (channelWindowLevel[c].active == true) {
                m_imageSlabs[c]->SetSliceRange(zLower,zUpper);
                m_imageSlabs[c]->Update();

                im[c]->SetDimensions(static_cast<int>(size.x), static_cast<int>(size.y), 1);
                im[c]->AllocateScalars(m_imageSlabs[c]->GetOutput()->GetScalarType(), 1);
                im[c]->DeepCopy(m_imageSlabs[c]->GetOutput());

                m_mapColors[c]->SetInputData(im[c]);
                m_mapColors[c]->Update();
                if (firstChannel) {
                    firstChannel = false;
                    appendFilter->SetInputConnection(0, m_mapColors[c]->GetOutputPort());
                }
                else
                    appendFilter->AddInputConnection(0, m_mapColors[c]->GetOutputPort());
            }
        }
        appendFilter->Update();
        return vtkImageDataToQImageOriginal(appendFilter->GetOutput());
    }
}

