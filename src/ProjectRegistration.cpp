#include <QObject>
#include <QFileInfo>

#ifdef MATLAB_SUPPORT
#include "MatFileDenseRoi.hpp"
#endif

#include "ApplicationData.hpp"
#include "ProjectRegistration.hpp"
#include "HyperstackTiff.hpp"
#

ProjectRegistration::ProjectRegistration() {
    QObject::connect(&m_landmarks, SIGNAL(transformModeChanged()), &(ApplicationData::instance()), SIGNAL(transformModeChanged()));
    QObject::connect(&m_landmarks, SIGNAL(transformModeChanged()), this, SLOT(saveConfig()));

    QObject::connect(&m_landmarks, SIGNAL(transformsChanged()), &(ApplicationData::instance()), SLOT(updateRegistration()));
    QObject::connect(&m_landmarks, SIGNAL(transformsChanged()), this, SLOT(transformROIs()));

    QObject::connect(&m_landmarks, SIGNAL(landmarksChanged()), this, SLOT(saveConfig()));
    QObject::connect(&ApplicationData::instance(), SIGNAL(fixedSessionChanged()), this, SLOT(saveConfig()));

    QObject::connect(&m_landmarks, SIGNAL(availableTransformModesChanged()), &(ApplicationData::instance()), SIGNAL(availableTransformModesChanged()));
}

ProjectRegistration::~ProjectRegistration() {
    disconnectLandmarkSignals();
    ApplicationData::instance().disconnect(SIGNAL(fixedSessionChanged()));
}

void ProjectRegistration::projectSpecificInit() {
    this->transformROIs();
}

void ProjectRegistration::addStackToLandmarks(stack_ptr_t stack) {
    m_landmarks.addNewSession(stack);
}

void ProjectRegistration::removeStackFromLandmarks(QString name) {
    m_landmarks.removeSession(name);
}

/// sets both the current stack window and the current stack to the associated stack index
void ProjectRegistration::setCurrentStackWindow(const stack_index_t index)
{
    m_currentStackWindow = index;
    if (index == REG_WINDOW_INDEX) {
        QString fixed_name = m_landmarks.fixedSessionName();
        for (stack_index_t i = 0; i < m_stack.size(); i++) {
            if (m_stack[i]->name() == fixed_name) {
                m_currentStack = i;
                break;
            }
        }
    } else {
        m_currentStack = m_stackWindows[index].stackIndex;
    }
}

void ProjectRegistration::setFixedSessionName(QString name){
    m_landmarks.setFixedSessionName(name);
};

void ProjectRegistration::writeConfigProjectSpecific() const
{
    writeStacks();
    m_jsonObject["roi_transform_mode"] = m_roiTransformMode;
    m_jsonObject["landmarks"] = m_landmarks.toJson();
    m_jsonObject["fixed"] = m_landmarks.fixedSessionName();
    m_jsonObject["registrationHints"] = registrationHints();
    m_jsonObject["ref"] = m_roiData->refSessionName();

}

QJsonObject ProjectRegistration::registrationHints() const
{
    try {
        QString fixedName = m_landmarks.fixedSessionName();
        if (fixedName.isEmpty())
            return QJsonObject();
        HyperstackSize fixedSize = getStack(fixedName)->size();

        auto movingName = movingSessionName();
        HyperstackSize movingSize = getStack(movingName)->size();

        QJsonObject result = {
            {
                "fixed", QJsonArray {
                 static_cast<int>(fixedSize.x),
                 static_cast<int>(fixedSize.y),
                 static_cast<int>(fixedSize.z)
             }
            },
            {
                "moving", QJsonArray {
                 static_cast<int>(movingSize.x),
                 static_cast<int>(movingSize.y),
                 static_cast<int>(movingSize.z)
             }
            }
        };
        return result;
    }
    catch (...) {
        return QJsonObject {};
    }
}

void ProjectRegistration::readRoiTransformMode(const QJsonObject &json)
{
    if (json.contains("roi_transform_mode") && json["roi_transform_mode"].isString()) {
        m_roiTransformMode = json["roi_transform_mode"].toString();
    } else {
        m_roiTransformMode = "Transform ROIs";
    }
}

void ProjectRegistration::readConfigProjectSpecific(const QJsonObject &json)
{
    readRoiTransformMode(json);
    readStacks(json);
    readRoiFiles(json);
    readLandmarks(json);
    readRegistrationHints(json);    
}

void ProjectRegistration::readRegistrationHints(const QJsonObject &json)
{
    if (json.contains("registrationHints") && json["registrationHints"].isObject()) {
        m_registrationHints = json["registrationHints"].toObject();
    }
}

QJsonObject ProjectRegistration::getRegistrationHints() {
    return m_registrationHints;
}

void ProjectRegistration::readLandmarks(const QJsonObject &json)
{
    m_landmarks.bFlipVTK = this->bFlipVTK;
    if (json.contains("landmarks") && json["landmarks"].isObject()) {
        m_landmarks.fromJson(json["landmarks"].toObject());
    }
    if (json.contains("fixed")) {
        QString fixedName = json["fixed"].toString();
        if (fixedName != "") {
            m_landmarks.setFixedSessionName(fixedName);
        }
    }
    m_landmarks.updateTransforms();
}

QString ProjectRegistration::movingSessionName() const {
    auto fixedName = m_landmarks.fixedSessionName();
    // Find first moving session in order to name output files
    auto it = std::find_if(m_stack.begin(), m_stack.end(),
                           [&fixedName](const auto *s) {
                               return s->name() != fixedName;
                           });
    if (it == m_stack.end())
        return QString();
    return (*it)->name();
}

// XXX: non-member
void ProjectRegistration::renderTransform()
{
    auto fixedName = m_landmarks.fixedSessionName();
    auto *fixed = getStack(fixedName);

    // Find first moving session in order to name output files
    auto it = std::find_if(m_stack.begin(), m_stack.end(),
                           [&fixedName](const auto *s) {
                               return s->name() != fixedName;
                           });
    if (fixed == nullptr || it == m_stack.end()) {
        return;
    }

    auto movingName = (*it)->name();
    QString dirName = QFileInfo(m_filePath.toLocalFile()).absolutePath();
    QString filenameBase = dirName + "/" + movingName + "-registered";
    QString tiffPath = filenameBase + ".tif";
    QString roiPath = filenameBase + ".mat";

    auto stacks = renderList();
    renderToTiff(tiffPath, stacks, fixed->size());
    bool translate = m_roiTransformMode == "Translate ROIs";

    if (m_roiData->transformRois(stackNames(), m_landmarks, translate)) {
#ifdef MATLAB_SUPPORT
        qInfo() << "Saving " << roiPath;
        MatFileDenseRoi m(m_roiData);
        m.save(TRANSFORMED_SESSION, roiPath.toStdString());
#else
        LOG(INFO) << "Did not save! Matlab support disabled.";
#endif
    }
}

void ProjectRegistration::transformROIs()
{
    bool translate = m_roiTransformMode == "Translate ROIs";
    roiData()->transformRois(stackNames(), m_landmarks, translate);
    ApplicationData::instance().broadcastROIModified();
}

void ProjectRegistration::setRoiTransformMode(const QString &mode)
{
    m_roiTransformMode = mode;
    saveConfig();
}

stack_render_list ProjectRegistration::renderList()
{
    stack_render_list stacks;
    for (auto &s : stackNames()) {
        if (s != fixedSessionName()) {
            stacks.emplace_back(*getStack(s), getTransform(s));
        }
    }
    return stacks;
}

