#include "ProjectAnnotationDense.hpp"

void ProjectAnnotationDense::writeConfigProjectSpecific() const
{
    writeStacks();
    m_jsonObject["ref"] = m_roiData->refSessionName();
}
