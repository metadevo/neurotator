/* Max-Z Projection Based 3D Template Registration
 * author: Ozgur Gonen
 * ozgur.gonen@gmail.com
*/

#include <opencv2\opencv.hpp>
#include <map>
#include <qdebug.h>

#include "RegisterRigid3D.h"
#include "ApplicationData.hpp"

using namespace cv;

double getPSNR(const Mat& I1, const Mat& I2)
{
    Mat s1;
    absdiff(I1, I2, s1);       // |I1 - I2|
    s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
    s1 = s1.mul(s1);           // |I1 - I2|^2

    Scalar s = sum(s1);         // sum elements per channel

    double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels

    if( sse <= 1e-10) // for small values return zero
        return 0;
    else
    {
        double  mse =sse /(double)(I1.channels() * I1.total());
        double psnr = 10.0*log10((255*255)/mse);
        return psnr;
    }
}



double getSR(const Mat& I1)
{
    Mat s1(I1);
    s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
    s1 = s1.mul(s1);

    Scalar s = sum(s1);         // sum elements per channel
    double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels

    if( sse <= 1e-10) // for small values return zero
        return 0;
    else
    {
        double  mse = sse /(double)(I1.channels() * I1.total());
        return mse / (255*255);
        //double psnr = 10.0*log10((255*255)/mse);
        //return psnr;
    }
}

void maxZProject(const VolumeBuffer& vb, int iZ0, int iZ1, Mat3b & mat)
{
    int size = vb.size.x * vb.size.y;
    for(int iZ = iZ0; iZ < iZ1; iZ++)
    {
        Vec3b * data_mat = ((Vec3b*)mat.data);
        const VoxelType* data_vb = vb.c_ptr(0, 0, iZ);
        for(int i = 0; i < size; i++)
        {
            Vec3b & di = *(data_mat + i);
            const VoxelType* vb_di = data_vb + (i*3);

            di[0] = qMax(di[0], vb_di[0]);
            di[1] = qMax(di[1], vb_di[1]);
            di[2] = qMax(di[2], vb_di[2]);
        }
    }
}

void maxZProject(const VolumeBuffer& vb, int iZ0, int iZ1, Mat2b & mat)
{
    int size = vb.size.x * vb.size.y;
    for(int iZ = iZ0; iZ < iZ1; iZ++)
    {
        Vec2b * data_mat = ((Vec2b*)mat.data);
        const VoxelType* data_vb = vb.c_ptr(0, 0, iZ);
        for(int i = 0; i < size; i++)
        {
            Vec2b & di = *(data_mat + i);
            const VoxelType* vb_di = data_vb + (i*2);
            di[0] = qMax(di[0], vb_di[0]);
            di[1] = qMax(di[1], vb_di[1]);
        }
    }
}

void maxZProject(const VolumeBuffer& vb, int iZ0, int iZ1, Mat1b & mat)
{
    int size = vb.size.x * vb.size.y;
    for(int iZ = iZ0; iZ < iZ1; iZ++)
    {
        const VoxelType* data_vb = vb.c_ptr(0, 0, iZ);
        for(int i = 0; i < size; i++)
        {
            const VoxelType* vb_di = data_vb + i;
            uchar * di = mat.data + i;
            di[0] = qMax(di[0], vb_di[0]);
        }
    }
}

void myBlur(Mat & m0, Mat m1, int k)
{
    int sx = m0.size[0], sy = m0.size[1];

    uchar * m0dat = m0.data;
    uchar * m1dat = m1.data;

    for(int iy = k/2; iy < sy-k/2; iy++)
    {
        for(int ix = k/2; ix < sx-k/2; ix++)
        {
            int sum = 0;
            for(int jk = 0; jk < k; jk++)
                for(int ik = 0; ik < k; ik++)
                {
                    sum += *(m0dat + ((ix-k/2+ik) + (iy-k/2+jk)*sx));
                }
            m1dat[ix + iy*sx] = sum*1.0 / (k*k);
        }
    }
}

double stdOverMean(Mat & m0)
{
    int size = m0.size[0] * m0.size[1];

    uchar * m0dat = m0.data;
    int sum = 0;
    for(int i=0; i <size ; i++)
    {
        sum += *(m0dat + i);
    }
    int mean = sum*1.0 / size;

    sum = 0;
    for(int i=0; i <size ; i++)
    {
        sum += pow((*(m0dat + i) - mean), 2);
    }
    return sqrt(sum/(size-1.0)) / mean;
}

float registerRigid3D(const VolumeBuffer & refenceVB,
                     const VolumeBuffer & offsetVB,
                     float & rSTDThreshold0, float &rSTDThreshold1,
                     VoxelCoordinate & offset, bool isTakeCaps)
{    
    offset = {0,0,0};
    if (refenceVB.size.c != offsetVB.size.c)
    {
        return 0.0;
    }

    assert(refenceVB.size.c == offsetVB.size.c);

    int
            nZOffsetLen = refenceVB.size.z - offsetVB.size.z,
            nYOffsetLen = refenceVB.size.y - offsetVB.size.y,
            nXOffsetLen = refenceVB.size.x - offsetVB.size.x;

    Mat matMaxZ;

    if (refenceVB.size.c == 1)
    {
        Mat1b matMaxZ_1b(offsetVB.size.y, offsetVB.size.x, uchar(0));
        maxZProject(offsetVB, 0, offsetVB.size.z, matMaxZ_1b);
        matMaxZ = matMaxZ_1b;
    }else if (refenceVB.size.c == 2){
        Mat2b matMaxZ_2b(offsetVB.size.y, offsetVB.size.x, Vec2b(0,0));
        maxZProject(offsetVB, 0, offsetVB.size.z, matMaxZ_2b);
        matMaxZ = matMaxZ_2b;
    }else if (refenceVB.size.c == 3){
        Mat3b matMaxZ_3b(offsetVB.size.y, offsetVB.size.x, Vec3b(0,0,0));
        maxZProject(offsetVB, 0, offsetVB.size.z, matMaxZ_3b);
        matMaxZ = matMaxZ_3b;
    }else{
        return 0.0; //not supported
    }

    //Mat matMaxZ_b = Mat::zeros(matMaxZ.rows, matMaxZ.cols, CV_8U);
    //myBlur(matMaxZ, matMaxZ_b, 8);
    //GaussianBlur(matMaxZ, matMaxZ_b, Size(4, 4), 0);
    //double psnr = getPSNR(matMaxZ, matMaxZ_b);

    double std1 = stdOverMean(matMaxZ);

    if (rSTDThreshold1 > std1)
    {
        // skip
        rSTDThreshold1 = std1;
        rSTDThreshold0 = 0;
        return -1.0;
    }

    double maxCorr = 0.0, initCorr;
    int iZMaxCorr = 0;
    Point MaxOffset;

    for(int iZ = 0; iZ < nZOffsetLen; iZ++)
    {
        //retrieve a moving MaxProjZ (iZ:iZ+offsetVB.size.z) of refence volume
        Mat refenceVB_matMaxZ;
        if (refenceVB.size.c == 1)
        {
            Mat1b refenceVB_matMaxZ_1b(refenceVB.size.y, refenceVB.size.x, uchar(0));
            maxZProject(refenceVB, iZ, offsetVB.size.z + iZ, refenceVB_matMaxZ_1b);
            refenceVB_matMaxZ = refenceVB_matMaxZ_1b;
        }else if (refenceVB.size.c == 2){
            Mat2b refenceVB_matMaxZ_2b(refenceVB.size.y, refenceVB.size.x, Vec2b(0,0));
            maxZProject(refenceVB, iZ, offsetVB.size.z + iZ, refenceVB_matMaxZ_2b);
            refenceVB_matMaxZ = refenceVB_matMaxZ_2b;
        }else if (refenceVB.size.c == 3){
            Mat3b refenceVB_matMaxZ_3b(refenceVB.size.y, refenceVB.size.x, Vec3b(0,0,0));
            maxZProject(refenceVB, iZ, offsetVB.size.z + iZ, refenceVB_matMaxZ_3b);
            refenceVB_matMaxZ = refenceVB_matMaxZ_3b;
        }else{
            return 0.0; //not supported
        }

        //compute corr matrix
        Mat result_mat;
        matchTemplate(refenceVB_matMaxZ, matMaxZ, result_mat, TM_CCORR_NORMED);
        Point offset_iZ;

        //pick max corr point
        double val;
        minMaxLoc(result_mat, NULL, &val, NULL, &offset_iZ);
        if (val > maxCorr)
        {
            maxCorr = val;
            iZMaxCorr = iZ;
            MaxOffset = offset_iZ;
        }

        if (iZ == 0)
        {
            initCorr = ((float*)(result_mat.data))[ nXOffsetLen/2-1 + nXOffsetLen*(nYOffsetLen/2-1)];
        }

        if (maxCorr > 0.999998) //early bail
        {
            break;
        }        
    }

    Mat1b refenceVB_matMaxZ_1b(refenceVB.size.y, refenceVB.size.x, uchar(0));
    maxZProject(refenceVB, iZMaxCorr, offsetVB.size.z + iZMaxCorr, refenceVB_matMaxZ_1b);

    double std0 = stdOverMean(refenceVB_matMaxZ_1b);

    if (rSTDThreshold0 > std0)
    {
        rSTDThreshold0 = std0;
        rSTDThreshold1 = 0;
        // skip
        return -1.0;
    }

    offset.x = MaxOffset.x - (nXOffsetLen/2);
    offset.y = MaxOffset.y - (nYOffsetLen/2);
    offset.z = iZMaxCorr - (nZOffsetLen/2);

    rSTDThreshold0 = std0;
    rSTDThreshold1 = std1;

    if (isTakeCaps)
    {
        VolumeBufferCapsProvider::offsetCaps(refenceVB_matMaxZ_1b.data, QSize(refenceVB.size.x,refenceVB.size.y),
                                             matMaxZ.data, QSize(offsetVB.size.x, offsetVB.size.y),
                                             offset.x, offset.y);
    }


    qDebug()<<"registerRigid3D: offset=("<< offset.x<<", "<< offset.y <<", "<<offset.z<<
              ") correlation=" <<initCorr<<"to"<<maxCorr<<"("<< ((maxCorr/initCorr-1.0)*100)<<"%)" << " std ="<<std1;

    return (float)maxCorr;
}

QImage VolumeBufferCapsProvider::caps;

QImage VolumeBufferCapsProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    return caps;
}

void VolumeBufferCapsProvider::setBitMap(unsigned char *data, QImage & img)
{
    QSize size = img.size();
    for(int iy = 0; iy < size.height(); iy++)
    {
        for(int ix = 0; ix < size.width(); ix++)
        {
            unsigned char col = data[ix + iy*size.width()];
            img.setPixelColor(ix, iy, QColor(0, col, 0));
        }
    }
}

void VolumeBufferCapsProvider::offsetCaps(unsigned char* data0, const QSize & size0,
                                          unsigned char* data1, const QSize & size1,
                                          int x0, int y0)
{
    caps = QImage(size0, QImage::Format_RGB888);
    setBitMap(data0);

    int
            x_off = x0 + (size0.width() - size1.width())/2,
            y_off = y0 + (size0.height() - size1.height())/2;

    for(int iy = 0; iy < size1.height(); iy++)
    {
        for(int ix = 0; ix < size1.width(); ix++)
        {
            int c1 = data1[ix + iy*size1.width()];
            QColor c0 = caps.pixelColor(ix + x_off, iy + y_off);
            if (ix ==0 || ix == (size1.width()-1) || iy ==0 || iy == (size1.height()-1))
            {
                c1 += 64;
            }
            c1 = qMin(c1, 255);
            caps.setPixelColor(ix + x_off, iy + y_off, QColor(c1, c0.green(), 0));
        }
    }
}


//tmp dump
/*
//VolumeBuffer::dumpToFile("c:/tmp/nero/fixed_maxz.dat", refenceVB_matMaxZ_1b.data, refenceVB.size.x*refenceVB.size.y);
//VolumeBuffer::dumpToFile("c:/tmp/nero/moving_maxz.dat", matMaxZ.data, offsetVB.size.x*offsetVB.size.y);
*/
