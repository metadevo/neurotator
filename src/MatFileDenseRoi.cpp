/// @author Sam Kenyon <sam@metadevo.com>
#include <string>
#include <vector>
#include <QDebug>
#include <QString>
#include "mat.h"

#include "MatFileDenseRoi.hpp"

MatFileDenseRoi::MatFileDenseRoi(RoiDataDense *roiData) :
    MatFile(dynamic_cast<RoiDataBase*>(roiData))
{
}

MatFileDenseRoi::~MatFileDenseRoi()
{
}

bool MatFileDenseRoi::examine()
{
    bool foundToplevel = false;
    int num;
    char** nameList = matGetDir(m_mf, &num);
    if (nameList != nullptr) {
        if (num > 0) {
            m_topArrayName = nameList[0];
            for (int i = 0; i < num; ++i) {
                qInfo() << "Array found: " << nameList[i];
            }
        }
        else {
            qWarning() << "Zero arrays found.";
        }
        mxFree(nameList);
    } else {
        qWarning() << "Error finding arrays.";
    }
    if (m_topArrayName != m_defaultToplevel) {
        qWarning() << "First array is not named \"" << m_defaultToplevel.c_str() << "\". Attempting to load anyway.";
    } else {
        foundToplevel = true;
    }

    // Matlab API requires closing and reopening between some of these functions
    if (!close()) {
        return false;
    }
    if (!open()) {
        return false;
    }
    qInfo() << "Examining headers for " << m_topArrayName.c_str();
    mxArray *topArray;
    topArray = matGetVariableInfo(m_mf, m_topArrayName.c_str());
    if (topArray == nullptr) {
        qWarning() << "Error reading array.";
        return false;
    }
    m_numElements = examineArray(topArray);

    mxDestroyArray(topArray);
    if (!close()) {
        return false;
    }

    if (!foundToplevel && !m_toplevelIsField) {
        qWarning() << "Error: Could not find " << m_defaultToplevel.c_str();
        return false;
    } else {
        return true;
    }
}


bool MatFileDenseRoi::loadCustomData()
{
    // Matlab API requires closing and reopening between some of these functions
    if (!close()) {
        return false;
    }
    if (!open()) {
        return false;
    }

    qInfo() << "Reading data from " << m_topArrayName.c_str();
    mxArray* metaArray = nullptr;
    mxArray* topArray = nullptr;

    if (m_toplevelIsField) {
        qInfo() << "Getting " << m_defaultToplevel.c_str() << " out of " << m_topArrayName.c_str();
        metaArray = matGetVariable(m_mf, m_topArrayName.c_str());
        if (metaArray == nullptr) {
            qWarning() << "Error reading top level field array.";
            return false;
        }
        topArray = mxGetField(metaArray, 0, m_defaultToplevel.c_str());
    } else {
        topArray = matGetVariable(m_mf, m_topArrayName.c_str());
    }
    if (topArray == nullptr) {
        qWarning() << "Error reading array.";
        return false;
    }

    qInfo() << "Reading each element...";
    std::shared_ptr<RoiFieldsDense> newRoi(nullptr);
    mxArray* fieldArray;
    mwIndex index = 0;
    for (; index < m_numElements; index++)
    {
        newRoi = std::make_shared<RoiFieldsDense>();

        fieldArray = mxGetField(topArray, index, "name");
        newRoi->name = QString::fromStdString(loadString(fieldArray));
//        qDebug() << "name: " << newRoi->name;

        fieldArray = mxGetField(topArray, index, "linked_name");
        newRoi->setLinkedName(QString::fromStdString(loadString(fieldArray)));
        //LOG(DBUG) << "linked_name: " << newRoi->linkedName.toStdString();

        fieldArray = mxGetField(topArray, index, "linked_session");
        newRoi->setLinkedSession(QString::fromStdString(loadString(fieldArray)));
        //LOG(DBUG) << "linked_session:" << newRoi->linkedSession.toStdString();

        fieldArray = mxGetField(topArray, index, "centroid");
        newRoi->setCentroid(loadCoordinate(fieldArray));
        //LOG(DBUG) << "centroid: " << newRoi->centroid.x << "," << newRoi->centroid.y << "," << newRoi->centroid.z;

        fieldArray = mxGetField(topArray, index, "roi");
        newRoi->setMask(loadMask(fieldArray));
        //LOG(DBUG) << "roi vector size:" << newRoi->mask.size();

        // optional field
        fieldArray = mxGetField(topArray, index, "flag");
        if (fieldArray != nullptr) {
            bool* flag = mxGetLogicals(fieldArray);
            if (flag) {
                newRoi->setFlagged(*flag);
            }
        }

        // Insert it into the application's data manager
        dynamic_cast<RoiDataDense*>(m_roiData)->addNewRoi(newRoi->name, newRoi);
    }
    qInfo() << "Loaded " << index << " elements";

    if (metaArray) {
        mxDestroyArray(metaArray);
    } else {
        mxDestroyArray(topArray);
    }

    if (!close()) {
        return false;
    }
    return true;
}




bool MatFileDenseRoi::saveCustomData()
{
    QList<RoiDataBase::roi_t> roiList = dynamic_cast<RoiDataDense*>(m_roiData)->roisAtSession(m_sessionKey);
    m_topArrayName = "ROI_list";
    mxArray *topArray;
    mwSize nDims = 2;
    mwSize dims[2];
    dims[0] = 1;
    dims[1] = static_cast<mwSize>(roiList.size());
    const int numFields = 8;
    const char* fieldnames[numFields] = {"roi", "name", "centroid", "flag", "confirmed", "candidates", "linked_name", "linked_session"};
    topArray = mxCreateStructArray(nDims, dims, numFields, fieldnames);
    if (topArray == nullptr) {
        qWarning() << "Error reading array.";
        return false;
    }

    mxArray* tmp;
    mwSize centroidDims[2];
    centroidDims[0] = 1;
    centroidDims[1] = 3;
    mwSize singleDims[2];
    singleDims[0] = 1;
    singleDims[1] = 1;
    unsigned int index = 0;
    for (; index < dims[1]; index++)
    {
        //LOG(DBUG) << "save name [" << index << "]: " << roiList.at(index)->name.toStdString();

        // the string fields
        mxSetField(topArray, index, "name", mxCreateString(roiList.at(static_cast<int>(index))->name.toUtf8()));
        mxSetField(topArray, index, "linked_name", mxCreateString(roiList.at(static_cast<int>(index))->linkedName().toUtf8()));
        mxSetField(topArray, index, "linked_session", mxCreateString(roiList.at(static_cast<int>(index))->linkedSession().toUtf8()));

        tmp = mxCreateLogicalArray(2, singleDims);
        mxLogical logicalData[1] = {static_cast<mxLogical>(roiList.at(static_cast<int>(index))->flagged())};
        memcpy(mxGetData(tmp), logicalData, sizeof(logicalData));
        mxSetField(topArray, index, "flag", tmp);

        ///@todo how to use the confirmed field
        //mxSetField(topArray, index, "confirmed", mxCreateString(roiList.at(index)->confirmed));

        // centroid
        // y and x are out of order to match the format we've been getting for import
        double data[3] = {static_cast<double>(roiList.at(static_cast<int>(index))->getCentroid().y),
                            static_cast<double>(roiList.at(static_cast<int>(index))->getCentroid().x),
                            static_cast<double>(roiList.at(static_cast<int>(index))->getCentroid().z)};
        tmp = mxCreateNumericArray(2, centroidDims, mxDOUBLE_CLASS, mxREAL); // note we do not need to free this manually
        memcpy(mxGetData(tmp), data, sizeof(data));
        mxSetField(topArray, index, "centroid", tmp);

        // fill in the masks vector
        QVector<VoxelCoordinate>& coords = std::dynamic_pointer_cast<RoiFieldsDense>(roiList.at(static_cast<int>(index)))->mask;
        dynamic_cast<RoiDataDense*>(m_roiData)->maskImageToVector(coords, std::dynamic_pointer_cast<RoiFieldsDense>(roiList.at(static_cast<int>(index)))->masks);

        // copy that into the matlab array
        mwSize maskDims[2];
        maskDims[0] = static_cast<mwSize>(coords.size());
        if (coords.size() == 0) {
            qWarning() << roiList.at(index)->name << ": mask is empty";
        }
        maskDims[1] = 3;
//        tmp = mxCreateNumericArray(2, maskDims, mxUINT16_CLASS, mxREAL); // note we do not need to free this manually
//        mxUint16* tmpData = mxGetUint16s(tmp);
//        int si = 0;
//        for (mwSize i = 0; i < maskDims[0]; ++i)
//        {
//            si = static_cast<int>(i);
//            // y and x are out of order to match the format we've been getting for import
//            tmpData[i] = static_cast<mxUint16>(coords[si].y);
//            tmpData[maskDims[0] + i] = static_cast<mxUint16>(coords[si].x);
//            tmpData[maskDims[0] * 2 + i] = static_cast<mxUint16>(coords[si].z);
//        }
        tmp = mxCreateNumericArray(2, maskDims, mxDOUBLE_CLASS, mxREAL); // note we do not need to free this manually
        double* tmpData = mxGetDoubles(tmp);
        int si = 0;
        for (mwSize i = 0; i < maskDims[0]; ++i)
        {
            si = static_cast<int>(i);
            // y and x are out of order to match the format we've been getting for import
            tmpData[i] = static_cast<double>(coords[si].y);
            tmpData[maskDims[0] + i] = static_cast<double>(coords[si].x);
            tmpData[maskDims[0] * 2 + i] = static_cast<double>(coords[si].z);
        }
        mxSetField(topArray, index, "roi", tmp);

    }
    qInfo() << "Created " << index << " elements";

    int status = matPutVariable(m_mf, "ROI_list", topArray);
    if (status != 0) {
        qWarning() << "Put failed.";
        return false;
    }

    qDebug() << "Destroying array...";
    mxDestroyArray(topArray);

    if (!close()) {
        return false;
    }
    return true;
}
