#include <QDebug>
#include <QPainter>

#include "AppSettings.hpp"
#include "RoiDataSparse.hpp"

RoiDataSparse::RoiDataSparse()
{
    if (AppSettings::instance().find("roi_origin")) {
        QString origin = AppSettings::instance().get("roi_origin").toString();
        if (origin == "bottomleft") {
            m_originTopLeft = false;
        } else {
            m_originTopLeft = true;
        }
    } else {
        AppSettings::instance().set("roi_origin", "topleft");
    }
    qInfo() << "roi_origin: " << m_originTopLeft;
}

bool RoiDataSparse::addNewRoi(session_key_t session, roi_key_t name, roi_t fields)
{
    m_currSession = session;
    return addNewRoi(name, fields);
}

/// Can call this directly if adding all the rois right after adding a new session
/// because the class keeps state of current session.
bool RoiDataSparse::addNewRoi(roi_key_t name, roi_t fields)
{
    if (m_currSession != "") {
        auto it = m_sessions.find(m_currSession);
        if(it != m_sessions.end()){
            refreshRoi(m_currSession, std::dynamic_pointer_cast<RoiFieldsSparse>(fields));
            if (!it.value().contains(name)) {
                it.value()[name] = fields;
            }
        } else {
            qWarning() << "Session key ("<< m_currSession << ") unknown.";
            return false;
        }
    } else {
        qWarning() << "Current session key is empty";
        return false;
    }
    return true;
}

///@returns list of roi smart pointers (list may be empty)
QList<RoiDataBase::roi_t> RoiDataSparse::roisAtSessionFilterZ(session_key_t sessionKey, const z_key_t z) const
{
    auto it = m_maskTables.find(sessionKey);
    if (it != m_maskTables.end()) {
        return it.value()[z];
    } else {
        return QList<RoiDataBase::roi_t>();
    }
}

/// Converts a flat list of 3d coordinates into a stack of 2d images
void RoiDataSparse::createMaskImages(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks)
{
    QVector<QRgb> colors{QColor(0, 0, 0, 0).rgba(), QColor(255, 0, 0).rgba()};
    for (auto v : coords) {
        if (masks[v.z].mask.isNull()) {
            QImage img(masks[v.z].extent.width(), masks[v.z].extent.height(), QImage::Format_Mono);
            img.fill(0);
            img.setColorTable(colors);
            masks[v.z].mask = img;
        }

        masks[v.z].mask.setPixel(v.x - masks[v.z].extent.x(), v.y - masks[v.z].extent.y(), 1);
    }
}


void RoiDataSparse::createMaskOutlines(QHash<z_key_t, RoiMask>& masks)
{
    float scale = 0.82f;

    for (auto k : masks.keys()) {
        int sx = static_cast<int>(static_cast<float>(masks[k].mask.width()) * scale);
        int sy = static_cast<int>(static_cast<float>(masks[k].mask.height()) * scale);
        QImage middle = masks[k].mask.scaled(sx, sy, Qt::KeepAspectRatio);
        QImage outline = masks[k].mask.copy();
        QPainter p(&outline);
        p.setCompositionMode(QPainter::CompositionMode_DestinationOut);
        p.drawImage(masks[k].mask.width() / 2 - middle.width() / 2, masks[k].mask.height() / 2 - middle.height() / 2, middle); ///todo coords
        p.end();
        masks[k].maskOutline = outline;
    }
}

/// Initialize / construct extra ROI data we'll need based on the raw data
void RoiDataSparse::refreshRoi(session_key_t session, std::shared_ptr<RoiFieldsSparse> roi)
{
    //if (roi->linkedSession() != "" && roi->linkedName() != "") {
    //    roi->setConfirmed(true);
    // }

    if(roi->name == "invivo"){
       roi ->setInVivo(true);
    }

//    qDebug()<< "refreshRoi"<< m_currSession << "size" << roi->mask.size();

    for (auto v : roi->mask) {
        // look at all the mask coords and find the extents
        if (roi->masks[v.z].extent.isNull()) {
            roi->masks[v.z].extent.setCoords(v.x, v.y, 1, 1);
        }
        if (v.x < roi->masks[v.z].extent.x()) {
            roi->masks[v.z].extent.setLeft(v.x);
        }
        if (v.x > roi->masks[v.z].extent.right()) {
            roi->masks[v.z].extent.setRight(v.x);
        }
        if (v.y < roi->masks[v.z].extent.top()) {
            roi->masks[v.z].extent.setTop(v.y);
        }
        if (v.y > roi->masks[v.z].extent.bottom()) {
            roi->masks[v.z].extent.setBottom(v.y);
        }

        // add this to the z tables
        if (!m_maskTables[session][v.z].contains(roi)) {
            m_maskTables[session][v.z].append(roi);
        }
    }

    createMaskImages(roi->mask, roi->masks);
    createMaskOutlines(roi->masks);

    roi->mask.clear(); // don't need to waste this memory anymore
}

bool RoiDataSparse::addToCandidateList(const QString& sessionKey, const QString& roiKey){
    if (roiKey == "invivo"){
        return false;
    }
    QVector<int> candidates;
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        for(QHash<QString, std::shared_ptr<RoiFields> >::iterator roi = it.value().begin(); roi != it.value().end(); ++roi ){
            candidates = std::dynamic_pointer_cast<RoiFieldsSparse>(roi.value())->candidates();
            if(candidates.contains(roiKey.toInt())){
                qDebug() << "selected roi is already a candidate";
                return false;
            } else{
                candidates.append(roiKey.toInt());
            }
            std::dynamic_pointer_cast<RoiFieldsSparse>(roi.value())->setCandidates(candidates);
        }
        return true;
    }else{
        qDebug() << "session not found";
        return false;
    }
};

bool RoiDataSparse::deleteFromCandidateList(const QString& sessionKey, const QString& roiKey){
    if (roiKey == "invivo"){
        return false;
    }
    QVector<int> candidates;
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        for(QHash<QString, std::shared_ptr<RoiFields> >::iterator roiName = it.value().begin(); roiName != it.value().end(); ++roiName ){
            candidates = std::dynamic_pointer_cast<RoiFieldsSparse>(roiName.value())->candidates();
            if (candidates.contains(roiKey.toInt())) {
                candidates.remove(candidates.indexOf(roiKey.toInt()));
            }else{
                qDebug() << "selected roi is not a candidate";
                return false;
            }
            std::dynamic_pointer_cast<RoiFieldsSparse>(roiName.value())->setCandidates(candidates);
        }
        return true;
    }else{
        qDebug() << "session not found";
        return false;
    }
};

QList<int> RoiDataSparse::getCandidatelist(const QString& sessionKey){
    QVector<int> candidates;
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        candidates = std::dynamic_pointer_cast<RoiFieldsSparse>(it.value().value("1"))->candidates();
    }
    return candidates.toList();
}

QList<QVector<QVector<int>>> RoiDataSparse::getDecodeList(const QString& sessionKey){
    QList<QVector<QVector<int>>> decodeList;
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        for(int i = 0; i < it.value().size(); ++i){
            if(i < it.value().size() - 1){
                decodeList.append(std::dynamic_pointer_cast<RoiFieldsSparse>(it.value().value(QString::number(i+1)))->decode());
            } else if (i == it.value().size() - 1) {
                decodeList.append(std::dynamic_pointer_cast<RoiFieldsSparse>(it.value().value("invivo"))->decode());
            }
        }
    }
    return decodeList;
}

StringMatrix RoiDataSparse::getStackMatrix (session_key_t name, StackMatrix tiff)
{
    StringMatrix stackMatrix;
    auto it = m_sessions.find(name);
    if(it != m_sessions.end()){
        if (it.value().contains("1")) {
            if (tiff == tiff_aligned){
                stackMatrix = std::dynamic_pointer_cast<RoiFieldsSparse>(it.value().value("1"))->tiffAligned();
            }else if (tiff == tiff_exvivo){
                stackMatrix = std::dynamic_pointer_cast<RoiFieldsSparse>(it.value().value("1"))->tiffExVivo();
            }else{
                qWarning() << "sparse::stackPaths Roi_list type is neither tiff_aligned nor tiff_exvivo";
            }
        } else {
            qWarning() << "sparse::stackPaths Roi name ("<< name << ") unknown.";
        }
    } else {
        qWarning() << "sparse::stackPaths Session key ("<< name << ") unknown.";
    }
    return stackMatrix;
}

int RoiDataSparse::getCentroidInvivoZ(const QString& sessionKey) {
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        for(int i = 0; i < it.value().size(); ++i){
            if (it.value().contains("1")) {
                return std::dynamic_pointer_cast<RoiFieldsSparse>(it.value().value("1"))->centroidInVivo().z;
            }
        }
    }
    return 0;
}

