#include <QVector3D>
#include "ApplicationData.hpp"

#include <vtkLandmarkTransform.h>
#include <vtkThinPlateSplineTransform.h>
#include <vtkPoints.h>
#include <vtkTransform.h>

#include "LandmarkModel.hpp"
#include "HyperstackTiff.hpp"
#include "RegisterRigid3D.h"

#include <QElapsedTimer>

Landmark::Landmark(int x, int y, int z, const QString &label, int row) :
    location(x, y, z), label(label), row(row)
{}

const int LandmarkRole = Qt::UserRole + 1;
const int LabelRole = Qt::UserRole + 2;
const int FixedRole = Qt::UserRole + 3;
const int MovingRole = Qt::UserRole + 4;
const int CorrelationRole = Qt::UserRole + 5;

LandmarkModel::LandmarkModel() : QObject()
{
    QObject::connect(&m_model,
                     SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &,
                                        const QVector<int> &)),
                     this,
                     SIGNAL(landmarksChanged()));
    QObject::connect(&m_model,
                     SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
                     this,
                     SIGNAL(landmarksChanged()));
    QObject::connect(&m_model,
                     SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
                     this,
                     SLOT(updateTransforms()));
    QObject::connect(&m_model, SIGNAL(selectionChanged()),
                     &(ApplicationData::instance()), SIGNAL(landmarkSelectionChanged()));
    QObject::connect(this, SIGNAL(landmarkPredicted(int )),
                     &(ApplicationData::instance()), SIGNAL(landmarkPredicted(int )));

    QObject::connect(this, SIGNAL(landmarkAutoRegistered(int )),
                     &(ApplicationData::instance()), SIGNAL(landmarkAutoRegistered(int )));

}

void LandmarkDataModel::removeSelected()
{
    auto end = m_selected.crend();
    for (auto it = m_selected.crbegin(); it != end; it++) {
        removeRows(*it, 1);
    }
    m_selected.clear();
    emit selectionChanged();
}

void LandmarkModel::autoRegisterSelected()
{
    const int rows = m_model.rowCount();
    for (int i = 0; i < rows; i++)
    {
        if (m_model.isSelected(i))
            this->autoRegisterLandmark(i);
    }
}

QHash<int, QByteArray> LandmarkDataModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[LabelRole] = "label";
    roles[FixedRole] = "fixedSession";
    roles[MovingRole] = "movingSession";
    return roles;
}

void LandmarkModel::cachePreRotation(QString name, int pre_rotation) {
    preRotations[name] = pre_rotation;
}

QStringList LandmarkModel::availableTransformModes() const
{
    return { "Rigid Body", "Similarity", "Affine", "Thinplate" };
}

void LandmarkModel::addNewSession(const stack_ptr_t stack)
{
    session_key_t name = stack->name();
    assert(!m_sessions.contains(name));
    m_sessions.append(name);
    m_sessionData[name] = {static_cast<int>(stack->size().y), static_cast<int>(stack->size().x), nullptr, stack };
    m_model.setColumnCount(m_sessions.size() + 1);
}

void LandmarkModel::removeSession(const session_key_t &name)
{
    m_model.removeColumns(columnForSession(name), 1);

    int index = m_sessions.indexOf(name);
    assert(index >= 0);
    m_sessions.removeAt(index);
    m_sessionData.erase(name);
    qDebug() << "Session \""<< name << "\" removed.";
}

void LandmarkModel::addLandmark(const session_key_t &session,
                                float x, float y, int z)
{    
    // Can't add landmarks yet
    if (m_sessions.size() < 2) {
        return;
    }

    QVector3D value(x, y, static_cast<float>(z));   
    int column = columnForSession(session);

    // Fill in incomplete landmark
    if (!isLandmarkComplete())
    {
        int row = m_model.rowCount() - 1;
        m_model.setData(m_model.index(row, column), value, LandmarkRole);
        if (isLandmarkComplete())
        {
            updateSessionsForRow(row);
            if (ApplicationData::instance().autoRegisterOnDown() || m_populateLandmarksInProgress)
            {
                this->autoRegisterLandmark(row);
            }
            else
            {
                setCorrelation(row, 0.0);
                emit updateTransforms();
            }
            ApplicationData::instance().broadcastLandmarkCompleted();
        }
        return;
    }

    // Creating a new label for a brand new landmark
    QList<QStandardItem *> row;
    auto *label = new QStandardItem();
    label->setData("L" + QString::fromStdString(std::to_string(m_nextIndex++)),
                   LabelRole);
    row.push_back(label);

    for (int i = 0; i < m_sessions.size(); i++)
    {
        auto *item = new QStandardItem();
        if (i + 1 == column) {
            item->setData(value, LandmarkRole);
        }
        else {
            item->setData(QVariant(), LandmarkRole);
        }
        row.push_back(item);
    }
    m_model.appendRow(row);

    int iRowIndex = m_model.rowCount() - 1;

    qDebug()<<"add landmark";
    if (ApplicationData::instance().landmarkPredictionDown() || m_populateLandmarksInProgress) {
        // if we have enough data points to define a transform,
        // let's predict where the other landmarks will be and add them
        for (int i = 0; i < m_sessions.size(); i++) {
            if (i + 1 != column)
            {
                VoxelCoordinate
                        point_to_transform(x, y, static_cast<float>(z));
    //            qDebug() << "Predicting landmark for " << m_sessions[i];
    //            qDebug() << point_to_transform.x << " " << point_to_transform.y << " " << point_to_transform.z ;
                predictLandmark(point_to_transform, m_sessions[column-1], m_sessions[i]);
    //            qDebug() << point_to_transform.x << " " << point_to_transform.y << " " << point_to_transform.z ;

                this->addLandmark(m_sessions[i],point_to_transform.x,
                                                point_to_transform.y,
                                                point_to_transform.z);

                emit landmarkPredicted(iRowIndex);

            }
        }
    }
    else {
    //  qDebug() << "landmark Prediction is off";
    }
}

float LandmarkModel::getCorrelation(int indx) const
{
    auto data = m_model.data(m_model.index(indx, 0), CorrelationRole);
    if (data.isNull())
        return 0.0;
    return data.value<float>();
}

void LandmarkModel::setCorrelation(int indx, float corr)
{
    m_model.setData(m_model.index(indx, 0), corr, CorrelationRole);
}

void LandmarkModel::sortLandmarksByCorrelation()
{
    m_model.setSortRole(CorrelationRole);
    m_model.sort(0);
    m_model.updateOrder();
}

QVector3D LandmarkModel::vectorAt(int row, int column) const
{
    auto data = m_model.data(m_model.index(row, column), LandmarkRole);
    assert(!data.isNull());

    return data.value<QVector3D>();
}

bool LandmarkModel::isVectorAt(int row, int column) const
{
    return !m_model.data(m_model.index(row, column), LandmarkRole).isNull();
}

QString LandmarkModel::label(int row) const
{
    auto data = m_model.data(m_model.index(row, 0), LabelRole);
    if (data.isNull()) {
        return "L??";
    }
    else {
        return data.toString();
    }
}

void LandmarkModel::setXY(const session_key_t &session, const Landmark &landmark,
                          float x, float y)
{
    int column = columnForSession(session);
    auto pos = vectorAt(landmark.row, column);
    pos.setX(x);
    pos.setY(y);

    m_model.setData(m_model.index(landmark.row, column), pos, LandmarkRole);
}

void LandmarkModel::setZ(const session_key_t &session, const Landmark &landmark,
                         float z)
{
    int column = columnForSession(session);
    auto pos = vectorAt(landmark.row, column);
    pos.setZ(z);

    m_model.setData(m_model.index(landmark.row, column), pos, LandmarkRole);
}

QList<Landmark> LandmarkModel::getLandmarks(const session_key_t &session) const
{
    QList<Landmark> result;
    int column = columnForSession(session);
    int rows = m_model.rowCount();

    for (int i = 0; i < rows; i++) {
        if (isVectorAt(i, column)) {
            auto pos = vectorAt(i, column);
            result.push_back(Landmark(pos.x(), pos.y(), pos.z(), label(i), i));
        }
    }

    return result;
}

int LandmarkModel::columnForSession(const session_key_t &name) const
{
    int column = m_sessions.indexOf(name);
    assert(column >= 0);
    return column + 1;
}

QJsonObject LandmarkModel::toJson() const
{
    QJsonArray values;

    const int rows = m_model.rowCount();
    const int columns = m_model.columnCount();
    for (int i = 0; i < rows; i++) {
        QJsonArray lm;
        // Label
        lm.push_back(label(i));
        // Locations
        for (int j = 1; j < columns; j++) {
            QJsonArray a;
            if (isVectorAt(i, j)) {
                auto pos = vectorAt(i, j);
                a.push_back(pos.x());
                a.push_back(pos.y());
                a.push_back(pos.z());
            }
            // Leave array empty for null values
            lm.push_back(a);
        }
        values.push_back(lm);
    }
    QJsonArray correlations;
    for (int i = 0; i < rows; i++)
    {
        float corr = getCorrelation(i);
        correlations.push_back(corr);
    }

    QJsonObject result;
    result.insert("next_index", m_nextIndex);
    result.insert("values", values);
    result.insert("correlations", correlations);
    result.insert("transform_mode", m_transformMode);

    return result;
}

void LandmarkModel::fromJson(const QJsonObject &data)
{
    auto values = data["values"].toArray();

    m_model.clear();
    m_model.setColumnCount(m_sessions.size() + 1);
    m_model.setRowCount(values.size());
    int row = 0;
    for (const auto &lm : values) {
        if (lm.isArray()) {
            auto landmark = lm.toArray();
            m_model.setData(m_model.index(row, 0),
                            landmark.at(0).toString(),
                            LabelRole);
            int size = landmark.size();
            for (int i = 1; i < size; i++) {
                auto loc = landmark.at(i).toArray();
                if (loc.size() == 3) {
                    QVector3D v(loc.at(0).toDouble(),
                                loc.at(1).toDouble(),
                                loc.at(2).toDouble());
                    m_model.setData(m_model.index(row, i), v, LandmarkRole);
                }
                else if (loc.size() != 0) {
                    qWarning()
                        << "Expected 3 elements for landmark location, found "
                        << loc.size();
                }
            }
        }
        else {
            qWarning() << "Found non-array value while loading landmark.";
        }
        row++;
    }

    auto correlations = data["correlations"].toArray();
    row = 0;
    for (const auto &corr : correlations) {
        setCorrelation(row++, (float)corr.toDouble());
    }

    if (data.contains("next_index") && data["next_index"].isDouble()) {
        m_nextIndex = data["next_index"].toInt();
    }
    else {
        m_nextIndex = m_model.rowCount() + 1;
    }

    if (data.contains("transform_mode") && data["transform_mode"].isString()) {
        m_transformMode = data["transform_mode"].toString();
    }
}

/* Some notes on coordinate systems:

   In Qt, (0,0) is top left, and +y goes down. VTK is the opposite. So we
   need to flip y coordinates going in and out of VTK.

   Each stack may have a different resolution. To handle this, we use world
   coordinates based on the fixed session, and align all images at the bottom
   left corner. So in VTK coords (0,0) is the bottom left of all images.

   This gives us three coordinate systems:

    * Image local. Uses Qt orientation. Each image has its own local
      coordinate system.
    * Qt world. Same as the local coordinates of the fixed session.
    * VTK world. Flipped so +y goes up.
 */

// Transform Qt y into world coordinates and flip to VTK orientation.
static int flipToVTK(int worldHeight, int imageHeight, int qtY)
{
    int ymax = worldHeight - 1;
    return ymax - (qtY + (worldHeight - imageHeight));
}

// Flip VTK y into Qt orientation, keeping as world coordinates.
static int flipToQtWorld(int worldHeight, int vtkY)
{
    int ymax = worldHeight - 1;
    return ymax - vtkY;
}

#if 0
// Not used
// Flip VTK y coordinate back to Qt and transform into the local
// coordinate system of the original image.
static int flipToQtLocal(int worldHeight, int imageHeight, int vtkY)
{
    int ymax = worldHeight - 1;
    return (ymax - vtkY) - (worldHeight - imageHeight);
}
#endif

xform_ptr LandmarkModel::createTransform(const session_key_t &name,
                                         int imageHeight,
                                         const int *upSample) const
{
    if (m_fixedSession.isEmpty() || name == m_fixedSession)
        return nullptr;

    int movingScaling[3] = {1, 1, 1};
    int targetScaling[3] = {1, 1, 1};
    if ( upSample != nullptr)
        for (int i=0;i < 3; i++) {
            movingScaling[i] = upSample[i];
            targetScaling[i] = upSample[i];
    }
    // Flip y coords
    const int fixedHeight = m_sessionData.find(m_fixedSession)->second.height;
    const int fixedWidth = m_sessionData.find(m_fixedSession)->second.width;
    const int movingHeight = m_sessionData.find(name)->second.height;
    const int movingWidth = m_sessionData.find(name)->second.width;

    int fixedColumn = columnForSession(m_fixedSession);
    int movingColumn = columnForSession(name);

    QTransform fixedPreRotation, movingPreRotation;
    if (preRotations.contains(m_fixedSession))
        fixedPreRotation.rotate(-preRotations[m_fixedSession], Qt::ZAxis);
    else
        fixedPreRotation.rotate(0, Qt::ZAxis);

    if (preRotations.contains(name))
        movingPreRotation.rotate(-preRotations[name], Qt::ZAxis);
    else
        movingPreRotation.rotate(0, Qt::ZAxis);


    auto fixedPoints = vtkSmartPointer<vtkPoints>::New();
    auto movingPoints = vtkSmartPointer<vtkPoints>::New();
    for (int i = 0; i < m_model.rowCount(); i++) {
        if (isVectorAt(i, fixedColumn) && isVectorAt(i, movingColumn)) {

            auto f = vectorAt(i, fixedColumn);
            QPointF f_rotated = fixedPreRotation.map(QPointF(f.x()-fixedWidth/2.0, f.y()-fixedHeight/2.0)) +
                    QPointF(fixedWidth/2.0, fixedHeight/2.0);

            fixedPoints->InsertNextPoint(f_rotated.x()*targetScaling[0],
                                         flipToVTK(fixedHeight, fixedHeight, f_rotated.y())*targetScaling[1],
                                         f.z()*targetScaling[2]);

            auto m = vectorAt(i, movingColumn);
            QPointF m_rotated = movingPreRotation.map(QPointF(m.x()-movingWidth/2.0, m.y()-movingHeight/2.0))+
                    QPointF(movingWidth/2.0, movingHeight/2.0);

            if (bFlipVTK) {
                movingPoints->InsertNextPoint(m_rotated.x()*movingScaling[0],
                                          flipToVTK(fixedHeight, imageHeight, m_rotated.y())*movingScaling[1],
                                          m.z()*movingScaling[2]);
            } else {
                movingPoints->InsertNextPoint(m_rotated.x()*movingScaling[0],
                                          m_rotated.y()*movingScaling[1],
                                          m.z()*movingScaling[2]);

            }
        }

    }
    fixedPoints->Modified();
    movingPoints->Modified();
    xform_ptr result;
    if (m_transformMode == "Thinplate") {
        auto xform = vtkSmartPointer<vtkThinPlateSplineTransform>::New();
        xform->SetTargetLandmarks(movingPoints);
        xform->SetSourceLandmarks(fixedPoints);
        result = xform;
    }
    else {
        auto xform = vtkSmartPointer<vtkLandmarkTransform>::New();
        if (m_model.rowCount() < 3 || m_transformMode == "Rigid Body") {
            xform->SetModeToRigidBody();
        } else if (m_transformMode == "Similarity") {
            xform->SetModeToSimilarity();
        } else if (m_transformMode == "Affine") {
            xform->SetModeToAffine();
        }
        else {
            qWarning() << "Unknown transform mode " << m_transformMode;
        }
        xform->SetTargetLandmarks(movingPoints);
        xform->SetSourceLandmarks(fixedPoints);
        result = xform;
    }
    result->Update();

#if 0
    result->PrintSelf(std::cout, vtkIndent());
#endif
    return result;
}


void LandmarkModel::updateTransforms()
{
    for (auto &session : m_sessionData) {
        session.second.xform = createTransform(session.first,
                                               session.second.height);
    }
    emit transformsChanged();
}

//vtkSmartPointer<vtkAbstractTransform>
//LandmarkModel::getScaledTransform(const session_key_t &session_key, const int *upSample)
//{
//    qDebug() << session_key;
//    qDebug() << m_sessionData.find(session_key)->second.height;
//    vtkSmartPointer<vtkAbstractTransform> xform = createTransform(session_key,
//                                                   m_sessionData.find(session_key)->second.height,
//                                                   upSample);
//    return xform;
//}

vtkSmartPointer<vtkAbstractTransform>
LandmarkModel::getTransform(const session_key_t &session) const
{
    return m_sessionData.find(session)->second.xform;
}

stack_ptr_t
LandmarkModel::getStackPtr(const session_key_t &session) const
{
    return m_sessionData.find(session)->second.stack;
}

void
LandmarkModel::transformPoints(QVector<VoxelCoordinate> &points,
                               session_key_t session) const
{
    vtkSmartPointer<vtkAbstractTransform> xform = getTransform(session);
    if (xform == nullptr)
        return;

    const int fixedHeight = m_sessionData.find(m_fixedSession)->second.height;
    const int movingHeight = m_sessionData.find(session)->second.height;
    for (auto &p : points) {
        double tmp[] = { static_cast<double>(p.x),
                         static_cast<double>(flipToVTK(fixedHeight, movingHeight, p.y)),
                         static_cast<double>(p.z) };
        xform->GetInverse()->TransformPoint(tmp, tmp);
        p.x = static_cast<int>(tmp[0] + 0.5);
        p.y = static_cast<int>(flipToQtWorld(fixedHeight, tmp[1]) + 0.5);
        // Truncate Z coordinate instead of rounding. This seems to better
        // match the behavior of vtkImageReslice, putting the ROI in the same
        // slice as the corresponding image pixels.
        //p.z = static_cast<int>(tmp[2]);

        // 02/11/19 Changed to rounding after using GetInverse()
        p.z = static_cast<int>(tmp[2]+0.5);

    }
}

void
LandmarkModel::predictLandmark(VoxelCoordinate &p,
                               session_key_t landmark_ref_session,
                               const session_key_t& session) const
{
    // qDebug() << " Fixed = " << m_fixedSession;
    // qDebug() << " Landmark ref session " <<  landmark_ref_session << " predict in = " << session;
    // xform takes fixed and moves them to moving

    const int refHeight = m_sessionData.find(landmark_ref_session)->second.height;
    const int targetHeight = m_sessionData.find(session)->second.height;
    double tmp[] = { static_cast<double>(p.x),
                     static_cast<double>(flipToVTK(refHeight, refHeight, p.y)),
                     static_cast<double>(p.z) };

    if ( landmark_ref_session == m_fixedSession) {
        vtkSmartPointer<vtkAbstractTransform> xform = getTransform(session);
        if (xform == nullptr)
            return;
        xform->TransformPoint(tmp, tmp);
    } else { // landmark_ref_session = moving      session = m_fixedSession
        vtkSmartPointer<vtkAbstractTransform> xform = getTransform(landmark_ref_session);   // xForm: session -> landmark_ref_session .... so we need inverse
        if (xform == nullptr)
            return;
        xform->GetInverse()->TransformPoint(tmp, tmp);
    }
    p.x = static_cast<int>(tmp[0] + 0.5);
    p.y = static_cast<int>(flipToQtWorld(targetHeight, tmp[1]) + 0.5);
    p.z = static_cast<int>(tmp[2]+0.5);
}

void LandmarkModel::populateLandmarks()
{
    stack_ptr_t pFixedStack = getStackPtr(m_fixedSession);

    HyperstackSize
            size = pFixedStack->size();

    VoxelCoordinate
            sub_size  = ApplicationData::instance().getAutoRegisterSize(),
            sub_space = ApplicationData::instance().getAutoRegisterSearchSpace(),
            spacing   = ApplicationData::instance().getAutoPopulateSpacing();

    VoxelCoordinate
            step = {(sub_size.x + sub_space.x*2 + spacing.x),
                    (sub_size.y + sub_space.y*2 + spacing.y),
                    (sub_size.z + sub_space.z*2 + spacing.z)};

    VoxelCoordinate
            count = {(int)(size.x / step.x), (int)(size.y / step.y), (int)(size.z / step.z)};

    //update step
    step = {(int)(size.x*1.0/count.x), (int)(size.y*1.0/count.y), (int)(size.z*1.0/count.z)};
    int tot = (count.x*count.y*count.z);
    qDebug()<<"total:"<<tot<<" - "<< ", " << count.x << ","<<count.y<<","<<count.z;

    QElapsedTimer
            timer;
    timer.start();

    int dirVec[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    int ith = 0;
    int
            iZ0 = (count.z+1)/2 -1,
            iZ1 = iZ0 + 1;

    m_populateLandmarksInProgress = true;
    for(int iZ = 0; iZ < count.z/2; iZ++)
    {
        int
                iXCoord = count.x/2,
                iYCoord = count.y/2;
        int
            dir = 0,
            dirStep = 1,
            inc = 0;

        for(int i = 0; i < count.x*count.y; i++)
        {
                if ((iXCoord < count.x && iXCoord >= 0) &&
                    (iYCoord < count.y && iYCoord >= 0) &&
                    (iZ0 >= 0))
                {
                    qDebug()<<"adding landmark:"<< ith << "/"<< tot;
                    addLandmark(m_fixedSession, (iXCoord*step.x + step.x/2),
                                                (iYCoord*step.y + step.y/2),
                                                (iZ0*step.z + step.z/2));
                    ith++;
                }

                if ((iXCoord < count.x && iXCoord >= 0) &&
                    (iYCoord < count.y && iYCoord >= 0) &&
                    (iZ1 < count.z))
                {
                    qDebug()<<"adding landmark:"<< ith << "/"<< tot;
                    addLandmark(m_fixedSession, (iXCoord*step.x + step.x/2),
                                                (iYCoord*step.y + step.y/2),
                                                (iZ1*step.z + step.z/2));
                    ith++;
                }

                iXCoord += dirVec[dir][0];
                iYCoord += dirVec[dir][1];
                inc++;
                if (inc == dirStep)
                {
                    dir = (dir+1)%4;
                    inc = 0;
                    if (dir % 2 == 0)
                    {
                        dirStep++;
                    }
                }
        }
        iZ0--;
        iZ1++;
    }
    m_populateLandmarksInProgress = false;
/*  RegularGrid
    for(int iZ = 0; iZ < count.z; iZ++)
    {
        for(int iY = 0; iY < count.y; iY++)
        {
            for(int iX = 0; iX < count.x; iX++)
            {
             qDebug()<<"adding landmark:"<< ith << "/"<< tot;
             addLandmark(m_fixedSession, (step.x*iX + step.x/2),
                                         (step.y*iY + step.y/2),
                                         (step.z*iZ + step.z/2));
             ith++;
            }
        }
    }
*/
    qDebug()<<"populating landmarks finished:"<<timer.elapsed();
}

void LandmarkModel::autoRegisterLandmark(int iRow, bool isGray)
{
    QVariant
            fixed_val = m_model.data(m_model.index(iRow, 0), FixedRole),
            moving_val = m_model.data(m_model.index(iRow, 0), MovingRole);

    if (fixed_val.isNull() || moving_val.isNull())
    {
        updateSessionsForRow(iRow);
        fixed_val = m_model.data(m_model.index(iRow, 0), FixedRole),
        moving_val = m_model.data(m_model.index(iRow, 0), MovingRole);
    }

    int
        fixed_column  = columnForSession(fixed_val.toString());

    if (isVectorAt(iRow, fixed_column))
    {
        auto pos_fixed = vectorAt(iRow, fixed_column);
        VoxelCoordinate fixed_point = {(int)pos_fixed.x(), (int)pos_fixed.y(), (int)pos_fixed.z()};
        autoRegisterLandmark(iRow, fixed_point, fixed_val.toString(), moving_val.toString(), isGray);
    }
}

bool LandmarkModel::autoRegisterLandmark(int iRow, const VoxelCoordinate &ref_point, const session_key_t& ref_session,const session_key_t& session, bool isGray)
{    
    int channel = isGray ? 1:3;

    stack_ptr_t pRefStack = getStackPtr(ref_session);
    vtkSmartPointer<vtkAbstractTransform> ref_xform = getTransform(ref_session);

    stack_ptr_t pStack = getStackPtr(session);
    vtkSmartPointer<vtkAbstractTransform> xform = getTransform(session);

    VoxelCoordinate
            size  = ApplicationData::instance().getAutoRegisterSize(),
            space = ApplicationData::instance().getAutoRegisterSearchSpace();

    float std0 = -1.0, std1 = -1.0;
    float corrThreshold = -1.0;

    if (m_populateLandmarksInProgress) //then we should retireve STD threshold
    {
        std0 = ApplicationData::instance().getAutoRegisterSTD()/100.0;
        std1 = std0;
        corrThreshold = ApplicationData::instance().getAutoRegisterCorr()/100.0;
    }

    VolumeBuffer
            ref_vb(size.x + space.x*2, size.y + space.y*2, size.z + space.z*2, 1, channel), //144, 144, 36
            vb(size.x, size.y, size.z, 1, channel);

    auto tInd = vtkSmartPointer<vtkTransform>::New();
    tInd->Identity();

    if (ref_xform!=nullptr && xform==nullptr)
    {
        ApplicationData::instance().setAutoRegisterResult("Error");
        return false;
    }
    else
    { //ref is the main fixed
        pRefStack->retrieveSubVolume(ref_vb, ref_point, tInd, pRefStack->size());
        pStack->retrieveSubVolume(vb, ref_point, xform, pRefStack->size());
    }

    //ref_vb.dumpToFile("c:/tmp/nero/fixed_caps.dat");
    //vb.dumpToFile("c:/tmp/nero/moving_caps.dat");

    VoxelCoordinate offset;
    float corr = registerRigid3D(ref_vb, vb, std0, std1, offset, !m_populateLandmarksInProgress);
    if (corr < 0 || corr < corrThreshold)
    {
        qDebug()<<"removing landMark!";
        //ideally this should not happen here

        m_model.removeRow(iRow);
        emit updateTransforms();
        return false;
    }

    QString res, resSTD;
    ApplicationData::instance().setAutoRegisterResult(res.sprintf("%d,%d,%d", offset.x, offset.y, offset.z) );
    ApplicationData::instance().setAutoRegisterResultSTD(resSTD.sprintf("%d, %d", (int)(std0*100), (int)(std1*100)));

    //auto register fixed
    QVector3D offsetted_point((ref_point.x + offset.x), (ref_point.y + offset.y), (ref_point.z + offset.z));
    m_model.setData(m_model.index(iRow, columnForSession(ref_session)), offsetted_point, LandmarkRole);
    setCorrelation(iRow, corr);

    emit landmarkAutoRegistered(iRow);
    emit updateTransforms();

    return true;
}

#define SUBVOL_SIZE 144
#define SUBVOL_SIZE_MOV 96

void LandmarkModel::autoRegisterLandmarkUnitTest(VoxelCoordinate &point, session_key_t session, bool isGray)
{
    int channel = isGray ? 1:3;

    stack_ptr_t pStack = getStackPtr(session);
    vtkSmartPointer<vtkAbstractTransform> xform = getTransform(session);

    VolumeBuffer
            ref_vb(SUBVOL_SIZE, SUBVOL_SIZE, SUBVOL_SIZE/4, 1, channel),
            vb(SUBVOL_SIZE/2, SUBVOL_SIZE/2, SUBVOL_SIZE/8, 1, channel);

    xform = nullptr;
    pStack->retrieveSubVolume(ref_vb, point, xform, pStack->size());
    //return;

    //{32,32,16}
    VoxelCoordinate point_offsetted = point;
    VoxelCoordinate offset_tests[5] = {{5, 8, 4}, {22, 8, 7}, {-14, 18, 8}, {11, -12, -6}, {-9, 3, -7}};
    float std = 0.0;

    for(int i = 0; i < 5; i++)
    {
        point_offsetted.x = point.x + offset_tests[i].x;
        point_offsetted.y = point.y + offset_tests[i].y;
        point_offsetted.z = point.z + offset_tests[i].z;

        qDebug()<<"test offset:"<<offset_tests[i].x<<","<<offset_tests[i].y<<","<<offset_tests[i].z;
        pStack->retrieveSubVolume(vb, point_offsetted, xform, pStack->size());

        VoxelCoordinate offset2;
        registerRigid3D(ref_vb, vb, std, std, offset2);

        assert(offset_tests[i].x == offset2.x);
        assert(offset_tests[i].y == offset2.y);
        assert(offset_tests[i].z == offset2.z);

        qDebug()<<"pairs: ("<< offset_tests[i].x<<":"<<offset2.x << " , " <<offset_tests[i].y<<":"<<offset2.y<< " , "<<offset_tests[i].z<<":"<<offset2.z<<")";
    }
}

bool LandmarkModel::isLandmarkComplete() const
{
    if (m_model.rowCount() == 0) {
        return true;
    }

    int row = m_model.rowCount() - 1;
    int count = 0;
    for (int column = 1; column < m_model.columnCount(); column++) {
        if (isVectorAt(row, column)) {
            count++;
        }
        if (count == 2) {
            return true;
        }
    }

    return false;
}

void LandmarkModel::updateSessionsForRow(int row)
{
    std::vector<QString> sessions;
    bool haveFixed = true;
    for (const auto &name : m_sessions) {
        bool defined = isVectorAt(row, columnForSession(name));
        if (name == m_fixedSession) {
            haveFixed = defined;
        }
        else if (defined) {
            sessions.push_back(name);
        }
    }
    auto index = m_model.index(row, 0);
    if (haveFixed) {
        m_model.setData(index, m_fixedSession, FixedRole);
        m_model.setData(index, (sessions.size() > 0) ? sessions[0] : "<none>",
                        MovingRole);
    }
    else {
        // Not defined for fixed session. Arbitrarily pick first session as
        // fixed and second as moving.
        m_model.setData(index, (sessions.size() > 0) ? sessions[0] : "<none>",
                        FixedRole);
        m_model.setData(index, (sessions.size() > 1) ? sessions[1] : "<none>",
                        MovingRole);
    }
}

void LandmarkModel::setFixedSessionName(const session_key_t &name)
{
    m_fixedSession = name;
    updateTransforms();

    for (int row = 0; row < m_model.rowCount(); row++) {
        updateSessionsForRow(row);
    }
}
