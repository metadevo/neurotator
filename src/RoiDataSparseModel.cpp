#include "RoiDataSparseModel.hpp"


RoiDataSparseModel::RoiDataSparseModel(RoiDataSparse *roiData) {
    m_roiData = roiData;
}

RoiDataSparseModel::~RoiDataSparseModel() {
    m_roiData = nullptr;
}

QVariant RoiDataSparseModel::headerData(int section, Qt::Orientation orientation, int role) const
 {
     if (role != Qt::DisplayRole)
         return QVariant();

     if (orientation == Qt::Horizontal) {
         switch (section) {
             case 0:
                 return tr("Name");

             case 1:
                 return tr("Candidates");

             default:
                 return QVariant();
         }
     }
     return QVariant();
 }

int RoiDataSparseModel::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent)
    return m_roiData->sessionCount();
}

int RoiDataSparseModel::columnCount(const QModelIndex & parent) const {
    Q_UNUSED(parent)
    return 2;
}

QVariant RoiDataSparseModel::data(const QModelIndex & index, int role) const {
    Q_UNUSED(role)

    if (index.row() < 0 || index.row() >=  m_roiData->sessionCount())
        return QVariant();

    return dataAt(index.row(), index.column());
}

QVariant RoiDataSparseModel::dataAt(const int row, int col) const
{
    Q_UNUSED(col)
    // qDebug() << "model row from aml"<< row;
    QString sessionName = m_roiData->sessionNameAt(row);
    if (row >= 0 && row <  m_roiData->sessionCount()) {
        switch (col) {
        case 0: return sessionName;
        case 1: return std::dynamic_pointer_cast<RoiFieldsSparse>(
                        m_roiData->roisAtSession(sessionName).first()
                        )->numCandidates();
        default:
            qWarning() << "Column " << col << " is out of bounds";
            return QString();
        }

    }
    qWarning() << "row " << row << " is out of bounds of " << m_roiData->sessionCount();
    return QString();
}

bool RoiDataSparseModel::rowChanged() {
    layoutAboutToBeChanged();
    layoutChanged();
    return true;
}
