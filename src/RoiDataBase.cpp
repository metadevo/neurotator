
#include <QDebug>

#include "RoiDataBase.hpp"


QList<QString> RoiDataBase::sessionNames()
{
    QList<QString> x= m_sessions.keys();
    x.sort();
    return x;
}

QString RoiDataBase::sessionNameAt(int index) {
    QList<QString> x= m_sessions.keys();
    x.sort();
    return x.at(index);
}

bool RoiDataBase::addNewSession(session_key_t name)
{
    if (!m_sessions.contains(name)) {
        m_sessions[name];
        m_currSession = name;
    } else {
        qWarning() << "Session key ("<< name << ") already inserted.";
        return false;
    }
    return true;
}

bool RoiDataBase::removeSession(session_key_t name)
{
    if (m_sessions.contains(name)) {
        m_sessions.remove(name);
        qDebug() << "Session \""<< name << "\" removed.";
        if (m_maskTables.contains(name)){
            // remove session and its masks from m_maskTables
            m_maskTables.remove(name);
        }
    } else {
        qWarning()<< "Session key ("<< name << ") does not exist.";
        return false;
    }
    return true;
}

QList<RoiDataBase::roi_t> RoiDataBase::roisAtSession(session_key_t sessionKey) const
{
    QList<roi_t> ret;
    auto it = m_sessions.find(sessionKey);
    if (it != m_sessions.end()) {
        return it->values();
    } else {
        qWarning() << "Session key ("<< sessionKey << ") unknown.";
    }
    return ret;
}
