#include <vector>
#include <QString>
#include <tiffio.h>

#include <vtkAbstractTransform.h>
#include "HyperstackTiff.hpp"
#include "Project.hpp"

#include "Render.hpp"

QImage renderSlice(stack_render_list &stacks, int z, const HyperstackSize &size)
{
    QImage composite;
    for (auto &s : stacks) {
        QImage image = s.first.imageBuffer(z, s.second, size);
        if (composite.isNull()) {
            composite = image;
        }
        else {
            for (int y = 0; y < image.height(); y++) {
                for (int x = 0; x < image.width(); x++) {
                    // If pixel is non-black, overwrite previous values.
                    auto c = image.pixelColor(x, y);
                    if (c.red() > 0 || c.green() > 0 || c.blue() > 0) {
                        composite.setPixelColor(x, y, c);
                    }
                }
            }
        }
    }
    return composite;
}

static void saveSlice(TIFF *tif, const QImage &image,
                      const HyperstackSize &size, int z)
{
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, size.x);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, size.y);
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 3);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
    if (size.z > 1) {
        TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
        TIFFSetField(tif, TIFFTAG_PAGENUMBER, z, size.z);
    }

    const int scanSize = TIFFScanlineSize(tif);
    const int numRows = (4 * 1024 * 1024) / scanSize;
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tif, numRows));

    unsigned int byteSize = size.slicePixels * 3;
    uint8 *buffer = static_cast<uint8 *>(_TIFFmalloc(byteSize));

    for (unsigned int y = 0; y < size.y; y++) {
        for (unsigned int x = 0; x < size.x; x++) {
            QColor c = image.pixelColor(x, y);
            buffer[3 * x] = c.blue();
            buffer[3 * x + 1] = c.green();
            buffer[3 * x + 2] = c.red();
        }
        if (TIFFWriteScanline(tif, buffer, y, 0) < 0) {
            qWarning() << "TIFFWriteScanline failed.";
        }
    }
    _TIFFfree(buffer);

    TIFFWriteDirectory(tif);
}

void renderToTiff(const QString &filePath, stack_render_list &stacks,
                  const HyperstackSize &size)
{
    TIFF *tif = TIFFOpen(filePath.toStdString().c_str(), "w");
    if (!tif)
    {
        qDebug() << "ERROR: TIFF object null!";
        return;
    }
    qInfo() << "Trying to save file " << filePath;

    for (unsigned int z = 0; z < size.z; z++) {
        saveSlice(tif, renderSlice(stacks, z, size),
                  size, z);
    }
    TIFFClose(tif);
}
