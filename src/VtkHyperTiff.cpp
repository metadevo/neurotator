#include "VtkHyperTiff.hpp"
#include "vtkDataSetAttributes.h"
#include <vtkImageData.h>
#include <vtkObjectFactory.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkInformationVector.h>
#include <vtkInformation.h>
#include <vtkDataObject.h>
#include <vtkSmartPointer.h>

vtkStandardNewMacro(vtkHyperTiffFilter);


//----------------------------------------------------------------------------
// Constructor: Sets default filter to be identity.
vtkHyperTiffFilter::vtkHyperTiffFilter()
{
    this->NumberOfChannels = 1;
    this->OutputScalarType = -1; // invalid; output same as input
}

//----------------------------------------------------------------------------
// Computes any global image information associated with regions.
int vtkHyperTiffFilter::RequestInformation(
        vtkInformation *vtkNotUsed(request),
        vtkInformationVector **inputVector,
        vtkInformationVector *outputVector)
{
    // get the info objects
    vtkInformation *outInfo = outputVector->GetInformationObject(0);
    vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
    double inSpacing[3];
    int extent[6];

    inInfo->Get(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), extent);

    // Scale the output extent
    extent[5] /= this->NumberOfChannels;

    outInfo->Set(vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), extent, 6);

    inInfo->Get(vtkDataObject::SPACING(), inSpacing);
    outInfo->Set(vtkDataObject::SPACING(), inSpacing, 3);

    if (this->OutputScalarType == -1) {
        vtkInformation *inScalarInfo = vtkDataObject::GetActiveFieldInformation(inInfo,
                                                                                vtkDataObject::FIELD_ASSOCIATION_POINTS,
                                                                                vtkDataSetAttributes::SCALARS);
        if (!inScalarInfo) {
            vtkErrorMacro("Missing scalar field on input information!");
            return 0;
        }
        vtkDataObject::SetPointDataActiveScalarInfo(outInfo,
                                                    inScalarInfo->Get(vtkDataObject::FIELD_ARRAY_TYPE()),
                                                    this->NumberOfChannels);
    } else {
        vtkDataObject::SetPointDataActiveScalarInfo(outInfo, this->OutputScalarType, this->NumberOfChannels);
    }
    return 1;
}


//----------------------------------------------------------------------------
// This method computes the Region of input necessary to generate outRegion.
int vtkHyperTiffFilter::RequestUpdateExtent(
        vtkInformation *vtkNotUsed(request),
        vtkInformationVector **inputVector,
        vtkInformationVector *outputVector)
{
    // get the info objects
    vtkInformation *outInfo = outputVector->GetInformationObject(0);
    vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);

    int inExt[6], outExt[6];
    outInfo->Get(vtkStreamingDemandDrivenPipeline::UPDATE_EXTENT(), outExt);

    this->InternalRequestUpdateExtent(inExt, outExt);

    inInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_EXTENT(), inExt, 6);

    return 1;
}

void vtkHyperTiffFilter::InternalRequestUpdateExtent(int *inExt, int *outExt)
{
    for (int idx = 0; idx < 4; idx++)
        inExt[idx] = outExt[idx];
    inExt[4] = outExt[4] * (this->NumberOfChannels);
    inExt[5] = outExt[5] * (this->NumberOfChannels) + this->NumberOfChannels - 1;
}

//----------------------------------------------------------------------------
// The templated execute function handles all the data types.
template <class T>
void vtkImageRearrangeExecute(
        vtkHyperTiffFilter *self,
        vtkImageData *inData, T *inPtr, int inExt[6],
        vtkImageData *outData, T *outPtr,
        int outExt[6], int id)
{
    int idxC, idxX, idxY, idxZ;
    int maxC, maxX, maxY, maxZ;
    vtkIdType inIncX, inIncY, inIncZ;
    vtkIdType outIncX, outIncY, outIncZ;
    unsigned long count = 0;
    unsigned long target;
    T *inPtrZ, *inPtrY, *inPtrX, *outPtrC;
//    int inMinX, inMaxX, inMaxC;

    maxC = outData->GetNumberOfScalarComponents();
    maxX = outExt[1] - outExt[0];
    maxY = outExt[3] - outExt[2];
    maxZ = outExt[5] - outExt[4];


    target = static_cast<unsigned long>(maxC*(maxZ+1)*(maxY+1)/50.0);
    target++;

    //Get increments to march through data
//    inMaxC = inData->GetNumberOfScalarComponents();
//    inMinX = inExt[0] - outExt[0];
//    inMaxX = inExt[1] - outExt[0];

    inData->GetIncrements(inIncX, inIncY, inIncZ);
    outData->GetContinuousIncrements(outExt, outIncX, outIncY, outIncZ);
    // Loop through output pixels
    for (idxC = 0; idxC < maxC; idxC++)
    {
        inPtrZ = inPtr + idxC*inIncZ;
        outPtrC = outPtr + idxC;
        // Loop through output pixels
        for (idxZ = 0; idxZ <= maxZ; idxZ++)
        {
            inPtrY = inPtrZ;
            for (idxY = 0; !self->AbortExecute && idxY <= maxY; idxY++)
            {
                if (!id)
                {
                    if (!(count%target))
                    {
                        self->UpdateProgress(count/(50.0*target));
                    }
                    count++;
                }
                inPtrX = inPtrY;
                for (idxX = 0; idxX <= maxX; idxX++)
                {
                    *outPtrC = *inPtrX;
                    inPtrX += inIncX;
                    outPtrC += maxC;
                }
                outPtrC += outIncY;
                inPtrY += inIncY;
            }
            outPtrC += outIncZ;
            inPtrZ += (inIncZ*maxC);
        }
    }
}

void vtkHyperTiffFilter::ThreadedRequestData(
        vtkInformation *vtkNotUsed(request),
        vtkInformationVector **vtkNotUsed(inputVector),
        vtkInformationVector *vtkNotUsed(outputVector),
        vtkImageData ***inData,
        vtkImageData **outData,
        int outExt[6], int id)
{
    int inExt[6];
    this->InternalRequestUpdateExtent(inExt, outExt);

    void *inPtr = inData[0][0]->GetScalarPointerForExtent(inExt);
    void *outPtr = outData[0]->GetScalarPointerForExtent(outExt);

    // this filter expects that input is the same type as output.
    if (inData[0][0]->GetScalarType() != outData[0]->GetScalarType()) {
        vtkErrorMacro("Execute: input ScalarType, "
                              << inData[0][0]->GetScalarType()
                              << ", must match out ScalarType "
                              << outData[0]->GetScalarType());
        return;
    }

    switch (inData[0][0]->GetScalarType()) {
        vtkTemplateMacro(
                vtkImageRearrangeExecute(this, inData[0][0],
                                         static_cast<VTK_TT *>(inPtr), inExt, outData[0],
                                         static_cast<VTK_TT *>(outPtr),
                                         outExt, id));
        default:
            vtkErrorMacro( << "Execute: Unknown ScalarType");
            return;
    }
}


