#include "ProjectAnnotationSparse.hpp"
#include <QFileInfo>
#include <QJsonArray>
#ifdef MATLAB_SUPPORT
#include "MatFileSparseRoi.hpp"
#endif

ProjectAnnotationSparse::ProjectAnnotationSparse() {
    m_roiData = new RoiDataSparse;
    m_roiDataModel = new RoiDataSparseModel(m_roiData);

}

ProjectAnnotationSparse::~ProjectAnnotationSparse() {
    delete m_roiData;
}

void ProjectAnnotationSparse::writeConfigProjectSpecific() const
{
}

void ProjectAnnotationSparse::readConfigProjectSpecific(const QJsonObject &json)
{
    readRoiDirectories(json);
}

void ProjectAnnotationSparse::readRoiDirectories(const QJsonObject &json)
{
    if (json.contains("roi_files") && json["roi_files"].isArray()) {
        QJsonArray roiFilesArray = json["roi_files"].toArray();
        for (int i = 0; i < roiFilesArray.size(); ++i) {
            QUrl url = QUrl::fromLocalFile(roiFilesArray[i].toString());
            // qDebug() << "Read roi directory from JSON: " << url.toString();
            dynamic_cast<Project*>(this)->loadRoiData(url);
        }
    }
}

void ProjectAnnotationSparse::saveAllRoiFiles() {

}

bool ProjectAnnotationSparse::loadSparseRoiDirectory(RoiFile rf) {
#ifdef MATLAB_SUPPORT
    if (m_roiData->addNewSession(rf.sessionKey))
    {
        MatFileSparseRoi m(m_roiData);

        std::string filename;
        filename = rf.savePath.toStdString() + '/' + rf.sessionKey.toStdString() + ".mat";
        if (!m.load(filename)) {
            qDebug() << "Failed to load " << filename.c_str() << " so looking locally";
            filename = rf.sessionKey.toStdString() + ".mat";
            if (!m.load(filename)) {
                qDebug() << "Tried locally but failed as well: " << filename.c_str();
                m_roiData->removeSession(rf.sessionKey);
                return false;
            }
        }
        m_roiDataModel->rowChanged();
        return true;
    }
    else
    {
        qWarning() << "addNewSession failed.";
        return false;
    }
#else
    return false;
#endif
}

bool ProjectAnnotationSparse::unloadSparseRoiDirectory(QString name) {

    if (m_roiData->removeSession(name))
    {
        m_roiDataModel->rowChanged();
        return true;
    }
    else
    {
        qWarning() << "ProjectAnnotationSparse removeSession failed.";
        return false;
    }

    return false;

}

void ProjectAnnotationSparse::loadRoiData(const QString &pathStr)
{
    try {
        QFileInfo info(pathStr);
        QString keyName = info.baseName();
        RoiFile rf = RoiFile();
        rf.savePath  = pathStr;
        rf.sessionKey = keyName;

        if (!loadSparseRoiDirectory(rf)) {
            qWarning() << "Loading ROI Directory (" << keyName << ") failed.";
        } else {
            qInfo() << "Loading ROI Directory (" << keyName << ") succeeded.";
            m_roiFiles.append(rf);
            if (!m_openingProject)
                saveConfig();
        }
        // post processing
//        updateUnconfirmedNameMap();
//        m_roiData.refreshRefLinks();
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
}

void ProjectAnnotationSparse::unloadRoiData(QString name)
{
    try {
        if (!unloadSparseRoiDirectory(name)) {
            qWarning() << "Unloading ROI Directory (" << name << ") failed.";
        } else {
            qInfo() << "Unloading ROI Directory (" << name << ") succeeded.";

            auto it = m_roiFiles.begin();

            while(it != m_roiFiles.end()){
                if(it->sessionKey == name){
                    it = m_roiFiles.erase(it);
                }else{
                    ++it;
                }
            }

            if (!m_openingProject)
                saveConfig();
        }
        // post processing
//        updateUnconfirmedNameMap();
//        m_roiData.refreshRefLinks();
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }

}

stack_ptr_t ProjectAnnotationSparse::currentStack()
{
    return (m_stack.size() > 0) ? m_stack.at(m_currentStack) : nullptr;
}

stack_ptr_t ProjectAnnotationSparse::getStack(const QString &sessionName) const
{
    auto it = std::find_if(m_stack.begin(), m_stack.end(),
                           [&sessionName](const auto *s) {
                               return s->name() == sessionName;
                           });
    if (it != m_stack.end()) {
        return *it;
    } else {
        return nullptr;
    }
}

bool ProjectAnnotationSparse::loadStacks(QList<QUrl> urls)
{
    QVector<QString> urlsString;

    for (int i = 0; i < urls.length(); i++){
        if(urls[i].toString() != ""){
            urlsString.append(urls[i].toLocalFile());
        }
    }
    try {
        stack_ptr_t stack;
        //if (findStack(pathStr)) {
        //    qInfo() << "Stack " << pathStr << "already loaded.";
        //    m_currentStack = m_findResult;
        //    stack = m_stack[m_currentStack];
        //} else {
            qInfo() << "Creating stacks...";
            stack = new HyperstackTiff();
            // Record start time
            auto start = std::chrono::high_resolution_clock::now();
            if (stack->loadStreamingMultipleTiffs(urlsString)) {
                // Record end time
                auto finish = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> elapsed = finish - start;
                qDebug() << "Elapsed time: (Hyperstacktiff loadStreamingMultipleTiffs) " << elapsed.count() << " s\n";
                m_stack.push_back(stack);
                m_currentStack = m_stack.size() - 1;
                //stack->setColor(std::vector<int>(colorMap[m_currentStack]));
            } else
                return false;
        //}
        if (!m_openingProject) {
            // we always show a slice window after first loading
            StackWindow windowConfig;
            windowConfig.stackIndex = m_currentStack;
            windowConfig.width = stack->size().x;
            m_stackWindows.push_back(windowConfig);
            m_currentStackWindow = m_stackWindows.size() - 1;

            //saveConfig();
        }
        qDebug() << "Done loading file from JSON file.";
        return true;
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
    return false;
}

/// Removes the stack and associated ROIs from memory.
void ProjectAnnotationSparse::unloadStack(const QString &name)
{
    m_stack.clear();
    m_stackWindows.clear();
    //saveConfig();
    qInfo() << "Removed stacks ";
}

QStringList ProjectAnnotationSparse::stackNames()
{
    QStringList ret;
    std::vector<stack_ptr_t>::iterator it;
    for (it = m_stack.begin(); it != m_stack.end(); ++it) {
        ret.append((*it)->name());
    }
    return ret;
}

QList <QUrl> ProjectAnnotationSparse::stackUrls()
{
    QList <QUrl> ret;
    std::vector<stack_ptr_t>::iterator it;
    for (it = m_stack.begin(); it != m_stack.end(); ++it) {
        ret.append(QUrl::fromLocalFile((*it)->filePath()));
    }
    return ret;
}

QList <QUrl> ProjectAnnotationSparse::stackWindowUrls()
{
    QList <QUrl> ret;
    std::vector<StackWindow>::iterator it;
    for (it = m_stackWindows.begin(); it != m_stackWindows.end(); ++it) {
        ret.append(QUrl::fromLocalFile(m_stack[it->stackIndex]->filePath()));
    }
    return ret;
}

int ProjectAnnotationSparse::getImageSizeY(QString imageName) {
    try {
        return static_cast<int>(getStack(imageName)->size().y);
    }
    catch (...)
    {
        qCritical() << "Stack " << imageName << " not found.";
    }
    return 0;
}

void ProjectAnnotationSparse::setCurrentStackWindow(const stack_index_t index)
{
    m_currentStackWindow = index;
    m_currentStack = m_stackWindows[index].stackIndex;
}

QVariantList ProjectAnnotationSparse::getStackUrlMatrix(QString directoryName)
{
    QUrl directoryUrl;
    QJsonDocument jsonDoc(loadJsonDocumentFromFile(m_filePath.toLocalFile()));

    // read json file and find the url that matches the roi directory name
    if (jsonDoc.object().contains("roi_files") && jsonDoc.object()["roi_files"].isArray()) {
        QJsonArray roiFilesArray = jsonDoc["roi_files"].toArray();
        for (int i = 0; i < roiFilesArray.size(); ++i) {
            directoryUrl = QUrl::fromLocalFile(roiFilesArray[i].toString());
            if (directoryUrl.fileName() == directoryName){
                qDebug() << "ProjectAnnotationSparse::getStackUrlsRead found roi directory from JSON: "
                         << directoryUrl;
                break;
            }
        }
    }

    StringMatrix tiffAligned = m_roiData->getStackMatrix(directoryName,tiff_aligned);
    StringMatrix tiffExvivo = m_roiData->getStackMatrix(directoryName,tiff_exvivo);
    int numSessions = tiffAligned.size(); //number of rows in ROI_list.tiff_aligned
    int numChannels = tiffAligned.first().size();// number of columns in ROI_list.tiff_aligned

    // If elements are empty in ROI_list.tiff_aligned, check for the files listed in the
    // corresponding elements in ROI_list.tiff_exvivo. If exist, load that file into respective
    // session/channel position (can be noted as “coarse aligned”)
    m_currentStackUrls.clear();
    for (int session = 0; session < numSessions; session++){
        QVariantList channels;
        for (int channel = 0; channel < numChannels; channel++){
            if (tiffAligned[session][channel] == ""){
                tiffAligned[session][channel] = tiffExvivo[session][channel];
            }
            if(tiffAligned[session][channel] == ""){
                channels.append(QVariant(""));
            } else {
                channels.append(QVariant(QUrl::fromLocalFile
                (directoryUrl.toLocalFile() + QString::fromStdString(tiffAligned[session][channel]).replace("\\","/"))));
            }

        }
        m_currentStackUrls.append(QVariant::fromValue(channels));
    }

    return m_currentStackUrls;
}

void ProjectAnnotationSparse::saveCandidateList(const QString& sessionKey){
    qInfo() << "Attempting to update " << sessionKey << " candidate list";
    for (auto roifile : m_roiFiles) {
        if(roifile.sessionKey == sessionKey){
#ifdef MATLAB_SUPPORT
            MatFileSparseRoi m(m_roiData);

            // building the file path
            QString path = roifile.savePath + "/" + roifile.sessionKey + ".mat";
            QUrl fileUrl = QUrl::fromLocalFile(path);
            m.saveCandidateList(roifile.sessionKey, fileUrl.toLocalFile().toStdString());
#else
            LOG(INFO) << "Did not save! Matlab support disabled.";
#endif
            break;
        }
    }
}

void ProjectAnnotationSparse::saveDecodeList(const QString& sessionKey){
    qInfo() << "Attempting to update " << sessionKey << " decode list";
    for (auto roifile : m_roiFiles) {
        if(roifile.sessionKey == sessionKey){
#ifdef MATLAB_SUPPORT
            MatFileSparseRoi m(m_roiData);

            // building the file path
            QString path = roifile.savePath + "/" + roifile.sessionKey + ".mat";
            QUrl fileUrl = QUrl::fromLocalFile(path);
            m.saveDecodeList(roifile.sessionKey, fileUrl.toLocalFile().toStdString());
#else
            LOG(INFO) << "Did not save! Matlab support disabled.";
#endif
            break;
        }
    }
}
