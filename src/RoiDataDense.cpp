/// @author Sam Kenyon <sam@metadevo.com>
#include <QDebug>
#include <QPainter>

#include "AppSettings.hpp"
#include "LandmarkModel.hpp"
#include "RoiDataDense.hpp"
#include "SearchResults.hpp"

RoiDataDense::RoiDataDense()
{
    if (AppSettings::instance().find("roi_origin")) {
        QString origin = AppSettings::instance().get("roi_origin").toString();
        if (origin == "bottomleft") {
            m_originTopLeft = false;
        } else {
            m_originTopLeft = true;
        }
    } else {
        AppSettings::instance().set("roi_origin", "topleft");
    }
    qInfo() << "roi_origin: " << m_originTopLeft;
}

RoiDataDense::~RoiDataDense()
{
    qDebug() << "dtor";
}

bool RoiDataDense::addNewRoi(session_key_t session, roi_key_t name, roi_t fields)
{
    m_currSession = session;
    return addNewRoi(name, fields);
}

/// Can call this directly if adding all the rois right after adding a new session
/// because the class keeps state of current session.
bool RoiDataDense::addNewRoi(roi_key_t name, roi_t fields)
{
    if (m_currSession != "") {
        auto it = m_sessions.find(m_currSession);
        if(it != m_sessions.end()){
            refreshRoi(m_currSession, std::dynamic_pointer_cast<RoiFieldsDense>(fields));
            if (it.value().contains(name)) {
                qInfo() << "Duplicate ROI: " << name;
            } else {
                it.value()[name] = fields;
            }
        } else {
            qWarning() << "Session key ("<< m_currSession << ") unknown.";
            return false;
        }
    } else {
        qWarning() << "Current session key is empty";
        return false;
    }
    return true;
}

/// This doesn't check for duplicate names, that should be done first
bool RoiDataDense::renameRoi(session_key_t session, roi_key_t name, roi_key_t newName)
{
    if (m_sessions.contains(session)) {
        session_t& sess = m_sessions[session];
        if (sess.contains(name))
        {
            sess[newName] = sess[name];
            sess[name]->name = newName;
            sess.remove(name);
            return true;
        } else {
            qWarning() << "ROI key ("<< name << ") unknown.";
            return false;
        }
    } else {
        qWarning() << "Session key ("<< session << ") unknown.";
        return false;
    }
}

bool RoiDataDense::deleteRoi(session_key_t session, roi_key_t roiKey)
{
    if (m_sessions.contains(session)) {
        session_t& sess = m_sessions[session];
        if (sess.contains(roiKey))
        {
            removeRoiFromMaskTables(session, sess[roiKey]);
            int removedCount = sess.remove(roiKey); // delete ROI object
            qInfo() << "Removed: " << removedCount << " " << roiKey << " rois";

            refreshRefLinks();
            return true;
        } else {
            qWarning() << "ROI key ("<< roiKey << ") unknown.";
            return false;
        }
    } else {
        qWarning() << "Session key ("<< session << ") unknown.";
        return false;
    }
}

/// Deletes flagged ROIS for the specified session
unsigned int RoiDataDense::deleteFlagged(const QString& sessionKey)
{
    int delCount = 0;
    if (m_sessions.contains(sessionKey)) {
        session_t& sess = m_sessions[sessionKey];
        for (auto roiKey : sess.keys())
        {
            if (sess[roiKey]->flagged()) {
                removeRoiFromMaskTables(sessionKey, sess[roiKey]);
                delCount += sess.remove(roiKey);
            }
        }
    } else {
        qWarning() << "Session key ("<< sessionKey << ") unknown.";
    }
    if (delCount > 0) {
        refreshRefLinks();
    }
    return static_cast<unsigned int>(delCount);
}

/// Deletes all flagged ROIS (across all sessions)
unsigned int RoiDataDense::deleteFlagged()
{
    unsigned int delCount = 0;
    for (auto sessionKey : m_sessions.keys()) {
        delCount += deleteFlagged(sessionKey);
    }
    return delCount;
}

void RoiDataDense::setRefSession(const session_key_t& sessionKey)
{
    auto it = m_sessions.find(sessionKey);
    if(it != m_sessions.end()){
        m_refSession = sessionKey;
        qInfo() << "Set reference session to: " << m_refSession;
    } else {
        qWarning() << "Session key ("<< sessionKey << ") unknown.";
    }

}

void RoiDataDense::clearRefSession()
{
    qInfo() << "Clearing reference session.";

    if (m_refSession != "") {
        session_t& refSess = m_sessions[m_refSession];

        // clear all existing linked-in entries in ref session
        for (auto roi : refSess) {
            roi->clearLinkedIn();
            roi->setConfirmed(false);
        }
    }

    m_refSession = "";
}

///@return 1 if this was an update, 0 if this was a first-time link
int RoiDataDense::modifyRoiLink(RoiAddress& src, RoiAddress& dest)
{
    int ret = 0;

    if (src.sessionKey == m_refSession)
    {
        // swap src and dest
        RoiAddress temp = src;
        src = dest;
        dest = temp;
    }
    if (dest.sessionKey != m_refSession) {
        qWarning() << "Attempt to link to a non-reference session.";
        ret = -1;
        return ret;
    }

    // this ROI had a link previously
    if (m_sessions[src.sessionKey][src.roiKey]->linkedName() != "") {
        ret = 1;
    }

    // Remove existing link to this ref ROI from source session (if there are any)
    removeOldLink(dest.roiKey, src.sessionKey);

    // Modify source ROI to link to ref
    roi_t srcRoi = m_sessions[src.sessionKey][src.roiKey];
    srcRoi->setLinkedName(dest.roiKey);
    srcRoi->setLinkedSession(dest.sessionKey);
    srcRoi->setConfirmed(true);

    // Update ref's linked-in information
    m_sessions[dest.sessionKey][dest.roiKey]->setLinkedIn(src.sessionKey, srcRoi);
    m_sessions[dest.sessionKey][dest.roiKey]->setConfirmed(true);
    refreshFullyLinked(m_sessions[dest.sessionKey][dest.roiKey]);

    qInfo() << "Link info updated for " << src.sessionKey << " : " << src.roiKey
              << " -> " << m_sessions[src.sessionKey][src.roiKey]->linkedSession() << " : " << m_sessions[src.sessionKey][src.roiKey]->linkedName();
    return ret;
}

/// This will clear the previous filter results
unsigned int RoiDataDense::filterHighPassRadius(const QString& sessionKey, const int minRadius)
{
    unsigned int filteredOutCount = 0;
    const int minDiameter = minRadius * 2;
    if (m_sessions.contains(sessionKey)) {
        for (auto roi : m_sessions.value(sessionKey))
        {
            roi->setFiltered(false);
            // first find the largest slice in the mask volume
            int maxDiameter = 0;
            int tempDiameter = 0;
            for (auto mask : std::dynamic_pointer_cast<RoiFieldsDense>(roi)->masks) {
                tempDiameter = maxDim(mask.extent);
                if (tempDiameter > maxDiameter) {
                    maxDiameter = tempDiameter;
                }
            }

            // is largest slice below the threshold?
            if (maxDiameter < minDiameter) {
                roi->setFiltered(true);
                ++filteredOutCount;
            }
        }
    }
    return filteredOutCount;
}

unsigned int RoiDataDense::filterHighPassVolume(const QString&, const int)
{
    unsigned int filteredOutCount = 0;

    ///@todo

    return filteredOutCount;
}

/// This does not clear existing flags
unsigned int RoiDataDense::flagFiltered(const QString& sessionKey)
{
    unsigned int flaggedCount = 0;
    if (m_sessions.contains(sessionKey)) {
        for (auto roi : m_sessions.value(sessionKey))
        {
            if (roi->filtered()) {
                roi->setFlagged(true);
                ++flaggedCount;
            }
        }
    }
    return flaggedCount;
}

/// Clears the flagged field for all ROIs in the given session
unsigned int RoiDataDense::unflagAll(const QString& sessionKey)
{
    for (auto roi : m_sessions.value(sessionKey))
    {
        roi->setFlagged(false);
    }
    return static_cast<unsigned int>(m_sessions.value(sessionKey).size());
}

/// Sets/clears the flagged field for the given ROI
void RoiDataDense::setFlag(const QString& sessionKey, const QString& roiKey, bool flag)
{
    m_sessions.value(sessionKey).value(roiKey)->setFlagged(flag);
}

/// Sets/clears the flagged field for the given ROI
bool RoiDataDense::toggleFlag(const QString& sessionKey, const QString& roiKey)
{
    bool flag = m_sessions[sessionKey][roiKey]->flagged();
    m_sessions[sessionKey][roiKey]->setFlagged(!flag);
    return !flag;
}

bool RoiDataDense::removeRoiLink(RoiAddress& src)
{
    //LOG(DBUG) << "Removing link from " << src.sessionKey.toStdString() << " : " << src.roiKey.toStdString();

    // Update ref's linked-in information
    QString refRoiKey = m_sessions[src.sessionKey][src.roiKey]->linkedName();
    if (refRoiKey != "") {
        m_sessions[m_refSession][refRoiKey]->removeLinkedIn(src.sessionKey);
        if (m_sessions[m_refSession][refRoiKey]->emptyLinkedIn()) {
            m_sessions[m_refSession][refRoiKey]->setConfirmed(false);
        }

        // remove the link
        m_sessions[src.sessionKey][src.roiKey]->clearLinkedName();
        m_sessions[src.sessionKey][src.roiKey]->clearLinkedSession();
        m_sessions[src.sessionKey][src.roiKey]->setConfirmed(false);
        qInfo() << "Link info removed for " << src.sessionKey << " : " << src.roiKey;
        return true;
    } else {
        return false;
    }
}

void RoiDataDense::setSelected(roi_t roi)
{
    roi->setSelected(true);
    if (m_prevSelection && roi != m_prevSelection) {
        m_prevSelection->setSelected(false);
    }
    m_prevSelection = roi;
}

/// Returns the number of rows in the reference session
int RoiDataDense::refSessionSize() const
{
    if (m_refSession == "") {
        return 0;
    } else {
        return m_sessions[m_refSession].size();
    }
}

/// If an ROI changes its ref link, this method is called to get rid of the previous link
/// @return: true if a link was removed, false otherwise
bool RoiDataDense::removeOldLink(const roi_key_t refRoiName, const session_key_t sessionName)
{
    QStringList strlistTemp;
    if (m_sessions.size() > 0 && m_refSession != "")
    {
        std::shared_ptr<RoiFieldsDense> roi = std::dynamic_pointer_cast<RoiFieldsDense>(m_sessions[m_refSession][refRoiName]);

        // Does the session have a link in?
        auto it = roi->findLinkedIn(sessionName);
        if(it != roi->endLinkedIn()) {
            // Get that old ROI linking in (lock() is to convert weak_ptr to shared_ptr)
            if (auto roiPtr = it.value().lock()) {
                // Unlink it
                roiPtr->clearLinkedName();
                roiPtr->clearLinkedSession();
                roiPtr->setConfirmed(false);
                qInfo() << "Link removed from " << sessionName << " ["<< roiPtr->name << "]";

                // remove from ref roi's data
                roi->removeLinkedIn(sessionName);
                return true;
            }
            else {
                qWarning() << "Weak pointer expired.";
            }
        }
    }
    else
    {
        qWarning() << "No ref session yet.";
    }
    return false;
}

/**
 * @param rowIndex: The desired row (index must be in range of ref sessions's rows)
 * @param colNames: What columns to use (this also indicates the order)
 * @returns a row into a matrix showing a ref ROI and linked ROIs of other sessions
 */
QStringList RoiDataDense::traverseRefNames(const int rowIndex, const QStringList colNames) const
{
    QStringList strlistTemp;
    if (m_sessions.size() > 0)
    {
        if (m_refSession == "")
        {
            // ref not set yet, so just use whatever's the first added column for now
            return strlistTemp;
        }

        auto names = m_sessions[m_refSession].keys();
        //qDebug() << "names[rowIndex]:" << names[rowIndex];

        if (rowIndex >= names.size()) {
            qWarning() << "Row index " << rowIndex << " invalid (size: " << names.size() << ")";
            return strlistTemp;
        }

        // now to see if there are any linked sessions for this ROI
        for (QString col : colNames) {
            if (col != m_refSession) {
                QString roiName = "?";
                for (auto it : m_sessions[col].values()) {
                    //qDebug() << "name:" << it.value()->name << " linkedname: " << it.value()->linkedName << " linkedSess:" << it.value()->linkedSession;
                    if ((names[rowIndex] == it->linkedName()) && (m_refSession == it->linkedSession())) {
                        roiName = it->name;
                    }
                }
                strlistTemp.push_back(roiName);
            }
        }

        if (!strlistTemp.empty()) {
            // arrange first column to be the ref
            strlistTemp.push_front(names[rowIndex]);
        } else {
            //qDebug() << "No links found for " << names[rowIndex];
            //qWarning() << "No links found for " << names[rowIndex].toStdString();
        }
    }
    else
    {
        qWarning() << "No sessions added yet.";
    }
    return strlistTemp;
}

std::vector<LinkCount> RoiDataDense::refLinkCounts()
{
    std::vector<LinkCount> linkCounts;
    int i = 0;
    for (auto roi : m_sessions[m_refSession]) {
        linkCounts.push_back(LinkCount(i++, roi->sizeLinkedIn()));
    }
    return linkCounts;
}

std::vector<RoiDataDense::IndexedRoi> RoiDataDense::indexedRoisAtSession(session_key_t sessionKey) const
{
    std::vector<IndexedRoi> rois;
    int i = 0;
    for (auto roi : m_sessions[sessionKey]) {
        IndexedRoi indexedRoi;
        indexedRoi.index = i++;
        indexedRoi.roi = roi;
        rois.push_back(indexedRoi);
    }
    return rois;
}

std::vector<std::shared_ptr<SearchResult>> RoiDataDense::searchRois(const QString& sessionKey, QString namePattern)
{
    std::vector<std::shared_ptr<SearchResult>> results;
    bool rightWild = namePattern.right(1) == "*";
    if (rightWild) {
        namePattern.chop(1);
    }

    //QList<session_t> searchSessions;
    QHash<session_key_t, session_t> searchSessions;
    if (sessionKey == "All") {
        searchSessions = m_sessions;
    } else {
        searchSessions[sessionKey] = m_sessions[sessionKey];
    }

    for(auto session = searchSessions.begin(); session != searchSessions.end(); ++session)
    {
        if (rightWild)
        {
            for (session_t::iterator it = session.value().begin(); it != session.value().end(); ++it)
            {
                if ((*it)->name.startsWith(namePattern)) {
                    results.push_back(std::make_shared<SearchResult>((*it)->name, session.key(), false));
                }
            }
        }
        else
        {
            auto rit = session.value().find(namePattern);
            if (rit != session.value().end())
            {
                results.push_back(std::make_shared<SearchResult>(rit.value()->name, session.key(), false));
            }
        }
    }

    return results;
}

/// @returns roi struct smart ptr or nullptr if session key or roi key are not found
RoiDataBase::roi_t RoiDataDense::fieldsAt(session_key_t sessionKey, roi_key_t roiKey) const
{
    auto it = m_sessions.find(sessionKey);
    if (it != m_sessions.end()) {
        auto session = it.value();
        auto rit = session.find(roiKey);
        if (rit != session.end()) {
            return rit.value();
        } else {
            //qWarning() << "ROI key ("<< roiKey.toStdString() << ") unknown.";
            return nullptr;
        }
    } else {
        qWarning() << "Session key ("<< sessionKey << ") unknown.";
        return nullptr;
    }
}

int RoiDataDense::roiIndexAt(session_key_t sessionKey, roi_key_t roiKey) const
{
    auto it = m_sessions.find(sessionKey);
    if (it != m_sessions.end()) {
        auto session = it.value();
        int i = 0;
        for (auto rit = session.begin(); rit != session.end(); ++rit, ++i) {
            if (rit.key() == roiKey) {
                return i;
            }
        }
    } else {
        qWarning() << "Session key ("<< sessionKey << ") unknown.";
        return -1;
    }
    qWarning() << "ROI key ("<< roiKey << ") unknown.";
    return -1;
}

/**
 * @param sessionKey: a non-ref session
 * @param roi: ROI in question
 * @return ref session ROI index linked to from the argument or -1 if nothing found
 */
int RoiDataDense::linkedRefIndexAt(const session_key_t sessionKey, RoiDataBase::roi_t roi) const
{
    if (sessionKey == m_refSession) {
        // this is the ref session so ignore
        return -1;
    }
    int i = 0;
    for (auto it : m_sessions[m_refSession].values()) {
        if (it->linkedInContains(sessionKey)) {
            // is this the exact roi?
            if (it->getLinkedIn(sessionKey).lock() == roi) {
                return i;
            }
        }
        ++i;
    }
    return -1;
}

///@returns list of roi smart pointers (list may be empty)
QList<RoiDataBase::roi_t> RoiDataDense::roisAtSessionFilterZ(session_key_t sessionKey, const z_key_t z) const
{
    auto it = m_maskTables.find(sessionKey);
    if (it != m_maskTables.end()) {
        return it.value()[z];
    } else {
        return QList<RoiDataBase::roi_t>();
    }
}

QList<QString> RoiDataDense::columnNamesUnordered()
{
    return m_sessions.keys();
}

/// Populate/update the linkedIn structures in the ref session
void RoiDataDense::refreshRefLinks()
{
    qInfo() << "Refreshing ref link data";
    if (m_refSession != "") {
        session_t& refSess = m_sessions[m_refSession];

        // clear all existing linked-in entries in ref session
        for (auto roi : refSess) {
            roi->clearLinkedIn();
        }

        // rebuild: go through all non-ref ROIs, adding linked ones into ref's linked-in structures
        auto keys = m_sessions.keys();
        keys.removeOne(m_refSession);
        for (auto key : keys) {
            for (auto roi : m_sessions[key]) {
                if (roi->linkedSession() == m_refSession &&
                        refSess.contains(roi->linkedName())) {
                    refSess[roi->linkedName()]->setLinkedIn(key, roi);
                    refSess[roi->linkedName()]->setConfirmed(true);
                    refreshFullyLinked(refSess[roi->linkedName()]);
                }
            }
        }
    }
}

// Update the fullyLinked flag in a ref session roi data structures
void RoiDataDense::refreshFullyLinked(roi_t roi)
{
    bool flag = roi->sizeLinkedIn() >= (m_sessions.size() - 1);
    roi->setFullyLinked(flag);
}

/// Initialize / construct extra ROI data we'll need based on the raw data
void RoiDataDense::refreshRoi(session_key_t session, std::shared_ptr<RoiFieldsDense> roi)
{
    if (roi->linkedSession() != "" && roi->linkedName() != "") {
        roi->setConfirmed(true);
    }

    for (auto v : roi->mask) {
        // look at all the mask coords and find the extents
        if (roi->masks[v.z].extent.isNull()) {
            roi->masks[v.z].extent.setCoords(v.x, v.y, 1, 1);
        }
        if (v.x < roi->masks[v.z].extent.x()) {
            roi->masks[v.z].extent.setLeft(v.x);
        }
        if (v.x > roi->masks[v.z].extent.right()) {
            roi->masks[v.z].extent.setRight(v.x);
        }
        if (v.y < roi->masks[v.z].extent.top()) {
            roi->masks[v.z].extent.setTop(v.y);
        }
        if (v.y > roi->masks[v.z].extent.bottom()) {
            roi->masks[v.z].extent.setBottom(v.y);
        }

        // add this to the z tables
        if (!m_maskTables[session][v.z].contains(roi)) {
            m_maskTables[session][v.z].append(roi);
        }
    }

    createMaskImages(roi->mask, roi->masks);
    createMaskOutlines(roi->masks);

    roi->mask.clear(); // don't need to waste this memory anymore
}

void RoiDataDense::removeRoiFromMaskTables(session_key_t session, roi_t roi)
{
    qInfo() << "Removing " << roi->name << " from mask tables.";
    for (z_key_t z : std::dynamic_pointer_cast<RoiFieldsDense>(roi)->masks.keys()) {
        // remove from z tables
        m_maskTables[session][z].removeAll(roi);
    }
}

/// Converts a stack of 2d images representing a 3d mask into a flat list of 3d coordinates
void RoiDataDense::maskImageToVector(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks, bool append)
{
    QHash<z_key_t, RoiMask>::const_iterator it;
    if (!append) {
        coords.clear();
    }
    for (it = masks.constBegin(); it != masks.constEnd(); ++it)
    {
        int z = it.key();
        int x_offset = it.value().extent.x();
        int y_offset = it.value().extent.y();

        const QImage& img = it.value().mask;
        unsigned int maxY = static_cast<unsigned int>(img.height());
        unsigned int maxX = static_cast<unsigned int>(img.width());
        for (unsigned int y = 0; y < maxY; ++y) {
            for (unsigned int x = 0; x < maxX; ++x) {
                int p = img.pixelIndex(x, y);
                if (p == 1) {
                    VoxelCoordinate v(x_offset + x, y_offset + y, z);
                    coords.push_back(v);
                }
            }
        }
    }
}

/// Converts a flat list of 3d coordinates into a stack of 2d images
void RoiDataDense::createMaskImages(QVector<VoxelCoordinate>& coords, QHash<z_key_t, RoiMask>& masks)
{
    QVector<QRgb> colors{QColor(0, 0, 0, 0).rgba(), QColor(255, 0, 0).rgba()};
    for (auto v : coords) {
        if (masks[v.z].mask.isNull()) {
            QImage img(masks[v.z].extent.width(), masks[v.z].extent.height(), QImage::Format_Mono);
            img.fill(0);
            img.setColorTable(colors);
            masks[v.z].mask = img;
        }

        masks[v.z].mask.setPixel(v.x - masks[v.z].extent.x(), v.y - masks[v.z].extent.y(), 1);
    }
}


void RoiDataDense::createMaskOutlines(QHash<z_key_t, RoiMask>& masks)
{
    float scale = 0.82f;

    for (auto k : masks.keys()) {
        int sx = static_cast<int>(static_cast<float>(masks[k].mask.width()) * scale);
        int sy = static_cast<int>(static_cast<float>(masks[k].mask.height()) * scale);
        QImage middle = masks[k].mask.scaled(sx, sy, Qt::KeepAspectRatio);
        QImage outline = masks[k].mask.copy();
        QPainter p(&outline);
        p.setCompositionMode(QPainter::CompositionMode_DestinationOut);
        p.drawImage(masks[k].mask.width() / 2 - middle.width() / 2, masks[k].mask.height() / 2 - middle.height() / 2, middle); ///todo coords
        p.end();
        masks[k].maskOutline = outline;
    }
}

/**
 * @param mask: RoiMask member of an Roi object
 * @param x: relative to the mask's coordinates
 * @param y: relative to the mask's coordinates
 * @param set: true to set pixel, false to clear pixel
 * @return
 */
bool RoiDataDense::modifyMask(RoiMask& mask, const int x, const int y)
{
    int mx = x;
    int my = y;
    if (x >= mask.mask.width() || y >= mask.mask.height()) {
        int newWidth = (x >= mask.mask.width()) ? (x + 1) : mask.mask.width();
        int newHeight = (y >= mask.mask.height()) ? (y + 1) : mask.mask.height();
        mask.mask = mask.mask.copy(0, 0, newWidth, newHeight);
        mask.extent.setWidth(mask.mask.width());
        mask.extent.setHeight(mask.mask.height());
    } else if (x < 0 || y < 0) {
        int newWidth = mask.mask.width();
        int newHeight = mask.mask.height();
        int tx = 0;
        int ty = 0;
        if (x < 0) {
            newWidth = newWidth - x;
            tx = x;
            mx = 0;
        }
        if (y < 0) {
            newHeight = newHeight - y;
            ty = y;
            my = 0;
        }
        mask.mask = mask.mask.copy(tx, ty, newWidth, newHeight);
        mask.extent.adjust(tx, ty, 0, 0);
    }
    unsigned int newColor = (mask.mask.pixel(mx, my) == 0) ? 1 : 0;
    mask.mask.setPixel(mx, my, newColor);
    return true;
}

/// Merge ROI data to the first ROI in the given list
void RoiDataDense::mergeToFirst(session_key_t session, const QList<roi_t>& rois)
{
    qInfo() << "Merging " << rois.size() << " ROIs";

    // create one big flat list of mask coords from all ROIS
    QVector<VoxelCoordinate> coords;
    for (auto roi : rois) {
        maskImageToVector(coords, std::dynamic_pointer_cast<RoiFieldsDense>(roi)->masks, true);
    }
    std::dynamic_pointer_cast<RoiFieldsDense>(rois[0])->mask = coords;
    std::dynamic_pointer_cast<RoiFieldsDense>(rois[0])->masks.clear();

    // update extents and turn that into a new set of mask images
    refreshRoi(session, std::dynamic_pointer_cast<RoiFieldsDense>(rois[0]));
}

void RoiDataDense::split(session_key_t, roi_t, roi_t)
{
//    mask.mask = mask.mask.copy(tx, ty, newWidth, newHeight);
//    mask.extent.adjust(tx, ty, 0, 0);
}

bool RoiDataDense::transformRois(const QStringList &sessionNames,
                            const LandmarkModel &landmarks,
                            bool translateCentroids)
{
    m_maskTables[TRANSFORMED_SESSION].clear();
    session_t &xformed_session = m_sessions[TRANSFORMED_SESSION];
    xformed_session.clear();

    bool haveRois = false;
    for (const auto &session : sessionNames) {
        auto it = m_sessions.find(session);
        if (it == m_sessions.end()) {
            continue;
        }
        haveRois = true;
        for (auto roi : it.value()) {
            std::shared_ptr<RoiFieldsDense> xformed = std::make_shared<RoiFieldsDense>();
            *xformed = *std::dynamic_pointer_cast<RoiFieldsDense>(roi);
            // Start with empty vector and images
            xformed->mask.clear();
            xformed->masks.clear();
            maskImageToVector(xformed->mask, std::dynamic_pointer_cast<RoiFieldsDense>(roi)->masks);

            if (translateCentroids) {
                // Transform the centroid and then translate all voxels by the
                // same amount.
                QVector<VoxelCoordinate> v;
                v.push_back(xformed->getCentroid());
                landmarks.transformPoints(v, session);
                VoxelCoordinate diff = { v[0].x - xformed->getCentroid().x,
                                         v[0].y - xformed->getCentroid().y,
                                         v[0].z - xformed->getCentroid().z };
                for (auto &p : xformed->mask) {
                    p.x += diff.x;
                    p.y += diff.y;
                    p.z += diff.z;
                }
                xformed->setCentroid(v[0]);
            }
            else {
                // Transform all voxels and recompute the centroid.
                landmarks.transformPoints(xformed->mask, session);
                double scale = 1.0 / xformed->mask.size();
                double cx = 0, cy = 0, cz = 0;
                for (const auto &p : xformed->mask) {
                    cx += p.x * scale;
                    cy += p.y * scale;
                    cz += p.z * scale;
                }
                xformed->setCentroid({ static_cast<int>(cx + 0.5),
                                      static_cast<int>(cy + 0.5),
                                      static_cast<int>(cz + 0.5) });
            }

            addNewRoi(TRANSFORMED_SESSION, xformed->name, xformed);
        }
    }
    return haveRois;
}
