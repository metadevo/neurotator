/// @author Sam Kenyon <sam@metadevo.com>
#include <string>
#include <vector>
#include <QDebug>
#include <QString>
#include <QUrl>

#include "MatFileSparseRoi.hpp"

MatFileSparseRoi::MatFileSparseRoi(RoiDataSparse *roiData) :
    MatFile(dynamic_cast<RoiDataBase*>(roiData))
{
    m_roiDataSparse = roiData;
}

bool MatFileSparseRoi::examine()
{
    bool foundToplevel = false;
    int num;
    char** nameList = matGetDir(m_mf, &num);
    if (nameList != nullptr) {
        if (num > 0) {
            for (int i = 0; i < num; ++i)
                if (strcmp(m_defaultToplevel.c_str(), nameList[i]) == 0) {
                    m_topArrayName = nameList[i];
                    //                    qDebug() << "Found default toplevel " << m_defaultToplevel.c_str();
                    foundToplevel = true;
                }
        }
        else
            qWarning() << "Zero arrays found.";
        mxFree(nameList);
    } else
        qWarning() << "Error finding arrays.";

    if (m_topArrayName != m_defaultToplevel)
        qWarning() << "First array is not named \"" << m_defaultToplevel.c_str() << "\". Attempting to load anyway.";
    else
        foundToplevel = true;

    // Matlab API requires closing and reopening between some of these functions
    if (!close())
        return false;
    if (!open())
        return false;

    //    qInfo() << "Examining headers for " << m_topArrayName.c_str();
    mxArray *topArray;
    topArray = matGetVariableInfo(m_mf, m_topArrayName.c_str());
    if (topArray == nullptr) {
        qWarning() << "Error reading array.";
        return false;
    }

    m_numElements = examineArray(topArray);
    if (m_numElements != 1) {
        qWarning() << "Too many elements in " << m_topArrayName.c_str();
        return false;
    }

    mxDestroyArray(topArray);
    if (!close())
        return false;

    if (!foundToplevel && !m_toplevelIsField) {
        qWarning() << "Error: Could not find " << m_defaultToplevel.c_str();
        return false;
    } else
        return true;
}

bool MatFileSparseRoi::loadCustomData() {
    // Matlab API requires closing and reopening between some of these functions
    if (!close()) {
        return false;
    }
    if (!open()) {
        return false;
    }

    // qInfo() << "Reading data from " << m_topArrayName.c_str();
    mxArray* metaArray = nullptr;
    mxArray* topArray = nullptr;

    if (m_toplevelIsField) {
        qInfo() << "Getting " << m_defaultToplevel.c_str() << " out of " << m_topArrayName.c_str();
        metaArray = matGetVariable(m_mf, m_topArrayName.c_str());
        if (metaArray == nullptr) {
            qWarning() << "Error reading top level field array.";
            return false;
        }
        topArray = mxGetField(metaArray, 0, m_defaultToplevel.c_str());
    } else {
        topArray = matGetVariable(m_mf, m_topArrayName.c_str());
        //qDebug()<< " matfilesparse"<< m_defaultToplevel.c_str() <<m_topArrayName.c_str();
    }
    if (topArray == nullptr) {
        qWarning() << "Error reading array.";
        return false;
    }

    mxArray* baxterListArray = nullptr;
    baxterListArray = matGetVariable(m_mf, "Baxter_list");

    if (baxterListArray == nullptr) {
        qWarning() << "Error BaxterList array.";
        return false;
    }
    for(mwIndex baxterIndex = 0; baxterIndex < mxGetNumberOfElements(baxterListArray); baxterIndex++){
        //    qInfo() << "Reading each element...";
        std::shared_ptr<RoiFieldsSparse> newRoi(nullptr);
        mxArray* fieldArray;
        newRoi = std::make_shared<RoiFieldsSparse>();
        mwIndex index = 0;

        fieldArray = mxGetField(topArray, index, "name");
        QString sessionName = QString::fromStdString(loadString(fieldArray));

        fieldArray = mxGetField(topArray, index, "roi_original");
        newRoi->setRoiOriginal(loadVectorOfVoxels(fieldArray));

        fieldArray = mxGetField(topArray, index, "centroid_original");
        newRoi->setCentroidOriginal(loadVoxelCoordinate(fieldArray));

        fieldArray = mxGetField(topArray, index, "tiff_original");
        newRoi->setTiffOriginal(QString::fromStdString(loadString(fieldArray)));

        fieldArray = mxGetField(topArray, index, "roi_invivo");
        newRoi->setRoiInvivo(loadVectorOfVoxels(fieldArray));

        fieldArray = mxGetField(topArray, index, "roi_invivo_us");
        newRoi->setRoiInvivoUs(loadVectorOfVoxels(fieldArray));

        fieldArray = mxGetField(topArray, index, "candidates");
        newRoi->setCandidates(loadVectorOfInts(fieldArray));

        fieldArray = mxGetField(topArray, index, "centroid_invivo");
        newRoi->setCentroidInVivo(loadVoxelCoordinate(fieldArray));

        fieldArray = mxGetField(topArray, index, "centroid_invivo_us");
        newRoi->setCentroidInVivoUs(loadVoxelCoordinate(fieldArray));

        fieldArray = mxGetField(topArray, index, "tiff_invivo");
        newRoi->setTiffInVivo(QString::fromStdString(loadString(fieldArray)));

        fieldArray = mxGetField(topArray, index, "tiff_aligned");
        newRoi->setTiffAligned(loadVectorOfStrings(fieldArray));

        fieldArray = mxGetField(topArray, index, "tiff_exvivo");
        newRoi->setTiffExVivo(loadVectorOfStrings(fieldArray));

        // Baster_list fields: name, roi, decode
        fieldArray = mxGetField(baxterListArray, baxterIndex, "name");
        newRoi->name = QString::fromStdString(loadString(fieldArray));

        fieldArray = mxGetField(baxterListArray, baxterIndex, "roi");
        newRoi->setMask(loadVectorOfVoxels(fieldArray, true));

        fieldArray = mxGetField(baxterListArray, baxterIndex, "decode");
        if(fieldArray != nullptr)
        {
            newRoi->setDecode(loadMatrixOfInts(fieldArray));
        }

        dynamic_cast<RoiDataSparse*>(m_roiData)->addNewRoi(sessionName, newRoi->name, newRoi);
    }
    if (metaArray) {
        mxDestroyArray(metaArray);
    } else {
        mxDestroyArray(topArray);
    }

    if (!close()) {
        return false;
    }

    return true;
};


bool MatFileSparseRoi::saveCandidateList(const QString& sessionKey, const std::string& filepath){
    try
    {
        // building the file path
        m_sessionKey = sessionKey;
        m_filepath = filepath;
        qInfo() << "Updating file " << m_filepath.c_str();

        // open mat file in reading mode
        m_mf = matOpen(m_filepath.c_str(), "r");
        if (m_mf == nullptr) {
            qWarning() << "Error opening file " << m_filepath.c_str();
            return false;
        }

        // get and copy all current variables into a list of topArrays
        QList<mxArray*> topArrays;
        int roi_list;
        int num;
        char** nameList = matGetDir(m_mf, &num);
        QList<std::string> names;
        if (nameList != nullptr) {
            if (num > 0) {
                for (int i = 0; i < num; ++i){
                    mxArray *topArray = matGetVariable(m_mf, nameList[i]);
                    topArrays.append(topArray);
                    names.append(nameList[i]);
                    if(names[i] == m_defaultToplevel){
                        roi_list = i;
                    }
                }
            }else {qWarning() << "Zero arrays found.";}
            mxFree(nameList);
        } else {
            qWarning() << "Error finding arrays.";
        }

        // updating "candidates" field with current candidate list
        QList<int> candidates = m_roiDataSparse->getCandidatelist(sessionKey);
        mwSize dims[2];
        dims[0] = 1;
        dims[1] = candidates.size();
        mxArray* tmp = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL); // note we do not need to free this manually
        double* tmpData = mxGetDoubles(tmp);
        for (int i = 0; i < candidates.size(); i++) {
            tmpData[i] = static_cast<double>(candidates[i]);
        }
        mxSetField(topArrays[roi_list], 0, "candidates", tmp);

        // close mat file and then open it in writing mode
        if (!close()) {
            return false;
        }
        m_mf = matOpen(m_filepath.c_str(), "w");

        for(int i = 0; i < topArrays.size(); i++){
            int status = matPutVariable(m_mf, names[i].c_str(), topArrays[i]);
            if (status != 0) {
                qWarning() << "Put failed." << status;
                return false;
            }
            mxDestroyArray(topArrays[i]);
        }

        if (!close()) {
            return false;
        }
        return true;
    }
    catch (const std::exception &e)
    {
        qWarning() << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
}


bool MatFileSparseRoi::saveDecodeList(const QString& sessionKey, const std::string& filepath){
    try
    {
        // building the file path
        m_sessionKey = sessionKey;
        m_filepath = filepath;
        qInfo() << "Updating file " << m_filepath.c_str();

        // open mat file in reading mode
        m_mf = matOpen(m_filepath.c_str(), "r");
        if (m_mf == nullptr) {
            qWarning() << "Error opening file " << m_filepath.c_str();
            return false;
        }

        // get and copy all current variables into a list of topArrays
        QList<mxArray*> topArrays;
        int baxter_list_index = -1;
        int num;
        char** nameList = matGetDir(m_mf, &num);

        QList<std::string> names;
        if (nameList != nullptr) {
            if (num > 0) {
                for (int i = 0; i < num; ++i){
                    mxArray *topArray = matGetVariable(m_mf, nameList[i]);
                    topArrays.append(topArray);
                    names.append(nameList[i]);
                    if(names[i] == "Baxter_list"){
                        baxter_list_index = i;
                    }
                }
            }else {qWarning() << "Zero arrays found.";}
            mxFree(nameList);
        } else {
            qWarning() << "Error finding arrays.";
        }

        // updating "decode" field with all decode arrays
        QList<QVector<QVector<int>>> decodeList = m_roiDataSparse->getDecodeList(m_sessionKey);
        for(int i = 0; i < decodeList.size() - 1; ++i){
            mwSize dims[2];
            dims[0] = decodeList[i].size();
            dims[1] = decodeList[i].first().size();
            mxArray* tmp = mxCreateNumericArray(2, dims, mxUINT8_CLASS, mxREAL);
            mxUint8* tmpUint8 = mxGetUint8s(tmp);
            for(mwSize row = 0; row < dims[0]; ++row){
                for(mwSize col = 0; col < dims[1]; ++col){
                    tmpUint8[dims[0] * col + row] = static_cast<int>(decodeList[i].value(row).value(col));
                }
            }
            mxSetField(topArrays[baxter_list_index], i, "decode", tmp);
        }

        // close mat file and then open it in writing mode
        if (!close()) {
            return false;
        }
        m_mf = matOpen(m_filepath.c_str(), "w");

        for(int i = 0; i < topArrays.size(); i++){
            int status = matPutVariable(m_mf, names[i].c_str(), topArrays[i]);
            if (status != 0) {
                qWarning() << "Put failed." << status;
                return false;
            }
            mxDestroyArray(topArrays[i]);
        }

        if (!close()) {
            return false;
        }
        return true;
    }
    catch (const std::exception &e)
    {
        qWarning() << "Exception: " << e.what();
        return false;
    }
    catch (...)
    {
        qWarning() << "Unknown exception";
        return false;
    }
}


