#include "ProjectDense.hpp"
#include "HyperstackTiff.hpp"
#include <QJsonArray>
#include <QFile>
#include <QFileInfo>

#ifdef MATLAB_SUPPORT
#include "MatFileDenseRoi.hpp"
#endif

ProjectDense::ProjectDense() :
    Project()
{
    m_roiData = new RoiDataDense();
    m_confirmedModel = new RoiDataModel(m_roiData, true);

}
ProjectDense::~ProjectDense() {
    delete m_confirmedModel;
    delete m_roiData;
    for (auto i : m_stack)
        delete i;
}

void ProjectDense::readStacks(const QJsonObject &json)
{
    if (json.contains("stacks") && json["stacks"].isArray()) {
        m_stack.clear();
        QJsonArray imagesArray = json["stacks"].toArray();
        for (int i = 0; i < imagesArray.size(); ++i) {
            QUrl url = QUrl::fromLocalFile(imagesArray[i].toString());
            qDebug() << "stack/image file: " << url.toString();
            if (!loadStack(url)) {
                QUrl localUrl(
                        m_filePath.toString(QUrl::ComponentFormattingOptions(QUrl::RemoveFilename)) + url.fileName());
                qDebug() << "Couldn't find stack, looking more locally to the JSON" << localUrl;
                loadStack(localUrl);
            }
        }
    }
}

void ProjectDense::readRoiFiles(const QJsonObject &json)
{
    m_referenceName = "";
    if (json.contains("roi_files") && json["roi_files"].isArray()) {
        QJsonArray roiFilesArray = json["roi_files"].toArray();
        for (int i = 0; i < roiFilesArray.size(); ++i) {
            QUrl url = QUrl::fromLocalFile(roiFilesArray[i].toString());
            qDebug() << "roi file: " << url.toString();
            dynamic_cast<Project*>(this)->loadRoiData(url);
        }
    }
    if (json.contains("ref")) {
        QString refName = json["ref"].toString();
        if (refName != "") {
            qDebug() << "set ref session";
            m_referenceName = refName;
            //ApplicationData::instance().setRefSession(refName);
        }
    }
}

void ProjectDense::readConfigProjectSpecific(const QJsonObject &json) {
    readStacks(json);
    readRoiFiles(json);
}

/// sets both the current stack window and the current stack to the associated stack index
void ProjectDense::setCurrentStackWindow(const stack_index_t index)
{
    m_currentStackWindow = index;
    m_currentStack = m_stackWindows[index].stackIndex;
}

void ProjectDense::writeStacks() const
{
    QJsonArray imagesArray;
    for (auto stack : m_stack) {
        imagesArray.append(stack->filePath());
    }
    m_jsonObject["stacks"] = imagesArray;
}

bool ProjectDense::loadStack(const QUrl fileUrl)
{
    QString pathStr = fileUrl.toLocalFile();
    return loadStack(pathStr);
}

bool ProjectDense::loadStack(const QString &pathStr)
{
    try {
        stack_ptr_t stack;
        if (findStack(pathStr)) {
            qInfo() << "Stack " << pathStr << "already loaded.";
            m_currentStack = m_findResult;
            stack = m_stack[m_currentStack];
        } else {
            qInfo() << "Creating a stack...";
            stack = new HyperstackTiff();
            if (stack->loadStreaming(pathStr, lazyLoad)) {
                m_stack.push_back(stack);
                m_currentStack = m_stack.size() - 1;
                stack->setColor(std::vector<int>(colorMap[m_currentStack]));
            } else
                return false;
        }

        addStackToLandmarks(stack);

        if (!m_openingProject) {
            // we always show a slice window after first loading
            StackWindow windowConfig;
            windowConfig.stackIndex = m_currentStack;
            windowConfig.width = stack->size().x;
            m_stackWindows.push_back(windowConfig);
            m_currentStackWindow = m_stackWindows.size() - 1;

            saveConfig();
        }
        qDebug() << "Done loading file from JSON file.";
        return true;
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
    return false;
}


bool ProjectDense::findStack(const QString &filePath)
{
    for (stack_index_t i = 0; i < m_stack.size(); ++i) {
        if (m_stack[i]->filePath() == filePath) {
            // Right now we only have one context == 1 client per stack
            // but we might change that later to optimize multiple
            // views using the same HyperStack object.
            if (m_stack[i]->clients() < m_maxClientsPerStack) {
                m_findResult = i;
                return true;
            }
        }
    }
    return false;
}

QStringList ProjectDense::stackNames()
{
    QStringList ret;
    std::vector<stack_ptr_t>::iterator it;
    for (it = m_stack.begin(); it != m_stack.end(); ++it) {
        ret.append((*it)->name());
    }
    return ret;
}

QList <QUrl> ProjectDense::stackUrls()
{
    QList <QUrl> ret;
    std::vector<stack_ptr_t>::iterator it;
    for (it = m_stack.begin(); it != m_stack.end(); ++it) {
        ret.append(QUrl::fromLocalFile((*it)->filePath()));
    }
    return ret;
}

QList <QUrl> ProjectDense::stackWindowUrls()
{
    QList <QUrl> ret;
    std::vector<StackWindow>::iterator it;
    for (it = m_stackWindows.begin(); it != m_stackWindows.end(); ++it) {
        ret.append(QUrl::fromLocalFile(m_stack[it->stackIndex]->filePath()));
    }
    return ret;
}

/// Removes the stack and associated ROIs from memory.
void ProjectDense::unloadStack(const QString &name)
{
    m_roiData->removeSession(name);
    removeStackFromLandmarks(name);

    m_confirmedModel->removeColumn(name);
    m_confirmedModel->regen();

    //remove from m_unconfirmedModels
    int index = 0;
    for (auto model : m_unconfirmedModels) {
        if (model->columnNameAt(0) == name) {
            break;
        }
        ++index;
    }
    m_unconfirmedModels.removeAt(index);
    for (auto model : m_unconfirmedModels) {
        model->regen();
    }
    qInfo() << "Removed " << name << " from unconfirmed models.";

    // remove stack pointer(s)
    std::vector<stack_ptr_t>::iterator it;
    stack_index_t id = 0;
    bool found = false;
    for (it = m_stack.begin(); it != m_stack.end(); ++id) {
        if ((*it)->name() == name) {
            it = m_stack.erase(it);
            found = true;
            break;
        } else {
            ++it;
        }
    }

    if (found) {
        // remove file paths of imported ROI for this session from project json
        QVector<RoiFile>::iterator it_roi;
        for (it_roi = m_roiFiles.begin(); it_roi != m_roiFiles.end();) {
            qInfo() << "Removed " << name << (*it_roi).sessionKey;
            if ((*it_roi).sessionKey == name) {
                it_roi = m_roiFiles.erase(it_roi);
            } else {
                ++it_roi;
            }
        }

        // remove associated slice window(s)
        std::vector<StackWindow>::iterator it;
        for (it = m_stackWindows.begin(); it != m_stackWindows.end();) {
            if (it->stackIndex == id) {
                it = m_stackWindows.erase(it);
                found = true;
            } else {
                ++it;
            }
        }

        saveConfig();
        qInfo() << "Removed stack: " << name;
    } else {
        qWarning() << "Stack not found: " << name;
    }
}

int ProjectDense::getImageSizeY(QString imageName) {
    try {
        return static_cast<int>(getStack(imageName)->size().y);
    }
    catch (...)
    {
        qCritical() << "Stack " << imageName << " not found.";
    }
    return 0;
}

stack_ptr_t ProjectDense::currentStack()
{
    return (m_stack.size() > 0) ? m_stack.at(m_currentStack) : nullptr;
}

stack_ptr_t ProjectDense::getStack(const QString &sessionName) const
{
    auto it = std::find_if(m_stack.begin(), m_stack.end(),
                           [&sessionName](const auto *s) {
                               return s->name() == sessionName;
                           });
    if (it != m_stack.end()) {
        return *it;
    } else {
        return nullptr;
    }
}


void ProjectDense::loadRoiData(const QString &pathStr)
{
    try {
        QFileInfo info(pathStr);
        QString suffix = info.suffix().toLower();
        QString keyName = info.baseName();
        RoiFile rf;
        rf.savePath = pathStr;

        rf.sessionKey = keyName;
        m_roiFiles.append(rf);

        bool loaded = true;
        if (suffix == "mat") {
            qInfo() << "Attempting to load as Matlab file...";

            loaded = loadMatlabRoiFile(pathStr, keyName);
            if (!loaded) {
                qWarning() << "Loading Matlab file failed.";
            }
        } else if (suffix == "csv") {
            qInfo() << "Attempting to load as CSV file...";

            loaded = loadCsvRoiFile(pathStr, keyName);
            if (!loaded) {
                qWarning() << "Loading CSV file failed.";
            }
        } else {
            qWarning() << "Unrecognized file type.";
        }

        if (loaded && !m_openingProject) {
            saveConfig();
        }

        // post processing
        updateUnconfirmedNameMap();
        m_roiData->refreshRefLinks();
    }
    catch (const std::exception &e) {
        qWarning() << "ERROR: " << e.what();
    }
    catch (...) {
        qWarning() << "Unknown exception";
    }
}


bool ProjectDense::loadMatlabRoiFile(QString pathStr, QString keyName)
{
#ifdef MATLAB_SUPPORT
    if (m_roiData->addNewSession(keyName))
    {
        MatFileDenseRoi m(m_roiData);
        if (!m.load(pathStr.toStdString())) {
            qDebug() << "Load matlab file failed: " << pathStr << ". Looking more locally...";
            QUrl url = QUrl::fromLocalFile(pathStr);
            QUrl localUrl ( m_filePath.toString(QUrl::ComponentFormattingOptions(QUrl::RemoveFilename)) + url.fileName());
            if (!m.load(localUrl.toLocalFile().toStdString())) {
                qDebug() << "Tried locally but failed as well: " << localUrl.toLocalFile();
                return false;
            }
        }

        auto model = std::make_shared<RoiDataModel>(m_roiData);
        model->addColumn(keyName);
        m_unconfirmedModels.push_back(model);
        m_confirmedModel->addColumn(keyName);

        model->setSorted(SortChoice::NAME); // sort unconfirmed models by name
        //model->regen(); //will be done in resetModels below
        resetModels();

        return true;
    }
    else
    {
        qWarning() << "addNewSession failed.";
        return false;
    }
#else
    return false;
#endif
}

bool ProjectDense::loadCsvRoiFile(QString pathStr, QString keyName)
{
    if (m_roiData->addNewSession(keyName)) {
        QFile file(pathStr);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << file.errorString();
            file.close();
            return false;
        }

        auto model = std::make_shared<RoiDataModel>(m_roiData);
        model->addColumn(keyName);
        m_unconfirmedModels.push_back(model);
        m_confirmedModel->addColumn(keyName);

        //unsigned int n = 0;
        QStringList wordList;
        std::shared_ptr <RoiFieldsDense> fields(nullptr);
        while (!file.atEnd()) {
            QString line = file.readLine();
            QStringList row = line.split(",");
            if (row[0] == "\"roi\"") {
                line = file.readLine();
                row = line.split(",");
                if (fields) { // done with last ROI
                    qDebug() << "Adding new ROI " << fields->name;
                    m_roiData->addNewRoi(fields->name, fields);
//                    for (auto v : fields->mask) {
//                        LOG(DBUG) << "mask: " << v.x << ", " << v.y << ", " << v.z;
//                    }
                }
                fields = std::make_shared<RoiFieldsDense>();
                QString keyName = row[3].remove('\"');
                fields->name = keyName;
                /// For some reason it seems x and y are swapped in the data
                fields->setCentroid(VoxelCoordinate(row[5].toInt(), row[4].toInt(), row[6].toInt()));
                fields->addToMask(VoxelCoordinate(row[1].toInt(), row[0].toInt(), row[2].toInt()));

                if (row.size() >= 10) {
                    fields->setLinkedName(row[9].remove('\"'));
                    fields->setLinkedSession(row[10].remove('\"').remove('\n'));
                }
            } else {
                if (fields) {
                    if (row.size() > 2) {
                        /// For some reason it seems x and y are swapped in the data
                        fields->addToMask(VoxelCoordinate(row[1].toInt(), row[0].toInt(), row[2].toInt()));
                    }
                } else {
                    qWarning() << "Mask data for unknown ROI";
                }
            }
        }

        model->setSorted(SortChoice::NAME); // sort unconfirmed models by name
        model->regen();
        file.close();
    } else {
        qWarning() << "addNewSession failed.";
        return false;
    }
    return true;
}

void ProjectDense::saveAllRoiFiles()
{
    qInfo() << "Attempting to save " << m_roiFiles.size() << " files";
    for (auto roifile : m_roiFiles) {
#ifdef MATLAB_SUPPORT
        qInfo() << "Saving " << roifile.savePath;
        MatFileDenseRoi m(m_roiData);
        m.save(roifile.sessionKey, roifile.savePath.toStdString());
#else
        LOG(INFO) << "Did not save! Matlab support disabled.";
#endif
    }
    qInfo() << "Done saving.";
}

/// Move the ref session to index 0
void ProjectDense::resortModels(const QString refSessionKey)
{
    qDebug() << "ReSORTing models...";
    std::shared_ptr <RoiDataModel> it;
    foreach(it, m_unconfirmedModels)
    {
        if (it->columnNameAt(0) == refSessionKey) {
            m_unconfirmedModels.removeAll(it);
            m_unconfirmedModels.push_front(it);
        }
    }

    updateUnconfirmedNameMap();

    qDebug() << "Resorted models.";
}

RoiDataModel *ProjectDense::unconfirmedModel(const int index)
{
    //qDebug() << "Project: request for model at " << index;
    //qDebug() << "m_unconfirmedModels.size(): " << m_unconfirmedModels.size();
    if (index < m_unconfirmedModels.size()) {
        RoiDataModel *obj = m_unconfirmedModels[index].get();
        QQmlEngine::setObjectOwnership(obj, QQmlEngine::CppOwnership);
        return obj;
    } else {
        qWarning() << "Index out of bounds.";
        return nullptr;
    }
}

void ProjectDense::updateUnconfirmedNameMap()
{
    // update map for quick access via session name
    std::shared_ptr <RoiDataModel> it;
    foreach(it, m_unconfirmedModels)
    {
        m_unconfirmedModelNameMap[it->columnNameAt(0)] = it;
    }
}

/// A reset is a quicker operation to notify model users,
/// whereas a regen will in addition recreate whatever data/dimensions are in the models.
/// If ROI data has been modified, you should regenerate.
void ProjectDense::resetModels(bool regenConfirmed)
{
    if (regenConfirmed) {
        qDebug() << "REGENERATING models.";

        m_confirmedModel->regen();

        std::shared_ptr <RoiDataModel> it;
        foreach(it, m_unconfirmedModels)
        {
            it->regen();
        }
    } else {
        qDebug() << "Resetting models.";

        m_confirmedModel->signalReset();

        std::shared_ptr <RoiDataModel> it;
        foreach(it, m_unconfirmedModels)
        {
            it->signalReset();
        }
    }
}

void ProjectDense::resortConfirmedModel(const SortChoice sortChoice)
{
    m_confirmedModel->setSorted(sortChoice);
    m_confirmedModel->regen();
}

void ProjectDense::resortConfirmedModelByName(const int column)
{
    m_confirmedModel->setSorted(SortChoice::NAME);
    m_confirmedModel->setSortColumn(column);
    m_confirmedModel->regen();
}
